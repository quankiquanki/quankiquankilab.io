
//${CONFIG_BEGIN}
CFG_BINARY_FILES="*.bin|*.dat";
CFG_BRL_DATABUFFER_IMPLEMENTED="1";
CFG_BRL_GAMETARGET_IMPLEMENTED="1";
CFG_BRL_THREAD_IMPLEMENTED="1";
CFG_CD="";
CFG_CONFIG="debug";
CFG_HOST="winnt";
CFG_HTML5_WEBAUDIO_ENABLED="1";
CFG_IMAGE_FILES="*.png|*.jpg";
CFG_LANG="js";
CFG_MODPATH="";
CFG_MOJO_AUTO_SUSPEND_ENABLED="1";
CFG_MOJO_DRIVER_IMPLEMENTED="1";
CFG_MUSIC_FILES="*.wav|*.ogg|*.mp3|*.m4a";
CFG_OPENGL_DEPTH_BUFFER_ENABLED="1";
CFG_OPENGL_GLES20_ENABLED="1";
CFG_SAFEMODE="0";
CFG_SOUND_FILES="*.wav|*.ogg|*.mp3|*.m4a";
CFG_TARGET="html5";
CFG_TEXT_FILES="*.glsl;*.txt|*.xml|*.json";
//${CONFIG_END}

//${METADATA_BEGIN}
var META_DATA="[mojo_font.png];type=image/png;width=864;height=13;\n[mojo2_font.png];type=image/png;width=960;height=16;\n[levels/level_1.png];type=image/png;width=580;height=580;\n";
//${METADATA_END}

//${TRANSCODE_BEGIN}

// Javascript Monkey runtime.
//
// Placed into the public domain 24/02/2011.
// No warranty implied; use at your own risk.

//***** JavaScript Runtime *****

var D2R=0.017453292519943295;
var R2D=57.29577951308232;

var err_info="";
var err_stack=[];

var dbg_index=0;

function push_err(){
	err_stack.push( err_info );
}

function pop_err(){
	err_info=err_stack.pop();
}

function stackTrace(){
	if( !err_info.length ) return "";
	var str=err_info+"\n";
	for( var i=err_stack.length-1;i>0;--i ){
		str+=err_stack[i]+"\n";
	}
	return str;
}

function print( str ){
	var cons=document.getElementById( "GameConsole" );
	if( cons ){
		cons.value+=str+"\n";
		cons.scrollTop=cons.scrollHeight-cons.clientHeight;
	}else if( window.console!=undefined ){
		window.console.log( str );
	}
	return 0;
}

function alertError( err ){
	if( typeof(err)=="string" && err=="" ) return;
	alert( "Monkey Runtime Error : "+err.toString()+"\n\n"+stackTrace() );
}

function error( err ){
	throw err;
}

function debugLog( str ){
	if( window.console!=undefined ) window.console.log( str );
}

function debugStop(){
	debugger;	//	error( "STOP" );
}

function dbg_object( obj ){
	if( obj ) return obj;
	error( "Null object access" );
}

function dbg_charCodeAt( str,index ){
	if( index<0 || index>=str.length ) error( "Character index out of range" );
	return str.charCodeAt( index );
}

function dbg_array( arr,index ){
	if( index<0 || index>=arr.length ) error( "Array index out of range" );
	dbg_index=index;
	return arr;
}

function new_bool_array( len ){
	var arr=Array( len );
	for( var i=0;i<len;++i ) arr[i]=false;
	return arr;
}

function new_number_array( len ){
	var arr=Array( len );
	for( var i=0;i<len;++i ) arr[i]=0;
	return arr;
}

function new_string_array( len ){
	var arr=Array( len );
	for( var i=0;i<len;++i ) arr[i]='';
	return arr;
}

function new_array_array( len ){
	var arr=Array( len );
	for( var i=0;i<len;++i ) arr[i]=[];
	return arr;
}

function new_object_array( len ){
	var arr=Array( len );
	for( var i=0;i<len;++i ) arr[i]=null;
	return arr;
}

function resize_bool_array( arr,len ){
	var i=arr.length;
	arr=arr.slice(0,len);
	if( len<=i ) return arr;
	arr.length=len;
	while( i<len ) arr[i++]=false;
	return arr;
}

function resize_number_array( arr,len ){
	var i=arr.length;
	arr=arr.slice(0,len);
	if( len<=i ) return arr;
	arr.length=len;
	while( i<len ) arr[i++]=0;
	return arr;
}

function resize_string_array( arr,len ){
	var i=arr.length;
	arr=arr.slice(0,len);
	if( len<=i ) return arr;
	arr.length=len;
	while( i<len ) arr[i++]="";
	return arr;
}

function resize_array_array( arr,len ){
	var i=arr.length;
	arr=arr.slice(0,len);
	if( len<=i ) return arr;
	arr.length=len;
	while( i<len ) arr[i++]=[];
	return arr;
}

function resize_object_array( arr,len ){
	var i=arr.length;
	arr=arr.slice(0,len);
	if( len<=i ) return arr;
	arr.length=len;
	while( i<len ) arr[i++]=null;
	return arr;
}

function string_compare( lhs,rhs ){
	var n=Math.min( lhs.length,rhs.length ),i,t;
	for( i=0;i<n;++i ){
		t=lhs.charCodeAt(i)-rhs.charCodeAt(i);
		if( t ) return t;
	}
	return lhs.length-rhs.length;
}

function string_replace( str,find,rep ){	//no unregex replace all?!?
	var i=0;
	for(;;){
		i=str.indexOf( find,i );
		if( i==-1 ) return str;
		str=str.substring( 0,i )+rep+str.substring( i+find.length );
		i+=rep.length;
	}
}

function string_trim( str ){
	var i=0,i2=str.length;
	while( i<i2 && str.charCodeAt(i)<=32 ) i+=1;
	while( i2>i && str.charCodeAt(i2-1)<=32 ) i2-=1;
	return str.slice( i,i2 );
}

function string_startswith( str,substr ){
	return substr.length<=str.length && str.slice(0,substr.length)==substr;
}

function string_endswith( str,substr ){
	return substr.length<=str.length && str.slice(str.length-substr.length,str.length)==substr;
}

function string_tochars( str ){
	var arr=new Array( str.length );
	for( var i=0;i<str.length;++i ) arr[i]=str.charCodeAt(i);
	return arr;
}

function string_fromchars( chars ){
	var str="",i;
	for( i=0;i<chars.length;++i ){
		str+=String.fromCharCode( chars[i] );
	}
	return str;
}

function object_downcast( obj,clas ){
	if( obj instanceof clas ) return obj;
	return null;
}

function object_implements( obj,iface ){
	if( obj && obj.implments && obj.implments[iface] ) return obj;
	return null;
}

function extend_class( clas ){
	var tmp=function(){};
	tmp.prototype=clas.prototype;
	return new tmp;
}

function ThrowableObject(){
}

ThrowableObject.prototype.toString=function(){ 
	return "Uncaught Monkey Exception"; 
}


function BBGameEvent(){}
BBGameEvent.KeyDown=1;
BBGameEvent.KeyUp=2;
BBGameEvent.KeyChar=3;
BBGameEvent.MouseDown=4;
BBGameEvent.MouseUp=5;
BBGameEvent.MouseMove=6;
BBGameEvent.TouchDown=7;
BBGameEvent.TouchUp=8;
BBGameEvent.TouchMove=9;
BBGameEvent.MotionAccel=10;

function BBGameDelegate(){}
BBGameDelegate.prototype.StartGame=function(){}
BBGameDelegate.prototype.SuspendGame=function(){}
BBGameDelegate.prototype.ResumeGame=function(){}
BBGameDelegate.prototype.UpdateGame=function(){}
BBGameDelegate.prototype.RenderGame=function(){}
BBGameDelegate.prototype.KeyEvent=function( ev,data ){}
BBGameDelegate.prototype.MouseEvent=function( ev,data,x,y ){}
BBGameDelegate.prototype.TouchEvent=function( ev,data,x,y ){}
BBGameDelegate.prototype.MotionEvent=function( ev,data,x,y,z ){}
BBGameDelegate.prototype.DiscardGraphics=function(){}

function BBDisplayMode( width,height ){
	this.width=width;
	this.height=height;
}

function BBGame(){
	BBGame._game=this;
	this._delegate=null;
	this._keyboardEnabled=false;
	this._updateRate=0;
	this._started=false;
	this._suspended=false;
	this._debugExs=(CFG_CONFIG=="debug");
	this._startms=Date.now();
}

BBGame.Game=function(){
	return BBGame._game;
}

BBGame.prototype.SetDelegate=function( delegate ){
	this._delegate=delegate;
}

BBGame.prototype.Delegate=function(){
	return this._delegate;
}

BBGame.prototype.SetUpdateRate=function( updateRate ){
	this._updateRate=updateRate;
}

BBGame.prototype.SetKeyboardEnabled=function( keyboardEnabled ){
	this._keyboardEnabled=keyboardEnabled;
}

BBGame.prototype.Started=function(){
	return this._started;
}

BBGame.prototype.Suspended=function(){
	return this._suspended;
}

BBGame.prototype.Millisecs=function(){
	return Date.now()-this._startms;
}

BBGame.prototype.GetDate=function( date ){
	var n=date.length;
	if( n>0 ){
		var t=new Date();
		date[0]=t.getFullYear();
		if( n>1 ){
			date[1]=t.getMonth()+1;
			if( n>2 ){
				date[2]=t.getDate();
				if( n>3 ){
					date[3]=t.getHours();
					if( n>4 ){
						date[4]=t.getMinutes();
						if( n>5 ){
							date[5]=t.getSeconds();
							if( n>6 ){
								date[6]=t.getMilliseconds();
							}
						}
					}
				}
			}
		}
	}
}

BBGame.prototype.SaveState=function( state ){
	localStorage.setItem( "monkeystate@"+document.URL,state );	//key can't start with dot in Chrome!
	return 1;
}

BBGame.prototype.LoadState=function(){
	var state=localStorage.getItem( "monkeystate@"+document.URL );
	if( state ) return state;
	return "";
}

BBGame.prototype.LoadString=function( path ){

	var xhr=new XMLHttpRequest();
	xhr.open( "GET",this.PathToUrl( path ),false );
	
//	if( navigator.userAgent.indexOf( "Chrome/48." )>0 ){
//		xhr.setRequestHeader( "If-Modified-Since","Sat, 1 Jan 2000 00:00:00 GMT" );
//	}
	
	xhr.send( null );
	
	if( xhr.status==200 || xhr.status==0 ) return xhr.responseText;
	
	return "";
}

BBGame.prototype.CountJoysticks=function( update ){
	return 0;
}

BBGame.prototype.PollJoystick=function( port,joyx,joyy,joyz,buttons ){
	return false;
}

BBGame.prototype.OpenUrl=function( url ){
	window.location=url;
}

BBGame.prototype.SetMouseVisible=function( visible ){
	if( visible ){
		this._canvas.style.cursor='default';	
	}else{
		this._canvas.style.cursor="url('data:image/cur;base64,AAACAAEAICAAAAAAAACoEAAAFgAAACgAAAAgAAAAQAAAAAEAIAAAAAAAgBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA55ZXBgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOeWVxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADnllcGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////9////////////////////+////////f/////////8%3D'), auto";
	}
}

BBGame.prototype.GetDeviceWidth=function(){
	return 0;
}

BBGame.prototype.GetDeviceHeight=function(){
	return 0;
}

BBGame.prototype.SetDeviceWindow=function( width,height,flags ){
}

BBGame.prototype.GetDisplayModes=function(){
	return new Array();
}

BBGame.prototype.GetDesktopMode=function(){
	return null;
}

BBGame.prototype.SetSwapInterval=function( interval ){
}

BBGame.prototype.PathToFilePath=function( path ){
	return "";
}

//***** js Game *****

BBGame.prototype.PathToUrl=function( path ){
	return path;
}

BBGame.prototype.LoadData=function( path ){

	var xhr=new XMLHttpRequest();
	xhr.open( "GET",this.PathToUrl( path ),false );

	if( xhr.overrideMimeType ) xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
	
//	if( navigator.userAgent.indexOf( "Chrome/48." )>0 ){
//		xhr.setRequestHeader( "If-Modified-Since","Sat, 1 Jan 2000 00:00:00 GMT" );
//	}

	xhr.send( null );
	if( xhr.status!=200 && xhr.status!=0 ) return null;

	var r=xhr.responseText;
	var buf=new ArrayBuffer( r.length );
	var bytes=new Int8Array( buf );
	for( var i=0;i<r.length;++i ){
		bytes[i]=r.charCodeAt( i );
	}
	return buf;
}

//***** INTERNAL ******

BBGame.prototype.Die=function( ex ){

	this._delegate=new BBGameDelegate();
	
	if( !ex.toString() ){
		return;
	}
	
	if( this._debugExs ){
		print( "Monkey Runtime Error : "+ex.toString() );
		print( stackTrace() );
	}
	
	throw ex;
}

BBGame.prototype.StartGame=function(){

	if( this._started ) return;
	this._started=true;
	
	if( this._debugExs ){
		try{
			this._delegate.StartGame();
		}catch( ex ){
			this.Die( ex );
		}
	}else{
		this._delegate.StartGame();
	}
}

BBGame.prototype.SuspendGame=function(){

	if( !this._started || this._suspended ) return;
	this._suspended=true;
	
	if( this._debugExs ){
		try{
			this._delegate.SuspendGame();
		}catch( ex ){
			this.Die( ex );
		}
	}else{
		this._delegate.SuspendGame();
	}
}

BBGame.prototype.ResumeGame=function(){

	if( !this._started || !this._suspended ) return;
	this._suspended=false;
	
	if( this._debugExs ){
		try{
			this._delegate.ResumeGame();
		}catch( ex ){
			this.Die( ex );
		}
	}else{
		this._delegate.ResumeGame();
	}
}

BBGame.prototype.UpdateGame=function(){

	if( !this._started || this._suspended ) return;

	if( this._debugExs ){
		try{
			this._delegate.UpdateGame();
		}catch( ex ){
			this.Die( ex );
		}	
	}else{
		this._delegate.UpdateGame();
	}
}

BBGame.prototype.RenderGame=function(){

	if( !this._started ) return;
	
	if( this._debugExs ){
		try{
			this._delegate.RenderGame();
		}catch( ex ){
			this.Die( ex );
		}	
	}else{
		this._delegate.RenderGame();
	}
}

BBGame.prototype.KeyEvent=function( ev,data ){

	if( !this._started ) return;
	
	if( this._debugExs ){
		try{
			this._delegate.KeyEvent( ev,data );
		}catch( ex ){
			this.Die( ex );
		}
	}else{
		this._delegate.KeyEvent( ev,data );
	}
}

BBGame.prototype.MouseEvent=function( ev,data,x,y ){

	if( !this._started ) return;
	
	if( this._debugExs ){
		try{
			this._delegate.MouseEvent( ev,data,x,y );
		}catch( ex ){
			this.Die( ex );
		}
	}else{
		this._delegate.MouseEvent( ev,data,x,y );
	}
}

BBGame.prototype.TouchEvent=function( ev,data,x,y ){

	if( !this._started ) return;
	
	if( this._debugExs ){
		try{
			this._delegate.TouchEvent( ev,data,x,y );
		}catch( ex ){
			this.Die( ex );
		}
	}else{
		this._delegate.TouchEvent( ev,data,x,y );
	}
}

BBGame.prototype.MotionEvent=function( ev,data,x,y,z ){

	if( !this._started ) return;
	
	if( this._debugExs ){
		try{
			this._delegate.MotionEvent( ev,data,x,y,z );
		}catch( ex ){
			this.Die( ex );
		}
	}else{
		this._delegate.MotionEvent( ev,data,x,y,z );
	}
}

BBGame.prototype.DiscardGraphics=function(){

	if( !this._started ) return;
	
	if( this._debugExs ){
		try{
			this._delegate.DiscardGraphics();
		}catch( ex ){
			this.Die( ex );
		}
	}else{
		this._delegate.DiscardGraphics();
	}
}


var webglGraphicsSeq=1;

function BBHtml5Game( canvas ){

	BBGame.call( this );
	BBHtml5Game._game=this;
	this._canvas=canvas;
	this._loading=0;
	this._timerSeq=0;
	this._gl=null;
	
	if( CFG_OPENGL_GLES20_ENABLED=="1" ){

		//can't get these to fire!
		canvas.addEventListener( "webglcontextlost",function( event ){
			event.preventDefault();
//			print( "WebGL context lost!" );
		},false );

		canvas.addEventListener( "webglcontextrestored",function( event ){
			++webglGraphicsSeq;
//			print( "WebGL context restored!" );
		},false );

		var attrs={ alpha:false };
	
		this._gl=this._canvas.getContext( "webgl",attrs );

		if( !this._gl ) this._gl=this._canvas.getContext( "experimental-webgl",attrs );
		
		if( !this._gl ) this.Die( "Can't create WebGL" );
		
		gl=this._gl;
	}
	
	// --- start gamepad api by skn3 ---------
	this._gamepads = null;
	this._gamepadLookup = [-1,-1,-1,-1];//support 4 gamepads
	var that = this;
	window.addEventListener("gamepadconnected", function(e) {
		that.connectGamepad(e.gamepad);
	});
	
	window.addEventListener("gamepaddisconnected", function(e) {
		that.disconnectGamepad(e.gamepad);
	});
	
	//need to process already connected gamepads (before page was loaded)
	var gamepads = this.getGamepads();
	if (gamepads && gamepads.length > 0) {
		for(var index=0;index < gamepads.length;index++) {
			this.connectGamepad(gamepads[index]);
		}
	}
	// --- end gamepad api by skn3 ---------
}

BBHtml5Game.prototype=extend_class( BBGame );

BBHtml5Game.Html5Game=function(){
	return BBHtml5Game._game;
}

// --- start gamepad api by skn3 ---------
BBHtml5Game.prototype.getGamepads = function() {
	return navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
}

BBHtml5Game.prototype.connectGamepad = function(gamepad) {
	if (!gamepad) {
		return false;
	}
	
	//check if this is a standard gamepad
	if (gamepad.mapping == "standard") {
		//yup so lets add it to an array of valid gamepads
		//find empty controller slot
		var slot = -1;
		for(var index = 0;index < this._gamepadLookup.length;index++) {
			if (this._gamepadLookup[index] == -1) {
				slot = index;
				break;
			}
		}
		
		//can we add this?
		if (slot != -1) {
			this._gamepadLookup[slot] = gamepad.index;
			
			//console.log("gamepad at html5 index "+gamepad.index+" mapped to monkey gamepad unit "+slot);
		}
	} else {
		console.log('Monkey has ignored gamepad at raw port #'+gamepad.index+' with unrecognised mapping scheme \''+gamepad.mapping+'\'.');
	}
}

BBHtml5Game.prototype.disconnectGamepad = function(gamepad) {
	if (!gamepad) {
		return false;
	}
	
	//scan all gamepads for matching index
	for(var index = 0;index < this._gamepadLookup.length;index++) {
		if (this._gamepadLookup[index] == gamepad.index) {
			//remove this gamepad
			this._gamepadLookup[index] = -1
			break;
		}
	}
}

BBHtml5Game.prototype.PollJoystick=function(port, joyx, joyy, joyz, buttons){
	//is this the first gamepad being polled
	if (port == 0) {
		//yes it is so we use the web api to get all gamepad info
		//we can then use this in subsequent calls to PollJoystick
		this._gamepads = this.getGamepads();
	}
	
	//dont bother processing if nothing to process
	if (!this._gamepads) {
	  return false;
	}
	
	//so use the monkey port to find the correct raw data
	var index = this._gamepadLookup[port];
	if (index == -1) {
		return false;
	}

	var gamepad = this._gamepads[index];
	if (!gamepad) {
		return false;
	}
	//so now process gamepad axis/buttons according to the standard mappings
	//https://w3c.github.io/gamepad/#remapping
	
	//left stick axis
	joyx[0] = gamepad.axes[0];
	joyy[0] = -gamepad.axes[1];
	
	//right stick axis
	joyx[1] = gamepad.axes[2];
	joyy[1] = -gamepad.axes[3];
	
	//left trigger
	joyz[0] = gamepad.buttons[6] ? gamepad.buttons[6].value : 0.0;
	
	//right trigger
	joyz[1] = gamepad.buttons[7] ? gamepad.buttons[7].value : 0.0;
	
	//clear button states
	for(var index = 0;index <32;index++) {
		buttons[index] = false;
	}
	
	//map html5 "standard" mapping to monkeys joy codes
	/*
	Const JOY_A=0
	Const JOY_B=1
	Const JOY_X=2
	Const JOY_Y=3
	Const JOY_LB=4
	Const JOY_RB=5
	Const JOY_BACK=6
	Const JOY_START=7
	Const JOY_LEFT=8
	Const JOY_UP=9
	Const JOY_RIGHT=10
	Const JOY_DOWN=11
	Const JOY_LSB=12
	Const JOY_RSB=13
	Const JOY_MENU=14
	*/
	buttons[0] = gamepad.buttons[0] && gamepad.buttons[0].pressed;
	buttons[1] = gamepad.buttons[1] && gamepad.buttons[1].pressed;
	buttons[2] = gamepad.buttons[2] && gamepad.buttons[2].pressed;
	buttons[3] = gamepad.buttons[3] && gamepad.buttons[3].pressed;
	buttons[4] = gamepad.buttons[4] && gamepad.buttons[4].pressed;
	buttons[5] = gamepad.buttons[5] && gamepad.buttons[5].pressed;
	buttons[6] = gamepad.buttons[8] && gamepad.buttons[8].pressed;
	buttons[7] = gamepad.buttons[9] && gamepad.buttons[9].pressed;
	buttons[8] = gamepad.buttons[14] && gamepad.buttons[14].pressed;
	buttons[9] = gamepad.buttons[12] && gamepad.buttons[12].pressed;
	buttons[10] = gamepad.buttons[15] && gamepad.buttons[15].pressed;
	buttons[11] = gamepad.buttons[13] && gamepad.buttons[13].pressed;
	buttons[12] = gamepad.buttons[10] && gamepad.buttons[10].pressed;
	buttons[13] = gamepad.buttons[11] && gamepad.buttons[11].pressed;
	buttons[14] = gamepad.buttons[16] && gamepad.buttons[16].pressed;
	
	//success
	return true
}
// --- end gamepad api by skn3 ---------


BBHtml5Game.prototype.ValidateUpdateTimer=function(){

	++this._timerSeq;
	if( this._suspended ) return;
	
	var game=this;
	var seq=game._timerSeq;
	
	var maxUpdates=4;
	var updateRate=this._updateRate;
	
	if( !updateRate ){

		var reqAnimFrame=(window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame);
	
		if( reqAnimFrame ){
			function animate(){
				if( seq!=game._timerSeq ) return;
	
				game.UpdateGame();
				if( seq!=game._timerSeq ) return;
	
				reqAnimFrame( animate );
				game.RenderGame();
			}
			reqAnimFrame( animate );
			return;
		}
		
		maxUpdates=1;
		updateRate=60;
	}
	
	var updatePeriod=1000.0/updateRate;
	var nextUpdate=0;

	function timeElapsed(){
		if( seq!=game._timerSeq ) return;
		
		if( !nextUpdate ) nextUpdate=Date.now();
		
		for( var i=0;i<maxUpdates;++i ){
		
			game.UpdateGame();
			if( seq!=game._timerSeq ) return;
			
			nextUpdate+=updatePeriod;
			var delay=nextUpdate-Date.now();
			
			if( delay>0 ){
				setTimeout( timeElapsed,delay );
				game.RenderGame();
				return;
			}
		}
		nextUpdate=0;
		setTimeout( timeElapsed,0 );
		game.RenderGame();
	}

	setTimeout( timeElapsed,0 );
}

//***** BBGame methods *****

BBHtml5Game.prototype.SetUpdateRate=function( updateRate ){

	BBGame.prototype.SetUpdateRate.call( this,updateRate );
	
	this.ValidateUpdateTimer();
}

BBHtml5Game.prototype.GetMetaData=function( path,key ){
	if( path.indexOf( "monkey://data/" )!=0 ) return "";
	path=path.slice(14);

	var i=META_DATA.indexOf( "["+path+"]" );
	if( i==-1 ) return "";
	i+=path.length+2;

	var e=META_DATA.indexOf( "\n",i );
	if( e==-1 ) e=META_DATA.length;

	i=META_DATA.indexOf( ";"+key+"=",i )
	if( i==-1 || i>=e ) return "";
	i+=key.length+2;

	e=META_DATA.indexOf( ";",i );
	if( e==-1 ) return "";

	return META_DATA.slice( i,e );
}

BBHtml5Game.prototype.PathToUrl=function( path ){
	if( path.indexOf( "monkey:" )!=0 ){
		return path;
	}else if( path.indexOf( "monkey://data/" )==0 ) {
		return "data/"+path.slice( 14 );
	}
	return "";
}

BBHtml5Game.prototype.GetLoading=function(){
	return this._loading;
}

BBHtml5Game.prototype.IncLoading=function(){
	++this._loading;
	return this._loading;
}

BBHtml5Game.prototype.DecLoading=function(){
	--this._loading;
	return this._loading;
}

BBHtml5Game.prototype.GetCanvas=function(){
	return this._canvas;
}

BBHtml5Game.prototype.GetWebGL=function(){
	return this._gl;
}

BBHtml5Game.prototype.GetDeviceWidth=function(){
	return this._canvas.width;
}

BBHtml5Game.prototype.GetDeviceHeight=function(){
	return this._canvas.height;
}

//***** INTERNAL *****

BBHtml5Game.prototype.UpdateGame=function(){

	if( !this._loading ) BBGame.prototype.UpdateGame.call( this );
}

BBHtml5Game.prototype.SuspendGame=function(){

	BBGame.prototype.SuspendGame.call( this );
	
	BBGame.prototype.RenderGame.call( this );
	
	this.ValidateUpdateTimer();
}

BBHtml5Game.prototype.ResumeGame=function(){

	BBGame.prototype.ResumeGame.call( this );
	
	this.ValidateUpdateTimer();
}

BBHtml5Game.prototype.Run=function(){

	var game=this;
	var canvas=game._canvas;
	
	var xscale=1;
	var yscale=1;
	
	var touchIds=new Array( 32 );
	for( i=0;i<32;++i ) touchIds[i]=-1;
	
	function eatEvent( e ){
		if( e.stopPropagation ){
			e.stopPropagation();
			e.preventDefault();
		}else{
			e.cancelBubble=true;
			e.returnValue=false;
		}
	}
	
	function keyToChar( key ){
		switch( key ){
		case 8:case 9:case 13:case 27:case 32:return key;
		case 33:case 34:case 35:case 36:case 37:case 38:case 39:case 40:case 45:return key|0x10000;
		case 46:return 127;
		}
		return 0;
	}
	
	function mouseX( e ){
		var x=e.clientX+document.body.scrollLeft;
		var c=canvas;
		while( c ){
			x-=c.offsetLeft;
			c=c.offsetParent;
		}
		return x*xscale;
	}
	
	function mouseY( e ){
		var y=e.clientY+document.body.scrollTop;
		var c=canvas;
		while( c ){
			y-=c.offsetTop;
			c=c.offsetParent;
		}
		return y*yscale;
	}

	function touchX( touch ){
		var x=touch.pageX;
		var c=canvas;
		while( c ){
			x-=c.offsetLeft;
			c=c.offsetParent;
		}
		return x*xscale;
	}			
	
	function touchY( touch ){
		var y=touch.pageY;
		var c=canvas;
		while( c ){
			y-=c.offsetTop;
			c=c.offsetParent;
		}
		return y*yscale;
	}
	
	canvas.onkeydown=function( e ){
		game.KeyEvent( BBGameEvent.KeyDown,e.keyCode );
		var chr=keyToChar( e.keyCode );
		if( chr ) game.KeyEvent( BBGameEvent.KeyChar,chr );
		if( e.keyCode<48 || (e.keyCode>111 && e.keyCode<122) ) eatEvent( e );
	}

	canvas.onkeyup=function( e ){
		game.KeyEvent( BBGameEvent.KeyUp,e.keyCode );
	}

	canvas.onkeypress=function( e ){
		if( e.charCode ){
			game.KeyEvent( BBGameEvent.KeyChar,e.charCode );
		}else if( e.which ){
			game.KeyEvent( BBGameEvent.KeyChar,e.which );
		}
	}

	canvas.onmousedown=function( e ){
		switch( e.button ){
		case 0:game.MouseEvent( BBGameEvent.MouseDown,0,mouseX(e),mouseY(e) );break;
		case 1:game.MouseEvent( BBGameEvent.MouseDown,2,mouseX(e),mouseY(e) );break;
		case 2:game.MouseEvent( BBGameEvent.MouseDown,1,mouseX(e),mouseY(e) );break;
		}
		eatEvent( e );
	}
	
	canvas.onmouseup=function( e ){
		switch( e.button ){
		case 0:game.MouseEvent( BBGameEvent.MouseUp,0,mouseX(e),mouseY(e) );break;
		case 1:game.MouseEvent( BBGameEvent.MouseUp,2,mouseX(e),mouseY(e) );break;
		case 2:game.MouseEvent( BBGameEvent.MouseUp,1,mouseX(e),mouseY(e) );break;
		}
		eatEvent( e );
	}
	
	canvas.onmousemove=function( e ){
		game.MouseEvent( BBGameEvent.MouseMove,-1,mouseX(e),mouseY(e) );
		eatEvent( e );
	}

	canvas.onmouseout=function( e ){
		game.MouseEvent( BBGameEvent.MouseUp,0,mouseX(e),mouseY(e) );
		game.MouseEvent( BBGameEvent.MouseUp,1,mouseX(e),mouseY(e) );
		game.MouseEvent( BBGameEvent.MouseUp,2,mouseX(e),mouseY(e) );
		eatEvent( e );
	}
	
	canvas.onclick=function( e ){
		if( game.Suspended() ){
			canvas.focus();
		}
		eatEvent( e );
		return;
	}
	
	canvas.oncontextmenu=function( e ){
		return false;
	}
	
	canvas.ontouchstart=function( e ){
		if( game.Suspended() ){
			canvas.focus();
		}
		for( var i=0;i<e.changedTouches.length;++i ){
			var touch=e.changedTouches[i];
			for( var j=0;j<32;++j ){
				if( touchIds[j]!=-1 ) continue;
				touchIds[j]=touch.identifier;
				game.TouchEvent( BBGameEvent.TouchDown,j,touchX(touch),touchY(touch) );
				break;
			}
		}
		eatEvent( e );
	}
	
	canvas.ontouchmove=function( e ){
		for( var i=0;i<e.changedTouches.length;++i ){
			var touch=e.changedTouches[i];
			for( var j=0;j<32;++j ){
				if( touchIds[j]!=touch.identifier ) continue;
				game.TouchEvent( BBGameEvent.TouchMove,j,touchX(touch),touchY(touch) );
				break;
			}
		}
		eatEvent( e );
	}
	
	canvas.ontouchend=function( e ){
		for( var i=0;i<e.changedTouches.length;++i ){
			var touch=e.changedTouches[i];
			for( var j=0;j<32;++j ){
				if( touchIds[j]!=touch.identifier ) continue;
				touchIds[j]=-1;
				game.TouchEvent( BBGameEvent.TouchUp,j,touchX(touch),touchY(touch) );
				break;
			}
		}
		eatEvent( e );
	}
	
	window.ondevicemotion=function( e ){
		var tx=e.accelerationIncludingGravity.x/9.81;
		var ty=e.accelerationIncludingGravity.y/9.81;
		var tz=e.accelerationIncludingGravity.z/9.81;
		var x,y;
		switch( window.orientation ){
		case   0:x=+tx;y=-ty;break;
		case 180:x=-tx;y=+ty;break;
		case  90:x=-ty;y=-tx;break;
		case -90:x=+ty;y=+tx;break;
		}
		game.MotionEvent( BBGameEvent.MotionAccel,0,x,y,tz );
		eatEvent( e );
	}

	canvas.onfocus=function( e ){
		if( CFG_MOJO_AUTO_SUSPEND_ENABLED=="1" ){
			game.ResumeGame();
		}else{
			game.ValidateUpdateTimer();
		}
	}
	
	canvas.onblur=function( e ){
		for( var i=0;i<256;++i ) game.KeyEvent( BBGameEvent.KeyUp,i );
		if( CFG_MOJO_AUTO_SUSPEND_ENABLED=="1" ){
			game.SuspendGame();
		}
	}

	canvas.updateSize=function(){
		xscale=canvas.width/canvas.clientWidth;
		yscale=canvas.height/canvas.clientHeight;
		game.RenderGame();
	}
	
	canvas.updateSize();
	
	canvas.focus();
	
	game.StartGame();
	
	game.RenderGame();
}


function BBMonkeyGame( canvas ){
	BBHtml5Game.call( this,canvas );
}

BBMonkeyGame.prototype=extend_class( BBHtml5Game );

BBMonkeyGame.Main=function( canvas ){

	var game=new BBMonkeyGame( canvas );

	try{

		bbInit();
		bbMain();

	}catch( ex ){
	
		game.Die( ex );
		return;
	}

	if( !game.Delegate() ) return;
	
	game.Run();
}


// HTML5 mojo runtime.
//
// Copyright 2011 Mark Sibly, all rights reserved.
// No warranty implied; use at your own risk.

// ***** gxtkGraphics class *****

function gxtkGraphics(){
	this.game=BBHtml5Game.Html5Game();
	this.canvas=this.game.GetCanvas()
	this.width=this.canvas.width;
	this.height=this.canvas.height;
	this.gl=null;
	this.gc=this.canvas.getContext( '2d' );
	this.tmpCanvas=null;
	this.r=255;
	this.b=255;
	this.g=255;
	this.white=true;
	this.color="rgb(255,255,255)"
	this.alpha=1;
	this.blend="source-over";
	this.ix=1;this.iy=0;
	this.jx=0;this.jy=1;
	this.tx=0;this.ty=0;
	this.tformed=false;
	this.scissorX=0;
	this.scissorY=0;
	this.scissorWidth=0;
	this.scissorHeight=0;
	this.clipped=false;
}

gxtkGraphics.prototype.BeginRender=function(){
	this.width=this.canvas.width;
	this.height=this.canvas.height;
	if( !this.gc ) return 0;
	this.gc.save();
	if( this.game.GetLoading() ) return 2;
	return 1;
}

gxtkGraphics.prototype.EndRender=function(){
	if( this.gc ) this.gc.restore();
}

gxtkGraphics.prototype.Width=function(){
	return this.width;
}

gxtkGraphics.prototype.Height=function(){
	return this.height;
}

gxtkGraphics.prototype.LoadSurface=function( path ){
	var game=this.game;

	var ty=game.GetMetaData( path,"type" );
	if( ty.indexOf( "image/" )!=0 ) return null;
	
	game.IncLoading();

	var image=new Image();
	image.onload=function(){ game.DecLoading(); }
	image.onerror=function(){ game.DecLoading(); }
	image.meta_width=parseInt( game.GetMetaData( path,"width" ) );
	image.meta_height=parseInt( game.GetMetaData( path,"height" ) );
	image.src=game.PathToUrl( path );

	return new gxtkSurface( image,this );
}

gxtkGraphics.prototype.CreateSurface=function( width,height ){
	var canvas=document.createElement( 'canvas' );
	
	canvas.width=width;
	canvas.height=height;
	canvas.meta_width=width;
	canvas.meta_height=height;
	canvas.complete=true;
	
	var surface=new gxtkSurface( canvas,this );
	
	surface.gc=canvas.getContext( '2d' );
	
	return surface;
}

gxtkGraphics.prototype.SetAlpha=function( alpha ){
	this.alpha=alpha;
	this.gc.globalAlpha=alpha;
}

gxtkGraphics.prototype.SetColor=function( r,g,b ){
	this.r=r;
	this.g=g;
	this.b=b;
	this.white=(r==255 && g==255 && b==255);
	this.color="rgb("+(r|0)+","+(g|0)+","+(b|0)+")";
	this.gc.fillStyle=this.color;
	this.gc.strokeStyle=this.color;
}

gxtkGraphics.prototype.SetBlend=function( blend ){
	switch( blend ){
	case 1:
		this.blend="lighter";
		break;
	default:
		this.blend="source-over";
	}
	this.gc.globalCompositeOperation=this.blend;
}

gxtkGraphics.prototype.SetScissor=function( x,y,w,h ){
	this.scissorX=x;
	this.scissorY=y;
	this.scissorWidth=w;
	this.scissorHeight=h;
	this.clipped=(x!=0 || y!=0 || w!=this.canvas.width || h!=this.canvas.height);
	this.gc.restore();
	this.gc.save();
	if( this.clipped ){
		this.gc.beginPath();
		this.gc.rect( x,y,w,h );
		this.gc.clip();
		this.gc.closePath();
	}
	this.gc.fillStyle=this.color;
	this.gc.strokeStyle=this.color;	
	this.gc.globalAlpha=this.alpha;	
	this.gc.globalCompositeOperation=this.blend;
	if( this.tformed ) this.gc.setTransform( this.ix,this.iy,this.jx,this.jy,this.tx,this.ty );
}

gxtkGraphics.prototype.SetMatrix=function( ix,iy,jx,jy,tx,ty ){
	this.ix=ix;this.iy=iy;
	this.jx=jx;this.jy=jy;
	this.tx=tx;this.ty=ty;
	this.gc.setTransform( ix,iy,jx,jy,tx,ty );
	this.tformed=(ix!=1 || iy!=0 || jx!=0 || jy!=1 || tx!=0 || ty!=0);
}

gxtkGraphics.prototype.Cls=function( r,g,b ){
	if( this.tformed ) this.gc.setTransform( 1,0,0,1,0,0 );
	this.gc.fillStyle="rgb("+(r|0)+","+(g|0)+","+(b|0)+")";
	this.gc.globalAlpha=1;
	this.gc.globalCompositeOperation="source-over";
	this.gc.fillRect( 0,0,this.canvas.width,this.canvas.height );
	this.gc.fillStyle=this.color;
	this.gc.globalAlpha=this.alpha;
	this.gc.globalCompositeOperation=this.blend;
	if( this.tformed ) this.gc.setTransform( this.ix,this.iy,this.jx,this.jy,this.tx,this.ty );
}

gxtkGraphics.prototype.DrawPoint=function( x,y ){
	if( this.tformed ){
		var px=x;
		x=px * this.ix + y * this.jx + this.tx;
		y=px * this.iy + y * this.jy + this.ty;
		this.gc.setTransform( 1,0,0,1,0,0 );
		this.gc.fillRect( x,y,1,1 );
		this.gc.setTransform( this.ix,this.iy,this.jx,this.jy,this.tx,this.ty );
	}else{
		this.gc.fillRect( x,y,1,1 );
	}
}

gxtkGraphics.prototype.DrawRect=function( x,y,w,h ){
	if( w<0 ){ x+=w;w=-w; }
	if( h<0 ){ y+=h;h=-h; }
	if( w<=0 || h<=0 ) return;
	//
	this.gc.fillRect( x,y,w,h );
}

gxtkGraphics.prototype.DrawLine=function( x1,y1,x2,y2 ){
	if( this.tformed ){
		var x1_t=x1 * this.ix + y1 * this.jx + this.tx;
		var y1_t=x1 * this.iy + y1 * this.jy + this.ty;
		var x2_t=x2 * this.ix + y2 * this.jx + this.tx;
		var y2_t=x2 * this.iy + y2 * this.jy + this.ty;
		this.gc.setTransform( 1,0,0,1,0,0 );
	  	this.gc.beginPath();
	  	this.gc.moveTo( x1_t,y1_t );
	  	this.gc.lineTo( x2_t,y2_t );
	  	this.gc.stroke();
	  	this.gc.closePath();
		this.gc.setTransform( this.ix,this.iy,this.jx,this.jy,this.tx,this.ty );
	}else{
	  	this.gc.beginPath();
	  	this.gc.moveTo( x1,y1 );
	  	this.gc.lineTo( x2,y2 );
	  	this.gc.stroke();
	  	this.gc.closePath();
	}
}

gxtkGraphics.prototype.DrawOval=function( x,y,w,h ){
	if( w<0 ){ x+=w;w=-w; }
	if( h<0 ){ y+=h;h=-h; }
	if( w<=0 || h<=0 ) return;
	//
  	var w2=w/2,h2=h/2;
	this.gc.save();
	this.gc.translate( x+w2,y+h2 );
	this.gc.scale( w2,h2 );
  	this.gc.beginPath();
	this.gc.arc( 0,0,1,0,Math.PI*2,false );
	this.gc.fill();
  	this.gc.closePath();
	this.gc.restore();
}

gxtkGraphics.prototype.DrawPoly=function( verts ){
	if( verts.length<2 ) return;
	this.gc.beginPath();
	this.gc.moveTo( verts[0],verts[1] );
	for( var i=2;i<verts.length;i+=2 ){
		this.gc.lineTo( verts[i],verts[i+1] );
	}
	this.gc.fill();
	this.gc.closePath();
}

gxtkGraphics.prototype.DrawPoly2=function( verts,surface,srx,srcy ){
	if( verts.length<4 ) return;
	this.gc.beginPath();
	this.gc.moveTo( verts[0],verts[1] );
	for( var i=4;i<verts.length;i+=4 ){
		this.gc.lineTo( verts[i],verts[i+1] );
	}
	this.gc.fill();
	this.gc.closePath();
}

gxtkGraphics.prototype.DrawSurface=function( surface,x,y ){
	if( !surface.image.complete ) return;
	
	if( this.white ){
		this.gc.drawImage( surface.image,x,y );
		return;
	}
	
	this.DrawImageTinted( surface.image,x,y,0,0,surface.swidth,surface.sheight );
}

gxtkGraphics.prototype.DrawSurface2=function( surface,x,y,srcx,srcy,srcw,srch ){
	if( !surface.image.complete ) return;

	if( srcw<0 ){ srcx+=srcw;srcw=-srcw; }
	if( srch<0 ){ srcy+=srch;srch=-srch; }
	if( srcw<=0 || srch<=0 ) return;

	if( this.white ){
		this.gc.drawImage( surface.image,srcx,srcy,srcw,srch,x,y,srcw,srch );
		return;
	}
	
	this.DrawImageTinted( surface.image,x,y,srcx,srcy,srcw,srch  );
}

gxtkGraphics.prototype.DrawImageTinted=function( image,dx,dy,sx,sy,sw,sh ){

	if( !this.tmpCanvas ){
		this.tmpCanvas=document.createElement( "canvas" );
	}

	if( sw>this.tmpCanvas.width || sh>this.tmpCanvas.height ){
		this.tmpCanvas.width=Math.max( sw,this.tmpCanvas.width );
		this.tmpCanvas.height=Math.max( sh,this.tmpCanvas.height );
	}
	
	var tmpGC=this.tmpCanvas.getContext( "2d" );
	tmpGC.globalCompositeOperation="copy";
	
	tmpGC.drawImage( image,sx,sy,sw,sh,0,0,sw,sh );
	
	var imgData=tmpGC.getImageData( 0,0,sw,sh );
	
	var p=imgData.data,sz=sw*sh*4,i;
	
	for( i=0;i<sz;i+=4 ){
		p[i]=p[i]*this.r/255;
		p[i+1]=p[i+1]*this.g/255;
		p[i+2]=p[i+2]*this.b/255;
	}
	
	tmpGC.putImageData( imgData,0,0 );
	
	this.gc.drawImage( this.tmpCanvas,0,0,sw,sh,dx,dy,sw,sh );
}

gxtkGraphics.prototype.ReadPixels=function( pixels,x,y,width,height,offset,pitch ){

	var imgData=this.gc.getImageData( x,y,width,height );
	
	var p=imgData.data,i=0,j=offset,px,py;
	
	for( py=0;py<height;++py ){
		for( px=0;px<width;++px ){
			pixels[j++]=(p[i+3]<<24)|(p[i]<<16)|(p[i+1]<<8)|p[i+2];
			i+=4;
		}
		j+=pitch-width;
	}
}

gxtkGraphics.prototype.WritePixels2=function( surface,pixels,x,y,width,height,offset,pitch ){

	if( !surface.gc ){
		if( !surface.image.complete ) return;
		var canvas=document.createElement( "canvas" );
		canvas.width=surface.swidth;
		canvas.height=surface.sheight;
		surface.gc=canvas.getContext( "2d" );
		surface.gc.globalCompositeOperation="copy";
		surface.gc.drawImage( surface.image,0,0 );
		surface.image=canvas;
	}

	var imgData=surface.gc.createImageData( width,height );

	var p=imgData.data,i=0,j=offset,px,py,argb;
	
	for( py=0;py<height;++py ){
		for( px=0;px<width;++px ){
			argb=pixels[j++];
			p[i]=(argb>>16) & 0xff;
			p[i+1]=(argb>>8) & 0xff;
			p[i+2]=argb & 0xff;
			p[i+3]=(argb>>24) & 0xff;
			i+=4;
		}
		j+=pitch-width;
	}
	
	surface.gc.putImageData( imgData,x,y );
}

// ***** gxtkSurface class *****

function gxtkSurface( image,graphics ){
	this.image=image;
	this.graphics=graphics;
	this.swidth=image.meta_width;
	this.sheight=image.meta_height;
}

// ***** GXTK API *****

gxtkSurface.prototype.Discard=function(){
	if( this.image ){
		this.image=null;
	}
}

gxtkSurface.prototype.Width=function(){
	return this.swidth;
}

gxtkSurface.prototype.Height=function(){
	return this.sheight;
}

gxtkSurface.prototype.Loaded=function(){
	return this.image.complete;
}

gxtkSurface.prototype.OnUnsafeLoadComplete=function(){
}

if( CFG_HTML5_WEBAUDIO_ENABLED=="1" && (window.AudioContext || window.webkitAudioContext) ){

//print( "Using WebAudio!" );

// ***** WebAudio *****

var wa=null;

// ***** WebAudio gxtkSample *****

var gxtkSample=function(){
	this.waBuffer=null;
	this.state=0;
}

gxtkSample.prototype.Load=function( path ){
	if( this.state ) return false;

	var req=new XMLHttpRequest();
	
	req.open( "get",BBGame.Game().PathToUrl( path ),true );
	req.responseType="arraybuffer";
	
	var abuf=this;
	
	req.onload=function(){
		wa.decodeAudioData( req.response,function( buffer ){
			//success!
			abuf.waBuffer=buffer;
			abuf.state=1;
		},function(){
			abuf.state=-1;
		} );
	}
	
	req.onerror=function(){
		abuf.state=-1;
	}
	
	req.send();
	
	this.state=2;
			
	return true;
}

gxtkSample.prototype.Discard=function(){
}

// ***** WebAudio gxtkChannel *****

var gxtkChannel=function(){
	this.buffer=null;
	this.flags=0;
	this.volume=1;
	this.pan=0;
	this.rate=1;
	this.waSource=null;
	this.waPan=wa.create
	this.waGain=wa.createGain();
	this.waGain.connect( wa.destination );
	this.waPanner=wa.createPanner();
	this.waPanner.rolloffFactor=0;
	this.waPanner.panningModel="equalpower";
	this.waPanner.connect( this.waGain );
	this.startTime=0;
	this.offset=0;
	this.state=0;
}

// ***** WebAudio gxtkAudio *****

var gxtkAudio=function(){

	if( !wa ){
		window.AudioContext=window.AudioContext || window.webkitAudioContext;
		wa=new AudioContext();
	}
	
	this.okay=true;
	this.music=null;
	this.musicState=0;
	this.musicVolume=1;
	this.channels=new Array();
	for( var i=0;i<32;++i ){
		this.channels[i]=new gxtkChannel();
	}
}

gxtkAudio.prototype.Suspend=function(){
	if( this.MusicState()==1 ) this.music.pause();
	for( var i=0;i<32;++i ){
		var chan=this.channels[i];
		if( chan.state!=1 ) continue;
		this.PauseChannel( i );
		chan.state=5;
	}
}

gxtkAudio.prototype.Resume=function(){
	if( this.MusicState()==1 ) this.music.play();
	for( var i=0;i<32;++i ){
		var chan=this.channels[i];
		if( chan.state!=5 ) continue;
		chan.state=2;
		this.ResumeChannel( i );
	}
}

gxtkAudio.prototype.LoadSample=function( path ){

	var sample=new gxtkSample();
	if( !sample.Load( BBHtml5Game.Html5Game().PathToUrl( path ) ) ) return null;
	
	return sample;
}

gxtkAudio.prototype.PlaySample=function( buffer,channel,flags ){

	if( buffer.state!=1 ) return;

	var chan=this.channels[channel];
	
	if( chan.state ){
		chan.waSource.onended=null
		chan.waSource.stop( 0 );
	}
	
	chan.buffer=buffer;
	chan.flags=flags;

	chan.waSource=wa.createBufferSource();
	chan.waSource.buffer=buffer.waBuffer;
	chan.waSource.playbackRate.value=chan.rate;
	chan.waSource.loop=(flags&1)!=0;
	chan.waSource.connect( chan.waPanner );
	
	chan.waSource.onended=function( e ){
		chan.waSource=null;
		chan.state=0;
	}

	chan.offset=0;	
	chan.startTime=wa.currentTime;
	chan.waSource.start( 0 );

	chan.state=1;
}

gxtkAudio.prototype.StopChannel=function( channel ){

	var chan=this.channels[channel];
	if( !chan.state ) return;
	
	if( chan.state==1 ){
		chan.waSource.onended=null;
		chan.waSource.stop( 0 );
		chan.waSource=null;
	}

	chan.state=0;
}

gxtkAudio.prototype.PauseChannel=function( channel ){

	var chan=this.channels[channel];
	if( chan.state!=1 ) return;
	
	chan.offset=(chan.offset+(wa.currentTime-chan.startTime)*chan.rate)%chan.buffer.waBuffer.duration;
	
	chan.waSource.onended=null;
	chan.waSource.stop( 0 );
	chan.waSource=null;
	
	chan.state=2;
}

gxtkAudio.prototype.ResumeChannel=function( channel ){

	var chan=this.channels[channel];
	if( chan.state!=2 ) return;
	
	chan.waSource=wa.createBufferSource();
	chan.waSource.buffer=chan.buffer.waBuffer;
	chan.waSource.playbackRate.value=chan.rate;
	chan.waSource.loop=(chan.flags&1)!=0;
	chan.waSource.connect( chan.waPanner );
	
	chan.waSource.onended=function( e ){
		chan.waSource=null;
		chan.state=0;
	}
	
	chan.startTime=wa.currentTime;
	chan.waSource.start( 0,chan.offset );

	chan.state=1;
}

gxtkAudio.prototype.ChannelState=function( channel ){
	return this.channels[channel].state & 3;
}

gxtkAudio.prototype.SetVolume=function( channel,volume ){
	var chan=this.channels[channel];

	chan.volume=volume;
	
	chan.waGain.gain.value=volume;
}

gxtkAudio.prototype.SetPan=function( channel,pan ){
	var chan=this.channels[channel];

	chan.pan=pan;
	
	var sin=Math.sin( pan*3.14159265359/2 );
	var cos=Math.cos( pan*3.14159265359/2 );
	
	chan.waPanner.setPosition( sin,0,-cos );
}

gxtkAudio.prototype.SetRate=function( channel,rate ){

	var chan=this.channels[channel];

	if( chan.state==1 ){
		//update offset for pause/resume
		var time=wa.currentTime;
		chan.offset=(chan.offset+(time-chan.startTime)*chan.rate)%chan.buffer.waBuffer.duration;
		chan.startTime=time;
	}

	chan.rate=rate;
	
	if( chan.waSource ) chan.waSource.playbackRate.value=rate;
}

gxtkAudio.prototype.PlayMusic=function( path,flags ){
	if( this.musicState ) this.music.pause();
	this.music=new Audio( BBGame.Game().PathToUrl( path ) );
	this.music.loop=(flags&1)!=0;
	this.music.play();
	this.musicState=1;
}

gxtkAudio.prototype.StopMusic=function(){
	if( !this.musicState ) return;
	this.music.pause();
	this.music=null;
	this.musicState=0;
}

gxtkAudio.prototype.PauseMusic=function(){
	if( this.musicState!=1 ) return;
	this.music.pause();
	this.musicState=2;
}

gxtkAudio.prototype.ResumeMusic=function(){
	if( this.musicState!=2 ) return;
	this.music.play();
	this.musicState=1;
}

gxtkAudio.prototype.MusicState=function(){
	if( this.musicState==1 && this.music.ended && !this.music.loop ){
		this.music=null;
		this.musicState=0;
	}
	return this.musicState;
}

gxtkAudio.prototype.SetMusicVolume=function( volume ){
	this.musicVolume=volume;
	if( this.musicState ) this.music.volume=volume;
}

}else{

//print( "Using OldAudio!" );

// ***** gxtkChannel class *****

var gxtkChannel=function(){
	this.sample=null;
	this.audio=null;
	this.volume=1;
	this.pan=0;
	this.rate=1;
	this.flags=0;
	this.state=0;
}

// ***** gxtkAudio class *****

var gxtkAudio=function(){
	this.game=BBHtml5Game.Html5Game();
	this.okay=typeof(Audio)!="undefined";
	this.music=null;
	this.channels=new Array(33);
	for( var i=0;i<33;++i ){
		this.channels[i]=new gxtkChannel();
		if( !this.okay ) this.channels[i].state=-1;
	}
}

gxtkAudio.prototype.Suspend=function(){
	var i;
	for( i=0;i<33;++i ){
		var chan=this.channels[i];
		if( chan.state==1 ){
			if( chan.audio.ended && !chan.audio.loop ){
				chan.state=0;
			}else{
				chan.audio.pause();
				chan.state=3;
			}
		}
	}
}

gxtkAudio.prototype.Resume=function(){
	var i;
	for( i=0;i<33;++i ){
		var chan=this.channels[i];
		if( chan.state==3 ){
			chan.audio.play();
			chan.state=1;
		}
	}
}

gxtkAudio.prototype.LoadSample=function( path ){
	if( !this.okay ) return null;

	var audio=new Audio( this.game.PathToUrl( path ) );
	if( !audio ) return null;
	
	return new gxtkSample( audio );
}

gxtkAudio.prototype.PlaySample=function( sample,channel,flags ){
	if( !this.okay ) return;
	
	var chan=this.channels[channel];

	if( chan.state>0 ){
		chan.audio.pause();
		chan.state=0;
	}
	
	for( var i=0;i<33;++i ){
		var chan2=this.channels[i];
		if( chan2.state==1 && chan2.audio.ended && !chan2.audio.loop ) chan.state=0;
		if( chan2.state==0 && chan2.sample ){
			chan2.sample.FreeAudio( chan2.audio );
			chan2.sample=null;
			chan2.audio=null;
		}
	}

	var audio=sample.AllocAudio();
	if( !audio ) return;

	audio.loop=(flags&1)!=0;
	audio.volume=chan.volume;
	audio.play();

	chan.sample=sample;
	chan.audio=audio;
	chan.flags=flags;
	chan.state=1;
}

gxtkAudio.prototype.StopChannel=function( channel ){
	var chan=this.channels[channel];
	
	if( chan.state>0 ){
		chan.audio.pause();
		chan.state=0;
	}
}

gxtkAudio.prototype.PauseChannel=function( channel ){
	var chan=this.channels[channel];
	
	if( chan.state==1 ){
		if( chan.audio.ended && !chan.audio.loop ){
			chan.state=0;
		}else{
			chan.audio.pause();
			chan.state=2;
		}
	}
}

gxtkAudio.prototype.ResumeChannel=function( channel ){
	var chan=this.channels[channel];
	
	if( chan.state==2 ){
		chan.audio.play();
		chan.state=1;
	}
}

gxtkAudio.prototype.ChannelState=function( channel ){
	var chan=this.channels[channel];
	if( chan.state==1 && chan.audio.ended && !chan.audio.loop ) chan.state=0;
	if( chan.state==3 ) return 1;
	return chan.state;
}

gxtkAudio.prototype.SetVolume=function( channel,volume ){
	var chan=this.channels[channel];
	if( chan.state>0 ) chan.audio.volume=volume;
	chan.volume=volume;
}

gxtkAudio.prototype.SetPan=function( channel,pan ){
	var chan=this.channels[channel];
	chan.pan=pan;
}

gxtkAudio.prototype.SetRate=function( channel,rate ){
	var chan=this.channels[channel];
	chan.rate=rate;
}

gxtkAudio.prototype.PlayMusic=function( path,flags ){
	this.StopMusic();
	
	this.music=this.LoadSample( path );
	if( !this.music ) return;
	
	this.PlaySample( this.music,32,flags );
}

gxtkAudio.prototype.StopMusic=function(){
	this.StopChannel( 32 );

	if( this.music ){
		this.music.Discard();
		this.music=null;
	}
}

gxtkAudio.prototype.PauseMusic=function(){
	this.PauseChannel( 32 );
}

gxtkAudio.prototype.ResumeMusic=function(){
	this.ResumeChannel( 32 );
}

gxtkAudio.prototype.MusicState=function(){
	return this.ChannelState( 32 );
}

gxtkAudio.prototype.SetMusicVolume=function( volume ){
	this.SetVolume( 32,volume );
}

// ***** gxtkSample class *****

//function gxtkSample( audio ){
var gxtkSample=function( audio ){
	this.audio=audio;
	this.free=new Array();
	this.insts=new Array();
}

gxtkSample.prototype.FreeAudio=function( audio ){
	this.free.push( audio );
}

gxtkSample.prototype.AllocAudio=function(){
	var audio;
	while( this.free.length ){
		audio=this.free.pop();
		try{
			audio.currentTime=0;
			return audio;
		}catch( ex ){
//			print( "AUDIO ERROR1!" );
		}
	}
	
	//Max out?
	if( this.insts.length==8 ) return null;
	
	audio=new Audio( this.audio.src );
	
	//yucky loop handler for firefox!
	//
	audio.addEventListener( 'ended',function(){
		if( this.loop ){
			try{
				this.currentTime=0;
				this.play();
			}catch( ex ){
//				print( "AUDIO ERROR2!" );
			}
		}
	},false );

	this.insts.push( audio );
	return audio;
}

gxtkSample.prototype.Discard=function(){
}

}


function BBThread(){
	this.result=null;
	this.running=false;
}

BBThread.prototype.Start=function(){
	this.result=null;
	this.running=true;
	this.Run__UNSAFE__();
}

BBThread.prototype.IsRunning=function(){
	return this.running;
}

BBThread.prototype.Result=function(){
	return this.result;
}

BBThread.prototype.Run__UNSAFE__=function(){
	this.running=false;
}


function BBDataBuffer(){
	this.arrayBuffer=null;
	this.length=0;
}

BBDataBuffer.tbuf=new ArrayBuffer(4);
BBDataBuffer.tbytes=new Int8Array( BBDataBuffer.tbuf );
BBDataBuffer.tshorts=new Int16Array( BBDataBuffer.tbuf );
BBDataBuffer.tints=new Int32Array( BBDataBuffer.tbuf );
BBDataBuffer.tfloats=new Float32Array( BBDataBuffer.tbuf );

BBDataBuffer.prototype._Init=function( buffer ){
	this.arrayBuffer=buffer;
	this.length=buffer.byteLength;
	this.bytes=new Int8Array( buffer );	
	this.shorts=new Int16Array( buffer,0,this.length/2 );	
	this.ints=new Int32Array( buffer,0,this.length/4 );	
	this.floats=new Float32Array( buffer,0,this.length/4 );
}

BBDataBuffer.prototype._New=function( length ){
	if( this.arrayBuffer ) return false;
	
	var buf=new ArrayBuffer( length );
	if( !buf ) return false;
	
	this._Init( buf );
	return true;
}

BBDataBuffer.prototype._Load=function( path ){
	if( this.arrayBuffer ) return false;
	
	var buf=BBGame.Game().LoadData( path );
	if( !buf ) return false;
	
	this._Init( buf );
	return true;
}

BBDataBuffer.prototype._LoadAsync=function( path,thread ){

	var buf=this;
	
	var xhr=new XMLHttpRequest();
	xhr.open( "GET",BBGame.Game().PathToUrl( path ),true );
	xhr.responseType="arraybuffer";
	
	xhr.onload=function(e){
		if( this.status==200 || this.status==0 ){
			buf._Init( xhr.response );
			thread.result=buf;
		}
		thread.running=false;
	}
	
	xhr.onerror=function(e){
		thread.running=false;
	}
	
	xhr.send();
}


BBDataBuffer.prototype.GetArrayBuffer=function(){
	return this.arrayBuffer;
}

BBDataBuffer.prototype.Length=function(){
	return this.length;
}

BBDataBuffer.prototype.Discard=function(){
	if( this.arrayBuffer ){
		this.arrayBuffer=null;
		this.length=0;
	}
}

BBDataBuffer.prototype.PokeByte=function( addr,value ){
	this.bytes[addr]=value;
}

BBDataBuffer.prototype.PokeShort=function( addr,value ){
	if( addr&1 ){
		BBDataBuffer.tshorts[0]=value;
		this.bytes[addr]=BBDataBuffer.tbytes[0];
		this.bytes[addr+1]=BBDataBuffer.tbytes[1];
		return;
	}
	this.shorts[addr>>1]=value;
}

BBDataBuffer.prototype.PokeInt=function( addr,value ){
	if( addr&3 ){
		BBDataBuffer.tints[0]=value;
		this.bytes[addr]=BBDataBuffer.tbytes[0];
		this.bytes[addr+1]=BBDataBuffer.tbytes[1];
		this.bytes[addr+2]=BBDataBuffer.tbytes[2];
		this.bytes[addr+3]=BBDataBuffer.tbytes[3];
		return;
	}
	this.ints[addr>>2]=value;
}

BBDataBuffer.prototype.PokeFloat=function( addr,value ){
	if( addr&3 ){
		BBDataBuffer.tfloats[0]=value;
		this.bytes[addr]=BBDataBuffer.tbytes[0];
		this.bytes[addr+1]=BBDataBuffer.tbytes[1];
		this.bytes[addr+2]=BBDataBuffer.tbytes[2];
		this.bytes[addr+3]=BBDataBuffer.tbytes[3];
		return;
	}
	this.floats[addr>>2]=value;
}

BBDataBuffer.prototype.PeekByte=function( addr ){
	return this.bytes[addr];
}

BBDataBuffer.prototype.PeekShort=function( addr ){
	if( addr&1 ){
		BBDataBuffer.tbytes[0]=this.bytes[addr];
		BBDataBuffer.tbytes[1]=this.bytes[addr+1];
		return BBDataBuffer.tshorts[0];
	}
	return this.shorts[addr>>1];
}

BBDataBuffer.prototype.PeekInt=function( addr ){
	if( addr&3 ){
		BBDataBuffer.tbytes[0]=this.bytes[addr];
		BBDataBuffer.tbytes[1]=this.bytes[addr+1];
		BBDataBuffer.tbytes[2]=this.bytes[addr+2];
		BBDataBuffer.tbytes[3]=this.bytes[addr+3];
		return BBDataBuffer.tints[0];
	}
	return this.ints[addr>>2];
}

BBDataBuffer.prototype.PeekFloat=function( addr ){
	if( addr&3 ){
		BBDataBuffer.tbytes[0]=this.bytes[addr];
		BBDataBuffer.tbytes[1]=this.bytes[addr+1];
		BBDataBuffer.tbytes[2]=this.bytes[addr+2];
		BBDataBuffer.tbytes[3]=this.bytes[addr+3];
		return BBDataBuffer.tfloats[0];
	}
	return this.floats[addr>>2];
}


var bb_texs_loading=0;

function BBLoadStaticTexImage( path,info ){

	var game=BBHtml5Game.Html5Game();

	var ty=game.GetMetaData( path,"type" );
	if( ty.indexOf( "image/" )!=0 ) return null;
	
	if( info.length>0 ) info[0]=parseInt( game.GetMetaData( path,"width" ) );
	if( info.length>1 ) info[1]=parseInt( game.GetMetaData( path,"height" ) );
	
	var img=new Image();
	img.src=game.PathToUrl( path );
	
	return img;
}

function BBTextureLoading( tex ){
	return tex && tex._loading;
}

function BBTexturesLoading(){
	return bb_texs_loading;
}

function _glGenerateMipmap( target ){

	var tex=gl.getParameter( gl.TEXTURE_BINDING_2D );
	
	if( tex && tex._loading ){
		tex._genmipmap=true;
	}else{
		gl.generateMipmap( target );
	}
}

function _glBindTexture( target,tex ){
	if( tex ){
		gl.bindTexture( target,tex );
	}else{
		gl.bindTexture( target,null );
	}
}

function _glTexImage2D( target,level,internalformat,width,height,border,format,type,pixels ){

	gl.texImage2D( target,level,internalformat,width,height,border,format,type,pixels ? new Uint8Array(pixels.arrayBuffer) : null );
}

function _glTexImage2D2( target,level,internalformat,format,type,img ){

	if( img.complete ){
		gl.pixelStorei( gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL,true );	
		gl.texImage2D( target,level,internalformat,format,type,img );
		gl.pixelStorei( gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL,false );	
		return;
	}
	
	var tex=gl.getParameter( gl.TEXTURE_BINDING_2D );
	if( 	tex._loading ){
		tex._loading+=1;
	}else{
		tex._loading=1;
	}

	bb_texs_loading+=1;
	
	var onload=function(){
	
		var tmp=gl.getParameter( gl.TEXTURE_BINDING_2D );
		gl.bindTexture( target,tex );
		
		gl.pixelStorei( gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL,true );	
		gl.texImage2D( target,level,internalformat,format,type,img );
		gl.pixelStorei( gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL,false );	

		if( tex._genmipmap && tex._loading==1 ){
			gl.generateMipmap( target );
			tex._genmipmap=false;
		}
		gl.bindTexture( target,tmp );
		
		img.removeEventListener( "load",onload );
		tex._loading-=1;
		
		bb_texs_loading-=1;
	}
	
	img.addEventListener( "load",onload );
}

function _glTexImage2D3( target,level,internalformat,format,type,path ){

	var game=BBHtml5Game.Html5Game();

	var ty=game.GetMetaData( path,"type" );
	if( ty.indexOf( "image/" )!=0 ) return null;
	
	var img=new Image();
	img.src=game.PathToUrl( path );
	
	_glTexImage2D2( target,level,internalformat,format,type,img );
}

function _glTexSubImage2D( target,level,xoffset,yoffset,width,height,format,type,data,dataOffset ){

	gl.texSubImage2D( target,level,xoffset,yoffset,width,height,format,type,new Uint8Array( data.arrayBuffer,dataOffset ) );
	
}

function _glTexSubImage2D2( target,level,xoffset,yoffset,format,type,img ){

	if( img.complete ){
		gl.texSubImage2D( target,level,xoffset,yoffset,format,type,img );
		return;
	}
	
	var tex=gl.getParameter( gl.TEXTURE_BINDING_2D );
	if( 	tex._loading>0 ){
		tex._loading+=1;
	}else{
		tex._loading=1;
	}
	
	bb_texs_loading+=1;

	var onload=function(){
	
		var tmp=gl.getParameter( gl.TEXTURE_BINDING_2D );
		gl.bindTexture( target,tex );

		gl.pixelStorei( gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL,true );	
		gl.texSubImage2D( target,level,xoffset,yoffset,format,type,img );
		gl.pixelStorei( gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL,false );

		if( tex._genmipmap && tex._loading==1 ){
			gl.generateMipmap( target );
			tex._genmipmap=false;
		}
		gl.bindTexture( target,tmp );
		
		img.removeEventListener( "load",onload );
		tex._loading-=1;
		
		bb_texs_loading-=1;
	}
	
	img.addEventListener( "load",onload );
}

function _glTexSubImage2D3( target,level,xoffset,yoffset,format,type,path ){

	var game=BBHtml5Game.Html5Game();

	var ty=game.GetMetaData( path,"type" );
	if( ty.indexOf( "image/" )!=0 ) return null;
	
	var img=new Image();
	img.src=game.PathToUrl( path );
	
	_glTexSubImage2D2( target,level,xoffset,yoffset,format,type,img );
}

// Dodgy code to convert 'any' to i,f,iv,fv...
//
function _mkf( p ){
	if( typeof(p)=="boolean" ) return p?1.0:0.0;
	if( typeof(p)=="number" ) return p;
	return 0.0;
}

function _mki( p ){
	if( typeof(p)=="boolean" ) return p?1:0;
	if( typeof(p)=="number" ) return p|0;
	if( typeof(p)=="object" ) return p;
	return 0;
}

function _mkb( p ){
	if( typeof(p)=="boolean" ) return p;
	if( typeof(p)=="number" ) return p!=0;
	return false;
}

function _mkfv( p,params ){
	if( !params || !params.length ) return;
	if( (p instanceof Array) || (p instanceof Int32Array) || (p instanceof Float32Array) ){
		var n=Math.min( params.length,p.length );
		for( var i=0;i<n;++i ){
			params[i]=_mkf(p[i]);
		}
	}else{
		params[0]=_mkf(p);
	}
}

function _mkiv( p,params ){
	if( !params || !params.length ) return;
	if( (p instanceof Array) || (p instanceof Int32Array) || (p instanceof Float32Array) ){
		var n=Math.min( params.length,p.length );
		for( var i=0;i<n;++i ){
			params[i]=_mki(p[i]);
		}
	}else{
		params[0]=_mki(p);
	}
}

function _mkbv( p,params ){
	if( !params || !params.length ) return;
	if( (p instanceof Array) || (p instanceof Int32Array) || (p instanceof Float32Array) ){
		var n=Math.min( params.length,p.length );
		for( var i=0;i<n;++i ){
			params[i]=_mkb(p[i]);
		}
	}else{
		params[0]=_mkb(p);
	}
}

function _glBufferData( target,size,data,usage ){
	if( !data ){
		gl.bufferData( target,size,usage );
	}else if( size==data.size ){
		gl.bufferData( target,data.arrayBuffer,usage );
	}else{
		gl.bufferData( target,new Int8Array( data.arrayBuffer,0,size ),usage );
	}
}

function _glBufferSubData( target,offset,size,data,dataOffset ){
	if( size==data.size && dataOffset==0 ){
		gl.bufferSubData( target,offset,data.arrayBuffer );
	}else{
		gl.bufferSubData( target,offset,new Int8Array( data.arrayBuffer,dataOffset,size ) );
	}
}


function _glClearDepthf( depth ){
	gl.clearDepth( depth );
}

function _glDepthRange( zNear,zFar ){
	gl.depthRange( zNear,zFar );
}

function _glGetActiveAttrib( program,index,size,type,name ){
	var info=gl.getActiveAttrib( program,index );
	if( size && size.length ) size[0]=info.size;
	if( type && type.length ) type[0]=info.type;
	if( name && name.length ) name[0]=info.name;
}

function _glGetActiveUniform( program,index,size,type,name ){
	var info=gl.getActiveUniform( program,index );
	if( size && size.length ) size[0]=info.size;
	if( type && type.length ) type[0]=info.type;
	if( name && name.length ) name[0]=info.name;
}

function _glGetAttachedShaders( program, maxcount, count, shaders ){
	var t=gl.getAttachedShaders();
	if( count && count.length ) count[0]=t.length;
	if( shaders ){
		var n=t.length;
		if( maxcount<n ) n=maxcount;
		if( shaders.length<n ) n=shaders.length;
		for( var i=0;i<n;++i ) shaders[i]=t[i];
	}
}

function _glGetBooleanv( pname,params ){
	_mkbv( gl.getParameter( pname ),params );
}

function _glGetBufferParameteriv( target, pname, params ){
	_mkiv( gl.glGetBufferParameter( target,pname ),params );
}

function _glGetFloatv( pname,params ){
	_mkfv( gl.getParameter( pname ),params );
}

function _glGetFramebufferAttachmentParameteriv( target, attachment, pname, params ){
	_mkiv( gl.getFrameBufferAttachmentParameter( target,attachment,pname ),params );
}

function _glGetIntegerv( pname, params ){
	_mkiv( gl.getParameter( pname ),params );
}

function _glGetProgramiv( program, pname, params ){
	_mkiv( gl.getProgramParameter( program,pname ),params );
}

function _glGetRenderbufferParameteriv( target, pname, params ){
	_mkiv( gl.getRenderbufferParameter( target,pname ),params );
}

function _glGetShaderiv( shader, pname, params ){
	_mkiv( gl.getShaderParameter( shader,pname ),params );
}

function _glGetString( pname ){
	var p=gl.getParameter( pname );
	if( typeof(p)=="string" ) return p;
	return "";
}

function _glGetTexParameterfv( target, pname, params ){
	_mkfv( gl.getTexParameter( target,pname ),params );
}

function _glGetTexParameteriv( target, pname, params ){
	_mkiv( gl.getTexParameter( target,pname ),params );
}

function _glGetUniformfv( program, location, params ){
	_mkfv( gl.getUniform( program,location ),params );
}

function _glGetUniformiv( program, location, params ){
	_mkiv( gl.getUniform( program,location ),params );
}

function _glGetUniformLocation( program, name ){
	var l=gl.getUniformLocation( program,name );
	if( l ) return l;
	return -1;
}

function _glGetVertexAttribfv( index, pname, params ){
	_mkfv( gl.getVertexAttrib( index,pname ),params );
}

function _glGetVertexAttribiv( index, pname, params ){
	_mkiv( gl.getVertexAttrib( index,pname ),params );
}

function _glReadPixels( x,y,width,height,format,type,data,dataOffset ){
	gl.readPixels( x,y,width,height,format,type,new Uint8Array( data.arrayBuffer,dataOffset,data.length-dataOffset ) );
}

function _glBindBuffer( target,buffer ){
	if( buffer ){
		gl.bindBuffer( target,buffer );
	}else{
		gl.bindBuffer( target,null );
	}
}

function _glBindFramebuffer( target,framebuffer ){
	if( framebuffer ){
		gl.bindFramebuffer( target,framebuffer );
	}else{
		gl.bindFramebuffer( target,null );
	}
}

function _glBindRenderbuffer( target,renderbuffer ){
	if( renderbuffer ){
		gl.bindRenderbuffer( target,renderbuffer );
	}else{
		gl.bindRenderbuffer( target,null );
	}
}

function _glUniform1fv( location, count, v ){
	if( v.length==count ){
		gl.uniform1fv( location,v );
	}else{
		gl.uniform1fv( location,v.slice(0,cont) );
	}
}

function _glUniform1iv( location, count, v ){
	if( v.length==count ){
		gl.uniform1iv( location,v );
	}else{
		gl.uniform1iv( location,v.slice(0,cont) );
	}
}

function _glUniform2fv( location, count, v ){
	var n=count*2;
	if( v.length==n ){
		gl.uniform2fv( location,v );
	}else{
		gl.uniform2fv( location,v.slice(0,n) );
	}
}

function _glUniform2iv( location, count, v ){
	var n=count*2;
	if( v.length==n ){
		gl.uniform2iv( location,v );
	}else{
		gl.uniform2iv( location,v.slice(0,n) );
	}
}

function _glUniform3fv( location, count, v ){
	var n=count*3;
	if( v.length==n ){
		gl.uniform3fv( location,v );
	}else{
		gl.uniform3fv( location,v.slice(0,n) );
	}
}

function _glUniform3iv( location, count, v ){
	var n=count*3;
	if( v.length==n ){
		gl.uniform3iv( location,v );
	}else{
		gl.uniform3iv( location,v.slice(0,n) );
	}
}

function _glUniform4fv( location, count, v ){
	var n=count*4;
	if( v.length==n ){
		gl.uniform4fv( location,v );
	}else{
		gl.uniform4fv( location,v.slice(0,n) );
	}
}

function _glUniform4iv( location, count, v ){
	var n=count*4;
	if( v.length==n ){
		gl.uniform4iv( location,v );
	}else{
		gl.uniform4iv( location,v.slice(0,n) );
	}
}

function _glUniformMatrix2fv( location, count, transpose, value ){
	var n=count*4;
	if( value.length==n ){
		gl.uniformMatrix2fv( location,transpose,value );
	}else{
		gl.uniformMatrix2fv( location,transpose,value.slice(0,n) );
	}
}

function _glUniformMatrix3fv( location, count, transpose, value ){
	var n=count*9;
	if( value.length==n ){
		gl.uniformMatrix3fv( location,transpose,value );
	}else{
		gl.uniformMatrix3fv( location,transpose,value.slice(0,n) );
	}
}

function _glUniformMatrix4fv( location, count, transpose, value ){
	var n=count*16;
	if( value.length==n ){
		gl.uniformMatrix4fv( location,transpose,value );
	}else{
		gl.uniformMatrix4fv( location,transpose,value.slice(0,n) );
	}
}

function c_App(){
	Object.call(this);
}
c_App.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<152>";
	if((bb_app__app)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<152>";
		error("App has already been created");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<153>";
	bb_app__app=this;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<154>";
	bb_app__delegate=c_GameDelegate.m_new.call(new c_GameDelegate);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<155>";
	bb_app__game.SetDelegate(bb_app__delegate);
	pop_err();
	return this;
}
c_App.prototype.p_OnResize=function(){
	push_err();
	pop_err();
	return 0;
}
c_App.prototype.p_OnCreate=function(){
	push_err();
	pop_err();
	return 0;
}
c_App.prototype.p_OnSuspend=function(){
	push_err();
	pop_err();
	return 0;
}
c_App.prototype.p_OnResume=function(){
	push_err();
	pop_err();
	return 0;
}
c_App.prototype.p_OnUpdate=function(){
	push_err();
	pop_err();
	return 0;
}
c_App.prototype.p_OnLoading=function(){
	push_err();
	pop_err();
	return 0;
}
c_App.prototype.p_OnRender=function(){
	push_err();
	pop_err();
	return 0;
}
c_App.prototype.p_OnClose=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<177>";
	bb_app_EndApp();
	pop_err();
	return 0;
}
c_App.prototype.p_OnBack=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<181>";
	this.p_OnClose();
	pop_err();
	return 0;
}
function c_prScreenManager(){
	c_App.call(this);
}
c_prScreenManager.prototype=extend_class(c_App);
c_prScreenManager.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<252>";
	c_App.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<252>";
	pop_err();
	return this;
}
c_prScreenManager.m__screenPointer=null;
c_prScreenManager.prototype.p_OnBack=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<257>";
	if((c_prScreenManager.m__screenPointer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<257>";
		c_prScreenManager.m__screenPointer.p_OnBack();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<258>";
	pop_err();
	return 0;
}
c_prScreenManager.prototype.p_OnClose=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<262>";
	if((c_prScreenManager.m__screenPointer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<262>";
		c_prScreenManager.m__screenPointer.p_OnClose();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<263>";
	pop_err();
	return 0;
}
c_prScreenManager.prototype.p_OnRender=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<267>";
	if(((c_prScreenManager.m__screenPointer)!=null) && c_prScreenManager.m__screenPointer.p_Initialized() && c_prScreenManager.m__screenPointer.p_Started() && c_prScreenManager.m__screenPointer.p_Visible()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<268>";
		c_prScreenManager.m__screenPointer.p_Render();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<270>";
	pop_err();
	return 0;
}
c_prScreenManager.prototype.p_OnResize=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<274>";
	if((c_prScreenManager.m__screenPointer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<274>";
		c_prScreenManager.m__screenPointer.p_OnResize();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<275>";
	pop_err();
	return 0;
}
c_prScreenManager.prototype.p_OnResume=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<279>";
	if((c_prScreenManager.m__screenPointer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<279>";
		c_prScreenManager.m__screenPointer.p_OnResume();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<280>";
	pop_err();
	return 0;
}
c_prScreenManager.prototype.p_OnSuspend=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<284>";
	if((c_prScreenManager.m__screenPointer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<284>";
		c_prScreenManager.m__screenPointer.p_OnSuspend();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<285>";
	pop_err();
	return 0;
}
c_prScreenManager.m__nextScreen=null;
c_prScreenManager.prototype.p_OnUpdate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<290>";
	if((c_prScreenManager.m__nextScreen)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<291>";
		if(c_prScreenManager.m__screenPointer!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<292>";
			bb_globals_prSetPause(false);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<293>";
			c_prScreenManager.m__screenPointer.p_OnStop();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<294>";
			c_prScreenManager.m__screenPointer.p_Started2(false);
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<296>";
		c_prScreenManager.m__screenPointer=c_prScreenManager.m__nextScreen;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<297>";
		c_prScreenManager.m__nextScreen=null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<300>";
	if((c_prScreenManager.m__screenPointer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<302>";
		if(c_prScreenManager.m__screenPointer.p_Initialized()==false){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<303>";
			c_prScreenManager.m__screenPointer.p_OnInit();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<304>";
			c_prScreenManager.m__screenPointer.p_Initialized2(true);
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<307>";
		if(c_prScreenManager.m__screenPointer.p_Started()==false){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<308>";
			c_prScreenManager.m__screenPointer.p_OnStart();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<309>";
			c_prScreenManager.m__screenPointer.p_Started2(true);
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<312>";
		c_prScreenManager.m__screenPointer.p_Update();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<316>";
	pop_err();
	return 0;
}
function c_MyApp(){
	c_prScreenManager.call(this);
	this.m_deltaTimer=null;
	this.m_inputMonitor=null;
}
c_MyApp.prototype=extend_class(c_prScreenManager);
c_MyApp.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<18>";
	c_prScreenManager.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<18>";
	pop_err();
	return this;
}
c_MyApp.prototype.p_OnCreate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<25>";
	(c_Config.m_new.call(new c_Config)).p_LoadConfig();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<26>";
	var t_config=bb_contentmanager_prContent.p_GetConfig("config");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<29>";
	var t_virtualWidth=t_config.p_ReadInt("virtualWidth",0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<30>";
	var t_virtualHeight=t_config.p_ReadInt("virtualHeight",0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<31>";
	var t_screenOrientation=t_config.p_ReadInt("screenOrientation",0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<32>";
	bb_screenanchor_screenAnchor.p_SetVirtualWithBackground(t_virtualWidth,t_virtualHeight,t_screenOrientation);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<35>";
	dbg_object(this).m_deltaTimer=c_prDeltaTimer.m_new2.call(new c_prDeltaTimer);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<36>";
	dbg_object(this).m_inputMonitor=c_prInput.m_new.call(new c_prInput,t_config.p_ReadInt("maxTouch",0));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<39>";
	var t_screenLoader=c_ScreenLoader.m_new.call(new c_ScreenLoader);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<40>";
	t_screenLoader.p_CreateScreens();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<41>";
	t_screenLoader.p_SetInputMonitor(dbg_object(this).m_inputMonitor);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<44>";
	t_screenLoader.p_GetPlayScreen().p_Set2();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<46>";
	pop_err();
	return 0;
}
c_MyApp.prototype.p_OnUpdate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<50>";
	c_prScreenManager.prototype.p_OnUpdate.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<51>";
	dbg_object(this).m_deltaTimer.p_Update();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<52>";
	dbg_object(this).m_inputMonitor.p_Update2(dbg_object(this).m_deltaTimer.p_DeltaTime());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<53>";
	bb_tweens_prTweens.p_Update2(dbg_object(this).m_deltaTimer.p_DeltaTime());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<54>";
	pop_err();
	return 0;
}
c_MyApp.prototype.p_OnResume=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<58>";
	c_prScreenManager.prototype.p_OnResume.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<59>";
	dbg_object(this).m_deltaTimer.p_Resume();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<60>";
	pop_err();
	return 0;
}
var bb_app__app=null;
function c_GameDelegate(){
	BBGameDelegate.call(this);
	this.m__graphics=null;
	this.m__audio=null;
	this.m__input=null;
}
c_GameDelegate.prototype=extend_class(BBGameDelegate);
c_GameDelegate.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<65>";
	pop_err();
	return this;
}
c_GameDelegate.prototype.StartGame=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<75>";
	this.m__graphics=(new gxtkGraphics);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<76>";
	bb_graphics_SetGraphicsDevice(this.m__graphics);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<77>";
	bb_graphics_SetFont(null,32);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<79>";
	this.m__audio=(new gxtkAudio);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<80>";
	bb_audio_SetAudioDevice(this.m__audio);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<82>";
	this.m__input=c_InputDevice.m_new.call(new c_InputDevice);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<83>";
	bb_input_SetInputDevice(this.m__input);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<85>";
	bb_app_ValidateDeviceWindow(false);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<87>";
	bb_app_EnumDisplayModes();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<89>";
	bb_app__app.p_OnCreate();
	pop_err();
}
c_GameDelegate.prototype.SuspendGame=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<93>";
	bb_app__app.p_OnSuspend();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<94>";
	this.m__audio.Suspend();
	pop_err();
}
c_GameDelegate.prototype.ResumeGame=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<98>";
	this.m__audio.Resume();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<99>";
	bb_app__app.p_OnResume();
	pop_err();
}
c_GameDelegate.prototype.UpdateGame=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<103>";
	bb_app_ValidateDeviceWindow(true);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<104>";
	this.m__input.p_BeginUpdate();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<105>";
	bb_app__app.p_OnUpdate();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<106>";
	this.m__input.p_EndUpdate();
	pop_err();
}
c_GameDelegate.prototype.RenderGame=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<110>";
	bb_app_ValidateDeviceWindow(true);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<111>";
	var t_mode=this.m__graphics.BeginRender();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<112>";
	if((t_mode)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<112>";
		bb_graphics_BeginRender();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<113>";
	if(t_mode==2){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<113>";
		bb_app__app.p_OnLoading();
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<113>";
		bb_app__app.p_OnRender();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<114>";
	if((t_mode)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<114>";
		bb_graphics_EndRender();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<115>";
	this.m__graphics.EndRender();
	pop_err();
}
c_GameDelegate.prototype.KeyEvent=function(t_event,t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<119>";
	this.m__input.p_KeyEvent(t_event,t_data);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<120>";
	if(t_event!=1){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<121>";
	var t_1=t_data;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<122>";
	if(t_1==432){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<123>";
		bb_app__app.p_OnClose();
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<124>";
		if(t_1==416){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<125>";
			bb_app__app.p_OnBack();
		}
	}
	pop_err();
}
c_GameDelegate.prototype.MouseEvent=function(t_event,t_data,t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<130>";
	this.m__input.p_MouseEvent(t_event,t_data,t_x,t_y);
	pop_err();
}
c_GameDelegate.prototype.TouchEvent=function(t_event,t_data,t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<134>";
	this.m__input.p_TouchEvent(t_event,t_data,t_x,t_y);
	pop_err();
}
c_GameDelegate.prototype.MotionEvent=function(t_event,t_data,t_x,t_y,t_z){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<138>";
	this.m__input.p_MotionEvent(t_event,t_data,t_x,t_y,t_z);
	pop_err();
}
c_GameDelegate.prototype.DiscardGraphics=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<142>";
	this.m__graphics.DiscardGraphics();
	pop_err();
}
var bb_app__delegate=null;
var bb_app__game=null;
function bbMain(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<66>";
	c_MyApp.m_new.call(new c_MyApp);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/livepuzzle.monkey<67>";
	pop_err();
	return 0;
}
var bb_graphics_device=null;
function bb_graphics_SetGraphicsDevice(t_dev){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<63>";
	bb_graphics_device=t_dev;
	pop_err();
	return 0;
}
function c_Image(){
	Object.call(this);
	this.m_surface=null;
	this.m_width=0;
	this.m_height=0;
	this.m_frames=[];
	this.m_flags=0;
	this.m_tx=.0;
	this.m_ty=.0;
	this.m_source=null;
}
c_Image.m_DefaultFlags=0;
c_Image.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<70>";
	pop_err();
	return this;
}
c_Image.prototype.p_SetHandle=function(t_tx,t_ty){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<114>";
	dbg_object(this).m_tx=t_tx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<115>";
	dbg_object(this).m_ty=t_ty;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<116>";
	dbg_object(this).m_flags=dbg_object(this).m_flags&-2;
	pop_err();
	return 0;
}
c_Image.prototype.p_ApplyFlags=function(t_iflags){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<198>";
	this.m_flags=t_iflags;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<200>";
	if((this.m_flags&2)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<201>";
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<201>";
		var t_=this.m_frames;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<201>";
		var t_2=0;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<201>";
		while(t_2<t_.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<201>";
			var t_f=dbg_array(t_,t_2)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<201>";
			t_2=t_2+1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<202>";
			dbg_object(t_f).m_x+=1;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<204>";
		this.m_width-=2;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<207>";
	if((this.m_flags&4)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<208>";
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<208>";
		var t_3=this.m_frames;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<208>";
		var t_4=0;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<208>";
		while(t_4<t_3.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<208>";
			var t_f2=dbg_array(t_3,t_4)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<208>";
			t_4=t_4+1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<209>";
			dbg_object(t_f2).m_y+=1;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<211>";
		this.m_height-=2;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<214>";
	if((this.m_flags&1)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<215>";
		this.p_SetHandle((this.m_width)/2.0,(this.m_height)/2.0);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<218>";
	if(this.m_frames.length==1 && dbg_object(dbg_array(this.m_frames,0)[dbg_index]).m_x==0 && dbg_object(dbg_array(this.m_frames,0)[dbg_index]).m_y==0 && this.m_width==this.m_surface.Width() && this.m_height==this.m_surface.Height()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<219>";
		this.m_flags|=65536;
	}
	pop_err();
	return 0;
}
c_Image.prototype.p_Init=function(t_surf,t_nframes,t_iflags){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<144>";
	if((this.m_surface)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<144>";
		error("Image already initialized");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<145>";
	this.m_surface=t_surf;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<147>";
	this.m_width=((this.m_surface.Width()/t_nframes)|0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<148>";
	this.m_height=this.m_surface.Height();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<150>";
	this.m_frames=new_object_array(t_nframes);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<151>";
	for(var t_i=0;t_i<t_nframes;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<152>";
		dbg_array(this.m_frames,t_i)[dbg_index]=c_Frame.m_new.call(new c_Frame,t_i*this.m_width,0);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<155>";
	this.p_ApplyFlags(t_iflags);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<156>";
	pop_err();
	return this;
}
c_Image.prototype.p_Init2=function(t_surf,t_x,t_y,t_iwidth,t_iheight,t_nframes,t_iflags,t_src,t_srcx,t_srcy,t_srcw,t_srch){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<160>";
	if((this.m_surface)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<160>";
		error("Image already initialized");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<161>";
	this.m_surface=t_surf;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<162>";
	this.m_source=t_src;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<164>";
	this.m_width=t_iwidth;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<165>";
	this.m_height=t_iheight;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<167>";
	this.m_frames=new_object_array(t_nframes);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<169>";
	var t_ix=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<169>";
	var t_iy=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<171>";
	for(var t_i=0;t_i<t_nframes;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<172>";
		if(t_ix+this.m_width>t_srcw){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<173>";
			t_ix=0;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<174>";
			t_iy+=this.m_height;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<176>";
		if(t_ix+this.m_width>t_srcw || t_iy+this.m_height>t_srch){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<177>";
			error("Image frame outside surface");
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<179>";
		dbg_array(this.m_frames,t_i)[dbg_index]=c_Frame.m_new.call(new c_Frame,t_ix+t_srcx,t_iy+t_srcy);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<180>";
		t_ix+=this.m_width;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<183>";
	this.p_ApplyFlags(t_iflags);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<184>";
	pop_err();
	return this;
}
function c_GraphicsContext(){
	Object.call(this);
	this.m_defaultFont=null;
	this.m_font=null;
	this.m_firstChar=0;
	this.m_matrixSp=0;
	this.m_ix=1.0;
	this.m_iy=.0;
	this.m_jx=.0;
	this.m_jy=1.0;
	this.m_tx=.0;
	this.m_ty=.0;
	this.m_tformed=0;
	this.m_matDirty=0;
	this.m_color_r=.0;
	this.m_color_g=.0;
	this.m_color_b=.0;
	this.m_alpha=.0;
	this.m_blend=0;
	this.m_scissor_x=.0;
	this.m_scissor_y=.0;
	this.m_scissor_width=.0;
	this.m_scissor_height=.0;
}
c_GraphicsContext.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<29>";
	pop_err();
	return this;
}
var bb_graphics_context=null;
function bb_data_FixDataPath(t_path){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/data.monkey<7>";
	var t_i=t_path.indexOf(":/",0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/data.monkey<8>";
	if(t_i!=-1 && t_path.indexOf("/",0)==t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/data.monkey<8>";
		pop_err();
		return t_path;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/data.monkey<9>";
	if(string_startswith(t_path,"./") || string_startswith(t_path,"/")){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/data.monkey<9>";
		pop_err();
		return t_path;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/data.monkey<10>";
	var t_="monkey://data/"+t_path;
	pop_err();
	return t_;
}
function c_Frame(){
	Object.call(this);
	this.m_x=0;
	this.m_y=0;
}
c_Frame.m_new=function(t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<23>";
	dbg_object(this).m_x=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<24>";
	dbg_object(this).m_y=t_y;
	pop_err();
	return this;
}
c_Frame.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<18>";
	pop_err();
	return this;
}
function bb_graphics_LoadImage(t_path,t_frameCount,t_flags){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<240>";
	var t_surf=bb_graphics_device.LoadSurface(bb_data_FixDataPath(t_path));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<241>";
	if((t_surf)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<241>";
		var t_=(c_Image.m_new.call(new c_Image)).p_Init(t_surf,t_frameCount,t_flags);
		pop_err();
		return t_;
	}
	pop_err();
	return null;
}
function bb_graphics_LoadImage2(t_path,t_frameWidth,t_frameHeight,t_frameCount,t_flags){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<245>";
	var t_surf=bb_graphics_device.LoadSurface(bb_data_FixDataPath(t_path));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<246>";
	if((t_surf)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<246>";
		var t_=(c_Image.m_new.call(new c_Image)).p_Init2(t_surf,0,0,t_frameWidth,t_frameHeight,t_frameCount,t_flags,null,0,0,t_surf.Width(),t_surf.Height());
		pop_err();
		return t_;
	}
	pop_err();
	return null;
}
function bb_graphics_SetFont(t_font,t_firstChar){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<549>";
	if(!((t_font)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<550>";
		if(!((dbg_object(bb_graphics_context).m_defaultFont)!=null)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<551>";
			dbg_object(bb_graphics_context).m_defaultFont=bb_graphics_LoadImage("mojo_font.png",96,2);
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<553>";
		t_font=dbg_object(bb_graphics_context).m_defaultFont;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<554>";
		t_firstChar=32;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<556>";
	dbg_object(bb_graphics_context).m_font=t_font;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<557>";
	dbg_object(bb_graphics_context).m_firstChar=t_firstChar;
	pop_err();
	return 0;
}
var bb_audio_device=null;
function bb_audio_SetAudioDevice(t_dev){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/audio.monkey<22>";
	bb_audio_device=t_dev;
	pop_err();
	return 0;
}
function c_InputDevice(){
	Object.call(this);
	this.m__joyStates=new_object_array(4);
	this.m__keyDown=new_bool_array(512);
	this.m__keyHitPut=0;
	this.m__keyHitQueue=new_number_array(33);
	this.m__keyHit=new_number_array(512);
	this.m__charGet=0;
	this.m__charPut=0;
	this.m__charQueue=new_number_array(32);
	this.m__mouseX=.0;
	this.m__mouseY=.0;
	this.m__touchX=new_number_array(32);
	this.m__touchY=new_number_array(32);
	this.m__accelX=.0;
	this.m__accelY=.0;
	this.m__accelZ=.0;
}
c_InputDevice.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<26>";
	for(var t_i=0;t_i<4;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<27>";
		dbg_array(this.m__joyStates,t_i)[dbg_index]=c_JoyState.m_new.call(new c_JoyState);
	}
	pop_err();
	return this;
}
c_InputDevice.prototype.p_PutKeyHit=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<241>";
	if(this.m__keyHitPut==this.m__keyHitQueue.length){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<242>";
	dbg_array(this.m__keyHit,t_key)[dbg_index]+=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<243>";
	dbg_array(this.m__keyHitQueue,this.m__keyHitPut)[dbg_index]=t_key;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<244>";
	this.m__keyHitPut+=1;
	pop_err();
}
c_InputDevice.prototype.p_BeginUpdate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<193>";
	for(var t_i=0;t_i<4;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<194>";
		var t_state=dbg_array(this.m__joyStates,t_i)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<195>";
		if(!BBGame.Game().PollJoystick(t_i,dbg_object(t_state).m_joyx,dbg_object(t_state).m_joyy,dbg_object(t_state).m_joyz,dbg_object(t_state).m_buttons)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<195>";
			break;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<196>";
		for(var t_j=0;t_j<32;t_j=t_j+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<197>";
			var t_key=256+t_i*32+t_j;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<198>";
			if(dbg_array(dbg_object(t_state).m_buttons,t_j)[dbg_index]){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<199>";
				if(!dbg_array(this.m__keyDown,t_key)[dbg_index]){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<200>";
					dbg_array(this.m__keyDown,t_key)[dbg_index]=true;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<201>";
					this.p_PutKeyHit(t_key);
				}
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<204>";
				dbg_array(this.m__keyDown,t_key)[dbg_index]=false;
			}
		}
	}
	pop_err();
}
c_InputDevice.prototype.p_EndUpdate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<211>";
	for(var t_i=0;t_i<this.m__keyHitPut;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<212>";
		dbg_array(this.m__keyHit,dbg_array(this.m__keyHitQueue,t_i)[dbg_index])[dbg_index]=0;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<214>";
	this.m__keyHitPut=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<215>";
	this.m__charGet=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<216>";
	this.m__charPut=0;
	pop_err();
}
c_InputDevice.prototype.p_KeyEvent=function(t_event,t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<115>";
	var t_1=t_event;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<116>";
	if(t_1==1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<117>";
		if(!dbg_array(this.m__keyDown,t_data)[dbg_index]){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<118>";
			dbg_array(this.m__keyDown,t_data)[dbg_index]=true;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<119>";
			this.p_PutKeyHit(t_data);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<120>";
			if(t_data==1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<121>";
				dbg_array(this.m__keyDown,384)[dbg_index]=true;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<122>";
				this.p_PutKeyHit(384);
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<123>";
				if(t_data==384){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<124>";
					dbg_array(this.m__keyDown,1)[dbg_index]=true;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<125>";
					this.p_PutKeyHit(1);
				}
			}
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<128>";
		if(t_1==2){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<129>";
			if(dbg_array(this.m__keyDown,t_data)[dbg_index]){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<130>";
				dbg_array(this.m__keyDown,t_data)[dbg_index]=false;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<131>";
				if(t_data==1){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<132>";
					dbg_array(this.m__keyDown,384)[dbg_index]=false;
				}else{
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<133>";
					if(t_data==384){
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<134>";
						dbg_array(this.m__keyDown,1)[dbg_index]=false;
					}
				}
			}
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<137>";
			if(t_1==3){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<138>";
				if(this.m__charPut<this.m__charQueue.length){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<139>";
					dbg_array(this.m__charQueue,this.m__charPut)[dbg_index]=t_data;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<140>";
					this.m__charPut+=1;
				}
			}
		}
	}
	pop_err();
}
c_InputDevice.prototype.p_MouseEvent=function(t_event,t_data,t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<146>";
	var t_2=t_event;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<147>";
	if(t_2==4){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<148>";
		this.p_KeyEvent(1,1+t_data);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<149>";
		if(t_2==5){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<150>";
			this.p_KeyEvent(2,1+t_data);
			pop_err();
			return;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<152>";
			if(t_2==6){
			}else{
				pop_err();
				return;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<156>";
	this.m__mouseX=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<157>";
	this.m__mouseY=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<158>";
	dbg_array(this.m__touchX,0)[dbg_index]=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<159>";
	dbg_array(this.m__touchY,0)[dbg_index]=t_y;
	pop_err();
}
c_InputDevice.prototype.p_TouchEvent=function(t_event,t_data,t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<163>";
	var t_3=t_event;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<164>";
	if(t_3==7){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<165>";
		this.p_KeyEvent(1,384+t_data);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<166>";
		if(t_3==8){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<167>";
			this.p_KeyEvent(2,384+t_data);
			pop_err();
			return;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<169>";
			if(t_3==9){
			}else{
				pop_err();
				return;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<173>";
	dbg_array(this.m__touchX,t_data)[dbg_index]=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<174>";
	dbg_array(this.m__touchY,t_data)[dbg_index]=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<175>";
	if(t_data==0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<176>";
		this.m__mouseX=t_x;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<177>";
		this.m__mouseY=t_y;
	}
	pop_err();
}
c_InputDevice.prototype.p_MotionEvent=function(t_event,t_data,t_x,t_y,t_z){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<182>";
	var t_4=t_event;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<183>";
	if(t_4==10){
	}else{
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<187>";
	this.m__accelX=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<188>";
	this.m__accelY=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<189>";
	this.m__accelZ=t_z;
	pop_err();
}
c_InputDevice.prototype.p_KeyDown=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<47>";
	if(t_key>0 && t_key<512){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<47>";
		pop_err();
		return dbg_array(this.m__keyDown,t_key)[dbg_index];
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<48>";
	pop_err();
	return false;
}
c_InputDevice.prototype.p_TouchX=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<77>";
	if(t_index>=0 && t_index<32){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<77>";
		pop_err();
		return dbg_array(this.m__touchX,t_index)[dbg_index];
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<78>";
	pop_err();
	return 0.0;
}
c_InputDevice.prototype.p_TouchY=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<82>";
	if(t_index>=0 && t_index<32){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<82>";
		pop_err();
		return dbg_array(this.m__touchY,t_index)[dbg_index];
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<83>";
	pop_err();
	return 0.0;
}
function c_JoyState(){
	Object.call(this);
	this.m_joyx=new_number_array(2);
	this.m_joyy=new_number_array(2);
	this.m_joyz=new_number_array(2);
	this.m_buttons=new_bool_array(32);
}
c_JoyState.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/inputdevice.monkey<14>";
	pop_err();
	return this;
}
var bb_input_device=null;
function bb_input_SetInputDevice(t_dev){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/input.monkey<22>";
	bb_input_device=t_dev;
	pop_err();
	return 0;
}
var bb_app__devWidth=0;
var bb_app__devHeight=0;
function bb_app_ValidateDeviceWindow(t_notifyApp){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<57>";
	var t_w=bb_app__game.GetDeviceWidth();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<58>";
	var t_h=bb_app__game.GetDeviceHeight();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<59>";
	if(t_w==bb_app__devWidth && t_h==bb_app__devHeight){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<60>";
	bb_app__devWidth=t_w;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<61>";
	bb_app__devHeight=t_h;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<62>";
	if(t_notifyApp){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<62>";
		bb_app__app.p_OnResize();
	}
	pop_err();
}
function c_DisplayMode(){
	Object.call(this);
	this.m__width=0;
	this.m__height=0;
}
c_DisplayMode.m_new=function(t_width,t_height){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<192>";
	this.m__width=t_width;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<193>";
	this.m__height=t_height;
	pop_err();
	return this;
}
c_DisplayMode.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<189>";
	pop_err();
	return this;
}
function c_Map(){
	Object.call(this);
	this.m_root=null;
}
c_Map.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<7>";
	pop_err();
	return this;
}
c_Map.prototype.p_Compare=function(t_lhs,t_rhs){
}
c_Map.prototype.p_FindNode=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<157>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<159>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<160>";
		var t_cmp=this.p_Compare(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<161>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<162>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<163>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<164>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<166>";
				pop_err();
				return t_node;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<169>";
	pop_err();
	return t_node;
}
c_Map.prototype.p_Contains=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<25>";
	var t_=this.p_FindNode(t_key)!=null;
	pop_err();
	return t_;
}
c_Map.prototype.p_RotateLeft=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<251>";
	var t_child=dbg_object(t_node).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<252>";
	dbg_object(t_node).m_right=dbg_object(t_child).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<253>";
	if((dbg_object(t_child).m_left)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<254>";
		dbg_object(dbg_object(t_child).m_left).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<256>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<257>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<258>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<259>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<261>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<264>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<266>";
	dbg_object(t_child).m_left=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<267>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map.prototype.p_RotateRight=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<271>";
	var t_child=dbg_object(t_node).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<272>";
	dbg_object(t_node).m_left=dbg_object(t_child).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<273>";
	if((dbg_object(t_child).m_right)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<274>";
		dbg_object(dbg_object(t_child).m_right).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<276>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<277>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<278>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<279>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<281>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<284>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<286>";
	dbg_object(t_child).m_right=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<287>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map.prototype.p_InsertFixup=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<212>";
	while(((dbg_object(t_node).m_parent)!=null) && dbg_object(dbg_object(t_node).m_parent).m_color==-1 && ((dbg_object(dbg_object(t_node).m_parent).m_parent)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<213>";
		if(dbg_object(t_node).m_parent==dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<214>";
			var t_uncle=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_right;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<215>";
			if(((t_uncle)!=null) && dbg_object(t_uncle).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<216>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<217>";
				dbg_object(t_uncle).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<218>";
				dbg_object(dbg_object(t_uncle).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<219>";
				t_node=dbg_object(t_uncle).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<221>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<222>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<223>";
					this.p_RotateLeft(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<225>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<226>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<227>";
				this.p_RotateRight(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<230>";
			var t_uncle2=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<231>";
			if(((t_uncle2)!=null) && dbg_object(t_uncle2).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<232>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<233>";
				dbg_object(t_uncle2).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<234>";
				dbg_object(dbg_object(t_uncle2).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<235>";
				t_node=dbg_object(t_uncle2).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<237>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<238>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<239>";
					this.p_RotateRight(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<241>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<242>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<243>";
				this.p_RotateLeft(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<247>";
	dbg_object(this.m_root).m_color=1;
	pop_err();
	return 0;
}
c_Map.prototype.p_Set=function(t_key,t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<29>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_parent=null;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_cmp=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<32>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<33>";
		t_parent=t_node;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<34>";
		t_cmp=this.p_Compare(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<35>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<36>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<37>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<38>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<40>";
				dbg_object(t_node).m_value=t_value;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<41>";
				pop_err();
				return false;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<45>";
	t_node=c_Node.m_new.call(new c_Node,t_key,t_value,-1,t_parent);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<47>";
	if((t_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<48>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<49>";
			dbg_object(t_parent).m_right=t_node;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<51>";
			dbg_object(t_parent).m_left=t_node;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<53>";
		this.p_InsertFixup(t_node);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<55>";
		this.m_root=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<57>";
	pop_err();
	return true;
}
c_Map.prototype.p_Insert=function(t_key,t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<146>";
	var t_=this.p_Set(t_key,t_value);
	pop_err();
	return t_;
}
function c_IntMap(){
	c_Map.call(this);
}
c_IntMap.prototype=extend_class(c_Map);
c_IntMap.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<534>";
	c_Map.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<534>";
	pop_err();
	return this;
}
c_IntMap.prototype.p_Compare=function(t_lhs,t_rhs){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<537>";
	var t_=t_lhs-t_rhs;
	pop_err();
	return t_;
}
function c_Stack(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack.prototype.p_Push=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack.prototype.p_Push2=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack.prototype.p_Push3=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push2(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
c_Stack.prototype.p_ToArray=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<18>";
	var t_t=new_object_array(this.m_length);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<19>";
	for(var t_i=0;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<20>";
		dbg_array(t_t,t_i)[dbg_index]=dbg_array(this.m_data,t_i)[dbg_index];
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<22>";
	pop_err();
	return t_t;
}
function c_Node(){
	Object.call(this);
	this.m_key=0;
	this.m_right=null;
	this.m_left=null;
	this.m_value=null;
	this.m_color=0;
	this.m_parent=null;
}
c_Node.m_new=function(t_key,t_value,t_color,t_parent){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<364>";
	dbg_object(this).m_key=t_key;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<365>";
	dbg_object(this).m_value=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<366>";
	dbg_object(this).m_color=t_color;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<367>";
	dbg_object(this).m_parent=t_parent;
	pop_err();
	return this;
}
c_Node.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<361>";
	pop_err();
	return this;
}
var bb_app__displayModes=[];
var bb_app__desktopMode=null;
function bb_app_DeviceWidth(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<263>";
	pop_err();
	return bb_app__devWidth;
}
function bb_app_DeviceHeight(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<267>";
	pop_err();
	return bb_app__devHeight;
}
function bb_app_EnumDisplayModes(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<33>";
	var t_modes=bb_app__game.GetDisplayModes();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<34>";
	var t_mmap=c_IntMap.m_new.call(new c_IntMap);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<35>";
	var t_mstack=c_Stack.m_new.call(new c_Stack);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<36>";
	for(var t_i=0;t_i<t_modes.length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<37>";
		var t_w=dbg_object(dbg_array(t_modes,t_i)[dbg_index]).width;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<38>";
		var t_h=dbg_object(dbg_array(t_modes,t_i)[dbg_index]).height;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<39>";
		var t_size=t_w<<16|t_h;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<40>";
		if(t_mmap.p_Contains(t_size)){
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<42>";
			var t_mode=c_DisplayMode.m_new.call(new c_DisplayMode,dbg_object(dbg_array(t_modes,t_i)[dbg_index]).width,dbg_object(dbg_array(t_modes,t_i)[dbg_index]).height);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<43>";
			t_mmap.p_Insert(t_size,t_mode);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<44>";
			t_mstack.p_Push(t_mode);
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<47>";
	bb_app__displayModes=t_mstack.p_ToArray();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<48>";
	var t_mode2=bb_app__game.GetDesktopMode();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<49>";
	if((t_mode2)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<50>";
		bb_app__desktopMode=c_DisplayMode.m_new.call(new c_DisplayMode,dbg_object(t_mode2).width,dbg_object(t_mode2).height);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<52>";
		bb_app__desktopMode=c_DisplayMode.m_new.call(new c_DisplayMode,bb_app_DeviceWidth(),bb_app_DeviceHeight());
	}
	pop_err();
}
var bb_graphics_renderDevice=null;
function bb_graphics_SetMatrix(t_ix,t_iy,t_jx,t_jy,t_tx,t_ty){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<313>";
	dbg_object(bb_graphics_context).m_ix=t_ix;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<314>";
	dbg_object(bb_graphics_context).m_iy=t_iy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<315>";
	dbg_object(bb_graphics_context).m_jx=t_jx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<316>";
	dbg_object(bb_graphics_context).m_jy=t_jy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<317>";
	dbg_object(bb_graphics_context).m_tx=t_tx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<318>";
	dbg_object(bb_graphics_context).m_ty=t_ty;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<319>";
	dbg_object(bb_graphics_context).m_tformed=((t_ix!=1.0 || t_iy!=0.0 || t_jx!=0.0 || t_jy!=1.0 || t_tx!=0.0 || t_ty!=0.0)?1:0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<320>";
	dbg_object(bb_graphics_context).m_matDirty=1;
	pop_err();
	return 0;
}
function bb_graphics_SetMatrix2(t_m){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<309>";
	bb_graphics_SetMatrix(dbg_array(t_m,0)[dbg_index],dbg_array(t_m,1)[dbg_index],dbg_array(t_m,2)[dbg_index],dbg_array(t_m,3)[dbg_index],dbg_array(t_m,4)[dbg_index],dbg_array(t_m,5)[dbg_index]);
	pop_err();
	return 0;
}
function bb_graphics_SetColor(t_r,t_g,t_b){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<255>";
	dbg_object(bb_graphics_context).m_color_r=t_r;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<256>";
	dbg_object(bb_graphics_context).m_color_g=t_g;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<257>";
	dbg_object(bb_graphics_context).m_color_b=t_b;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<258>";
	bb_graphics_renderDevice.SetColor(t_r,t_g,t_b);
	pop_err();
	return 0;
}
function bb_graphics_SetAlpha(t_alpha){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<272>";
	dbg_object(bb_graphics_context).m_alpha=t_alpha;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<273>";
	bb_graphics_renderDevice.SetAlpha(t_alpha);
	pop_err();
	return 0;
}
function bb_graphics_SetBlend(t_blend){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<281>";
	dbg_object(bb_graphics_context).m_blend=t_blend;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<282>";
	bb_graphics_renderDevice.SetBlend(t_blend);
	pop_err();
	return 0;
}
function bb_graphics_SetScissor(t_x,t_y,t_width,t_height){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<290>";
	dbg_object(bb_graphics_context).m_scissor_x=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<291>";
	dbg_object(bb_graphics_context).m_scissor_y=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<292>";
	dbg_object(bb_graphics_context).m_scissor_width=t_width;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<293>";
	dbg_object(bb_graphics_context).m_scissor_height=t_height;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<294>";
	bb_graphics_renderDevice.SetScissor(((t_x)|0),((t_y)|0),((t_width)|0),((t_height)|0));
	pop_err();
	return 0;
}
function bb_graphics_BeginRender(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<226>";
	bb_graphics_renderDevice=bb_graphics_device;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<227>";
	dbg_object(bb_graphics_context).m_matrixSp=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<228>";
	bb_graphics_SetMatrix(1.0,0.0,0.0,1.0,0.0,0.0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<229>";
	bb_graphics_SetColor(255.0,255.0,255.0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<230>";
	bb_graphics_SetAlpha(1.0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<231>";
	bb_graphics_SetBlend(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<232>";
	bb_graphics_SetScissor(0.0,0.0,(bb_app_DeviceWidth()),(bb_app_DeviceHeight()));
	pop_err();
	return 0;
}
function bb_graphics_EndRender(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/graphics.monkey<236>";
	bb_graphics_renderDevice=null;
	pop_err();
	return 0;
}
function c_BBGameEvent(){
	Object.call(this);
}
function bb_app_EndApp(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<259>";
	error("");
	pop_err();
}
function c_prScreen(){
	Object.call(this);
	this.m__initialized=false;
	this.m__started=false;
	this.m__visible=true;
}
c_prScreen.prototype.p_OnBack=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<60>";
	pop_err();
	return 0;
}
c_prScreen.prototype.p_OnClose=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<77>";
	pop_err();
	return 0;
}
c_prScreen.prototype.p_Initialized=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<37>";
	pop_err();
	return this.m__initialized;
}
c_prScreen.prototype.p_Initialized2=function(t_initialized){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<43>";
	this.m__initialized=t_initialized;
	pop_err();
}
c_prScreen.prototype.p_Started=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<187>";
	pop_err();
	return this.m__started;
}
c_prScreen.prototype.p_Started2=function(t_started){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<194>";
	this.m__started=t_started;
	pop_err();
}
c_prScreen.prototype.p_Visible=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<207>";
	pop_err();
	return this.m__visible;
}
c_prScreen.prototype.p_Visible2=function(t_visible){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<214>";
	this.m__visible=t_visible;
	pop_err();
}
c_prScreen.prototype.p_OnRender=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<107>";
	pop_err();
	return 0;
}
c_prScreen.prototype.p_Render=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<173>";
	this.p_OnRender();
	pop_err();
}
c_prScreen.prototype.p_OnResize=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<116>";
	pop_err();
	return 0;
}
c_prScreen.prototype.p_OnResume=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<125>";
	pop_err();
	return 0;
}
c_prScreen.prototype.p_OnSuspend=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<150>";
	pop_err();
	return 0;
}
c_prScreen.prototype.p_OnStop=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<139>";
	pop_err();
	return 0;
}
c_prScreen.prototype.p_OnInit=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<93>";
	pop_err();
	return 0;
}
c_prScreen.prototype.p_OnStart=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<132>";
	pop_err();
	return 0;
}
c_prScreen.prototype.p_OnUpdate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<167>";
	pop_err();
	return 0;
}
c_prScreen.prototype.p_Update=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<200>";
	this.p_OnUpdate();
	pop_err();
}
c_prScreen.prototype.p_Set2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<180>";
	c_prScreenManager.m__nextScreen=this;
	pop_err();
}
c_prScreen.prototype.p_OnCreate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<86>";
	pop_err();
	return 0;
}
c_prScreen.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<30>";
	this.p_Set2();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/screenmanager.monkey<31>";
	this.p_OnCreate();
	pop_err();
	return this;
}
var bb_globals__pause=false;
function bb_globals_prSetPause(t_pause){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/globals.monkey<20>";
	bb_globals__pause=t_pause;
	pop_err();
}
function c_Config(){
	Object.call(this);
}
c_Config.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/resources/configloader.monkey<6>";
	pop_err();
	return this;
}
c_Config.prototype.p_LoadConfig=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/resources/configloader.monkey<24>";
	var t_config=c_prConfig.m_new2.call(new c_prConfig);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/resources/configloader.monkey<25>";
	t_config.p_WriteInt("virtualWidth",640);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/resources/configloader.monkey<26>";
	t_config.p_WriteInt("virtualHeight",960);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/resources/configloader.monkey<27>";
	t_config.p_WriteInt("screenOrientation",0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/resources/configloader.monkey<28>";
	t_config.p_WriteInt("maxTouch",1);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/resources/configloader.monkey<30>";
	bb_contentmanager_prContent.p_SetConfig(t_config,"config");
	pop_err();
}
function c_prConfig(){
	Object.call(this);
	this.m__path="";
	this.m__rawData="";
	this.m__data=[];
	this.m__index=0;
}
c_prConfig.prototype.p_CreateData=function(t_str){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<72>";
	this.m__data=string_trim(t_str).split("\n");
	pop_err();
}
c_prConfig.prototype.p_Load=function(t_path){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<139>";
	if(this.m__path==t_path && ((this.m__rawData).length!=0)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<139>";
		pop_err();
		return true;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<141>";
	this.m__rawData=bb_app_LoadString(t_path);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<143>";
	this.p_CreateData(this.m__rawData);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<145>";
	if(!((this.m__data).length!=0)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<145>";
		pop_err();
		return false;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<147>";
	this.m__path=t_path;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<149>";
	pop_err();
	return true;
}
c_prConfig.m_new=function(t_path){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<176>";
	this.p_Load(t_path);
	pop_err();
	return this;
}
c_prConfig.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<13>";
	pop_err();
	return this;
}
c_prConfig.prototype.p_Find=function(t_name){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<95>";
	if(!((this.m__data.length)!=0)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<95>";
		pop_err();
		return -1;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<97>";
	var t_c=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<99>";
	var t_s=dbg_array(this.m__data,this.m__index)[dbg_index].split("=");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<101>";
	if(string_trim(dbg_array(t_s,0)[dbg_index].toLowerCase())==string_trim(t_name.toLowerCase())){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<101>";
		pop_err();
		return this.m__index;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<103>";
	t_c+=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<105>";
	this.m__index+=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<105>";
	if(this.m__index>this.m__data.length-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<105>";
		this.m__index=0;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<107>";
	while(t_c<this.m__data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<109>";
		var t_s2=dbg_array(this.m__data,this.m__index)[dbg_index].split("=");
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<111>";
		if(string_trim(dbg_array(t_s2,0)[dbg_index].toLowerCase())==string_trim(t_name.toLowerCase())){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<111>";
			pop_err();
			return this.m__index;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<113>";
		t_c+=1;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<115>";
		this.m__index+=1;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<115>";
		if(this.m__index>this.m__data.length-1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<115>";
			this.m__index=0;
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<119>";
	pop_err();
	return -1;
}
c_prConfig.prototype.p_Write=function(t_str,t_overwrite){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<484>";
	if(t_str==""){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<486>";
	var t_l=[];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<488>";
	t_l=t_str.split("=");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<490>";
	if(((t_l).length!=0) && t_l.length==2){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<492>";
		var t_i=this.p_Find(dbg_array(t_l,0)[dbg_index]);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<494>";
		if(t_i==-1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<495>";
			this.m__data=resize_string_array(this.m__data,this.m__data.length+1);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<496>";
			dbg_array(this.m__data,this.m__data.length-1)[dbg_index]=t_str;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<498>";
			if(t_overwrite){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<498>";
				dbg_array(this.m__data,t_i)[dbg_index]=string_trim(dbg_array(t_l,0)[dbg_index])+"="+string_trim(dbg_array(t_l,1)[dbg_index]);
			}
		}
	}
	pop_err();
}
c_prConfig.prototype.p_WriteInt=function(t_name,t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<525>";
	if(t_name==""){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<526>";
	this.p_Write(t_name+"="+String(t_value),true);
	pop_err();
}
c_prConfig.prototype.p_ReadInt=function(t_name,t_defaultValue){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<284>";
	if(this.m__data.length<=this.m__index){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<284>";
		pop_err();
		return t_defaultValue;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<286>";
	var t_c=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<288>";
	var t_s=bb_strings_prSplitInTwo(dbg_array(this.m__data,this.m__index)[dbg_index],"=");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<290>";
	if(string_trim(dbg_array(t_s,0)[dbg_index].toLowerCase())==t_name.toLowerCase()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<290>";
		var t_=parseInt((string_trim(dbg_array(t_s,1)[dbg_index])),10);
		pop_err();
		return t_;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<292>";
	t_c+=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<294>";
	this.m__index+=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<294>";
	if(this.m__index>this.m__data.length-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<294>";
		this.m__index=0;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<296>";
	while(t_c<this.m__data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<298>";
		var t_s2=bb_strings_prSplitInTwo(dbg_array(this.m__data,this.m__index)[dbg_index],"=");
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<300>";
		if(string_trim(dbg_array(t_s2,0)[dbg_index].toLowerCase())==t_name.toLowerCase()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<300>";
			var t_2=parseInt((string_trim(dbg_array(t_s2,1)[dbg_index])),10);
			pop_err();
			return t_2;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<302>";
		t_c+=1;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<304>";
		this.m__index+=1;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<304>";
		if(this.m__index>this.m__data.length-1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<304>";
			this.m__index=0;
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/config.monkey<308>";
	pop_err();
	return t_defaultValue;
}
function bb_app_LoadString(t_path){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<220>";
	var t_=bb_app__game.LoadString(bb_data_FixDataPath(t_path));
	pop_err();
	return t_;
}
function c_prContentManager(){
	Object.call(this);
	this.m__data=new_object_array(128);
	this.m__length=0;
	this.m__cache=0;
	this.m__imageScale=1.0;
}
c_prContentManager.prototype.p__SystemInit=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<651>";
	for(var t_i=0;t_i<this.m__data.length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<652>";
		dbg_array(this.m__data,t_i)[dbg_index]=c_prContentObject.m_new.call(new c_prContentObject);
	}
	pop_err();
}
c_prContentManager.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<37>";
	this.p__SystemInit();
	pop_err();
	return this;
}
c_prContentManager.prototype.p__Expand=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<638>";
	if(this.m__length==this.m__data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<640>";
		this.m__data=resize_object_array(this.m__data,this.m__length*2+10);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<642>";
		for(var t_i=0;t_i<this.m__data.length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<643>";
			if(!((dbg_array(this.m__data,t_i)[dbg_index])!=null)){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<643>";
				dbg_array(this.m__data,t_i)[dbg_index]=c_prContentObject.m_new.call(new c_prContentObject);
			}
		}
	}
	pop_err();
}
c_prContentManager.prototype.p_SetConfig=function(t_config,t_path){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<129>";
	this.p__Expand();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<131>";
	var t_o=dbg_array(this.m__data,this.m__length)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<133>";
	this.m__length+=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<135>";
	dbg_object(t_o).m__config=t_config;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<137>";
	dbg_object(t_o).m__path=t_path;
	pop_err();
}
c_prContentManager.prototype.p_Get=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<109>";
	if(t_index<0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<109>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<110>";
	if(t_index>this.m__length-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<110>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<111>";
	pop_err();
	return dbg_array(this.m__data,t_index)[dbg_index];
}
c_prContentManager.prototype.p_Get2=function(t_path){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<118>";
	for(this.m__cache=0;this.m__cache<this.m__length;this.m__cache=this.m__cache+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<119>";
		if(dbg_object(dbg_array(this.m__data,this.m__cache)[dbg_index]).m__path==t_path){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<119>";
			pop_err();
			return dbg_array(this.m__data,this.m__cache)[dbg_index];
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<121>";
	pop_err();
	return null;
}
c_prContentManager.prototype.p_GetConfig=function(t_path){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<146>";
	if(this.m__length>0 && ((dbg_array(this.m__data,this.m__cache)[dbg_index])!=null) && dbg_object(dbg_array(this.m__data,this.m__cache)[dbg_index]).m__path==t_path){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<146>";
		pop_err();
		return dbg_object(dbg_array(this.m__data,this.m__cache)[dbg_index]).m__config;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<148>";
	this.p__Expand();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<150>";
	var t_o=this.p_Get2(t_path);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<152>";
	if((t_o)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<154>";
		pop_err();
		return dbg_object(t_o).m__config;
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<158>";
		t_o=dbg_array(this.m__data,this.m__length)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<160>";
		this.m__length+=1;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<162>";
		dbg_object(t_o).m__config=c_prConfig.m_new.call(new c_prConfig,t_path);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<164>";
		dbg_object(t_o).m__path=t_path;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<166>";
		pop_err();
		return dbg_object(t_o).m__config;
	}
}
c_prContentManager.prototype.p_GetConfig2=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<177>";
	var t_pointer=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<179>";
	for(var t_i=0;t_i<this.m__length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<180>";
		if((dbg_object(dbg_array(this.m__data,t_i)[dbg_index]).m__config)!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<181>";
			if(t_pointer==t_index){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<182>";
				pop_err();
				return dbg_object(dbg_array(this.m__data,t_i)[dbg_index]).m__config;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<184>";
				t_pointer+=1;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<189>";
	pop_err();
	return null;
}
c_prContentManager.prototype.p_GetImageScale=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<318>";
	pop_err();
	return this.m__imageScale;
}
function c_prContentObject(){
	Object.call(this);
	this.m__config=null;
	this.m__path="";
}
c_prContentObject.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/contentmanager.monkey<667>";
	pop_err();
	return this;
}
var bb_contentmanager_prContent=null;
function bb_strings_prLeft(t_str,t_length){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<120>";
	if(t_length>t_str.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<120>";
		t_length=t_str.length;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<121>";
	var t_=t_str.slice(0,t_length);
	pop_err();
	return t_;
}
function bb_strings_prMid(t_str,t_pos,t_size){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<156>";
	t_pos-=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<158>";
	var t_e=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<160>";
	if(t_size==-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<160>";
		t_e=t_str.length;
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<160>";
		t_e=t_pos+t_size;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<162>";
	var t_r="";
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<164>";
	for(var t_c=t_pos;t_c<t_e;t_c=t_c+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<165>";
		t_r=t_r+String.fromCharCode(dbg_charCodeAt(t_str,t_c));
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<168>";
	pop_err();
	return t_r;
}
function bb_strings_prSplitInTwo(t_data,t_separator){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<206>";
	var t_d=["",""];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<208>";
	var t_p=t_data.indexOf(t_separator,0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<209>";
	if(t_p==-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<209>";
		pop_err();
		return t_d;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<211>";
	dbg_array(t_d,0)[dbg_index]=bb_strings_prLeft(t_data,t_p);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<212>";
	if(dbg_array(t_d,0)[dbg_index]==""){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<212>";
		pop_err();
		return t_d;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<214>";
	var t_l=t_data.length-dbg_array(t_d,0)[dbg_index].length-1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<215>";
	if(t_l<=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<215>";
		pop_err();
		return t_d;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<217>";
	dbg_array(t_d,1)[dbg_index]=bb_strings_prMid(t_data,t_p+2,t_l);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/strings.monkey<219>";
	pop_err();
	return t_d;
}
function c_prScreenAnchor(){
	Object.call(this);
	this.m_orientation=0;
	this.m_virtualWidth=0;
	this.m_virtualHeight=0;
	this.m_offSetY=0;
	this.m_offSetX=0;
}
c_prScreenAnchor.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<7>";
	pop_err();
	return this;
}
c_prScreenAnchor.prototype.p_Initialize=function(t_virtualWidth,t_virtualHeight){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<32>";
	dbg_object(this).m_virtualWidth=t_virtualWidth;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<33>";
	dbg_object(this).m_virtualHeight=t_virtualHeight;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<34>";
	var t_deviceWidth=bb_app_DeviceWidth();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<35>";
	var t_deviceHeight=bb_app_DeviceHeight();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<37>";
	var t_offSetRatioX=(t_deviceWidth)/(dbg_object(this).m_virtualWidth);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<38>";
	var t_offSetRatioY=(t_deviceHeight)/(dbg_object(this).m_virtualHeight);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<40>";
	var t_virtualAspectRatio=(dbg_object(this).m_virtualWidth)/(dbg_object(this).m_virtualHeight);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<41>";
	var t_deviceAspectRatio=(t_deviceWidth)/(t_deviceHeight);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<42>";
	if(t_virtualAspectRatio<t_deviceAspectRatio){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<44>";
		dbg_object(this).m_offSetY=((((t_deviceHeight)/t_offSetRatioY-(dbg_object(this).m_virtualHeight))/2.0)|0);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<45>";
		dbg_object(this).m_offSetX=((((t_deviceWidth)/t_offSetRatioX-(dbg_object(this).m_virtualWidth))/2.0)|0);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<48>";
		dbg_object(this).m_offSetY=((((t_deviceHeight)/t_offSetRatioX-(dbg_object(this).m_virtualHeight))/2.0)|0);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<49>";
		dbg_object(this).m_offSetX=((((t_deviceWidth)/t_offSetRatioX-(dbg_object(this).m_virtualWidth))/2.0)|0);
	}
	pop_err();
}
c_prScreenAnchor.prototype.p_SetVirtualWithBackground=function(t_virtualWidth,t_virtualHeight,t_orientation){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<26>";
	dbg_object(this).m_orientation=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/engine/screenanchor.monkey<27>";
	this.p_Initialize(t_virtualWidth,t_virtualHeight);
	pop_err();
}
var bb_screenanchor_screenAnchor=null;
function c_prDeltaTimer(){
	Object.call(this);
	this.m__targetFPS=60.0;
	this.m__lastTicks=0;
	this.m__frameTime=0.0;
	this.m__elapsedTime=0.0;
	this.m__timeScale=1.0;
	this.m__deltaTime=0.0;
	this.m__elapsedDelta=0.0;
}
c_prDeltaTimer.m_new=function(t_targetFPS){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/deltatimer.monkey<17>";
	this.m__targetFPS=t_targetFPS;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/deltatimer.monkey<18>";
	this.m__lastTicks=bb_app_Millisecs();
	pop_err();
	return this;
}
c_prDeltaTimer.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/deltatimer.monkey<11>";
	pop_err();
	return this;
}
c_prDeltaTimer.prototype.p_Update=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/deltatimer.monkey<102>";
	this.m__frameTime=(bb_app_Millisecs()-this.m__lastTicks);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/deltatimer.monkey<103>";
	this.m__lastTicks=bb_app_Millisecs();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/deltatimer.monkey<104>";
	this.m__elapsedTime+=this.m__frameTime;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/deltatimer.monkey<105>";
	this.m__deltaTime=this.m__frameTime/(1000.0/this.m__targetFPS)*this.m__timeScale;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/deltatimer.monkey<106>";
	this.m__elapsedDelta=this.m__elapsedDelta+this.m__deltaTime/this.m__targetFPS;
	pop_err();
}
c_prDeltaTimer.prototype.p_DeltaTime=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/deltatimer.monkey<25>";
	pop_err();
	return this.m__deltaTime;
}
c_prDeltaTimer.prototype.p_Resume=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/deltatimer.monkey<53>";
	this.m__lastTicks=bb_app_Millisecs();
	pop_err();
}
function bb_app_Millisecs(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/app.monkey<233>";
	var t_=bb_app__game.Millisecs();
	pop_err();
	return t_;
}
function c_prInput(){
	Object.call(this);
	this.m_maxTouches=0;
	this.m_touchDowns=[];
	this.m_touchMoves=[];
	this.m_touchUps=[];
	this.m_touchDurations=[];
	this.m_touchTaps=[];
	this.m_touchXs=[];
	this.m_touchYs=[];
}
c_prInput.m_new=function(t_maxTouches){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<24>";
	if(t_maxTouches<1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<25>";
		t_maxTouches=1;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<27>";
	dbg_object(this).m_maxTouches=t_maxTouches;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<29>";
	dbg_object(this).m_touchDowns=new_number_array(dbg_object(this).m_maxTouches);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<30>";
	dbg_object(this).m_touchMoves=new_number_array(dbg_object(this).m_maxTouches);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<31>";
	dbg_object(this).m_touchUps=new_number_array(dbg_object(this).m_maxTouches);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<33>";
	dbg_object(this).m_touchDurations=new_number_array(dbg_object(this).m_maxTouches);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<34>";
	dbg_object(this).m_touchTaps=new_number_array(dbg_object(this).m_maxTouches);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<36>";
	dbg_object(this).m_touchXs=new_number_array(dbg_object(this).m_maxTouches);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<37>";
	dbg_object(this).m_touchYs=new_number_array(dbg_object(this).m_maxTouches);
	pop_err();
	return this;
}
c_prInput.prototype.p_Update2=function(t_deltaTime){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<41>";
	for(var t_i=0;t_i<dbg_object(this).m_maxTouches;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<42>";
		dbg_array(dbg_object(this).m_touchTaps,t_i)[dbg_index]=0;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<44>";
		if(bb_input_TouchDown(t_i)==1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<45>";
			if(dbg_array(dbg_object(this).m_touchDowns,t_i)[dbg_index]==1 || dbg_array(dbg_object(this).m_touchMoves,t_i)[dbg_index]==1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<46>";
				dbg_array(dbg_object(this).m_touchDowns,t_i)[dbg_index]=0;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<47>";
				dbg_array(dbg_object(this).m_touchDurations,t_i)[dbg_index]+=t_deltaTime;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<48>";
				dbg_array(dbg_object(this).m_touchMoves,t_i)[dbg_index]=1;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<50>";
				dbg_array(dbg_object(this).m_touchDowns,t_i)[dbg_index]=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<51>";
				dbg_array(dbg_object(this).m_touchDurations,t_i)[dbg_index]=0.0;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<54>";
			dbg_array(dbg_object(this).m_touchUps,t_i)[dbg_index]=0;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<56>";
			if(dbg_array(dbg_object(this).m_touchDowns,t_i)[dbg_index]==1 || dbg_array(dbg_object(this).m_touchMoves,t_i)[dbg_index]==1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<57>";
				dbg_array(dbg_object(this).m_touchUps,t_i)[dbg_index]=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<58>";
				if(dbg_array(dbg_object(this).m_touchDurations,t_i)[dbg_index]+t_deltaTime<=10.0){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<59>";
					dbg_array(dbg_object(this).m_touchTaps,t_i)[dbg_index]=1;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<61>";
				dbg_array(dbg_object(this).m_touchDurations,t_i)[dbg_index]=0.0;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<63>";
				dbg_array(dbg_object(this).m_touchUps,t_i)[dbg_index]=0;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<65>";
			dbg_array(dbg_object(this).m_touchDowns,t_i)[dbg_index]=0;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<66>";
			dbg_array(dbg_object(this).m_touchMoves,t_i)[dbg_index]=0;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<69>";
		dbg_array(dbg_object(this).m_touchXs,t_i)[dbg_index]=bb_input_TouchX(t_i);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<70>";
		dbg_array(dbg_object(this).m_touchYs,t_i)[dbg_index]=bb_input_TouchY(t_i);
	}
	pop_err();
}
c_prInput.prototype.p_TouchDown=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<75>";
	pop_err();
	return dbg_array(dbg_object(this).m_touchDowns,t_index)[dbg_index];
}
c_prInput.prototype.p_TouchX=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<91>";
	pop_err();
	return dbg_array(dbg_object(this).m_touchXs,t_index)[dbg_index];
}
c_prInput.prototype.p_TouchY=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/input.monkey<94>";
	pop_err();
	return dbg_array(dbg_object(this).m_touchYs,t_index)[dbg_index];
}
function c_ScreenLoader(){
	Object.call(this);
	this.m_playScreen=null;
}
c_ScreenLoader.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/screenloader.monkey<7>";
	pop_err();
	return this;
}
c_ScreenLoader.prototype.p_CreateScreens=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/screenloader.monkey<12>";
	dbg_object(this).m_playScreen=c_PlayScreen.m_new.call(new c_PlayScreen);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/screenloader.monkey<13>";
	dbg_object(this).m_playScreen.p_SetScreenLoader(this);
	pop_err();
}
c_ScreenLoader.prototype.p_SetInputMonitor=function(t_inputMonitor){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/screenloader.monkey<17>";
	dbg_object(this).m_playScreen.p_SetInputMonitor(t_inputMonitor);
	pop_err();
}
c_ScreenLoader.prototype.p_GetPlayScreen=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/screenloader.monkey<21>";
	pop_err();
	return dbg_object(this).m_playScreen;
}
function c_GameScreen(){
	c_prScreen.call(this);
	this.m_screenLoader=null;
	this.m_inputMonitor=null;
	this.m_scene=null;
	this.m_camera=null;
}
c_GameScreen.prototype=extend_class(c_prScreen);
c_GameScreen.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<14>";
	c_prScreen.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<14>";
	pop_err();
	return this;
}
c_GameScreen.prototype.p_SetScreenLoader=function(t_screenLoader){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<104>";
	dbg_object(this).m_screenLoader=t_screenLoader;
	pop_err();
}
c_GameScreen.prototype.p_SetInputMonitor=function(t_inputMonitor){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<92>";
	dbg_object(this).m_inputMonitor=t_inputMonitor;
	pop_err();
}
c_GameScreen.prototype.p_OnCreate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<23>";
	c_prScreen.prototype.p_OnCreate.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<26>";
	var t_config=bb_contentmanager_prContent.p_GetConfig("config");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<27>";
	var t_virtualWidth=t_config.p_ReadInt("virtualWidth",0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<28>";
	var t_virtualHeight=t_config.p_ReadInt("virtualHeight",0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<31>";
	dbg_object(this).m_scene=c_prScene.m_new.call(new c_prScene);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<42>";
	for(var t_i=0;t_i<3;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<43>";
		for(var t_j=0;t_j<3;t_j=t_j+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<44>";
			var t_camera=c_prCamera.m_new.call(new c_prCamera,dbg_object(this).m_scene);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<45>";
			t_camera.p_SetViewport(200*t_i,200*t_j,200,200);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<46>";
			t_camera.p_SetProjection2d((200*t_i),(200+200*t_i),(200*t_j),(200+200*t_j),-1.0,1.0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<47>";
			if(t_i==0 && t_j==0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<48>";
				dbg_object(this).m_camera=t_camera;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<53>";
	pop_err();
	return 0;
}
c_GameScreen.prototype.p_OnInit=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<57>";
	c_prScreen.prototype.p_OnInit.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<60>";
	var t_layers=dbg_object(this).m_scene.p_Layers();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<61>";
	for(var t_i=0;t_i<t_layers.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<62>";
		var t_entities=t_layers.p_Get(t_i).p_Entities();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<63>";
		for(var t_j=0;t_j<t_entities.p_Length2();t_j=t_j+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<64>";
			t_entities.p_Get(t_j).p_SetScale(1.0/bb_contentmanager_prContent.p_GetImageScale());
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<68>";
	pop_err();
	return 0;
}
c_GameScreen.prototype.p_OnUpdate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<72>";
	c_prScreen.prototype.p_OnUpdate.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<73>";
	dbg_object(this).m_scene.p_Update();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<74>";
	pop_err();
	return 0;
}
c_GameScreen.prototype.p_OnRender=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<78>";
	c_prScreen.prototype.p_OnRender.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<79>";
	dbg_object(this).m_scene.p_Render();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<80>";
	pop_err();
	return 0;
}
c_GameScreen.prototype.p_GetScene=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/gamescreen.monkey<84>";
	pop_err();
	return dbg_object(this).m_scene;
}
function c_PlayScreen(){
	c_GameScreen.call(this);
	this.m_puzzle=null;
}
c_PlayScreen.prototype=extend_class(c_GameScreen);
c_PlayScreen.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<7>";
	c_GameScreen.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<7>";
	pop_err();
	return this;
}
c_PlayScreen.prototype.p_OnCreate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<17>";
	c_GameScreen.prototype.p_OnCreate.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<20>";
	var t_puzzleLayer=c_prLayer.m_new.call(new c_prLayer,this.p_GetScene());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<21>";
	t_puzzleLayer.p_Name2("puzzleLayer");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<24>";
	var t_img=c_Image2.m_Load("levels/level_1.png",.5,.5,3,null);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<25>";
	dbg_object(this).m_puzzle=c_Puzzle.m_new.call(new c_Puzzle,t_puzzleLayer,t_img,3,3);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<27>";
	pop_err();
	return 0;
}
c_PlayScreen.prototype.p_OnStart=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<31>";
	c_prScreen.prototype.p_OnStart.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<32>";
	pop_err();
	return 0;
}
c_PlayScreen.prototype.p_OnUpdate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<36>";
	c_GameScreen.prototype.p_OnUpdate.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<37>";
	dbg_object(this).m_puzzle.p_OnUpdate();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<39>";
	if((this.m_inputMonitor.p_TouchDown(0))!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<40>";
		dbg_object(this).m_camera.p_SetScene(this.p_GetScene());
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<41>";
		dbg_object(this).m_camera.p_SetViewport(((this.m_inputMonitor.p_TouchX(0))|0),((this.m_inputMonitor.p_TouchY(0))|0),200,200);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<44>";
	pop_err();
	return 0;
}
c_PlayScreen.prototype.p_OnRender=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<48>";
	c_GameScreen.prototype.p_OnRender.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/screens/playscreen.monkey<49>";
	pop_err();
	return 0;
}
function bb_input_TouchDown(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/input.monkey<84>";
	var t_=((bb_input_device.p_KeyDown(384+t_index))?1:0);
	pop_err();
	return t_;
}
function bb_input_TouchX(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/input.monkey<76>";
	var t_=bb_input_device.p_TouchX(t_index);
	pop_err();
	return t_;
}
function bb_input_TouchY(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo/input.monkey<80>";
	var t_=bb_input_device.p_TouchY(t_index);
	pop_err();
	return t_;
}
function c_prTweenPool(){
	Object.call(this);
	this.m_tweenPool=[];
}
c_prTweenPool.m_new=function(t_maxTweens){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<80>";
	bb_tweens_InitTweenSystem();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<82>";
	dbg_object(this).m_tweenPool=new_object_array(t_maxTweens);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<83>";
	for(var t_i=0;t_i<t_maxTweens;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<84>";
		dbg_array(dbg_object(this).m_tweenPool,t_i)[dbg_index]=c_prTween.m_new.call(new c_prTween,t_i);
	}
	pop_err();
	return this;
}
c_prTweenPool.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<75>";
	pop_err();
	return this;
}
c_prTweenPool.prototype.p_Update2=function(t_delta){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<103>";
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<103>";
	var t_=this.m_tweenPool;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<103>";
	var t_2=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<103>";
	while(t_2<t_.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<103>";
		var t_tween=dbg_array(t_,t_2)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<103>";
		t_2=t_2+1;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<104>";
		t_tween.p_Update2(t_delta);
	}
	pop_err();
}
function c_LinearTween(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_LinearTween.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<245>";
	pop_err();
	return this;
}
var bb_tweens_TweenFunc=[];
function c_EaseInQuad(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInQuad.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<256>";
	pop_err();
	return this;
}
function c_EaseOutQuad(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseOutQuad.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<263>";
	pop_err();
	return this;
}
function c_EaseInOutQuad(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInOutQuad.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<270>";
	pop_err();
	return this;
}
function c_EaseInCubic(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInCubic.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<284>";
	pop_err();
	return this;
}
function c_EaseOutCubic(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseOutCubic.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<291>";
	pop_err();
	return this;
}
function c_EaseInOutCubic(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInOutCubic.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<299>";
	pop_err();
	return this;
}
function c_EaseInQuart(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInQuart.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<312>";
	pop_err();
	return this;
}
function c_EaseOutQuart(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseOutQuart.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<319>";
	pop_err();
	return this;
}
function c_EaseInOutQuart(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInOutQuart.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<327>";
	pop_err();
	return this;
}
function c_EaseInQuint(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInQuint.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<341>";
	pop_err();
	return this;
}
function c_EaseOutQuint(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseOutQuint.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<348>";
	pop_err();
	return this;
}
function c_EaseInOutQuint(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInOutQuint.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<356>";
	pop_err();
	return this;
}
function c_EaseInSine(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInSine.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<372>";
	pop_err();
	return this;
}
function c_EaseOutSine(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseOutSine.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<378>";
	pop_err();
	return this;
}
function c_EaseInOutSine(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInOutSine.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<384>";
	pop_err();
	return this;
}
function c_EaseInExpo(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInExpo.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<395>";
	pop_err();
	return this;
}
function c_EaseOutExpo(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseOutExpo.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<401>";
	pop_err();
	return this;
}
function c_EaseInOutExpo(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInOutExpo.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<407>";
	pop_err();
	return this;
}
function c_EaseInCirc(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInCirc.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<423>";
	pop_err();
	return this;
}
function c_EaseOutCirc(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseOutCirc.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<430>";
	pop_err();
	return this;
}
function c_EaseInOutCirc(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInOutCirc.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<438>";
	pop_err();
	return this;
}
function c_EaseInBack(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInBack.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<454>";
	pop_err();
	return this;
}
function c_EaseOutBack(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseOutBack.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<461>";
	pop_err();
	return this;
}
function c_EaseInOutBack(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInOutBack.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<468>";
	pop_err();
	return this;
}
function c_EaseInBounce(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInBounce.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<486>";
	pop_err();
	return this;
}
function c_EaseOutBounce(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseOutBounce.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<492>";
	pop_err();
	return this;
}
function c_EaseInOutBounce(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInOutBounce.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<510>";
	pop_err();
	return this;
}
function c_EaseInElastic(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInElastic.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<524>";
	pop_err();
	return this;
}
function c_EaseOutElastic(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseOutElastic.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<549>";
	pop_err();
	return this;
}
function c_EaseInOutElastic(){
	Object.call(this);
	this.implments={c_Tweener:1};
}
c_EaseInOutElastic.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<573>";
	pop_err();
	return this;
}
function bb_tweens_InitTweenSystem(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<620>";
	dbg_array(bb_tweens_TweenFunc,0)[dbg_index]=(c_LinearTween.m_new.call(new c_LinearTween));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<622>";
	dbg_array(bb_tweens_TweenFunc,1)[dbg_index]=(c_EaseInQuad.m_new.call(new c_EaseInQuad));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<623>";
	dbg_array(bb_tweens_TweenFunc,2)[dbg_index]=(c_EaseOutQuad.m_new.call(new c_EaseOutQuad));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<624>";
	dbg_array(bb_tweens_TweenFunc,3)[dbg_index]=(c_EaseInOutQuad.m_new.call(new c_EaseInOutQuad));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<626>";
	dbg_array(bb_tweens_TweenFunc,4)[dbg_index]=(c_EaseInCubic.m_new.call(new c_EaseInCubic));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<627>";
	dbg_array(bb_tweens_TweenFunc,5)[dbg_index]=(c_EaseOutCubic.m_new.call(new c_EaseOutCubic));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<628>";
	dbg_array(bb_tweens_TweenFunc,6)[dbg_index]=(c_EaseInOutCubic.m_new.call(new c_EaseInOutCubic));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<630>";
	dbg_array(bb_tweens_TweenFunc,7)[dbg_index]=(c_EaseInQuart.m_new.call(new c_EaseInQuart));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<631>";
	dbg_array(bb_tweens_TweenFunc,8)[dbg_index]=(c_EaseOutQuart.m_new.call(new c_EaseOutQuart));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<632>";
	dbg_array(bb_tweens_TweenFunc,9)[dbg_index]=(c_EaseInOutQuart.m_new.call(new c_EaseInOutQuart));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<634>";
	dbg_array(bb_tweens_TweenFunc,10)[dbg_index]=(c_EaseInQuint.m_new.call(new c_EaseInQuint));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<635>";
	dbg_array(bb_tweens_TweenFunc,11)[dbg_index]=(c_EaseOutQuint.m_new.call(new c_EaseOutQuint));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<636>";
	dbg_array(bb_tweens_TweenFunc,12)[dbg_index]=(c_EaseInOutQuint.m_new.call(new c_EaseInOutQuint));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<638>";
	dbg_array(bb_tweens_TweenFunc,13)[dbg_index]=(c_EaseInSine.m_new.call(new c_EaseInSine));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<639>";
	dbg_array(bb_tweens_TweenFunc,14)[dbg_index]=(c_EaseOutSine.m_new.call(new c_EaseOutSine));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<640>";
	dbg_array(bb_tweens_TweenFunc,15)[dbg_index]=(c_EaseInOutSine.m_new.call(new c_EaseInOutSine));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<642>";
	dbg_array(bb_tweens_TweenFunc,16)[dbg_index]=(c_EaseInExpo.m_new.call(new c_EaseInExpo));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<643>";
	dbg_array(bb_tweens_TweenFunc,17)[dbg_index]=(c_EaseOutExpo.m_new.call(new c_EaseOutExpo));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<644>";
	dbg_array(bb_tweens_TweenFunc,18)[dbg_index]=(c_EaseInOutExpo.m_new.call(new c_EaseInOutExpo));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<646>";
	dbg_array(bb_tweens_TweenFunc,16)[dbg_index]=(c_EaseInExpo.m_new.call(new c_EaseInExpo));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<647>";
	dbg_array(bb_tweens_TweenFunc,17)[dbg_index]=(c_EaseOutExpo.m_new.call(new c_EaseOutExpo));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<648>";
	dbg_array(bb_tweens_TweenFunc,18)[dbg_index]=(c_EaseInOutExpo.m_new.call(new c_EaseInOutExpo));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<650>";
	dbg_array(bb_tweens_TweenFunc,19)[dbg_index]=(c_EaseInCirc.m_new.call(new c_EaseInCirc));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<651>";
	dbg_array(bb_tweens_TweenFunc,20)[dbg_index]=(c_EaseOutCirc.m_new.call(new c_EaseOutCirc));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<652>";
	dbg_array(bb_tweens_TweenFunc,21)[dbg_index]=(c_EaseInOutCirc.m_new.call(new c_EaseInOutCirc));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<654>";
	dbg_array(bb_tweens_TweenFunc,22)[dbg_index]=(c_EaseInBack.m_new.call(new c_EaseInBack));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<655>";
	dbg_array(bb_tweens_TweenFunc,23)[dbg_index]=(c_EaseOutBack.m_new.call(new c_EaseOutBack));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<656>";
	dbg_array(bb_tweens_TweenFunc,24)[dbg_index]=(c_EaseInOutBack.m_new.call(new c_EaseInOutBack));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<658>";
	dbg_array(bb_tweens_TweenFunc,25)[dbg_index]=(c_EaseInBounce.m_new.call(new c_EaseInBounce));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<659>";
	dbg_array(bb_tweens_TweenFunc,26)[dbg_index]=(c_EaseOutBounce.m_new.call(new c_EaseOutBounce));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<660>";
	dbg_array(bb_tweens_TweenFunc,27)[dbg_index]=(c_EaseInOutBounce.m_new.call(new c_EaseInOutBounce));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<662>";
	dbg_array(bb_tweens_TweenFunc,28)[dbg_index]=(c_EaseInElastic.m_new.call(new c_EaseInElastic));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<663>";
	dbg_array(bb_tweens_TweenFunc,29)[dbg_index]=(c_EaseOutElastic.m_new.call(new c_EaseOutElastic));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<664>";
	dbg_array(bb_tweens_TweenFunc,30)[dbg_index]=(c_EaseInOutElastic.m_new.call(new c_EaseInOutElastic));
	pop_err();
}
function c_prTween(){
	Object.call(this);
	this.m_index=0;
	this.m_eventState=0;
	this.m_tweenState=0;
	this.m_elapsedTime=.0;
	this.m_duration=.0;
	this.m_chainedTween=null;
	this.m_tweenType=0;
	this.m_startValue=.0;
	this.m_endValue=.0;
	this.m_delayTime=0;
	this.m_repeatTween=false;
}
c_prTween.m_new=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<134>";
	dbg_object(this).m_index=t_index;
	pop_err();
	return this;
}
c_prTween.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<110>";
	pop_err();
	return this;
}
c_prTween.prototype.p_Create=function(t_tweenType,t_startValue,t_endValue,t_duration){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<138>";
	dbg_object(this).m_tweenType=t_tweenType;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<139>";
	dbg_object(this).m_startValue=t_startValue;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<140>";
	dbg_object(this).m_endValue=t_endValue;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<141>";
	dbg_object(this).m_duration=t_duration;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<142>";
	dbg_object(this).m_elapsedTime=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<144>";
	dbg_object(this).m_tweenState=2;
	pop_err();
}
c_prTween.prototype.p_SetDelay=function(t_delayTime){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<205>";
	dbg_object(this).m_delayTime=((t_delayTime)|0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<206>";
	dbg_object(this).m_elapsedTime-=t_delayTime;
	pop_err();
}
c_prTween.prototype.p_SetRepeat=function(t_repeatTween){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<201>";
	dbg_object(this).m_repeatTween=t_repeatTween;
	pop_err();
}
c_prTween.prototype.p_SetState=function(t_tweenState){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<210>";
	dbg_object(this).m_tweenState=t_tweenState;
	pop_err();
}
c_prTween.prototype.p_SetChain=function(t_tween){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<194>";
	if((t_tween)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<195>";
		t_tween.p_SetState(1);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<197>";
	dbg_object(this).m_chainedTween=t_tween;
	pop_err();
}
c_prTween.prototype.p_Create2=function(t_tween){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<148>";
	this.p_Create(dbg_object(t_tween).m_tweenType,dbg_object(t_tween).m_startValue,dbg_object(t_tween).m_endValue,dbg_object(t_tween).m_duration);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<149>";
	this.p_SetDelay(dbg_object(t_tween).m_delayTime);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<150>";
	this.p_SetRepeat(dbg_object(t_tween).m_repeatTween);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<151>";
	this.p_SetChain(dbg_object(t_tween).m_chainedTween);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<153>";
	t_tween.p_SetState(0);
	pop_err();
}
c_prTween.prototype.p_Clear=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<189>";
	dbg_object(this).m_tweenState=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<190>";
	dbg_object(this).m_repeatTween=false;
	pop_err();
}
c_prTween.prototype.p_Update2=function(t_delta){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<157>";
	dbg_object(this).m_eventState=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<158>";
	if(dbg_object(this).m_tweenState!=2){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<162>";
	dbg_object(this).m_elapsedTime+=t_delta;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<164>";
	if(dbg_object(this).m_elapsedTime>=dbg_object(this).m_duration){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<165>";
		if((dbg_object(this).m_chainedTween)!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<166>";
			dbg_object(this).m_eventState=2;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<167>";
			this.p_Create2(dbg_object(this).m_chainedTween);
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<169>";
			if(dbg_object(this).m_repeatTween){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<170>";
				this.p_Create(dbg_object(this).m_tweenType,dbg_object(this).m_endValue,dbg_object(this).m_startValue,dbg_object(this).m_duration);
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyromaniac/framework/tweens.monkey<172>";
				this.p_Clear();
			}
		}
	}
	pop_err();
}
var bb_tweens_prTweens=null;
function c_prScene(){
	Object.call(this);
	this.m__cameras=c_Stack2.m_new.call(new c_Stack2);
	this.m__layers=c_Stack3.m_new.call(new c_Stack3);
	this.m__updateCounter=0;
	this.m__tilemap=null;
	this.m__collisionMonitor=null;
	this.m__clearColor=[0.0,0.0,0.0,1.0];
	this.m__ambientLight=[1.0,1.0,1.0,1.0];
}
c_prScene.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3895>";
	pop_err();
	return this;
}
c_prScene.prototype.p_Layers=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3981>";
	pop_err();
	return this.m__layers;
}
c_prScene.prototype.p_UpdateExtensions=function(){
	push_err();
	pop_err();
}
c_prScene.prototype.p__UpdateEntities=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4132>";
	for(var t_i=0;t_i<this.m__layers.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4133>";
		this.m__layers.p_Get(t_i).p_UpdateEntities(this.m__updateCounter);
	}
	pop_err();
}
c_prScene.prototype.p__UpdateLights=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4140>";
	for(var t_i=0;t_i<this.m__layers.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4141>";
		this.m__layers.p_Get(t_i).p_UpdateLights(this.m__updateCounter);
	}
	pop_err();
}
c_prScene.prototype.p__UpdateCameras=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4108>";
	var t_cameraIndex=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4110>";
	while(t_cameraIndex<this.m__cameras.p_Length2()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4112>";
		if(t_cameraIndex<0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4112>";
			break;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4114>";
		var t_camera=this.m__cameras.p_Get(t_cameraIndex);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4116>";
		if(!((t_camera.p_Scene())!=null)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4116>";
			t_cameraIndex+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4116>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4118>";
		if(bb_globals_prPause() && t_camera.p_Pausable()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4118>";
			t_cameraIndex+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4118>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4120>";
		var t_cameras=this.m__cameras.p_Length2();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4121>";
		if(t_camera.p_Enabled()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4121>";
			t_camera.p_Update();
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4121>";
			t_camera.p_OnDisabled();
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4122>";
		if(this.m__cameras.p_Length2()<t_cameras){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4122>";
			t_cameraIndex+=this.m__cameras.p_Length2()-t_cameras;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4124>";
		t_cameraIndex+=1;
	}
	pop_err();
}
c_prScene.prototype.p_Update=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4082>";
	this.p_UpdateExtensions();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4084>";
	this.p__UpdateEntities();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4085>";
	this.p__UpdateLights();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4086>";
	this.p__UpdateCameras();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4088>";
	if((this.m__collisionMonitor)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4088>";
		this.m__collisionMonitor.p_Update();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4090>";
	this.m__updateCounter+=1;
	pop_err();
}
c_prScene.prototype.p_UpdateCounter=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4098>";
	pop_err();
	return this.m__updateCounter;
}
c_prScene.prototype.p_Render=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4003>";
	for(var t_i=0;t_i<this.m__cameras.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4004>";
		this.m__cameras.p_Get(t_i).p_Render();
	}
	pop_err();
}
function c_prEntity(){
	Object.call(this);
	this.m__tattooed=false;
	this.m__pausable=true;
	this.m__lastUpdate=0;
	this.m__enabled=true;
	this.m__startTime=bb_app_Millisecs();
	this.m__visible=true;
	this.m__name="";
}
c_prEntity.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<793>";
	pop_err();
	return this;
}
c_prEntity.prototype.p_Tattooed=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1036>";
	pop_err();
	return this.m__tattooed;
}
c_prEntity.prototype.p_Tattooed2=function(t_tattooed){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1043>";
	this.m__tattooed=t_tattooed;
	pop_err();
}
c_prEntity.prototype.p_Pausable=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<868>";
	pop_err();
	return this.m__pausable;
}
c_prEntity.prototype.p_Pausable2=function(t_pausable){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<877>";
	this.m__pausable=t_pausable;
	pop_err();
}
c_prEntity.prototype.p_LastUpdate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<817>";
	pop_err();
	return this.m__lastUpdate;
}
c_prEntity.prototype.p_LastUpdate2=function(t_lastUpdate){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<824>";
	this.m__lastUpdate=t_lastUpdate;
	pop_err();
}
c_prEntity.prototype.p_Enabled=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<799>";
	pop_err();
	return this.m__enabled;
}
c_prEntity.prototype.p_Enabled2=function(t_enabled){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<807>";
	this.m__enabled=t_enabled;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<809>";
	this.m__startTime=bb_app_Millisecs();
	pop_err();
}
c_prEntity.prototype.p_OnDisabled=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<854>";
	pop_err();
	return 0;
}
c_prEntity.prototype.p_Update=function(){
	push_err();
	pop_err();
}
c_prEntity.prototype.p_OnUpdate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<861>";
	pop_err();
	return 0;
}
c_prEntity.prototype.p_Visible=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1055>";
	pop_err();
	return this.m__visible;
}
c_prEntity.prototype.p_Visible2=function(t_visible){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1062>";
	this.m__visible=t_visible;
	pop_err();
}
c_prEntity.prototype.p_Name=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<840>";
	pop_err();
	return this.m__name;
}
c_prEntity.prototype.p_Name2=function(t_name){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<847>";
	this.m__name=t_name;
	pop_err();
}
function c_prEntity3d(){
	c_prEntity.call(this);
	this.m__matrix=c_prMat4.m_new3.call(new c_prMat4);
}
c_prEntity3d.prototype=extend_class(c_prEntity);
c_prEntity3d.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1532>";
	c_prEntity.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1532>";
	pop_err();
	return this;
}
c_prEntity3d.prototype.p_X=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1573>";
	var t_=this.m__matrix.p_Tx();
	pop_err();
	return t_;
}
c_prEntity3d.prototype.p_Y=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1587>";
	var t_=this.m__matrix.p_Ty();
	pop_err();
	return t_;
}
c_prEntity3d.prototype.p_Z=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1601>";
	var t_=this.m__matrix.p_Tz();
	pop_err();
	return t_;
}
c_prEntity3d.prototype.p_SetMatrix=function(t_matrix){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1545>";
	this.m__matrix=t_matrix;
	pop_err();
}
c_prEntity3d.prototype.p_SetMatrix2=function(t_tx,t_ty,t_tz,t_rx,t_ry,t_rz,t_sx,t_sy,t_sz){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1566>";
	this.p_SetMatrix(c_prMat4.m_Translation(t_tx,t_ty,t_tz).p_Times2(c_prMat4.m_Rotation(t_rx,t_ry,t_rz)).p_Times2(c_prMat4.m_Scale(t_sx,t_sy,t_sz)));
	pop_err();
}
c_prEntity3d.prototype.p_SetMatrix3=function(t_tx,t_ty,t_tz,t_rx,t_ry,t_rz){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1559>";
	this.p_SetMatrix(c_prMat4.m_Translation(t_tx,t_ty,t_tz).p_Times2(c_prMat4.m_Rotation(t_rx,t_ry,t_rz)));
	pop_err();
}
c_prEntity3d.prototype.p_SetMatrix4=function(t_tx,t_ty,t_tz){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1552>";
	this.p_SetMatrix(c_prMat4.m_Translation(t_tx,t_ty,t_tz));
	pop_err();
}
c_prEntity3d.prototype.p_Z2=function(t_z){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1608>";
	this.p_SetMatrix4(this.p_X(),this.p_Y(),t_z);
	pop_err();
}
c_prEntity3d.prototype.p_Y2=function(t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1594>";
	this.p_SetMatrix4(this.p_X(),t_y,this.p_Z());
	pop_err();
}
c_prEntity3d.prototype.p_X2=function(t_x){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1580>";
	this.p_SetMatrix4(t_x,this.p_Y(),this.p_Z());
	pop_err();
}
c_prEntity3d.prototype.p_Matrix=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1538>";
	pop_err();
	return this.m__matrix;
}
function c_prCamera(){
	c_prEntity3d.call(this);
	this.m__scene=null;
	this.m__viewport=[0,0,bb_app_DeviceWidth(),bb_app_DeviceHeight()];
	this.m__projectionMatrix=null;
	this.m__virtualWidth=bb_app_DeviceWidth();
	this.m__virtualHeight=bb_app_DeviceHeight();
	this.m__cullingX=0.0;
	this.m__cullingY=0.0;
	this.m__cullingWidth=bb_app_DeviceWidth();
	this.m__cullingHeight=bb_app_DeviceHeight();
	this.m__renderer=c_Renderer.m_new.call(new c_Renderer);
}
c_prCamera.prototype=extend_class(c_prEntity3d);
c_prCamera.prototype.p_SetScene=function(t_scene){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<425>";
	if((this.m__scene)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<425>";
		dbg_object(this.m__scene).m__cameras.p_RemoveFirst(this);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<426>";
	this.m__scene=t_scene;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<427>";
	if((this.m__scene)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<427>";
		dbg_object(this.m__scene).m__cameras.p_Push4(this);
	}
	pop_err();
}
c_prCamera.prototype.p_SetViewport=function(t_x,t_y,t_w,t_h){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<436>";
	this.m__viewport=[t_x,t_y,t_w,t_h];
	pop_err();
}
c_prCamera.prototype.p_SetProjection2d=function(t_left,t_right,t_top,t_bottom,t_near,t_far){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<393>";
	this.m__projectionMatrix=c_prMat4.m_Ortho(t_left,t_right,t_top,t_bottom,t_near,t_far);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<395>";
	this.m__virtualWidth=((t_right-t_left)|0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<396>";
	this.m__virtualHeight=((t_bottom-t_top)|0);
	pop_err();
}
c_prCamera.m_new=function(t_scene){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<249>";
	c_prEntity3d.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<251>";
	this.p_SetScene(t_scene);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<252>";
	var t_w=bb_app_DeviceWidth();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<252>";
	var t_h=bb_app_DeviceHeight();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<253>";
	this.p_SetViewport(0,0,t_w,t_h);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<254>";
	this.p_SetProjection2d(0.0,(t_w),0.0,(t_h),-1.0,1.0);
	pop_err();
	return this;
}
c_prCamera.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<244>";
	c_prEntity3d.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<244>";
	pop_err();
	return this;
}
c_prCamera.prototype.p_Scene=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<363>";
	pop_err();
	return this.m__scene;
}
c_prCamera.prototype.p__UpdateCulling=function(t_x,t_y,t_width,t_height){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<645>";
	for(var t_i=0;t_i<dbg_object(this.m__scene).m__layers.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<646>";
		if(dbg_object(this.m__scene).m__layers.p_Get(t_i).p_Visible()==true){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<647>";
			dbg_object(this.m__scene).m__layers.p_Get(t_i).p__UpdateCulling(t_x,t_y,t_width,t_height);
		}
	}
	pop_err();
}
c_prCamera.prototype.p__UpdateCulling2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<640>";
	this.p__UpdateCulling(((this.p_X()+this.m__cullingX)|0),((this.p_Y()+this.m__cullingY)|0),this.m__cullingWidth,this.m__cullingHeight);
	pop_err();
}
c_prCamera.prototype.p_Update=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<587>";
	if(bb_globals_prPause() && this.p_Pausable()){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<588>";
	if(!((this.p_Scene())!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<590>";
	this.p_OnUpdate();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<592>";
	this.p__UpdateCulling2();
	pop_err();
}
c_prCamera.prototype.p_Render=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<314>";
	if(!((this.p_Scene().p_UpdateCounter())!=0)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<314>";
		this.p_Update();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<316>";
	this.m__renderer.p_SetRenderTarget(null);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<317>";
	this.m__renderer.p_SetViewport(dbg_array(this.m__viewport,0)[dbg_index],dbg_array(this.m__viewport,1)[dbg_index],dbg_array(this.m__viewport,2)[dbg_index],dbg_array(this.m__viewport,3)[dbg_index]);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<318>";
	this.m__renderer.p_SetClearColor(dbg_object(this.m__scene).m__clearColor);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<319>";
	this.m__renderer.p_SetAmbientLight(dbg_object(this.m__scene).m__ambientLight);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<320>";
	this.m__renderer.p_SetCameraMatrix(this.p_Matrix().p_ToArray());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<321>";
	this.m__renderer.p_SetProjectionMatrix(this.m__projectionMatrix.p_ToArray());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<323>";
	this.m__renderer.p_Layers().p_Clear();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<325>";
	for(var t_i=0;t_i<dbg_object(this.m__scene).m__layers.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<326>";
		if(!dbg_object(this.m__scene).m__layers.p_Get(t_i).p_Enabled()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<326>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<327>";
		if(!dbg_object(this.m__scene).m__layers.p_Get(t_i).p_Visible()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<327>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<328>";
		this.m__renderer.p_Layers().p_Push31(dbg_object(this.m__scene).m__layers.p_Get(t_i));
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<331>";
	this.m__renderer.p_Render();
	pop_err();
}
c_prCamera.prototype.p_Render2=function(t_image){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<337>";
	if(!((this.p_Scene().p_UpdateCounter())!=0)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<337>";
		this.p_Update();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<339>";
	this.m__renderer.p_SetRenderTarget(t_image);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<340>";
	this.m__renderer.p_SetViewport(dbg_array(this.m__viewport,0)[dbg_index],dbg_array(this.m__viewport,1)[dbg_index],dbg_array(this.m__viewport,2)[dbg_index],dbg_array(this.m__viewport,3)[dbg_index]);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<341>";
	this.m__renderer.p_SetViewport(0,0,t_image.p_Width(),t_image.p_Height());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<342>";
	this.m__renderer.p_SetClearColor(dbg_object(this.m__scene).m__clearColor);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<343>";
	this.m__renderer.p_SetAmbientLight(dbg_object(this.m__scene).m__ambientLight);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<344>";
	this.m__renderer.p_SetCameraMatrix(this.p_Matrix().p_ToArray());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<345>";
	this.m__renderer.p_SetProjectionMatrix(this.m__projectionMatrix.p_ToArray());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<347>";
	this.m__renderer.p_Layers().p_Clear();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<349>";
	for(var t_i=0;t_i<dbg_object(this.m__scene).m__layers.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<350>";
		if(!dbg_object(this.m__scene).m__layers.p_Get(t_i).p_Enabled()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<350>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<351>";
		if(!dbg_object(this.m__scene).m__layers.p_Get(t_i).p_Visible()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<351>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<352>";
		this.m__renderer.p_Layers().p_Push31(dbg_object(this.m__scene).m__layers.p_Get(t_i));
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<355>";
	this.m__renderer.p_Render();
	pop_err();
}
function c_Stack2(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack2.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack2.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack2.prototype.p_Equals=function(t_lhs,t_rhs){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<26>";
	var t_=t_lhs==t_rhs;
	pop_err();
	return t_;
}
c_Stack2.prototype.p_Find2=function(t_value,t_start){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<108>";
	for(var t_i=t_start;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<109>";
		if(this.p_Equals(dbg_array(this.m_data,t_i)[dbg_index],t_value)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<109>";
			pop_err();
			return t_i;
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<111>";
	pop_err();
	return -1;
}
c_Stack2.m_NIL=null;
c_Stack2.prototype.p_Remove=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<137>";
	for(var t_i=t_index;t_i<this.m_length-1;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<138>";
		dbg_array(this.m_data,t_i)[dbg_index]=dbg_array(this.m_data,t_i+1)[dbg_index];
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<140>";
	this.m_length-=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<141>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=c_Stack2.m_NIL;
	pop_err();
}
c_Stack2.prototype.p_RemoveFirst=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<145>";
	var t_i=this.p_Find2(t_value,0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<146>";
	if(t_i!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<146>";
		this.p_Remove(t_i);
	}
	pop_err();
}
c_Stack2.prototype.p_Push4=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack2.prototype.p_Push5=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push4(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack2.prototype.p_Push6=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push5(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
c_Stack2.prototype.p_Length=function(t_newlength){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<45>";
	if(t_newlength<this.m_length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<46>";
		for(var t_i=t_newlength;t_i<this.m_length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<47>";
			dbg_array(this.m_data,t_i)[dbg_index]=c_Stack2.m_NIL;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<49>";
		if(t_newlength>this.m_data.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<50>";
			this.m_data=resize_object_array(this.m_data,bb_math_Max(this.m_length*2+10,t_newlength));
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<52>";
	this.m_length=t_newlength;
	pop_err();
}
c_Stack2.prototype.p_Length2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<56>";
	pop_err();
	return this.m_length;
}
c_Stack2.prototype.p_Get=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<104>";
	pop_err();
	return dbg_array(this.m_data,t_index)[dbg_index];
}
function c_prMat4(){
	Object.call(this);
	this.m_mat4=[1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0];
}
c_prMat4.m_new=function(t_m){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<8>";
	dbg_array(dbg_object(this).m_mat4,0)[dbg_index]=dbg_array(t_m,0)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<8>";
	dbg_array(dbg_object(this).m_mat4,1)[dbg_index]=dbg_array(t_m,1)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<8>";
	dbg_array(dbg_object(this).m_mat4,2)[dbg_index]=dbg_array(t_m,2)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<8>";
	dbg_array(dbg_object(this).m_mat4,3)[dbg_index]=dbg_array(t_m,3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<9>";
	dbg_array(dbg_object(this).m_mat4,4)[dbg_index]=dbg_array(t_m,4)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<9>";
	dbg_array(dbg_object(this).m_mat4,5)[dbg_index]=dbg_array(t_m,5)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<9>";
	dbg_array(dbg_object(this).m_mat4,6)[dbg_index]=dbg_array(t_m,6)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<9>";
	dbg_array(dbg_object(this).m_mat4,7)[dbg_index]=dbg_array(t_m,7)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<10>";
	dbg_array(dbg_object(this).m_mat4,8)[dbg_index]=dbg_array(t_m,8)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<10>";
	dbg_array(dbg_object(this).m_mat4,9)[dbg_index]=dbg_array(t_m,9)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<10>";
	dbg_array(dbg_object(this).m_mat4,10)[dbg_index]=dbg_array(t_m,10)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<10>";
	dbg_array(dbg_object(this).m_mat4,11)[dbg_index]=dbg_array(t_m,11)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<11>";
	dbg_array(dbg_object(this).m_mat4,12)[dbg_index]=dbg_array(t_m,12)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<11>";
	dbg_array(dbg_object(this).m_mat4,13)[dbg_index]=dbg_array(t_m,13)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<11>";
	dbg_array(dbg_object(this).m_mat4,14)[dbg_index]=dbg_array(t_m,14)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<11>";
	dbg_array(dbg_object(this).m_mat4,15)[dbg_index]=dbg_array(t_m,15)[dbg_index];
	pop_err();
	return this;
}
c_prMat4.m_new2=function(t_ix,t_iy,t_iz,t_iw,t_jx,t_jy,t_jz,t_jw,t_kx,t_ky,t_kz,t_kw,t_tx,t_ty,t_tz,t_tw){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<15>";
	dbg_array(dbg_object(this).m_mat4,0)[dbg_index]=t_ix;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<15>";
	dbg_array(dbg_object(this).m_mat4,1)[dbg_index]=t_iy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<15>";
	dbg_array(dbg_object(this).m_mat4,2)[dbg_index]=t_iz;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<15>";
	dbg_array(dbg_object(this).m_mat4,3)[dbg_index]=t_iw;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<16>";
	dbg_array(dbg_object(this).m_mat4,4)[dbg_index]=t_jx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<16>";
	dbg_array(dbg_object(this).m_mat4,5)[dbg_index]=t_jy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<16>";
	dbg_array(dbg_object(this).m_mat4,6)[dbg_index]=t_jz;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<16>";
	dbg_array(dbg_object(this).m_mat4,7)[dbg_index]=t_jw;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<17>";
	dbg_array(dbg_object(this).m_mat4,8)[dbg_index]=t_kx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<17>";
	dbg_array(dbg_object(this).m_mat4,9)[dbg_index]=t_ky;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<17>";
	dbg_array(dbg_object(this).m_mat4,10)[dbg_index]=t_kz;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<17>";
	dbg_array(dbg_object(this).m_mat4,11)[dbg_index]=t_kw;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<18>";
	dbg_array(dbg_object(this).m_mat4,12)[dbg_index]=t_tx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<18>";
	dbg_array(dbg_object(this).m_mat4,13)[dbg_index]=t_ty;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<18>";
	dbg_array(dbg_object(this).m_mat4,14)[dbg_index]=t_tz;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<18>";
	dbg_array(dbg_object(this).m_mat4,15)[dbg_index]=t_tw;
	pop_err();
	return this;
}
c_prMat4.m_new3=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<5>";
	pop_err();
	return this;
}
c_prMat4.m_Ortho=function(t_left,t_right,t_bottom,t_top,t_near,t_far){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<55>";
	var t_w=t_right-t_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<55>";
	var t_h=t_top-t_bottom;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<55>";
	var t_d=t_far-t_near;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<60>";
	var t_=c_prMat4.m_new2.call(new c_prMat4,2.0/t_w,0.0,0.0,0.0,0.0,2.0/t_h,0.0,0.0,0.0,0.0,2.0/t_d,0.0,-(t_right+t_left)/t_w,-(t_top+t_bottom)/t_h,-(t_far+t_near)/t_d,1.0);
	pop_err();
	return t_;
}
c_prMat4.prototype.p_Tx=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<198>";
	pop_err();
	return dbg_array(this.m_mat4,12)[dbg_index];
}
c_prMat4.prototype.p_Ty=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<202>";
	pop_err();
	return dbg_array(this.m_mat4,13)[dbg_index];
}
c_prMat4.prototype.p_Tz=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<206>";
	pop_err();
	return dbg_array(this.m_mat4,14)[dbg_index];
}
c_prMat4.m_Translation=function(t_tx,t_ty,t_tz){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<122>";
	var t_=c_prMat4.m_new2.call(new c_prMat4,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,t_tx,t_ty,t_tz,1.0);
	pop_err();
	return t_;
}
c_prMat4.m_Roll=function(t_t){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<74>";
	var t_s=Math.sin((t_t)*D2R);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<74>";
	var t_c=Math.cos((t_t)*D2R);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<75>";
	var t_=c_prMat4.m_new2.call(new c_prMat4,t_c,t_s,0.0,0.0,-t_s,t_c,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0);
	pop_err();
	return t_;
}
c_prMat4.m_Pitch=function(t_t){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<69>";
	var t_s=Math.sin((t_t)*D2R);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<69>";
	var t_c=Math.cos((t_t)*D2R);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<70>";
	var t_=c_prMat4.m_new2.call(new c_prMat4,1.0,0.0,0.0,0.0,0.0,t_c,t_s,0.0,0.0,-t_s,t_c,0.0,0.0,0.0,0.0,1.0);
	pop_err();
	return t_;
}
c_prMat4.m_Yaw=function(t_t){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<214>";
	var t_s=Math.sin((t_t)*D2R);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<214>";
	var t_c=Math.cos((t_t)*D2R);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<215>";
	var t_=c_prMat4.m_new2.call(new c_prMat4,t_c,0.0,t_s,0.0,0.0,1.0,0.0,0.0,-t_s,0.0,t_c,0.0,0.0,0.0,0.0,1.0);
	pop_err();
	return t_;
}
c_prMat4.prototype.p_Times=function(t_v){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<91>";
	var t_=c_prVec4.m_new2.call(new c_prVec4,dbg_array(dbg_object(this).m_mat4,0)[dbg_index]*dbg_object(t_v).m_x+dbg_array(dbg_object(this).m_mat4,4)[dbg_index]*dbg_object(t_v).m_y+dbg_array(dbg_object(this).m_mat4,8)[dbg_index]*dbg_object(t_v).m_z+dbg_array(dbg_object(this).m_mat4,12)[dbg_index]*dbg_object(t_v).m_w,dbg_array(dbg_object(this).m_mat4,1)[dbg_index]*dbg_object(t_v).m_x+dbg_array(dbg_object(this).m_mat4,5)[dbg_index]*dbg_object(t_v).m_y+dbg_array(dbg_object(this).m_mat4,9)[dbg_index]*dbg_object(t_v).m_z+dbg_array(dbg_object(this).m_mat4,13)[dbg_index]*dbg_object(t_v).m_w,dbg_array(dbg_object(this).m_mat4,2)[dbg_index]*dbg_object(t_v).m_x+dbg_array(dbg_object(this).m_mat4,6)[dbg_index]*dbg_object(t_v).m_y+dbg_array(dbg_object(this).m_mat4,10)[dbg_index]*dbg_object(t_v).m_z+dbg_array(dbg_object(this).m_mat4,14)[dbg_index]*dbg_object(t_v).m_w,dbg_array(dbg_object(this).m_mat4,3)[dbg_index]*dbg_object(t_v).m_x+dbg_array(dbg_object(this).m_mat4,7)[dbg_index]*dbg_object(t_v).m_y+dbg_array(dbg_object(this).m_mat4,11)[dbg_index]*dbg_object(t_v).m_z+dbg_array(dbg_object(this).m_mat4,15)[dbg_index]*dbg_object(t_v).m_w);
	pop_err();
	return t_;
}
c_prMat4.prototype.p_Times2=function(t_m){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<99>";
	var t_=c_prMat4.m_new2.call(new c_prMat4,dbg_array(dbg_object(this).m_mat4,0)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,0)[dbg_index]+dbg_array(dbg_object(this).m_mat4,4)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,1)[dbg_index]+dbg_array(dbg_object(this).m_mat4,8)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,2)[dbg_index]+dbg_array(dbg_object(this).m_mat4,12)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,3)[dbg_index],dbg_array(dbg_object(this).m_mat4,1)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,0)[dbg_index]+dbg_array(dbg_object(this).m_mat4,5)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,1)[dbg_index]+dbg_array(dbg_object(this).m_mat4,9)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,2)[dbg_index]+dbg_array(dbg_object(this).m_mat4,13)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,3)[dbg_index],dbg_array(dbg_object(this).m_mat4,2)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,0)[dbg_index]+dbg_array(dbg_object(this).m_mat4,6)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,1)[dbg_index]+dbg_array(dbg_object(this).m_mat4,10)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,2)[dbg_index]+dbg_array(dbg_object(this).m_mat4,14)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,3)[dbg_index],dbg_array(dbg_object(this).m_mat4,3)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,0)[dbg_index]+dbg_array(dbg_object(this).m_mat4,7)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,1)[dbg_index]+dbg_array(dbg_object(this).m_mat4,11)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,2)[dbg_index]+dbg_array(dbg_object(this).m_mat4,15)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,3)[dbg_index],dbg_array(dbg_object(this).m_mat4,0)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,4)[dbg_index]+dbg_array(dbg_object(this).m_mat4,4)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,5)[dbg_index]+dbg_array(dbg_object(this).m_mat4,8)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,6)[dbg_index]+dbg_array(dbg_object(this).m_mat4,12)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,7)[dbg_index],dbg_array(dbg_object(this).m_mat4,1)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,4)[dbg_index]+dbg_array(dbg_object(this).m_mat4,5)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,5)[dbg_index]+dbg_array(dbg_object(this).m_mat4,9)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,6)[dbg_index]+dbg_array(dbg_object(this).m_mat4,13)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,7)[dbg_index],dbg_array(dbg_object(this).m_mat4,2)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,4)[dbg_index]+dbg_array(dbg_object(this).m_mat4,6)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,5)[dbg_index]+dbg_array(dbg_object(this).m_mat4,10)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,6)[dbg_index]+dbg_array(dbg_object(this).m_mat4,14)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,7)[dbg_index],dbg_array(dbg_object(this).m_mat4,3)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,4)[dbg_index]+dbg_array(dbg_object(this).m_mat4,7)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,5)[dbg_index]+dbg_array(dbg_object(this).m_mat4,11)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,6)[dbg_index]+dbg_array(dbg_object(this).m_mat4,15)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,7)[dbg_index],dbg_array(dbg_object(this).m_mat4,0)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,8)[dbg_index]+dbg_array(dbg_object(this).m_mat4,4)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,9)[dbg_index]+dbg_array(dbg_object(this).m_mat4,8)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,10)[dbg_index]+dbg_array(dbg_object(this).m_mat4,12)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,11)[dbg_index],dbg_array(dbg_object(this).m_mat4,1)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,8)[dbg_index]+dbg_array(dbg_object(this).m_mat4,5)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,9)[dbg_index]+dbg_array(dbg_object(this).m_mat4,9)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,10)[dbg_index]+dbg_array(dbg_object(this).m_mat4,13)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,11)[dbg_index],dbg_array(dbg_object(this).m_mat4,2)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,8)[dbg_index]+dbg_array(dbg_object(this).m_mat4,6)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,9)[dbg_index]+dbg_array(dbg_object(this).m_mat4,10)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,10)[dbg_index]+dbg_array(dbg_object(this).m_mat4,14)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,11)[dbg_index],dbg_array(dbg_object(this).m_mat4,3)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,8)[dbg_index]+dbg_array(dbg_object(this).m_mat4,7)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,9)[dbg_index]+dbg_array(dbg_object(this).m_mat4,11)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,10)[dbg_index]+dbg_array(dbg_object(this).m_mat4,15)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,11)[dbg_index],dbg_array(dbg_object(this).m_mat4,0)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,12)[dbg_index]+dbg_array(dbg_object(this).m_mat4,4)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,13)[dbg_index]+dbg_array(dbg_object(this).m_mat4,8)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,14)[dbg_index]+dbg_array(dbg_object(this).m_mat4,12)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,15)[dbg_index],dbg_array(dbg_object(this).m_mat4,1)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,12)[dbg_index]+dbg_array(dbg_object(this).m_mat4,5)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,13)[dbg_index]+dbg_array(dbg_object(this).m_mat4,9)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,14)[dbg_index]+dbg_array(dbg_object(this).m_mat4,13)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,15)[dbg_index],dbg_array(dbg_object(this).m_mat4,2)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,12)[dbg_index]+dbg_array(dbg_object(this).m_mat4,6)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,13)[dbg_index]+dbg_array(dbg_object(this).m_mat4,10)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,14)[dbg_index]+dbg_array(dbg_object(this).m_mat4,14)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,15)[dbg_index],dbg_array(dbg_object(this).m_mat4,3)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,12)[dbg_index]+dbg_array(dbg_object(this).m_mat4,7)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,13)[dbg_index]+dbg_array(dbg_object(this).m_mat4,11)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,14)[dbg_index]+dbg_array(dbg_object(this).m_mat4,15)[dbg_index]*dbg_array(dbg_object(t_m).m_mat4,15)[dbg_index]);
	pop_err();
	return t_;
}
c_prMat4.m_Rotation=function(t_rx,t_ry,t_rz){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<79>";
	var t_=c_prMat4.m_Yaw(t_ry).p_Times2(c_prMat4.m_Pitch(t_rx)).p_Times2(c_prMat4.m_Roll(t_rz));
	pop_err();
	return t_;
}
c_prMat4.m_Scale=function(t_sx,t_sy,t_sz){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<83>";
	var t_=c_prMat4.m_new2.call(new c_prMat4,t_sx,0.0,0.0,0.0,0.0,t_sy,0.0,0.0,0.0,0.0,t_sz,0.0,0.0,0.0,0.0,1.0);
	pop_err();
	return t_;
}
c_prMat4.prototype.p_ToArray=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<103>";
	pop_err();
	return this.m_mat4;
}
c_prMat4.prototype.p_ToArray2=function(t_m){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<107>";
	dbg_array(t_m,0)[dbg_index]=dbg_array(dbg_object(this).m_mat4,0)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<107>";
	dbg_array(t_m,1)[dbg_index]=dbg_array(dbg_object(this).m_mat4,1)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<107>";
	dbg_array(t_m,2)[dbg_index]=dbg_array(dbg_object(this).m_mat4,2)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<107>";
	dbg_array(t_m,3)[dbg_index]=dbg_array(dbg_object(this).m_mat4,3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<108>";
	dbg_array(t_m,4)[dbg_index]=dbg_array(dbg_object(this).m_mat4,4)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<108>";
	dbg_array(t_m,5)[dbg_index]=dbg_array(dbg_object(this).m_mat4,5)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<108>";
	dbg_array(t_m,6)[dbg_index]=dbg_array(dbg_object(this).m_mat4,6)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<108>";
	dbg_array(t_m,7)[dbg_index]=dbg_array(dbg_object(this).m_mat4,7)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<109>";
	dbg_array(t_m,8)[dbg_index]=dbg_array(dbg_object(this).m_mat4,8)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<109>";
	dbg_array(t_m,9)[dbg_index]=dbg_array(dbg_object(this).m_mat4,9)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<109>";
	dbg_array(t_m,10)[dbg_index]=dbg_array(dbg_object(this).m_mat4,10)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<109>";
	dbg_array(t_m,11)[dbg_index]=dbg_array(dbg_object(this).m_mat4,11)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<110>";
	dbg_array(t_m,12)[dbg_index]=dbg_array(dbg_object(this).m_mat4,12)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<110>";
	dbg_array(t_m,13)[dbg_index]=dbg_array(dbg_object(this).m_mat4,13)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<110>";
	dbg_array(t_m,14)[dbg_index]=dbg_array(dbg_object(this).m_mat4,14)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/mat4.monkey<110>";
	dbg_array(t_m,15)[dbg_index]=dbg_array(dbg_object(this).m_mat4,15)[dbg_index];
	pop_err();
}
function c_prLayer(){
	c_prEntity3d.call(this);
	this.m__entities=c_Stack4.m_new.call(new c_Stack4);
	this.m__cullings=null;
	this.m__scene=null;
	this.m__lights=c_Stack5.m_new.call(new c_Stack5);
	this.m__tiles=[];
	this.m__layerFogColor=[0.0,0.0,0.0,0.0];
	this.m__layerLightMaskImage=null;
	this.m__drawList=new_object_array(1);
	this.m__entitiesDrawListIndex=0;
	this.m__sorter=null;
	this.m__debugDrawList=null;
	this.implments={c_ILayer:1};
}
c_prLayer.prototype=extend_class(c_prEntity3d);
c_prLayer.prototype.p_Entities=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2100>";
	pop_err();
	return this.m__entities;
}
c_prLayer.prototype.p_CulledEntities=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2073>";
	if((this.m__cullings)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2073>";
		pop_err();
		return this.m__cullings;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2074>";
	pop_err();
	return this.m__entities;
}
c_prLayer.prototype.p_Scene=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2261>";
	pop_err();
	return this.m__scene;
}
c_prLayer.prototype.p_UpdateEntities=function(t_updateCounter){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2420>";
	var t_updates=this.p_CulledEntities();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2422>";
	var t_entityIndex=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2424>";
	while(t_entityIndex<t_updates.p_Length2()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2426>";
		if(t_entityIndex<0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2426>";
			break;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2428>";
		var t_entity=t_updates.p_Get(t_entityIndex);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2430>";
		if(!((t_entity.p_Layer())!=null)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2430>";
			t_entityIndex+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2430>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2432>";
		if(bb_globals_prPause() && t_entity.p_Pausable()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2432>";
			t_entityIndex+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2432>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2434>";
		if(t_entity.p_LastUpdate()==t_updateCounter){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2434>";
			t_entityIndex+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2434>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2436>";
		t_entity.p_LastUpdate2(t_updateCounter);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2438>";
		var t_entities=t_updates.p_Length2();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2439>";
		if(!t_entity.p_Enabled()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2439>";
			t_entity.p_OnDisabled();
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2440>";
		if(t_entity.p_Enabled()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2440>";
			t_entity.p_Update();
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2441>";
		if(t_updates.p_Length2()<t_entities){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2441>";
			t_entityIndex+=t_updates.p_Length2()-t_entities;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2443>";
		t_entityIndex+=1;
	}
	pop_err();
}
c_prLayer.prototype.p_UpdateLights=function(t_updateCounter){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2454>";
	var t_lightIndex=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2456>";
	while(t_lightIndex<this.m__lights.p_Length2()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2458>";
		if(t_lightIndex<0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2458>";
			break;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2460>";
		var t_light=this.m__lights.p_Get(t_lightIndex);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2462>";
		if(!((t_light.p_Layer())!=null)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2462>";
			t_lightIndex+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2462>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2464>";
		if(bb_globals_prPause() && t_light.p_Pausable()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2464>";
			t_lightIndex+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2464>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2466>";
		if(t_light.p_LastUpdate()==t_updateCounter){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2466>";
			t_lightIndex+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2466>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2468>";
		t_light.p_LastUpdate2(t_updateCounter);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2470>";
		var t_lights=this.m__lights.p_Length2();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2471>";
		if(!t_light.p_Enabled()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2471>";
			t_light.p_OnDisabled();
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2472>";
		if(t_light.p_Enabled()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2472>";
			t_light.p_Update();
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2473>";
		if(this.m__lights.p_Length2()<t_lights){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2473>";
			t_lightIndex+=this.m__lights.p_Length2()-t_lights;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2475>";
		t_lightIndex+=1;
	}
	pop_err();
}
c_prLayer.prototype.p__GetTile=function(t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2503>";
	if(!((this.m__scene)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2503>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2504>";
	if(!((dbg_object(this.m__scene).m__tilemap)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2504>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2506>";
	if(dbg_object(dbg_object(this.m__scene).m__tilemap).m__orientation==0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2507>";
		var t_=bb_scenegraph_prGetTileOrtho((t_x),(t_y),(dbg_object(dbg_object(this.m__scene).m__tilemap).m__tileWidth),(dbg_object(dbg_object(this.m__scene).m__tilemap).m__tileHeight),0.0,0.0);
		pop_err();
		return t_;
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2508>";
		if(dbg_object(dbg_object(this.m__scene).m__tilemap).m__orientation==1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2509>";
			var t_2=bb_scenegraph_prGetTileIso((t_x),(t_y),(dbg_object(dbg_object(this.m__scene).m__tilemap).m__tileHeight),(dbg_object(dbg_object(this.m__scene).m__tilemap).m__tileHeight),0.0,0.0);
			pop_err();
			return t_2;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2510>";
			if(dbg_object(dbg_object(this.m__scene).m__tilemap).m__orientation==2){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2511>";
				var t_3=bb_scenegraph_prGetTileStaggered((t_x),(t_y),(dbg_object(dbg_object(this.m__scene).m__tilemap).m__halfTileWidth),(dbg_object(dbg_object(this.m__scene).m__tilemap).m__halfTileHeight),0.0,0.0);
				pop_err();
				return t_3;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2514>";
	pop_err();
	return null;
}
c_prLayer.prototype.p__UpdateTileCulling=function(t_x,t_y,t_width,t_height){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2549>";
	var t_startX=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2550>";
	var t_startY=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2551>";
	var t_endX=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2552>";
	var t_endY=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2554>";
	var t_centerTile=this.p__GetTile((((t_x)+(t_width)*0.5)|0),(((t_y)+(t_height)*0.5)|0));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2555>";
	if(!((t_centerTile)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2557>";
	if(dbg_object(dbg_object(this.m__scene).m__tilemap).m__orientation==0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2559>";
		t_startX=((t_centerTile.p_X()-(t_width)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__tileWidth))|0);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2560>";
		t_startY=((t_centerTile.p_Y()-(t_height)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__tileHeight))|0);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2561>";
		t_endX=((t_centerTile.p_X()+(t_width)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__tileWidth)+2.0)|0);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2562>";
		t_endY=((t_centerTile.p_Y()+(t_height)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__tileHeight)+2.0)|0);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2564>";
		if(dbg_object(dbg_object(this.m__scene).m__tilemap).m__orientation==1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2566>";
			t_startX=((t_centerTile.p_X()-(t_width)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__halfTileHeight))|0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2567>";
			t_startY=((t_centerTile.p_Y()-(t_height)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__halfTileHeight))|0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2568>";
			t_endX=((t_centerTile.p_X()+(t_width)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__halfTileHeight))|0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2569>";
			t_endY=((t_centerTile.p_Y()+(t_height)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__halfTileHeight))|0);
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2571>";
			if(dbg_object(dbg_object(this.m__scene).m__tilemap).m__orientation==2){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2573>";
				t_startX=((t_centerTile.p_X()-(t_width)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__tileWidth)-1.0)|0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2574>";
				t_startY=((t_centerTile.p_Y()-(t_height)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__halfTileHeight)-4.0)|0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2575>";
				t_endX=((t_centerTile.p_X()+(t_width)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__tileWidth)+2.0)|0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2576>";
				t_endY=((t_centerTile.p_Y()+(t_height)*0.5/(dbg_object(dbg_object(this.m__scene).m__tilemap).m__halfTileHeight)+4.0)|0);
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2580>";
	if(t_startX<0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2580>";
		t_startX=0;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2581>";
	if(t_startY<0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2581>";
		t_startY=0;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2583>";
	if(t_endX>dbg_object(dbg_object(this.m__scene).m__tilemap).m__width){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2583>";
		t_endX=dbg_object(dbg_object(this.m__scene).m__tilemap).m__width;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2584>";
	if(t_endY>dbg_object(dbg_object(this.m__scene).m__tilemap).m__height){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2584>";
		t_endY=dbg_object(dbg_object(this.m__scene).m__tilemap).m__height;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2586>";
	for(var t_y2=t_startY;t_y2<t_endY;t_y2=t_y2+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2587>";
		for(var t_x2=t_startX;t_x2<t_endX;t_x2=t_x2+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2588>";
			if((dbg_array(dbg_array(this.m__tiles,t_y2)[dbg_index],t_x2)[dbg_index])!=null){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2589>";
				if(!dbg_array(dbg_array(this.m__tiles,t_y2)[dbg_index],t_x2)[dbg_index].p_Enabled()){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2589>";
					continue;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2590>";
				this.m__cullings.p_Push10(dbg_array(dbg_array(this.m__tiles,t_y2)[dbg_index],t_x2)[dbg_index]);
			}
		}
	}
	pop_err();
}
c_prLayer.prototype.p__UpdateEntityCulling=function(t_x,t_y,t_width,t_height){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2599>";
	for(var t_i=0;t_i<this.m__entities.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2600>";
		var t_e=this.m__entities.p_Get(t_i);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2601>";
		if(t_e.p_X()>=(t_x) && t_e.p_Y()>=(t_y) && t_e.p_X()<=(t_x+t_width) && t_e.p_Y()<=(t_y+t_height)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2602>";
			this.m__cullings.p_Push10(t_e);
		}
	}
	pop_err();
}
c_prLayer.prototype.p__UpdateCulling=function(t_x,t_y,t_width,t_height){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2532>";
	if(!((this.m__scene)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2533>";
	if(!((dbg_object(this.m__scene).m__tilemap)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2535>";
	if(!((this.m__tiles.length)!=0)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2536>";
	if(!((dbg_array(this.m__tiles,0)[dbg_index].length)!=0)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2538>";
	if(this.m__cullings==null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2538>";
		this.m__cullings=c_Stack4.m_new.call(new c_Stack4);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2540>";
	this.m__cullings.p_Clear();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2542>";
	this.p__UpdateTileCulling(t_x,t_y,t_width,t_height);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2543>";
	this.p__UpdateEntityCulling(t_x,t_y,t_width,t_height);
	pop_err();
}
c_prLayer.prototype.p_SetScene=function(t_scene){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2294>";
	if((this.m__scene)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2294>";
		dbg_object(this.m__scene).m__layers.p_RemoveFirst2(this);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2295>";
	this.m__scene=t_scene;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2296>";
	if((this.m__scene)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2296>";
		dbg_object(this.m__scene).m__layers.p_Push7(this);
	}
	pop_err();
}
c_prLayer.m_new=function(t_scene){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2006>";
	c_prEntity3d.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2007>";
	this.p_SetScene(t_scene);
	pop_err();
	return this;
}
c_prLayer.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2004>";
	c_prEntity3d.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2004>";
	pop_err();
	return this;
}
c_prLayer.prototype.p_EnumLayerLights=function(t_lights){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2105>";
	for(var t_i=0;t_i<this.m__lights.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2106>";
		if(!this.m__lights.p_Get(t_i).p_Enabled()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2106>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2107>";
		if(!this.m__lights.p_Get(t_i).p_Visible()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2107>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2108>";
		t_lights.p_Push37(this.m__lights.p_Get(t_i));
	}
	pop_err();
}
c_prLayer.prototype.p_LayerFogColor=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2169>";
	pop_err();
	return this.m__layerFogColor;
}
c_prLayer.prototype.p_LayerLightMaskImage=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2176>";
	pop_err();
	return this.m__layerLightMaskImage;
}
c_prLayer.prototype.p_LayerMatrix=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2183>";
	var t_=this.p_Matrix().p_ToArray();
	pop_err();
	return t_;
}
c_prLayer.prototype.p_OnRenderLayer=function(t_drawLists){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2197>";
	if(dbg_array(this.m__drawList,this.m__entitiesDrawListIndex)[dbg_index]==null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2197>";
		dbg_array(this.m__drawList,this.m__entitiesDrawListIndex)[dbg_index]=c_DrawList.m_new.call(new c_DrawList);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2199>";
	dbg_array(this.m__drawList,this.m__entitiesDrawListIndex)[dbg_index].p_Reset();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2201>";
	var t_renders=this.p_CulledEntities();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2203>";
	if((this.m__sorter)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2203>";
		this.m__sorter.p_Update3(t_renders);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2205>";
	for(var t_i=0;t_i<t_renders.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2206>";
		if(t_renders.p_Get(t_i).p_Layer()==null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2206>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2207>";
		if(t_renders.p_Get(t_i).p_Enabled()==false){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2207>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2208>";
		if(t_renders.p_Get(t_i).p_Visible()==false){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2208>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2209>";
		t_renders.p_Get(t_i).p_Draw(dbg_array(this.m__drawList,this.m__entitiesDrawListIndex)[dbg_index]);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2212>";
	for(var t_i2=0;t_i2<this.m__drawList.length;t_i2=t_i2+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2213>";
		if(dbg_array(this.m__drawList,t_i2)[dbg_index]!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2213>";
			t_drawLists.p_Push34(dbg_array(this.m__drawList,t_i2)[dbg_index]);
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2216>";
	if(this.m__debugDrawList!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2216>";
		t_drawLists.p_Push34(this.m__debugDrawList);
	}
	pop_err();
}
function c_Stack3(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack3.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack3.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack3.m_NIL=null;
c_Stack3.prototype.p_Length=function(t_newlength){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<45>";
	if(t_newlength<this.m_length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<46>";
		for(var t_i=t_newlength;t_i<this.m_length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<47>";
			dbg_array(this.m_data,t_i)[dbg_index]=c_Stack3.m_NIL;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<49>";
		if(t_newlength>this.m_data.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<50>";
			this.m_data=resize_object_array(this.m_data,bb_math_Max(this.m_length*2+10,t_newlength));
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<52>";
	this.m_length=t_newlength;
	pop_err();
}
c_Stack3.prototype.p_Length2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<56>";
	pop_err();
	return this.m_length;
}
c_Stack3.prototype.p_Get=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<104>";
	pop_err();
	return dbg_array(this.m_data,t_index)[dbg_index];
}
c_Stack3.prototype.p_Equals2=function(t_lhs,t_rhs){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<26>";
	var t_=t_lhs==t_rhs;
	pop_err();
	return t_;
}
c_Stack3.prototype.p_Find3=function(t_value,t_start){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<108>";
	for(var t_i=t_start;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<109>";
		if(this.p_Equals2(dbg_array(this.m_data,t_i)[dbg_index],t_value)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<109>";
			pop_err();
			return t_i;
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<111>";
	pop_err();
	return -1;
}
c_Stack3.prototype.p_Remove=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<137>";
	for(var t_i=t_index;t_i<this.m_length-1;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<138>";
		dbg_array(this.m_data,t_i)[dbg_index]=dbg_array(this.m_data,t_i+1)[dbg_index];
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<140>";
	this.m_length-=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<141>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=c_Stack3.m_NIL;
	pop_err();
}
c_Stack3.prototype.p_RemoveFirst2=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<145>";
	var t_i=this.p_Find3(t_value,0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<146>";
	if(t_i!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<146>";
		this.p_Remove(t_i);
	}
	pop_err();
}
c_Stack3.prototype.p_Push7=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack3.prototype.p_Push8=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push7(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack3.prototype.p_Push9=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push8(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
function bb_math_Max(t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<56>";
	if(t_x>t_y){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<56>";
		pop_err();
		return t_x;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<57>";
	pop_err();
	return t_y;
}
function bb_math_Max2(t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<83>";
	if(t_x>t_y){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<83>";
		pop_err();
		return t_x;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<84>";
	pop_err();
	return t_y;
}
function c_prEntity2d(){
	c_prEntity.call(this);
	this.m__scaleX=1.0;
	this.m__scaleY=1.0;
	this.m__x=0.0;
	this.m__y=0.0;
	this.m__width=0.0;
	this.m__height=0.0;
	this.m__z=0.0;
	this.m__rotation=0.0;
	this.m__xhandle=.5;
	this.m__yhandle=.5;
	this.m__blendMode=1;
	this.m__color=[1.0,1.0,1.0,1.0];
	this.m__flippedX=1;
	this.m__flippedY=1;
}
c_prEntity2d.prototype=extend_class(c_prEntity);
c_prEntity2d.prototype.p_SetScaleX=function(t_scaleX){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1395>";
	this.m__scaleX=t_scaleX;
	pop_err();
}
c_prEntity2d.prototype.p_SetScaleY=function(t_scaleY){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1402>";
	this.m__scaleY=t_scaleY;
	pop_err();
}
c_prEntity2d.prototype.p_SetScale=function(t_scale){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1379>";
	this.p_SetScaleX(t_scale);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1380>";
	this.p_SetScaleY(t_scale);
	pop_err();
}
c_prEntity2d.prototype.p_SetScale2=function(t_scaleX,t_scaleY){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1387>";
	this.p_SetScaleX(t_scaleX);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1388>";
	this.p_SetScaleY(t_scaleY);
	pop_err();
}
c_prEntity2d.prototype.p_Update=function(){
	push_err();
	pop_err();
}
c_prEntity2d.prototype.p_X=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1472>";
	pop_err();
	return this.m__x;
}
c_prEntity2d.prototype.p_X2=function(t_x){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1479>";
	this.m__x=t_x;
	pop_err();
}
c_prEntity2d.prototype.p_Y=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1486>";
	pop_err();
	return this.m__y;
}
c_prEntity2d.prototype.p_Y2=function(t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1493>";
	this.m__y=t_y;
	pop_err();
}
c_prEntity2d.prototype.p_Width=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1458>";
	var t_=this.m__width*this.m__scaleX;
	pop_err();
	return t_;
}
c_prEntity2d.prototype.p_Height=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1215>";
	var t_=this.m__height*this.m__scaleY;
	pop_err();
	return t_;
}
c_prEntity2d.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1114>";
	c_prEntity.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1114>";
	pop_err();
	return this;
}
c_prEntity2d.prototype.p_SetPosition=function(t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1355>";
	this.m__x=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1356>";
	this.m__y=t_y;
	pop_err();
}
c_prEntity2d.prototype.p_SetPosition2=function(t_x,t_y,t_z){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1363>";
	this.m__x=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1364>";
	this.m__y=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1365>";
	this.m__z=t_z;
	pop_err();
}
c_prEntity2d.prototype.p_SetRotation=function(t_rotation){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1372>";
	this.m__rotation=t_rotation;
	pop_err();
}
c_prEntity2d.prototype.p_Draw=function(t_drawList){
	push_err();
	pop_err();
}
c_prEntity2d.prototype.p_Width2=function(t_width){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1465>";
	this.m__width=t_width;
	pop_err();
}
c_prEntity2d.prototype.p_Height2=function(t_height){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1222>";
	this.m__height=t_height;
	pop_err();
}
c_prEntity2d.prototype.p_Rotation=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1229>";
	pop_err();
	return this.m__rotation;
}
c_prEntity2d.prototype.p_Rotation2=function(t_rotation){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1236>";
	this.m__rotation=t_rotation;
	pop_err();
}
c_prEntity2d.prototype.p_HandleX=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1204>";
	pop_err();
	return this.m__xhandle;
}
c_prEntity2d.prototype.p_HandleY=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1208>";
	pop_err();
	return this.m__yhandle;
}
function c_prLayerEntity(){
	c_prEntity2d.call(this);
	this.m__group=null;
	this.m__layer=null;
	this.m__orientation=0;
	this.m__selected=false;
	this.m__selectedBlendMode=1;
	this.m__selectedColor=[0.0,0.0,.25,.5];
	this.m__selectedType=0;
}
c_prLayerEntity.prototype=extend_class(c_prEntity2d);
c_prLayerEntity.prototype.p_Group=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2659>";
	pop_err();
	return this.m__group;
}
c_prLayerEntity.prototype.p_SetLayer=function(t_layer){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2768>";
	if(t_layer==null && this.p_Tattooed()){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2770>";
	if(this.m__group!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2770>";
		this.p_Group2(null);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2772>";
	if((this.m__layer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2772>";
		dbg_object(this.m__layer).m__entities.p_RemoveFirst3(this);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2773>";
	this.m__layer=t_layer;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2774>";
	if((this.m__layer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2774>";
		dbg_object(this.m__layer).m__entities.p_Push10(this);
	}
	pop_err();
}
c_prLayerEntity.prototype.p_SetLayer2=function(t_layer,t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2783>";
	if(t_layer==null && this.p_Tattooed()){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2785>";
	if(this.m__group!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2785>";
		this.p_Group2(null);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2787>";
	if(t_index>=dbg_object(this.m__layer).m__entities.p_Length2()){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2789>";
	if((this.m__layer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2789>";
		dbg_object(this.m__layer).m__entities.p_RemoveFirst3(this);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2790>";
	this.m__layer=t_layer;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2791>";
	if((this.m__layer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2791>";
		dbg_object(this.m__layer).m__entities.p_Insert2(t_index,this);
	}
	pop_err();
}
c_prLayerEntity.prototype.p_SetGroup=function(t_group){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2755>";
	if(this.m__layer!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2755>";
		this.p_SetLayer(null);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2757>";
	if((this.m__group)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2757>";
		dbg_object(this.m__group).m__children.p_RemoveEach(this);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2758>";
	this.m__group=t_group;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2759>";
	if((this.m__group)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2759>";
		dbg_object(this.m__group).m__children.p_Push10(this);
	}
	pop_err();
}
c_prLayerEntity.prototype.p_Group2=function(t_group){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2663>";
	this.p_SetGroup(t_group);
	pop_err();
}
c_prLayerEntity.prototype.p_Groups=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2671>";
	var t_group=this.p_Group();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2673>";
	var t_groups=[];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2675>";
	while((t_group)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2676>";
		var t_length=t_groups.length;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2677>";
		t_groups=resize_object_array(t_groups,t_groups.length+1);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2678>";
		dbg_array(t_groups,t_length)[dbg_index]=t_group;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2679>";
		t_group=t_group.p_Group();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2682>";
	pop_err();
	return t_groups;
}
c_prLayerEntity.prototype.p_Layer=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2691>";
	if(this.m__group!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2692>";
		var t_groups=this.p_Groups();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2693>";
		var t_=dbg_object(dbg_array(t_groups,t_groups.length-1)[dbg_index]).m__layer;
		pop_err();
		return t_;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2696>";
	pop_err();
	return this.m__layer;
}
c_prLayerEntity.prototype.p_Layer2=function(t_layer){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2704>";
	this.p_SetLayer(t_layer);
	pop_err();
}
c_prLayerEntity.prototype.p_Update=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2919>";
	if(!((this.p_Layer())!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2920>";
	if(!((this.p_Layer().p_Scene())!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2921>";
	this.p_OnUpdate();
	pop_err();
}
c_prLayerEntity.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2626>";
	c_prEntity2d.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2626>";
	pop_err();
	return this;
}
c_prLayerEntity.prototype.p_DrawSelected=function(t_drawList){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2630>";
	if(t_drawList.p_BlendMode()!=this.m__selectedBlendMode){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2630>";
		t_drawList.p_SetBlendMode(this.m__selectedBlendMode);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2632>";
	t_drawList.p_SetColor2(dbg_array(this.m__selectedColor,0)[dbg_index],dbg_array(this.m__selectedColor,1)[dbg_index],dbg_array(this.m__selectedColor,2)[dbg_index],dbg_array(this.m__selectedColor,3)[dbg_index]);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2636>";
	t_drawList.p_PushMatrix();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2637>";
	t_drawList.p_Translate(this.p_X(),this.p_Y());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2638>";
	t_drawList.p_Rotate(-this.p_Rotation());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2640>";
	var t_1=this.m__selectedType;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2641>";
	if(t_1==0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2642>";
		t_drawList.p_DrawRect(-this.p_Width()*this.p_HandleX(),-this.p_Height()*this.p_HandleY(),this.p_Width(),this.p_Height(),null,0.0,0.0,1.0,1.0);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2643>";
		if(t_1==1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2644>";
			bb_gfx_prDrawRect(t_drawList,-this.p_Width()*this.p_HandleX(),-this.p_Height()*this.p_HandleY(),((this.p_Width())|0),((this.p_Height())|0),1);
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<2647>";
	t_drawList.p_PopMatrix();
	pop_err();
}
function c_Stack4(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack4.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack4.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack4.m_NIL=null;
c_Stack4.prototype.p_Length=function(t_newlength){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<45>";
	if(t_newlength<this.m_length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<46>";
		for(var t_i=t_newlength;t_i<this.m_length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<47>";
			dbg_array(this.m_data,t_i)[dbg_index]=c_Stack4.m_NIL;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<49>";
		if(t_newlength>this.m_data.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<50>";
			this.m_data=resize_object_array(this.m_data,bb_math_Max(this.m_length*2+10,t_newlength));
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<52>";
	this.m_length=t_newlength;
	pop_err();
}
c_Stack4.prototype.p_Length2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<56>";
	pop_err();
	return this.m_length;
}
c_Stack4.prototype.p_Get=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<104>";
	pop_err();
	return dbg_array(this.m_data,t_index)[dbg_index];
}
c_Stack4.prototype.p_Equals3=function(t_lhs,t_rhs){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<26>";
	var t_=t_lhs==t_rhs;
	pop_err();
	return t_;
}
c_Stack4.prototype.p_Find4=function(t_value,t_start){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<108>";
	for(var t_i=t_start;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<109>";
		if(this.p_Equals3(dbg_array(this.m_data,t_i)[dbg_index],t_value)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<109>";
			pop_err();
			return t_i;
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<111>";
	pop_err();
	return -1;
}
c_Stack4.prototype.p_Remove=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<137>";
	for(var t_i=t_index;t_i<this.m_length-1;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<138>";
		dbg_array(this.m_data,t_i)[dbg_index]=dbg_array(this.m_data,t_i+1)[dbg_index];
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<140>";
	this.m_length-=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<141>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=c_Stack4.m_NIL;
	pop_err();
}
c_Stack4.prototype.p_RemoveFirst3=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<145>";
	var t_i=this.p_Find4(t_value,0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<146>";
	if(t_i!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<146>";
		this.p_Remove(t_i);
	}
	pop_err();
}
c_Stack4.prototype.p_Push10=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack4.prototype.p_Push11=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push10(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack4.prototype.p_Push12=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push11(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
c_Stack4.prototype.p_Insert2=function(t_index,t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<126>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<127>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<129>";
	for(var t_i=this.m_length;t_i>t_index;t_i=t_i+-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<130>";
		dbg_array(this.m_data,t_i)[dbg_index]=dbg_array(this.m_data,t_i-1)[dbg_index];
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<132>";
	dbg_array(this.m_data,t_index)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<133>";
	this.m_length+=1;
	pop_err();
}
c_Stack4.prototype.p_RemoveEach=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<155>";
	var t_i=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<155>";
	var t_j=this.m_length;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<156>";
	while(t_i<this.m_length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<157>";
		if(!this.p_Equals3(dbg_array(this.m_data,t_i)[dbg_index],t_value)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<158>";
			t_i+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<159>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<161>";
		var t_b=t_i;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<161>";
		var t_e=t_i+1;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<162>";
		while(t_e<this.m_length && this.p_Equals3(dbg_array(this.m_data,t_e)[dbg_index],t_value)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<163>";
			t_e+=1;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<165>";
		while(t_e<this.m_length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<166>";
			dbg_array(this.m_data,t_b)[dbg_index]=dbg_array(this.m_data,t_e)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<167>";
			t_b+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<168>";
			t_e+=1;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<170>";
		this.m_length-=t_e-t_b;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<171>";
		t_i+=1;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<173>";
	t_i=this.m_length;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<174>";
	while(t_i<t_j){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<175>";
		dbg_array(this.m_data,t_i)[dbg_index]=c_Stack4.m_NIL;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<176>";
		t_i+=1;
	}
	pop_err();
}
c_Stack4.prototype.p_Clear=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<34>";
	for(var t_i=0;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<35>";
		dbg_array(this.m_data,t_i)[dbg_index]=c_Stack4.m_NIL;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<37>";
	this.m_length=0;
	pop_err();
}
function c_prGroup(){
	c_prLayerEntity.call(this);
	this.m__children=c_Stack4.m_new.call(new c_Stack4);
	this.m__cameraX=0.0;
	this.m__cameraY=0.0;
}
c_prGroup.prototype=extend_class(c_prLayerEntity);
c_prGroup.prototype.p_DrawBackground=function(t_drawList){
	push_err();
	pop_err();
}
c_prGroup.prototype.p__DrawChildren=function(t_drawList){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1881>";
	for(var t_i=0;t_i<this.m__children.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1882>";
		this.m__children.p_Get(t_i).p_Draw(t_drawList);
	}
	pop_err();
}
c_prGroup.prototype.p_Draw=function(t_drawList){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1850>";
	t_drawList.p_PushMatrix();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1852>";
	t_drawList.p_Translate(this.m__x,this.m__y);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1854>";
	t_drawList.p_Scale(this.m__scaleX,this.m__scaleY);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1856>";
	t_drawList.p_Translate(-this.p_Width()*this.m__xhandle,-this.p_Height()*this.m__yhandle);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1858>";
	t_drawList.p_Translate(this.p_Width()*this.m__xhandle,this.p_Height()*this.m__yhandle);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1859>";
	t_drawList.p_Rotate(this.m__rotation);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1860>";
	t_drawList.p_Translate(-this.p_Width()*this.m__xhandle,-this.p_Height()*this.m__yhandle);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1862>";
	if(t_drawList.p_BlendMode()!=this.m__blendMode){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1862>";
		t_drawList.p_SetBlendMode(this.m__blendMode);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1865>";
	this.p_DrawBackground(t_drawList);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1867>";
	t_drawList.p_Translate(-this.m__cameraX,-this.m__cameraY);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1869>";
	this.p__DrawChildren(t_drawList);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1871>";
	t_drawList.p_PopMatrix();
	pop_err();
}
function bb_globals_prPause(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/globals.monkey<13>";
	pop_err();
	return bb_globals__pause;
}
function c_prLight(){
	c_prEntity3d.call(this);
	this.m__layer=null;
	this.m__lightColor=[1.0,1.0,1.0,1.0];
	this.m__lightImage=null;
	this.m__lightRange=100.0;
	this.implments={c_ILight:1};
}
c_prLight.prototype=extend_class(c_prEntity3d);
c_prLight.prototype.p_Layer=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3004>";
	pop_err();
	return this.m__layer;
}
c_prLight.prototype.p_SetLayer=function(t_layer){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3077>";
	if(t_layer==null && this.p_Tattooed()){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3079>";
	if((this.m__layer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3079>";
		dbg_object(this.m__layer).m__lights.p_RemoveFirst4(this);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3080>";
	this.m__layer=t_layer;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3081>";
	if((this.m__layer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3081>";
		dbg_object(this.m__layer).m__lights.p_Push13(this);
	}
	pop_err();
}
c_prLight.prototype.p_Layer2=function(t_layer){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3011>";
	this.p_SetLayer(t_layer);
	pop_err();
}
c_prLight.prototype.p_Update=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3139>";
	this.p_OnUpdate();
	pop_err();
}
c_prLight.prototype.p_LightColor=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3018>";
	pop_err();
	return this.m__lightColor;
}
c_prLight.prototype.p_LightImage=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3025>";
	pop_err();
	return this.m__lightImage;
}
c_prLight.prototype.p_LightMatrix=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3032>";
	var t_=this.p_Matrix().p_ToArray();
	pop_err();
	return t_;
}
c_prLight.prototype.p_LightRange=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<3039>";
	pop_err();
	return this.m__lightRange;
}
function c_Stack5(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack5.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack5.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack5.m_NIL=null;
c_Stack5.prototype.p_Length=function(t_newlength){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<45>";
	if(t_newlength<this.m_length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<46>";
		for(var t_i=t_newlength;t_i<this.m_length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<47>";
			dbg_array(this.m_data,t_i)[dbg_index]=c_Stack5.m_NIL;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<49>";
		if(t_newlength>this.m_data.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<50>";
			this.m_data=resize_object_array(this.m_data,bb_math_Max(this.m_length*2+10,t_newlength));
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<52>";
	this.m_length=t_newlength;
	pop_err();
}
c_Stack5.prototype.p_Length2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<56>";
	pop_err();
	return this.m_length;
}
c_Stack5.prototype.p_Get=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<104>";
	pop_err();
	return dbg_array(this.m_data,t_index)[dbg_index];
}
c_Stack5.prototype.p_Equals4=function(t_lhs,t_rhs){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<26>";
	var t_=t_lhs==t_rhs;
	pop_err();
	return t_;
}
c_Stack5.prototype.p_Find5=function(t_value,t_start){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<108>";
	for(var t_i=t_start;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<109>";
		if(this.p_Equals4(dbg_array(this.m_data,t_i)[dbg_index],t_value)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<109>";
			pop_err();
			return t_i;
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<111>";
	pop_err();
	return -1;
}
c_Stack5.prototype.p_Remove=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<137>";
	for(var t_i=t_index;t_i<this.m_length-1;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<138>";
		dbg_array(this.m_data,t_i)[dbg_index]=dbg_array(this.m_data,t_i+1)[dbg_index];
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<140>";
	this.m_length-=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<141>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=c_Stack5.m_NIL;
	pop_err();
}
c_Stack5.prototype.p_RemoveFirst4=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<145>";
	var t_i=this.p_Find5(t_value,0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<146>";
	if(t_i!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<146>";
		this.p_Remove(t_i);
	}
	pop_err();
}
c_Stack5.prototype.p_Push13=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack5.prototype.p_Push14=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push13(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack5.prototype.p_Push15=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push14(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
function c_prVec4(){
	Object.call(this);
	this.m_x=0.0;
	this.m_y=0.0;
	this.m_z=0.0;
	this.m_w=1.0;
}
c_prVec4.m_new=function(t_v){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec4.monkey<7>";
	dbg_object(this).m_x=dbg_array(t_v,0)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec4.monkey<7>";
	dbg_object(this).m_y=dbg_array(t_v,1)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec4.monkey<7>";
	dbg_object(this).m_z=dbg_array(t_v,2)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec4.monkey<7>";
	dbg_object(this).m_w=dbg_array(t_v,3)[dbg_index];
	pop_err();
	return this;
}
c_prVec4.m_new2=function(t_x,t_y,t_z,t_w){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec4.monkey<11>";
	dbg_object(this).m_x=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec4.monkey<11>";
	dbg_object(this).m_y=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec4.monkey<11>";
	dbg_object(this).m_z=t_z;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec4.monkey<11>";
	dbg_object(this).m_w=t_w;
	pop_err();
	return this;
}
c_prVec4.m_new3=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec4.monkey<4>";
	pop_err();
	return this;
}
function c_prTilemap(){
	Object.call(this);
	this.m__orientation=0;
	this.m__tileWidth=0;
	this.m__tileHeight=0;
	this.m__halfTileWidth=0;
	this.m__halfTileHeight=0;
	this.m__width=0;
	this.m__height=0;
}
function c_prTile(){
	c_prLayerEntity.call(this);
	this.m__tileset=null;
	this.m__gid=new_number_array(1);
	this.m__frame=0;
	this.m__frameTimer=0;
	this.m__duration=new_number_array(1);
}
c_prTile.prototype=extend_class(c_prLayerEntity);
c_prTile.prototype.p_Height=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4585>";
	var t_=(dbg_array(dbg_object(this.m__tileset).m__tilesetTile,dbg_array(this.m__gid,0)[dbg_index])[dbg_index].p_Height())*this.m__scaleY;
	pop_err();
	return t_;
}
c_prTile.prototype.p_Draw=function(t_drawList){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4462>";
	var t_tilesetTile=dbg_array(dbg_object(this.m__tileset).m__tilesetTile,dbg_array(this.m__gid,this.m__frame)[dbg_index])[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4464>";
	if(!((t_tilesetTile)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4466>";
	if(t_drawList.p_BlendMode()!=this.m__blendMode){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4466>";
		t_drawList.p_SetBlendMode(this.m__blendMode);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4468>";
	t_drawList.p_SetColor2(dbg_array(this.m__color,0)[dbg_index],dbg_array(this.m__color,1)[dbg_index],dbg_array(this.m__color,2)[dbg_index],dbg_array(this.m__color,3)[dbg_index]);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4470>";
	var t_padding=((dbg_object(t_tilesetTile).m__padding)?1:0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4471>";
	var t_x=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4472>";
	var t_y=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4474>";
	if(this.m__orientation==1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4475>";
		t_x=bb_scenegraph_prOrthoToIsoX(this.m__x,this.m__y)-(dbg_object(t_tilesetTile).m__width)*dbg_object(t_tilesetTile).m__image.p_HandleX()*this.m__scaleX;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4476>";
		t_y=bb_scenegraph_prOrthoToIsoY(this.m__x,this.m__y)-(dbg_object(t_tilesetTile).m__height)*dbg_object(t_tilesetTile).m__image.p_HandleY()*this.m__scaleY;
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4478>";
		t_x=this.m__x-(dbg_object(t_tilesetTile).m__width)*dbg_object(t_tilesetTile).m__image.p_HandleX()*this.m__scaleX;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4479>";
		t_y=this.m__y-(dbg_object(t_tilesetTile).m__height)*dbg_object(t_tilesetTile).m__image.p_HandleY()*this.m__scaleY;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4482>";
	if(this.m__rotation!=0.0 || this.m__scaleX!=1.0 || this.m__scaleY!=1.0 || this.m__flippedX==-1 || this.m__flippedY==-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4484>";
		if(this.m__scaleX!=1.0 || this.m__scaleY!=1.0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4486>";
			t_drawList.p_PushMatrix();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4488>";
			t_drawList.p_Translate(t_x,t_y+this.p_Height());
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4489>";
			t_drawList.p_Rotate(-this.m__rotation);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4490>";
			t_drawList.p_Translate(0.0,-this.p_Height());
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4491>";
			t_drawList.p_Scale(this.m__scaleX*(this.m__flippedX),this.m__scaleY*(this.m__flippedY));
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4493>";
			if(this.m__flippedX==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4493>";
				t_drawList.p_Translate((-dbg_object(t_tilesetTile).m__width),0.0);
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4494>";
			if(this.m__flippedY==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4494>";
				t_drawList.p_Translate(0.0,(-dbg_object(t_tilesetTile).m__height));
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4496>";
			t_drawList.p_DrawRect3(0.0,0.0,dbg_object(t_tilesetTile).m__image,t_padding,t_padding,dbg_object(t_tilesetTile).m__width,dbg_object(t_tilesetTile).m__height);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4498>";
			t_drawList.p_PopMatrix();
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4502>";
			t_drawList.p_PushMatrix();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4504>";
			t_drawList.p_Translate(t_x+(dbg_object(t_tilesetTile).m__width)*.5,t_y+(dbg_object(t_tilesetTile).m__height)*.5);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4505>";
			t_drawList.p_Rotate(-this.m__rotation);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4506>";
			t_drawList.p_Translate((-dbg_object(t_tilesetTile).m__width)*.5,(-dbg_object(t_tilesetTile).m__height)*.5);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4507>";
			t_drawList.p_Scale((this.m__flippedX),(this.m__flippedY));
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4509>";
			if(this.m__flippedX==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4509>";
				t_drawList.p_Translate((-dbg_object(t_tilesetTile).m__width),0.0);
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4510>";
			if(this.m__flippedY==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4510>";
				t_drawList.p_Translate(0.0,(-dbg_object(t_tilesetTile).m__height));
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4512>";
			t_drawList.p_DrawRect3(0.0,0.0,dbg_object(t_tilesetTile).m__image,t_padding,t_padding,dbg_object(t_tilesetTile).m__width,dbg_object(t_tilesetTile).m__height);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4514>";
			t_drawList.p_PopMatrix();
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4520>";
		if(t_padding==1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4521>";
			t_drawList.p_DrawRect3(t_x,t_y,dbg_object(t_tilesetTile).m__image,t_padding,t_padding,dbg_object(t_tilesetTile).m__width,dbg_object(t_tilesetTile).m__height);
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4523>";
			t_drawList.p_DrawImage4(dbg_object(t_tilesetTile).m__image,t_x+(dbg_object(t_tilesetTile).m__width)*dbg_object(t_tilesetTile).m__image.p_HandleX(),t_y+(dbg_object(t_tilesetTile).m__height)*dbg_object(t_tilesetTile).m__image.p_HandleY());
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4528>";
	if(this.m__selected){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4528>";
		this.p_DrawSelected(t_drawList);
	}
	pop_err();
}
c_prTile.prototype.p_Update=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4795>";
	if(!((this.p_Layer())!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4796>";
	if(!((this.p_Layer().p_Scene())!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4798>";
	if(this.m__gid.length>1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4799>";
		if(this.m__frameTimer==0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4799>";
			this.m__frameTimer=bb_app_Millisecs();
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4800>";
		if(bb_app_Millisecs()>=this.m__frameTimer+dbg_array(this.m__duration,this.m__frame)[dbg_index]){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4801>";
			this.m__frame+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4802>";
			if(this.m__frame>=this.m__gid.length){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4802>";
				this.m__frame=0;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4803>";
			this.m__frameTimer=bb_app_Millisecs();
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4807>";
	this.p_OnUpdate();
	pop_err();
}
c_prTile.prototype.p_Width=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4829>";
	var t_=(dbg_array(dbg_object(this.m__tileset).m__tilesetTile,dbg_array(this.m__gid,0)[dbg_index])[dbg_index].p_Width())*this.m__scaleX;
	pop_err();
	return t_;
}
function c_prVec2(){
	Object.call(this);
	this.m__x=0.0;
	this.m__y=0.0;
}
c_prVec2.m_new=function(t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec2.monkey<15>";
	this.m__x=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec2.monkey<16>";
	this.m__y=t_y;
	pop_err();
	return this;
}
c_prVec2.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec2.monkey<9>";
	pop_err();
	return this;
}
c_prVec2.prototype.p_X2=function(t_x){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec2.monkey<248>";
	this.m__x=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec2.monkey<249>";
	pop_err();
	return this;
}
c_prVec2.prototype.p_X=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec2.monkey<253>";
	pop_err();
	return this.m__x;
}
c_prVec2.prototype.p_Y2=function(t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec2.monkey<257>";
	this.m__y=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec2.monkey<258>";
	pop_err();
	return this;
}
c_prVec2.prototype.p_Y=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/vec2.monkey<262>";
	pop_err();
	return this.m__y;
}
function bb_scenegraph_prGetTileOrtho(t_pointX,t_pointY,t_tileWidth,t_tileHeight,t_offsetX,t_offsetY){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<202>";
	var t_=c_prVec2.m_new.call(new c_prVec2,Math.floor(t_pointX/t_tileWidth-t_offsetX),Math.floor(t_pointY/t_tileHeight-t_offsetY));
	pop_err();
	return t_;
}
function bb_scenegraph_prGetTileOrtho2(t_point,t_tileWidth,t_tileHeight,t_offsetX,t_offsetY){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<206>";
	var t_=bb_scenegraph_prGetTileOrtho(t_point.p_X(),t_point.p_Y(),t_tileWidth,t_tileHeight,t_offsetX,t_offsetY);
	pop_err();
	return t_;
}
function bb_scenegraph_prIsoToOrtho(t_pointX,t_pointY){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<218>";
	var t_=c_prVec2.m_new.call(new c_prVec2,(2.0*t_pointY+t_pointX)/2.0,(2.0*t_pointY-t_pointX)/2.0);
	pop_err();
	return t_;
}
function bb_scenegraph_prIsoToOrtho2(t_point){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<222>";
	var t_=bb_scenegraph_prIsoToOrtho(t_point.p_X(),t_point.p_Y());
	pop_err();
	return t_;
}
function bb_scenegraph_prGetTileIso(t_pointX,t_pointY,t_tileWidth,t_tileHeight,t_offsetX,t_offsetY){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<210>";
	var t_=bb_scenegraph_prGetTileOrtho2(bb_scenegraph_prIsoToOrtho(t_pointX-t_tileWidth,t_pointY),t_tileWidth,t_tileHeight,t_offsetX,t_offsetY);
	pop_err();
	return t_;
}
function bb_scenegraph_prGetTileIso2(t_point,t_tileWidth,t_tileHeight,t_offsetX,t_offsetY){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<214>";
	var t_=bb_scenegraph_prGetTileIso(t_point.p_X(),t_point.p_Y(),t_tileWidth,t_tileHeight,t_offsetX,t_offsetY);
	pop_err();
	return t_;
}
function bb_scenegraph_prGetPositionStaggered(t_ax,t_ay,t_bx,t_by,t_cx,t_cy){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<179>";
	var t_slope=(t_by-t_ay)/(t_bx-t_ax);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<180>";
	var t_yIntercept=t_ay-t_ax*t_slope;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<181>";
	var t_solution=t_slope*t_cx+t_yIntercept;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<183>";
	if(t_slope!=0.0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<185>";
		if(t_cy>t_solution){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<186>";
			if(t_bx>t_ax){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<186>";
				pop_err();
				return 1;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<187>";
			pop_err();
			return -1;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<190>";
		if(t_cy<t_solution){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<191>";
			if(t_bx>t_ax){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<191>";
				pop_err();
				return -1;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<192>";
			pop_err();
			return 1;
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<197>";
	pop_err();
	return 0;
}
function bb_scenegraph_prGetTileStaggered(t_pointX,t_pointY,t_halfTileWidth,t_tileHeight,t_offsetX,t_offsetY){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<135>";
	var t_ax=.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<136>";
	var t_ay=.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<137>";
	var t_bx=.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<138>";
	var t_by=.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<140>";
	var t_posX=(((t_pointX/t_halfTileWidth)|0)>>0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<141>";
	var t_posY=(((t_pointY/t_tileHeight)|0)>>0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<143>";
	if(t_posX % 2.0==t_posY % 2.0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<145>";
		t_ax=t_posX*t_halfTileWidth;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<146>";
		t_ay=(t_posY+1.0)*t_tileHeight;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<147>";
		t_bx=(t_posX+1.0)*t_halfTileWidth;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<148>";
		t_by=t_posY*t_tileHeight;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<150>";
		if(bb_scenegraph_prGetPositionStaggered(t_ax,t_ay,t_bx,t_by,t_pointX,t_pointY)<0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<151>";
			var t_ttest=(((((t_posY/1.0)|0)>>0)-1) % 2);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<152>";
			if(t_ttest!=0.0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<152>";
				t_ttest=-1.0;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<153>";
			var t_=c_prVec2.m_new.call(new c_prVec2,(((t_posX/2.0)|0)>>0)+t_ttest,((((t_posY/1.0)|0)>>0)-1));
			pop_err();
			return t_;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<155>";
			var t_2=c_prVec2.m_new.call(new c_prVec2,(((t_posX/2.0)|0)>>0),(((t_posY/1.0)|0)>>0));
			pop_err();
			return t_2;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<160>";
		t_ax=t_posX*t_halfTileWidth;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<161>";
		t_ay=t_posY*t_tileHeight;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<162>";
		t_bx=(t_posX+1.0)*t_halfTileWidth;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<163>";
		t_by=(t_posY+1.0)*t_tileHeight;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<165>";
		if(bb_scenegraph_prGetPositionStaggered(t_ax,t_ay,t_bx,t_by,t_pointX,t_pointY)<0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<166>";
			var t_3=c_prVec2.m_new.call(new c_prVec2,(((t_posX/2.0)|0)>>0),((((t_posY/1.0)|0)>>0)-1));
			pop_err();
			return t_3;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<168>";
			var t_ttest2=((((t_posY/1.0)|0)>>0)-1) % 2;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<169>";
			if(t_ttest2==0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<169>";
				t_ttest2=-1;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<169>";
				t_ttest2=0;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<170>";
			var t_4=c_prVec2.m_new.call(new c_prVec2,((((t_posX/2.0)|0)>>0)+t_ttest2),(((t_posY/1.0)|0)>>0));
			pop_err();
			return t_4;
		}
	}
}
function c_prCollisionMonitor(){
	Object.call(this);
	this.m__readers=c_Stack6.m_new.call(new c_Stack6);
	this.m__writers=c_Stack6.m_new.call(new c_Stack6);
}
c_prCollisionMonitor.prototype.p_Update=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<677>";
	var t_r=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<679>";
	while(t_r<this.m__readers.p_Length2()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<681>";
		if(t_r<0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<681>";
			break;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<683>";
		if(bb_globals_prPause() && this.m__readers.p_Get(t_r).p_Pausable()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<683>";
			t_r+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<683>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<685>";
		if(this.m__readers.p_Get(t_r).p_Enabled()==false){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<685>";
			t_r+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<685>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<687>";
		var t_w=0;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<689>";
		while(t_w<this.m__writers.p_Length2()){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<691>";
			if(t_r<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<691>";
				break;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<693>";
			if((this.m__readers.p_Get(t_r))!=null){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<695>";
				if(t_w<0){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<695>";
					t_w+=1;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<695>";
					continue;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<697>";
				if(bb_globals_prPause() && this.m__readers.p_Get(t_r).p_Pausable()){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<697>";
					t_r+=1;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<697>";
					continue;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<698>";
				if(bb_globals_prPause() && this.m__writers.p_Get(t_w).p_Pausable()){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<698>";
					t_w+=1;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<698>";
					continue;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<700>";
				if(this.m__readers.p_Get(t_r).p_Enabled()==false){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<700>";
					t_r+=1;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<700>";
					continue;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<701>";
				if(this.m__writers.p_Get(t_w).p_Enabled()==false){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<701>";
					t_w+=1;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<701>";
					continue;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<703>";
				if(this.m__readers.p_Get(t_r)==this.m__writers.p_Get(t_w)){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<703>";
					t_w+=1;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<703>";
					continue;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<705>";
				if(dbg_object(this.m__readers.p_Get(t_r)).m__scoreCollector==this.m__writers.p_Get(t_w)){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<705>";
					t_w+=1;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<705>";
					continue;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<707>";
				var t_readers=this.m__readers.p_Length2();
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<708>";
				var t_writers=this.m__writers.p_Length2();
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<710>";
				if(this.m__readers.p_Get(t_r).p_CollisionMethod(this.m__writers.p_Get(t_w))){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<710>";
					this.m__readers.p_Get(t_r).p_OnCollision(this.m__writers.p_Get(t_w));
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<712>";
				if(this.m__readers.p_Length2()<t_readers){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<712>";
					t_r+=this.m__readers.p_Length2()-t_readers;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<713>";
				if(this.m__writers.p_Length2()<t_writers){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<713>";
					t_w+=this.m__writers.p_Length2()-t_writers;
				}
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<717>";
			t_w+=1;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<721>";
		t_r+=1;
	}
	pop_err();
}
function c_prSprite(){
	c_prLayerEntity.call(this);
	this.m__image=new_object_array(1);
	this.m__frame=.0;
}
c_prSprite.prototype=extend_class(c_prLayerEntity);
c_prSprite.prototype.p_Image=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4363>";
	var t_=dbg_array(this.m__image,((this.m__frame % (this.m__image.length))|0))[dbg_index];
	pop_err();
	return t_;
}
c_prSprite.prototype.p_Width=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4422>";
	if((this.p_Image())!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4422>";
		var t_=(this.p_Image().p_Width())*this.m__scaleX;
		pop_err();
		return t_;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4423>";
	pop_err();
	return 0.0;
}
c_prSprite.prototype.p_Height=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4354>";
	if((this.p_Image())!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4354>";
		var t_=(this.p_Image().p_Height())*this.m__scaleY;
		pop_err();
		return t_;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4355>";
	pop_err();
	return 0.0;
}
c_prSprite.prototype.p_SetImage=function(t_image,t_frame){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4402>";
	if(t_image==null){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4404>";
	if(t_frame>this.m__image.length-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4404>";
		this.m__image=resize_object_array(this.m__image,t_frame+1);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4406>";
	dbg_array(this.m__image,t_frame)[dbg_index]=t_image;
	pop_err();
}
c_prSprite.prototype.p_SetImage2=function(t_image){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4414>";
	if(t_image.length==0){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4415>";
	this.m__image=t_image;
	pop_err();
}
c_prSprite.m_new=function(t_layer,t_image){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4307>";
	c_prLayerEntity.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4308>";
	this.p_SetLayer(t_layer);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4309>";
	this.p_SetImage(t_image,0);
	pop_err();
	return this;
}
c_prSprite.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4302>";
	c_prLayerEntity.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4302>";
	pop_err();
	return this;
}
c_prSprite.prototype.p_Draw=function(t_drawList){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4317>";
	if(this.p_Image()==null){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4319>";
	if(t_drawList.p_BlendMode()!=this.m__blendMode){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4319>";
		t_drawList.p_SetBlendMode(this.m__blendMode);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4321>";
	t_drawList.p_SetColor2(dbg_array(this.m__color,0)[dbg_index],dbg_array(this.m__color,1)[dbg_index],dbg_array(this.m__color,2)[dbg_index],dbg_array(this.m__color,3)[dbg_index]);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4323>";
	bb_gfx_prDrawImage(t_drawList,this.p_Image(),this.m__x,this.m__y,-this.m__rotation,this.m__scaleX*(this.m__flippedX),this.m__scaleY*(this.m__flippedY),this.m__xhandle,this.m__yhandle,false);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4325>";
	if(this.m__selected){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<4325>";
		this.p_DrawSelected(t_drawList);
	}
	pop_err();
}
function c_prGameSprite(){
	c_prSprite.call(this);
	this.m__scoreCollector=null;
	this.m__collisionRead=null;
	this.m__collisionWrite=null;
}
c_prGameSprite.prototype=extend_class(c_prSprite);
c_prGameSprite.prototype.p_CollisionMethod=function(t_gameSprite){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1628>";
	var t_=c_prCollision.m_Rectangles(this.p_X(),this.p_Y(),((this.p_Width())|0),((this.p_Height())|0),t_gameSprite.p_X(),t_gameSprite.p_Y(),((t_gameSprite.p_Width())|0),((t_gameSprite.p_Height())|0),1);
	pop_err();
	return t_;
}
c_prGameSprite.prototype.p_OnCollision=function(t_gameSprite){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1649>";
	pop_err();
	return 0;
}
c_prGameSprite.prototype.p_SetCollisionRead=function(t_collisionRead){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1678>";
	if(!t_collisionRead && ((this.m__collisionRead)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1679>";
		dbg_object(this.m__collisionRead).m__readers.p_RemoveFirst5(this);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1680>";
		this.m__collisionRead=null;
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1684>";
	if(!((this.m__layer)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1685>";
	if(!((dbg_object(this.m__layer).m__scene)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1686>";
	if(!((dbg_object(dbg_object(this.m__layer).m__scene).m__collisionMonitor)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1688>";
	if(t_collisionRead && dbg_object(dbg_object(this.m__layer).m__scene).m__collisionMonitor!=this.m__collisionRead){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1689>";
		dbg_object(dbg_object(dbg_object(this.m__layer).m__scene).m__collisionMonitor).m__readers.p_Push16(this);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1690>";
		this.m__collisionRead=dbg_object(dbg_object(this.m__layer).m__scene).m__collisionMonitor;
	}
	pop_err();
}
c_prGameSprite.prototype.p_SetCollisionWrite=function(t_collisionWrite){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1700>";
	if(!t_collisionWrite && ((this.m__collisionWrite)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1701>";
		dbg_object(this.m__collisionWrite).m__writers.p_RemoveFirst5(this);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1702>";
		this.m__collisionWrite=null;
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1706>";
	if(!((this.m__layer)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1707>";
	if(!((dbg_object(this.m__layer).m__scene)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1708>";
	if(!((dbg_object(dbg_object(this.m__layer).m__scene).m__collisionMonitor)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1710>";
	if(t_collisionWrite && dbg_object(dbg_object(this.m__layer).m__scene).m__collisionMonitor!=this.m__collisionWrite){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1711>";
		dbg_object(dbg_object(dbg_object(this.m__layer).m__scene).m__collisionMonitor).m__writers.p_Push16(this);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1712>";
		this.m__collisionWrite=dbg_object(dbg_object(this.m__layer).m__scene).m__collisionMonitor;
	}
	pop_err();
}
c_prGameSprite.prototype.p_SetLayer=function(t_layer){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1721>";
	if(t_layer==null && this.p_Tattooed()){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1723>";
	if(t_layer==null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1724>";
		this.p_SetCollisionRead(false);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1725>";
		this.p_SetCollisionWrite(false);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1728>";
	if((this.m__layer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1728>";
		dbg_object(this.m__layer).m__entities.p_RemoveFirst3(this);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1729>";
	this.m__layer=t_layer;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1730>";
	if((this.m__layer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1730>";
		dbg_object(this.m__layer).m__entities.p_Push10(this);
	}
	pop_err();
}
c_prGameSprite.prototype.p_SetLayer2=function(t_layer,t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1736>";
	if(t_layer==null && this.p_Tattooed()){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1738>";
	if(t_layer==null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1739>";
		this.p_SetCollisionRead(false);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1740>";
		this.p_SetCollisionWrite(false);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1743>";
	if((this.m__layer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1743>";
		dbg_object(this.m__layer).m__entities.p_RemoveFirst3(this);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1744>";
	this.m__layer=t_layer;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1745>";
	if((this.m__layer)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<1745>";
		dbg_object(this.m__layer).m__entities.p_Insert2(t_index,(this));
	}
	pop_err();
}
function c_Stack6(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack6.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack6.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack6.m_NIL=null;
c_Stack6.prototype.p_Length=function(t_newlength){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<45>";
	if(t_newlength<this.m_length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<46>";
		for(var t_i=t_newlength;t_i<this.m_length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<47>";
			dbg_array(this.m_data,t_i)[dbg_index]=c_Stack6.m_NIL;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<49>";
		if(t_newlength>this.m_data.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<50>";
			this.m_data=resize_object_array(this.m_data,bb_math_Max(this.m_length*2+10,t_newlength));
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<52>";
	this.m_length=t_newlength;
	pop_err();
}
c_Stack6.prototype.p_Length2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<56>";
	pop_err();
	return this.m_length;
}
c_Stack6.prototype.p_Get=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<104>";
	pop_err();
	return dbg_array(this.m_data,t_index)[dbg_index];
}
c_Stack6.prototype.p_Equals5=function(t_lhs,t_rhs){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<26>";
	var t_=t_lhs==t_rhs;
	pop_err();
	return t_;
}
c_Stack6.prototype.p_Find6=function(t_value,t_start){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<108>";
	for(var t_i=t_start;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<109>";
		if(this.p_Equals5(dbg_array(this.m_data,t_i)[dbg_index],t_value)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<109>";
			pop_err();
			return t_i;
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<111>";
	pop_err();
	return -1;
}
c_Stack6.prototype.p_Remove=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<137>";
	for(var t_i=t_index;t_i<this.m_length-1;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<138>";
		dbg_array(this.m_data,t_i)[dbg_index]=dbg_array(this.m_data,t_i+1)[dbg_index];
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<140>";
	this.m_length-=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<141>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=c_Stack6.m_NIL;
	pop_err();
}
c_Stack6.prototype.p_RemoveFirst5=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<145>";
	var t_i=this.p_Find6(t_value,0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<146>";
	if(t_i!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<146>";
		this.p_Remove(t_i);
	}
	pop_err();
}
c_Stack6.prototype.p_Push16=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack6.prototype.p_Push17=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push16(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack6.prototype.p_Push18=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push17(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
function c_Image2(){
	Object.call(this);
	this.m__width=0;
	this.m__height=0;
	this.m__material=null;
	this.m__x0=-1.0;
	this.m__x1=1.0;
	this.m__y0=-1.0;
	this.m__y1=1.0;
	this.m__x=0;
	this.m__s0=0.0;
	this.m__y=0;
	this.m__t0=0.0;
	this.m__s1=1.0;
	this.m__t1=1.0;
	this.m__caster=null;
}
c_Image2.prototype.p_Width=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1136>";
	pop_err();
	return this.m__width;
}
c_Image2.prototype.p_Height=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1140>";
	pop_err();
	return this.m__height;
}
c_Image2.m__flagsMask=0;
c_Image2.prototype.p_SetHandle=function(t_xhandle,t_yhandle){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1156>";
	this.m__x0=(this.m__width)*-t_xhandle;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1157>";
	this.m__x1=(this.m__width)*(1.0-t_xhandle);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1158>";
	this.m__y0=(this.m__height)*-t_yhandle;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1159>";
	this.m__y1=(this.m__height)*(1.0-t_yhandle);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1160>";
	this.m__s0=(this.m__x)/(this.m__material.p_Width());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1161>";
	this.m__t0=(this.m__y)/(this.m__material.p_Height());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1162>";
	this.m__s1=(this.m__x+this.m__width)/(this.m__material.p_Width());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1163>";
	this.m__t1=(this.m__y+this.m__height)/(this.m__material.p_Height());
	pop_err();
}
c_Image2.m_new=function(t_width,t_height,t_xhandle,t_yhandle,t_flags){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1068>";
	t_flags&=c_Image2.m__flagsMask;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1069>";
	var t_texture=c_Texture.m_new.call(new c_Texture,t_width,t_height,4,t_flags|12|16);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1070>";
	this.m__material=c_Material.m_new.call(new c_Material,bb_graphics2_fastShader);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1071>";
	this.m__material.p_SetTexture("ColorTexture",t_texture);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1072>";
	t_texture.p_Release();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1073>";
	this.m__width=t_width;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1074>";
	this.m__height=t_height;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1075>";
	this.p_SetHandle(t_xhandle,t_yhandle);
	pop_err();
	return this;
}
c_Image2.m_new2=function(t_image,t_x,t_y,t_width,t_height,t_xhandle,t_yhandle){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1079>";
	this.m__material=dbg_object(t_image).m__material;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1080>";
	this.m__material.p_Retain();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1081>";
	this.m__x=dbg_object(t_image).m__x+t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1082>";
	this.m__y=dbg_object(t_image).m__y+t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1083>";
	this.m__width=t_width;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1084>";
	this.m__height=t_height;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1085>";
	this.p_SetHandle(t_xhandle,t_yhandle);
	pop_err();
	return this;
}
c_Image2.m_new3=function(t_material,t_xhandle,t_yhandle){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1089>";
	var t_texture=t_material.p_ColorTexture();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1090>";
	if(!((t_texture)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1090>";
		error("Material has no ColorTexture");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1091>";
	this.m__material=t_material;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1092>";
	this.m__material.p_Retain();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1093>";
	this.m__width=this.m__material.p_Width();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1094>";
	this.m__height=this.m__material.p_Height();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1095>";
	this.p_SetHandle(t_xhandle,t_yhandle);
	pop_err();
	return this;
}
c_Image2.m_new4=function(t_material,t_x,t_y,t_width,t_height,t_xhandle,t_yhandle){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1099>";
	var t_texture=t_material.p_ColorTexture();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1100>";
	if(!((t_texture)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1100>";
		error("Material has no ColorTexture");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1101>";
	this.m__material=t_material;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1102>";
	this.m__material.p_Retain();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1103>";
	this.m__x=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1104>";
	this.m__y=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1105>";
	this.m__width=t_width;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1106>";
	this.m__height=t_height;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1107>";
	this.p_SetHandle(t_xhandle,t_yhandle);
	pop_err();
	return this;
}
c_Image2.m_new5=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1061>";
	pop_err();
	return this;
}
c_Image2.m_Load=function(t_path,t_xhandle,t_yhandle,t_flags,t_shader){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1191>";
	t_flags&=c_Image2.m__flagsMask;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1193>";
	var t_material=c_Material.m_Load(t_path,t_flags|12,t_shader);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1194>";
	if(!((t_material)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1194>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1196>";
	var t_=c_Image2.m_new3.call(new c_Image2,t_material,t_xhandle,t_yhandle);
	pop_err();
	return t_;
}
c_Image2.prototype.p_Material=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1116>";
	pop_err();
	return this.m__material;
}
c_Image2.prototype.p_Discard=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1111>";
	if((this.m__material)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1111>";
		this.m__material.p_Release();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1112>";
	this.m__material=null;
	pop_err();
}
c_Image2.prototype.p_HandleX=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1144>";
	var t_=-this.m__x0/(this.m__x1-this.m__x0);
	pop_err();
	return t_;
}
c_Image2.prototype.p_HandleY=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1148>";
	var t_=-this.m__y0/(this.m__y1-this.m__y0);
	pop_err();
	return t_;
}
function c_prCollision(){
	Object.call(this);
}
c_prCollision.m_Rectangles=function(t_x1,t_y1,t_width1,t_height1,t_x2,t_y2,t_width2,t_height2,t_centerRect){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<123>";
	if((t_centerRect)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<124>";
		t_x1=t_x1-(t_width1)*.5;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<125>";
		t_y1=t_y1-(t_height1)*.5;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<126>";
		t_x2=t_x2-(t_width2)*.5;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<127>";
		t_y2=t_y2-(t_height2)*.5;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<130>";
	if(t_x1+(t_width1)<=t_x2){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<130>";
		pop_err();
		return false;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<131>";
	if(t_y1+(t_height1)<=t_y2){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<131>";
		pop_err();
		return false;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<133>";
	if(t_x1>=t_x2+(t_width2)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<133>";
		pop_err();
		return false;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<134>";
	if(t_y1>=t_y2+(t_height2)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<134>";
		pop_err();
		return false;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/math/collision.monkey<136>";
	pop_err();
	return true;
}
function c_Renderer(){
	Object.call(this);
	this.m__canvas=null;
	this.m__fudgeList=null;
	this.m__projectionMatrix=bb_math3d_Mat4New();
	this.m__image=null;
	this.m__viewport=[0,0,640,480];
	this.m__clearColor=[0.0,0.0,0.0,1.0];
	this.m__ambientLight=[1.0,1.0,1.0,1.0];
	this.m__cameraMatrix=bb_math3d_Mat4New();
	this.m__layers=c_Stack11.m_new.call(new c_Stack11);
	this.m__timage=null;
	this.m__timage2=null;
	this.m__viewMatrix=bb_math3d_Mat4New();
	this.m__clearMode=1;
	this.m__invLayerMatrix=new_number_array(16);
	this.m__drawLists=c_Stack12.m_new.call(new c_Stack12);
	this.m__invProjMatrix=new_number_array(16);
	this.m__ptl=new_number_array(4);
	this.m__pbr=new_number_array(4);
}
c_Renderer.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<45>";
	this.m__canvas=c_Canvas.m_new.call(new c_Canvas,null);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<48>";
	this.m__fudgeList=c_DrawList.m_new.call(new c_DrawList);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<49>";
	this.m__fudgeList.p_DrawRect(0.0,0.0,0.0,0.0,null,0.0,0.0,1.0,1.0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<51>";
	bb_math3d_Mat4Ortho(0.0,(this.m__canvas.p_Width()),0.0,(this.m__canvas.p_Height()),-1.0,1.0,this.m__projectionMatrix);
	pop_err();
	return this;
}
c_Renderer.prototype.p_SetRenderTarget=function(t_image){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<55>";
	this.m__image=t_image;
	pop_err();
}
c_Renderer.prototype.p_SetViewport=function(t_x,t_y,t_width,t_height){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<59>";
	dbg_array(this.m__viewport,0)[dbg_index]=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<60>";
	dbg_array(this.m__viewport,1)[dbg_index]=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<61>";
	dbg_array(this.m__viewport,2)[dbg_index]=t_width;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<62>";
	dbg_array(this.m__viewport,3)[dbg_index]=t_height;
	pop_err();
}
c_Renderer.prototype.p_SetClearColor=function(t_clearColor){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<70>";
	this.m__clearColor=t_clearColor;
	pop_err();
}
c_Renderer.prototype.p_SetAmbientLight=function(t_ambientLight){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<74>";
	this.m__ambientLight=t_ambientLight;
	pop_err();
}
c_Renderer.prototype.p_SetCameraMatrix=function(t_cameraMatrix){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<78>";
	this.m__cameraMatrix=t_cameraMatrix;
	pop_err();
}
c_Renderer.prototype.p_SetProjectionMatrix=function(t_projectionMatrix){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<82>";
	this.m__projectionMatrix=t_projectionMatrix;
	pop_err();
}
c_Renderer.prototype.p_Layers=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<86>";
	pop_err();
	return this.m__layers;
}
c_Renderer.prototype.p_Render=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<91>";
	var t_vwidth=dbg_array(this.m__viewport,2)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<91>";
	var t_vheight=dbg_array(this.m__viewport,3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<93>";
	var t_twidth=((t_vwidth/1)|0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<93>";
	var t_theight=((t_vheight/1)|0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<95>";
	if(!((this.m__timage)!=null) || this.m__timage.p_Width()!=t_twidth || this.m__timage.p_Height()!=t_theight){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<96>";
		if((this.m__timage)!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<96>";
			this.m__timage.p_Discard();
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<97>";
		this.m__timage=c_Image2.m_new.call(new c_Image2,t_twidth,t_theight,0.0,0.0,1);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<100>";
	if(!((this.m__timage2)!=null) || this.m__timage2.p_Width()!=t_twidth || this.m__timage2.p_Height()!=t_theight){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<101>";
		if((this.m__timage2)!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<101>";
			this.m__timage2.p_Discard();
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<102>";
		this.m__timage2=c_Image2.m_new.call(new c_Image2,t_twidth,t_theight,0.0,0.0,1);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<105>";
	bb_math3d_Mat4Inverse(this.m__cameraMatrix,this.m__viewMatrix);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<107>";
	var t_invProj=false;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<110>";
	this.m__canvas.p_SetRenderTarget2(this.m__image);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<111>";
	this.m__canvas.p_SetViewport(dbg_array(this.m__viewport,0)[dbg_index],dbg_array(this.m__viewport,1)[dbg_index],dbg_array(this.m__viewport,2)[dbg_index],dbg_array(this.m__viewport,3)[dbg_index]);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<112>";
	var t_1=this.m__clearMode;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<113>";
	if(t_1==1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<114>";
		this.m__canvas.p_Clear2(dbg_array(this.m__clearColor,0)[dbg_index],dbg_array(this.m__clearColor,1)[dbg_index],dbg_array(this.m__clearColor,2)[dbg_index],dbg_array(this.m__clearColor,3)[dbg_index]);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<117>";
	for(var t_layerId=0;t_layerId<this.m__layers.p_Length2();t_layerId=t_layerId+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<119>";
		var t_layer=this.m__layers.p_Get(t_layerId);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<120>";
		var t_fog=t_layer.p_LayerFogColor();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<122>";
		var t_layerMatrix=t_layer.p_LayerMatrix();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<123>";
		bb_math3d_Mat4Inverse(t_layerMatrix,this.m__invLayerMatrix);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<125>";
		this.m__drawLists.p_Clear();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<126>";
		t_layer.p_OnRenderLayer(this.m__drawLists);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<128>";
		var t_lights=c_Stack13.m_new.call(new c_Stack13);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<129>";
		t_layer.p_EnumLayerLights(t_lights);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<131>";
		if(!((t_lights.p_Length2())!=0)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<133>";
			for(var t_i=0;t_i<4;t_i=t_i+1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<134>";
				this.m__canvas.p_SetLightType(t_i,0);
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<137>";
			this.m__canvas.p_SetRenderTarget2(this.m__image);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<138>";
			this.m__canvas.p_SetShadowMap(this.m__timage);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<139>";
			this.m__canvas.p_SetViewport(dbg_array(this.m__viewport,0)[dbg_index],dbg_array(this.m__viewport,1)[dbg_index],dbg_array(this.m__viewport,2)[dbg_index],dbg_array(this.m__viewport,3)[dbg_index]);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<140>";
			this.m__canvas.p_SetProjectionMatrix(this.m__projectionMatrix);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<141>";
			this.m__canvas.p_SetViewMatrix(this.m__viewMatrix);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<142>";
			this.m__canvas.p_SetModelMatrix(t_layerMatrix);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<143>";
			this.m__canvas.p_SetAmbientLight2(dbg_array(this.m__ambientLight,0)[dbg_index],dbg_array(this.m__ambientLight,1)[dbg_index],dbg_array(this.m__ambientLight,2)[dbg_index],1.0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<144>";
			this.m__canvas.p_SetFogColor(dbg_array(t_fog,0)[dbg_index],dbg_array(t_fog,1)[dbg_index],dbg_array(t_fog,2)[dbg_index],dbg_array(t_fog,3)[dbg_index]);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<146>";
			this.m__canvas.p_SetColor2(1.0,1.0,1.0,1.0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<147>";
			for(var t_i2=0;t_i2<this.m__drawLists.p_Length2();t_i2=t_i2+1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<148>";
				this.m__canvas.p_RenderDrawList(this.m__drawLists.p_Get(t_i2));
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<150>";
			this.m__canvas.p_Flush();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<152>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<156>";
		var t_light0=0;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<158>";
		do{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<160>";
			var t_numLights=bb_math_Min(t_lights.p_Length2()-t_light0,4);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<164>";
			this.m__canvas.p_SetRenderTarget2(this.m__timage);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<165>";
			this.m__canvas.p_SetShadowMap(null);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<166>";
			this.m__canvas.p_SetViewport(0,0,t_twidth,t_theight);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<167>";
			this.m__canvas.p_SetProjectionMatrix(this.m__projectionMatrix);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<168>";
			this.m__canvas.p_SetViewMatrix(this.m__viewMatrix);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<169>";
			this.m__canvas.p_SetModelMatrix(t_layerMatrix);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<170>";
			this.m__canvas.p_SetAmbientLight2(0.0,0.0,0.0,0.0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<171>";
			this.m__canvas.p_SetFogColor(0.0,0.0,0.0,0.0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<173>";
			this.m__canvas.p_Clear2(1.0,1.0,1.0,1.0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<174>";
			this.m__canvas.p_SetBlendMode(0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<175>";
			this.m__canvas.p_SetColor2(0.0,0.0,0.0,0.0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<177>";
			this.m__canvas.p_SetDefaultMaterial(c_Shader.m_ShadowShader().p_DefaultMaterial());
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<179>";
			for(var t_i3=0;t_i3<t_numLights;t_i3=t_i3+1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<181>";
				var t_light=t_lights.p_Get(t_light0+t_i3);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<183>";
				var t_matrix=t_light.p_LightMatrix();
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<185>";
				bb_math3d_Vec4Copy2(t_matrix,bb_renderer_lvector,12,0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<186>";
				bb_math3d_Mat4Transform(this.m__invLayerMatrix,bb_renderer_lvector,bb_renderer_tvector);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<187>";
				var t_lightx=dbg_array(bb_renderer_tvector,0)[dbg_index];
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<187>";
				var t_lighty=dbg_array(bb_renderer_tvector,1)[dbg_index];
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<189>";
				this.m__canvas.p_SetColorMask(t_i3==0,t_i3==1,t_i3==2,t_i3==3);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<191>";
				var t_image=t_light.p_LightImage();
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<192>";
				if((t_image)!=null){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<193>";
					this.m__canvas.p_Clear2(0.0,0.0,0.0,0.0);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<194>";
					this.m__canvas.p_PushMatrix();
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<195>";
					this.m__canvas.p_SetMatrix3(dbg_array(t_matrix,0)[dbg_index],dbg_array(t_matrix,1)[dbg_index],dbg_array(t_matrix,4)[dbg_index],dbg_array(t_matrix,5)[dbg_index],t_lightx,t_lighty);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<196>";
					this.m__canvas.p_DrawImage(t_image);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<197>";
					this.m__canvas.p_PopMatrix();
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<200>";
				for(var t_j=0;t_j<this.m__drawLists.p_Length2();t_j=t_j+1){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<201>";
					this.m__canvas.p_DrawShadows(t_lightx,t_lighty,this.m__drawLists.p_Get(t_j));
				}
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<205>";
			this.m__canvas.p_SetDefaultMaterial(c_Shader.m_FastShader().p_DefaultMaterial());
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<206>";
			this.m__canvas.p_SetColorMask(true,true,true,true);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<207>";
			this.m__canvas.p_Flush();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<211>";
			var t_lightMask=t_layer.p_LayerLightMaskImage();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<212>";
			if((t_lightMask)!=null){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<214>";
				if(!t_invProj){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<215>";
					bb_math3d_Mat4Inverse(this.m__projectionMatrix,this.m__invProjMatrix);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<216>";
					bb_math3d_Mat4Project(this.m__invProjMatrix,[-1.0,-1.0,-1.0,1.0],this.m__ptl);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<217>";
					bb_math3d_Mat4Project(this.m__invProjMatrix,[1.0,1.0,-1.0,1.0],this.m__pbr);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<220>";
				var t_fwidth=dbg_array(this.m__pbr,0)[dbg_index]-dbg_array(this.m__ptl,0)[dbg_index];
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<221>";
				var t_fheight=dbg_array(this.m__pbr,1)[dbg_index]-dbg_array(this.m__ptl,1)[dbg_index];
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<223>";
				if(dbg_array(this.m__projectionMatrix,15)[dbg_index]==0.0){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<224>";
					var t_scz=(dbg_array(t_layerMatrix,14)[dbg_index]-dbg_array(this.m__cameraMatrix,14)[dbg_index])/dbg_array(this.m__ptl,2)[dbg_index];
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<225>";
					t_fwidth*=t_scz;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<226>";
					t_fheight*=t_scz;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<229>";
				this.m__canvas.p_SetProjection2d(0.0,t_fwidth,0.0,t_fheight,-1.0,1.0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<230>";
				this.m__canvas.p_SetViewMatrix(bb_math3d_Mat4Identity);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<231>";
				this.m__canvas.p_SetModelMatrix(bb_math3d_Mat4Identity);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<238>";
				this.m__canvas.p_SetBlendMode(4);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<240>";
				var t_w=(t_lightMask.p_Width());
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<241>";
				var t_h=(t_lightMask.p_Height());
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<242>";
				var t_x=-t_w;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<243>";
				while(t_x<t_fwidth+t_w){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<244>";
					var t_y=-t_h;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<245>";
					while(t_y<t_fheight+t_h){
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<246>";
						this.m__canvas.p_DrawImage4(t_lightMask,t_x,t_y);
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<247>";
						t_y+=t_h;
					}
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<249>";
					t_x+=t_w;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<252>";
				this.m__canvas.p_Flush();
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<258>";
			for(var t_i4=0;t_i4<t_numLights;t_i4=t_i4+1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<260>";
				var t_light2=t_lights.p_Get(t_light0+t_i4);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<262>";
				var t_c=t_light2.p_LightColor();
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<263>";
				var t_m=t_light2.p_LightMatrix();
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<265>";
				this.m__canvas.p_SetLightType(t_i4,1);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<266>";
				this.m__canvas.p_SetLightColor(t_i4,dbg_array(t_c,0)[dbg_index],dbg_array(t_c,1)[dbg_index],dbg_array(t_c,2)[dbg_index],dbg_array(t_c,3)[dbg_index]);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<267>";
				this.m__canvas.p_SetLightPosition(t_i4,dbg_array(t_m,12)[dbg_index],dbg_array(t_m,13)[dbg_index],dbg_array(t_m,14)[dbg_index]);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<268>";
				this.m__canvas.p_SetLightRange(t_i4,t_light2.p_LightRange());
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<270>";
			for(var t_i5=t_numLights;t_i5<4;t_i5=t_i5+1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<271>";
				this.m__canvas.p_SetLightType(t_i5,0);
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<274>";
			if(t_light0==0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<278>";
				this.m__canvas.p_SetRenderTarget2(this.m__image);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<279>";
				this.m__canvas.p_SetShadowMap(this.m__timage);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<280>";
				this.m__canvas.p_SetViewport(dbg_array(this.m__viewport,0)[dbg_index],dbg_array(this.m__viewport,1)[dbg_index],dbg_array(this.m__viewport,2)[dbg_index],dbg_array(this.m__viewport,3)[dbg_index]);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<281>";
				this.m__canvas.p_SetProjectionMatrix(this.m__projectionMatrix);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<282>";
				this.m__canvas.p_SetViewMatrix(this.m__viewMatrix);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<283>";
				this.m__canvas.p_SetModelMatrix(t_layerMatrix);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<284>";
				this.m__canvas.p_SetAmbientLight2(dbg_array(this.m__ambientLight,0)[dbg_index],dbg_array(this.m__ambientLight,1)[dbg_index],dbg_array(this.m__ambientLight,2)[dbg_index],1.0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<285>";
				this.m__canvas.p_SetFogColor(dbg_array(t_fog,0)[dbg_index],dbg_array(t_fog,1)[dbg_index],dbg_array(t_fog,2)[dbg_index],dbg_array(t_fog,3)[dbg_index]);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<287>";
				this.m__canvas.p_SetColor2(1.0,1.0,1.0,1.0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<288>";
				for(var t_i6=0;t_i6<this.m__drawLists.p_Length2();t_i6=t_i6+1){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<289>";
					this.m__canvas.p_RenderDrawList(this.m__drawLists.p_Get(t_i6));
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<292>";
				this.m__canvas.p_RenderDrawList(this.m__fudgeList);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<294>";
				this.m__canvas.p_Flush();
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<300>";
				this.m__canvas.p_SetRenderTarget2(this.m__timage2);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<301>";
				this.m__canvas.p_SetShadowMap(this.m__timage);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<302>";
				this.m__canvas.p_SetViewport(0,0,t_twidth,t_theight);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<303>";
				this.m__canvas.p_SetProjectionMatrix(this.m__projectionMatrix);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<304>";
				this.m__canvas.p_SetViewMatrix(this.m__viewMatrix);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<305>";
				this.m__canvas.p_SetModelMatrix(t_layerMatrix);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<306>";
				this.m__canvas.p_SetAmbientLight2(0.0,0.0,0.0,0.0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<307>";
				this.m__canvas.p_SetFogColor(0.0,0.0,0.0,dbg_array(t_fog,3)[dbg_index]);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<309>";
				this.m__canvas.p_Clear2(0.0,0.0,0.0,1.0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<310>";
				this.m__canvas.p_SetColor2(1.0,1.0,1.0,1.0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<311>";
				for(var t_i7=0;t_i7<this.m__drawLists.p_Length2();t_i7=t_i7+1){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<312>";
					this.m__canvas.p_RenderDrawList(this.m__drawLists.p_Get(t_i7));
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<314>";
				this.m__canvas.p_Flush();
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<318>";
				this.m__canvas.p_SetRenderTarget2(this.m__image);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<319>";
				this.m__canvas.p_SetShadowMap(null);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<320>";
				this.m__canvas.p_SetViewport(dbg_array(this.m__viewport,0)[dbg_index],dbg_array(this.m__viewport,1)[dbg_index],dbg_array(this.m__viewport,2)[dbg_index],dbg_array(this.m__viewport,3)[dbg_index]);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<321>";
				this.m__canvas.p_SetProjection2d(0.0,(t_vwidth),0.0,(t_vheight),-1.0,1.0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<322>";
				this.m__canvas.p_SetViewMatrix(bb_math3d_Mat4Identity);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<323>";
				this.m__canvas.p_SetModelMatrix(bb_math3d_Mat4Identity);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<324>";
				this.m__canvas.p_SetAmbientLight2(0.0,0.0,0.0,1.0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<325>";
				this.m__canvas.p_SetFogColor(0.0,0.0,0.0,0.0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<327>";
				this.m__canvas.p_SetBlendMode(2);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<328>";
				this.m__canvas.p_SetColor2(1.0,1.0,1.0,1.0);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<329>";
				this.m__canvas.p_DrawImage(this.m__timage2);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<330>";
				this.m__canvas.p_Flush();
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/renderer.monkey<334>";
			t_light0+=4;
		}while(!(t_light0>=t_lights.p_Length2()));
	}
	pop_err();
}
function c_DrawList(){
	Object.call(this);
	this.m__font=null;
	this.m__defaultMaterial=null;
	this.m__next=0;
	this.m__ops=c_Stack8.m_new.call(new c_Stack8);
	this.m__data=c_DataBuffer.m_new.call(new c_DataBuffer,4096,true);
	this.m__op=bb_graphics2_nullOp;
	this.m__casters=c_Stack9.m_new.call(new c_Stack9);
	this.m__casterVerts=c_FloatStack.m_new2.call(new c_FloatStack);
	this.m__blend=1;
	this.m__ix=1.0;
	this.m__jx=.0;
	this.m__tx=.0;
	this.m__iy=.0;
	this.m__jy=1.0;
	this.m__ty=.0;
	this.m__pmcolor=-1;
	this.m__color=[1.0,1.0,1.0,1.0];
	this.m__alpha=255.0;
	this.m__matStack=new_number_array(384);
	this.m__matSp=0;
}
c_DrawList.prototype.p_SetFont=function(t_font){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1498>";
	if(!((t_font)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1498>";
		t_font=bb_graphics2_defaultFont;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1499>";
	this.m__font=t_font;
	pop_err();
}
c_DrawList.prototype.p_SetDefaultMaterial=function(t_material){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1507>";
	this.m__defaultMaterial=t_material;
	pop_err();
}
c_DrawList.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1362>";
	bb_graphics2_InitMojo2();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1364>";
	this.p_SetFont(null);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1365>";
	this.p_SetDefaultMaterial(bb_graphics2_fastShader.p_DefaultMaterial());
	pop_err();
	return this;
}
c_DrawList.prototype.p_IsEmpty=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1869>";
	var t_=this.m__next==0;
	pop_err();
	return t_;
}
c_DrawList.prototype.p_Render3=function(t_op,t_index,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1882>";
	if(!dbg_object(t_op).m_material.p_Bind()){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1884>";
	if(dbg_object(t_op).m_blend!=bb_graphics2_rs_blend){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1885>";
		bb_graphics2_rs_blend=dbg_object(t_op).m_blend;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1886>";
		var t_4=bb_graphics2_rs_blend;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1887>";
		if(t_4==0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1888>";
			gl.disable(3042);
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1889>";
			if(t_4==1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1890>";
				gl.enable(3042);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1891>";
				gl.blendFunc(1,771);
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1892>";
				if(t_4==2){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1893>";
					gl.enable(3042);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1894>";
					gl.blendFunc(1,1);
				}else{
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1895>";
					if(t_4==3){
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1896>";
						gl.enable(3042);
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1897>";
						gl.blendFunc(774,771);
					}else{
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1898>";
						if(t_4==4){
							err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1899>";
							gl.enable(3042);
							err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1900>";
							gl.blendFunc(774,0);
						}
					}
				}
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1904>";
	var t_5=dbg_object(t_op).m_order;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1905>";
	if(t_5==1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1906>";
		gl.drawArrays(0,t_index,t_count);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1907>";
		if(t_5==2){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1908>";
			gl.drawArrays(1,t_index,t_count);
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1909>";
			if(t_5==3){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1910>";
				gl.drawArrays(4,t_index,t_count);
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1911>";
				if(t_5==4){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1912>";
					gl.drawElements(4,((t_count/4)|0)*6,5123,(((t_index/4)|0)*6+(t_index&3)*3510)*2);
				}else{
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1914>";
					var t_j=0;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1915>";
					while(t_j<t_count){
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1916>";
						gl.drawArrays(6,t_index+t_j,dbg_object(t_op).m_order);
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1917>";
						t_j+=dbg_object(t_op).m_order;
					}
				}
			}
		}
	}
	pop_err();
}
c_DrawList.prototype.p_Render=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1924>";
	if(!((this.m__next)!=0)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1926>";
	var t_offset=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1926>";
	var t_opid=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1926>";
	var t_ops=this.m__ops.p_Data();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1926>";
	var t_length=this.m__ops.p_Length2();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1928>";
	while(t_offset<this.m__next){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1930>";
		var t_size=this.m__next-t_offset;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1930>";
		var t_lastop=t_length;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1932>";
		if(t_size>65520){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1934>";
			t_size=0;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1935>";
			t_lastop=t_opid;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1936>";
			while(t_lastop<t_length){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1937>";
				var t_op=dbg_array(t_ops,t_lastop)[dbg_index];
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1938>";
				var t_n=dbg_object(t_op).m_count*28;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1939>";
				if(t_size+t_n>65520){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1939>";
					break;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1940>";
				t_size+=t_n;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1941>";
				t_lastop+=1;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1944>";
			if(!((t_size)!=0)){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1945>";
				var t_op2=dbg_array(t_ops,t_opid)[dbg_index];
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1946>";
				var t_count=dbg_object(t_op2).m_count;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1947>";
				while((t_count)!=0){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1948>";
					var t_n2=t_count;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1949>";
					if(t_n2>2340){
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1949>";
						t_n2=((2340/dbg_object(t_op2).m_order)|0)*dbg_object(t_op2).m_order;
					}
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1950>";
					var t_size2=t_n2*28;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1952>";
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1953>";
					_glBufferSubData(34962,0,t_size2,this.m__data,t_offset);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1955>";
					this.p_Render3(t_op2,0,t_n2);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1957>";
					t_offset+=t_size2;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1958>";
					t_count-=t_n2;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1960>";
				t_opid+=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1961>";
				continue;
			}
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1966>";
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1967>";
		_glBufferSubData(34962,0,t_size,this.m__data,t_offset);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1969>";
		var t_index=0;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1970>";
		while(t_opid<t_lastop){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1971>";
			var t_op3=dbg_array(t_ops,t_opid)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1972>";
			this.p_Render3(t_op3,t_index,dbg_object(t_op3).m_count);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1973>";
			t_index+=dbg_object(t_op3).m_count;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1974>";
			t_opid+=1;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1976>";
		t_offset+=t_size;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1980>";
	gl.getError();
	pop_err();
}
c_DrawList.prototype.p_Reset=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1985>";
	this.m__next=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1987>";
	var t_data=this.m__ops.p_Data();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1988>";
	for(var t_i=0;t_i<this.m__ops.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1989>";
		dbg_object(dbg_array(t_data,t_i)[dbg_index]).m_material=null;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1990>";
		bb_graphics2_freeOps.p_Push22(dbg_array(t_data,t_i)[dbg_index]);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1992>";
	this.m__ops.p_Clear();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1993>";
	this.m__op=bb_graphics2_nullOp;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1995>";
	this.m__casters.p_Clear();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1996>";
	this.m__casterVerts.p_Clear();
	pop_err();
}
c_DrawList.prototype.p_Flush=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2000>";
	this.p_Render();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2001>";
	this.p_Reset();
	pop_err();
}
c_DrawList.prototype.p_BeginPrim=function(t_material,t_order){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2030>";
	if(!((t_material)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2030>";
		t_material=this.m__defaultMaterial;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2032>";
	if(this.m__next+t_order*28>this.m__data.Length()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2034>";
		var t_newsize=bb_math_Max(this.m__data.Length()+((this.m__data.Length()/2)|0),this.m__next+t_order*28);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2035>";
		var t_data=c_DataBuffer.m_new.call(new c_DataBuffer,t_newsize,true);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2036>";
		this.m__data.p_CopyBytes(0,t_data,0,this.m__next);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2037>";
		this.m__data.Discard();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2038>";
		this.m__data=t_data;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2041>";
	if(t_material==dbg_object(this.m__op).m_material && this.m__blend==dbg_object(this.m__op).m_blend && t_order==dbg_object(this.m__op).m_order){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2042>";
		dbg_object(this.m__op).m_count+=t_order;
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2046>";
	if((bb_graphics2_freeOps.p_Length2())!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2046>";
		this.m__op=bb_graphics2_freeOps.p_Pop();
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2046>";
		this.m__op=c_DrawOp.m_new.call(new c_DrawOp);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2048>";
	this.m__ops.p_Push22(this.m__op);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2049>";
	dbg_object(this.m__op).m_material=t_material;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2050>";
	dbg_object(this.m__op).m_blend=this.m__blend;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2051>";
	dbg_object(this.m__op).m_order=t_order;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2052>";
	dbg_object(this.m__op).m_count=t_order;
	pop_err();
}
c_DrawList.prototype.p_PrimVert=function(t_x0,t_y0,t_s0,t_t0){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2085>";
	this.m__data.PokeFloat(this.m__next+0,t_x0*this.m__ix+t_y0*this.m__jx+this.m__tx);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2086>";
	this.m__data.PokeFloat(this.m__next+4,t_x0*this.m__iy+t_y0*this.m__jy+this.m__ty);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2087>";
	this.m__data.PokeFloat(this.m__next+8,t_s0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2088>";
	this.m__data.PokeFloat(this.m__next+12,t_t0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2089>";
	this.m__data.PokeFloat(this.m__next+16,this.m__ix);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2090>";
	this.m__data.PokeFloat(this.m__next+20,this.m__iy);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2091>";
	this.m__data.PokeInt(this.m__next+24,this.m__pmcolor);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2092>";
	this.m__next+=28;
	pop_err();
}
c_DrawList.prototype.p_DrawRect=function(t_x0,t_y0,t_width,t_height,t_material,t_s0,t_t0,t_s1,t_t1){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1640>";
	var t_x1=t_x0+t_width;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1640>";
	var t_y1=t_y0+t_height;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1641>";
	this.p_BeginPrim(t_material,4);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1642>";
	this.p_PrimVert(t_x0,t_y0,t_s0,t_t0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1643>";
	this.p_PrimVert(t_x1,t_y0,t_s1,t_t0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1644>";
	this.p_PrimVert(t_x1,t_y1,t_s1,t_t1);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1645>";
	this.p_PrimVert(t_x0,t_y1,t_s0,t_t1);
	pop_err();
}
c_DrawList.prototype.p_DrawRect2=function(t_x0,t_y0,t_width,t_height,t_image,t_sourceX,t_sourceY,t_sourceWidth,t_sourceHeight){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1657>";
	var t_material=dbg_object(t_image).m__material;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1658>";
	var t_s0=(dbg_object(t_image).m__x+t_sourceX)/(t_material.p_Width());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1659>";
	var t_t0=(dbg_object(t_image).m__y+t_sourceY)/(t_material.p_Height());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1660>";
	var t_s1=(dbg_object(t_image).m__x+t_sourceX+t_sourceWidth)/(t_material.p_Width());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1661>";
	var t_t1=(dbg_object(t_image).m__y+t_sourceY+t_sourceHeight)/(t_material.p_Height());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1662>";
	this.p_DrawRect(t_x0,t_y0,t_width,t_height,t_material,t_s0,t_t0,t_s1,t_t1);
	pop_err();
}
c_DrawList.prototype.p_DrawRect3=function(t_x,t_y,t_image,t_sourceX,t_sourceY,t_sourceWidth,t_sourceHeight){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1653>";
	this.p_DrawRect2(t_x,t_y,(t_sourceWidth),(t_sourceHeight),t_image,t_sourceX,t_sourceY,t_sourceWidth,t_sourceHeight);
	pop_err();
}
c_DrawList.prototype.p_DrawRect4=function(t_x0,t_y0,t_width,t_height,t_image){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1649>";
	this.p_DrawRect(t_x0,t_y0,t_width,t_height,dbg_object(t_image).m__material,dbg_object(t_image).m__s0,dbg_object(t_image).m__t0,dbg_object(t_image).m__s1,dbg_object(t_image).m__t1);
	pop_err();
}
c_DrawList.prototype.p_SetColor=function(t_r,t_g,t_b){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1377>";
	dbg_array(this.m__color,0)[dbg_index]=t_r;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1378>";
	dbg_array(this.m__color,1)[dbg_index]=t_g;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1379>";
	dbg_array(this.m__color,2)[dbg_index]=t_b;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1380>";
	this.m__pmcolor=((this.m__alpha)|0)<<24|((dbg_array(this.m__color,2)[dbg_index]*this.m__alpha)|0)<<16|((dbg_array(this.m__color,1)[dbg_index]*this.m__alpha)|0)<<8|((dbg_array(this.m__color,0)[dbg_index]*this.m__alpha)|0);
	pop_err();
}
c_DrawList.prototype.p_SetColor2=function(t_r,t_g,t_b,t_a){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1384>";
	dbg_array(this.m__color,0)[dbg_index]=t_r;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1385>";
	dbg_array(this.m__color,1)[dbg_index]=t_g;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1386>";
	dbg_array(this.m__color,2)[dbg_index]=t_b;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1387>";
	dbg_array(this.m__color,3)[dbg_index]=t_a;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1388>";
	this.m__alpha=t_a*255.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1389>";
	this.m__pmcolor=((this.m__alpha)|0)<<24|((dbg_array(this.m__color,2)[dbg_index]*this.m__alpha)|0)<<16|((dbg_array(this.m__color,1)[dbg_index]*this.m__alpha)|0)<<8|((dbg_array(this.m__color,0)[dbg_index]*this.m__alpha)|0);
	pop_err();
}
c_DrawList.prototype.p_PushMatrix=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1484>";
	dbg_array(this.m__matStack,this.m__matSp+0)[dbg_index]=this.m__ix;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1484>";
	dbg_array(this.m__matStack,this.m__matSp+1)[dbg_index]=this.m__iy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1485>";
	dbg_array(this.m__matStack,this.m__matSp+2)[dbg_index]=this.m__jx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1485>";
	dbg_array(this.m__matStack,this.m__matSp+3)[dbg_index]=this.m__jy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1486>";
	dbg_array(this.m__matStack,this.m__matSp+4)[dbg_index]=this.m__tx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1486>";
	dbg_array(this.m__matStack,this.m__matSp+5)[dbg_index]=this.m__ty;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1487>";
	this.m__matSp+=6;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1487>";
	if(this.m__matSp>=this.m__matStack.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1487>";
		this.m__matSp-=this.m__matStack.length;
	}
	pop_err();
}
c_DrawList.prototype.p_SetMatrix3=function(t_ix,t_iy,t_jx,t_jy,t_tx,t_ty){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1420>";
	this.m__ix=t_ix;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1420>";
	this.m__iy=t_iy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1421>";
	this.m__jx=t_jx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1421>";
	this.m__jy=t_jy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1422>";
	this.m__tx=t_tx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1422>";
	this.m__ty=t_ty;
	pop_err();
}
c_DrawList.prototype.p_Transform=function(t_ix,t_iy,t_jx,t_jy,t_tx,t_ty){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1435>";
	var t_ix2=t_ix*this.m__ix+t_iy*this.m__jx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1435>";
	var t_iy2=t_ix*this.m__iy+t_iy*this.m__jy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1436>";
	var t_jx2=t_jx*this.m__ix+t_jy*this.m__jx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1436>";
	var t_jy2=t_jx*this.m__iy+t_jy*this.m__jy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1437>";
	var t_tx2=t_tx*this.m__ix+t_ty*this.m__jx+this.m__tx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1437>";
	var t_ty2=t_tx*this.m__iy+t_ty*this.m__jy+this.m__ty;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1438>";
	this.p_SetMatrix3(t_ix2,t_iy2,t_jx2,t_jy2,t_tx2,t_ty2);
	pop_err();
}
c_DrawList.prototype.p_Translate=function(t_tx,t_ty){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1442>";
	this.p_Transform(1.0,0.0,0.0,1.0,t_tx,t_ty);
	pop_err();
}
c_DrawList.prototype.p_Rotate=function(t_rz){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1446>";
	this.p_Transform(Math.cos((t_rz)*D2R),-Math.sin((t_rz)*D2R),Math.sin((t_rz)*D2R),Math.cos((t_rz)*D2R),0.0,0.0);
	pop_err();
	return 0;
}
c_DrawList.prototype.p_Scale=function(t_sx,t_sy){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1450>";
	this.p_Transform(t_sx,0.0,0.0,t_sy,0.0,0.0);
	pop_err();
}
c_DrawList.prototype.p_TranslateRotateScale=function(t_tx,t_ty,t_rz,t_sx,t_sy){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1469>";
	this.p_Translate(t_tx,t_ty);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1470>";
	this.p_Rotate(t_rz);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1471>";
	this.p_Scale(t_sx,t_sy);
	pop_err();
}
c_DrawList.prototype.p_PopMatrix=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1491>";
	this.m__matSp-=6;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1491>";
	if(this.m__matSp<0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1491>";
		this.m__matSp+=this.m__matStack.length;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1492>";
	this.m__ix=dbg_array(this.m__matStack,this.m__matSp+0)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1492>";
	this.m__iy=dbg_array(this.m__matStack,this.m__matSp+1)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1493>";
	this.m__jx=dbg_array(this.m__matStack,this.m__matSp+2)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1493>";
	this.m__jy=dbg_array(this.m__matStack,this.m__matSp+3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1494>";
	this.m__tx=dbg_array(this.m__matStack,this.m__matSp+4)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1494>";
	this.m__ty=dbg_array(this.m__matStack,this.m__matSp+5)[dbg_index];
	pop_err();
}
c_DrawList.prototype.p_SetBlendMode=function(t_blend){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1369>";
	this.m__blend=t_blend;
	pop_err();
}
c_DrawList.prototype.p_AddShadowCaster=function(t_caster){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1837>";
	this.m__casters.p_Push25(t_caster);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1838>";
	var t_verts=dbg_object(t_caster).m__verts;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1839>";
	for(var t_i=0;t_i<t_verts.length-1;t_i=t_i+2){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1840>";
		var t_x0=dbg_array(t_verts,t_i)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1841>";
		var t_y0=dbg_array(t_verts,t_i+1)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1842>";
		this.m__casterVerts.p_Push28(t_x0*this.m__ix+t_y0*this.m__jx+this.m__tx);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1843>";
		this.m__casterVerts.p_Push28(t_x0*this.m__iy+t_y0*this.m__jy+this.m__ty);
	}
	pop_err();
}
c_DrawList.prototype.p_TranslateRotate=function(t_tx,t_ty,t_rz){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1454>";
	this.p_Translate(t_tx,t_ty);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1455>";
	this.p_Rotate(t_rz);
	pop_err();
}
c_DrawList.prototype.p_AddShadowCaster2=function(t_caster,t_tx,t_ty,t_rz,t_sx,t_sy){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1862>";
	this.p_PushMatrix();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1863>";
	this.p_TranslateRotateScale(t_tx,t_ty,t_rz,t_sx,t_sy);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1864>";
	this.p_AddShadowCaster(t_caster);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1865>";
	this.p_PopMatrix();
	pop_err();
}
c_DrawList.prototype.p_AddShadowCaster3=function(t_caster,t_tx,t_ty,t_rz){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1855>";
	this.p_PushMatrix();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1856>";
	this.p_TranslateRotate(t_tx,t_ty,t_rz);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1857>";
	this.p_AddShadowCaster(t_caster);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1858>";
	this.p_PopMatrix();
	pop_err();
}
c_DrawList.prototype.p_AddShadowCaster4=function(t_caster,t_tx,t_ty){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1848>";
	this.p_PushMatrix();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1849>";
	this.p_Translate(t_tx,t_ty);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1850>";
	this.p_AddShadowCaster(t_caster);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1851>";
	this.p_PopMatrix();
	pop_err();
}
c_DrawList.prototype.p_DrawImage=function(t_image){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1704>";
	this.p_BeginPrim(dbg_object(t_image).m__material,4);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1705>";
	this.p_PrimVert(dbg_object(t_image).m__x0,dbg_object(t_image).m__y0,dbg_object(t_image).m__s0,dbg_object(t_image).m__t0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1706>";
	this.p_PrimVert(dbg_object(t_image).m__x1,dbg_object(t_image).m__y0,dbg_object(t_image).m__s1,dbg_object(t_image).m__t0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1707>";
	this.p_PrimVert(dbg_object(t_image).m__x1,dbg_object(t_image).m__y1,dbg_object(t_image).m__s1,dbg_object(t_image).m__t1);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1708>";
	this.p_PrimVert(dbg_object(t_image).m__x0,dbg_object(t_image).m__y1,dbg_object(t_image).m__s0,dbg_object(t_image).m__t1);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1709>";
	if((dbg_object(t_image).m__caster)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1709>";
		this.p_AddShadowCaster(dbg_object(t_image).m__caster);
	}
	pop_err();
}
c_DrawList.prototype.p_DrawImage2=function(t_image,t_tx,t_ty,t_rz,t_sx,t_sy){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1745>";
	this.p_PushMatrix();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1746>";
	this.p_TranslateRotateScale(t_tx,t_ty,t_rz,t_sx,t_sy);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1747>";
	this.p_DrawImage(t_image);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1748>";
	this.p_PopMatrix();
	pop_err();
}
c_DrawList.prototype.p_DrawImage3=function(t_image,t_tx,t_ty,t_rz){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1727>";
	this.p_PushMatrix();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1728>";
	this.p_TranslateRotate(t_tx,t_ty,t_rz);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1729>";
	this.p_DrawImage(t_image);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1730>";
	this.p_PopMatrix();
	pop_err();
}
c_DrawList.prototype.p_DrawImage4=function(t_image,t_tx,t_ty){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1713>";
	this.p_PushMatrix();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1714>";
	this.p_Translate(t_tx,t_ty);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1715>";
	this.p_DrawImage(t_image);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1716>";
	this.p_PopMatrix();
	pop_err();
}
c_DrawList.prototype.p_DrawTriangle=function(t_x0,t_y0,t_x1,t_y1,t_x2,t_y2,t_material,t_s0,t_t0,t_s1,t_t1,t_s2,t_t2){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1526>";
	this.p_BeginPrim(t_material,3);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1527>";
	this.p_PrimVert(t_x0,t_y0,t_s0,t_t0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1528>";
	this.p_PrimVert(t_x1,t_y1,t_s1,t_t1);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1529>";
	this.p_PrimVert(t_x2,t_y2,t_s2,t_t2);
	pop_err();
}
c_DrawList.prototype.p_DrawShadow=function(t_lx,t_ly,t_x0,t_y0,t_x1,t_y1){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1775>";
	var t_ext=1024;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1777>";
	var t_dx=t_x1-t_x0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1777>";
	var t_dy=t_y1-t_y0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1778>";
	var t_d0=Math.sqrt(t_dx*t_dx+t_dy*t_dy);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1779>";
	var t_nx=-t_dy/t_d0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1779>";
	var t_ny=t_dx/t_d0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1780>";
	var t_pd=-(t_x0*t_nx+t_y0*t_ny);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1782>";
	var t_d=t_lx*t_nx+t_ly*t_ny+t_pd;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1783>";
	if(t_d<0.0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1783>";
		pop_err();
		return false;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1785>";
	var t_x2=t_x1-t_lx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1785>";
	var t_y2=t_y1-t_ly;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1786>";
	var t_d2=(t_ext)/Math.sqrt(t_x2*t_x2+t_y2*t_y2);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1787>";
	t_x2=t_lx+t_x2*(t_ext);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1787>";
	t_y2=t_ly+t_y2*(t_ext);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1789>";
	var t_x3=t_x0-t_lx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1789>";
	var t_y3=t_y0-t_ly;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1790>";
	var t_d3=(t_ext)/Math.sqrt(t_x3*t_x3+t_y3*t_y3);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1791>";
	t_x3=t_lx+t_x3*(t_ext);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1791>";
	t_y3=t_ly+t_y3*(t_ext);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1793>";
	var t_x4=(t_x2+t_x3)/2.0-t_lx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1793>";
	var t_y4=(t_y2+t_y3)/2.0-t_ly;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1794>";
	var t_d4=(t_ext)/Math.sqrt(t_x4*t_x4+t_y4*t_y4);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1795>";
	t_x4=t_lx+t_x4*(t_ext);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1795>";
	t_y4=t_ly+t_y4*(t_ext);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1797>";
	this.p_DrawTriangle(t_x0,t_y0,t_x4,t_y4,t_x3,t_y3,null,.5,0.0,1.0,1.0,0.0,1.0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1798>";
	this.p_DrawTriangle(t_x0,t_y0,t_x1,t_y1,t_x4,t_y4,null,.5,0.0,1.0,1.0,0.0,1.0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1799>";
	this.p_DrawTriangle(t_x1,t_y1,t_x2,t_y2,t_x4,t_y4,null,.5,0.0,1.0,1.0,0.0,1.0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1801>";
	pop_err();
	return true;
}
c_DrawList.prototype.p_DrawShadows=function(t_x0,t_y0,t_drawList){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1806>";
	var t_lx=t_x0*this.m__ix+t_y0*this.m__jx+this.m__tx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1807>";
	var t_ly=t_x0*this.m__iy+t_y0*this.m__jy+this.m__ty;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1809>";
	var t_verts=dbg_object(t_drawList).m__casterVerts.p_Data();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1809>";
	var t_v0=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1811>";
	for(var t_i=0;t_i<dbg_object(t_drawList).m__casters.p_Length2();t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1813>";
		var t_caster=dbg_object(t_drawList).m__casters.p_Get(t_i);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1814>";
		var t_n=dbg_object(t_caster).m__verts.length;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1816>";
		var t_3=dbg_object(t_caster).m__type;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1817>";
		if(t_3==0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1818>";
			var t_x02=dbg_array(t_verts,t_v0+t_n-2)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1819>";
			var t_y02=dbg_array(t_verts,t_v0+t_n-1)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1820>";
			for(var t_i2=0;t_i2<t_n-1;t_i2=t_i2+2){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1821>";
				var t_x1=dbg_array(t_verts,t_v0+t_i2)[dbg_index];
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1822>";
				var t_y1=dbg_array(t_verts,t_v0+t_i2+1)[dbg_index];
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1823>";
				this.p_DrawShadow(t_lx,t_ly,t_x02,t_y02,t_x1,t_y1);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1824>";
				t_x02=t_x1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1825>";
				t_y02=t_y1;
			}
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1827>";
			if(t_3==1){
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1828>";
				if(t_3==2){
				}
			}
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1831>";
		t_v0+=t_n;
	}
	pop_err();
}
c_DrawList.prototype.p_BlendMode=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1373>";
	pop_err();
	return this.m__blend;
}
function c_Canvas(){
	c_DrawList.call(this);
	this.m__dirty=-1;
	this.m__lights=new_object_array(4);
	this.m__seq=0;
	this.m__texture=null;
	this.m__width=0;
	this.m__height=0;
	this.m__twidth=0;
	this.m__theight=0;
	this.m__image=null;
	this.m__viewport=[0,0,640,480];
	this.m__vpx=0;
	this.m__vpy=0;
	this.m__vpw=0;
	this.m__vph=0;
	this.m__scissor=[0,0,10000,10000];
	this.m__scx=0;
	this.m__scy=0;
	this.m__scw=0;
	this.m__sch=0;
	this.m__clsScissor=false;
	this.m__projMatrix=bb_math3d_Mat4New();
	this.m__viewMatrix=bb_math3d_Mat4New();
	this.m__modelMatrix=bb_math3d_Mat4New();
	this.m__ambientLight=[0.0,0.0,0.0,1.0];
	this.m__fogColor=[0.0,0.0,0.0,0.0];
	this.m__shadowMap=null;
	this.m__lineWidth=1.0;
	this.m__colorMask=[true,true,true,true];
}
c_Canvas.prototype=extend_class(c_DrawList);
c_Canvas.prototype.p_Init3=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2545>";
	this.m__dirty=-1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2546>";
	for(var t_i=0;t_i<4;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2547>";
		dbg_array(this.m__lights,t_i)[dbg_index]=c_LightData.m_new.call(new c_LightData);
	}
	pop_err();
}
c_Canvas.m__active=null;
c_Canvas.prototype.p_Flush=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2464>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2466>";
	if(!((this.m__texture)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2468>";
	if((dbg_object(this.m__texture).m__flags&256)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2469>";
		this.p_Validate();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2471>";
		gl.disable(3089);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2472>";
		gl.viewport(0,0,this.m__twidth,this.m__theight);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2474>";
		if(this.m__width==this.m__twidth && this.m__height==this.m__theight){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2475>";
			_glReadPixels(0,0,this.m__twidth,this.m__theight,6408,5121,object_downcast((dbg_object(this.m__texture).m__data),c_DataBuffer),0);
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2477>";
			for(var t_y=0;t_y<this.m__height;t_y=t_y+1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2478>";
				_glReadPixels(dbg_object(this.m__image).m__x,dbg_object(this.m__image).m__y+t_y,this.m__width,1,6408,5121,object_downcast((dbg_object(this.m__texture).m__data),c_DataBuffer),(dbg_object(this.m__image).m__y+t_y)*(this.m__twidth*4)+dbg_object(this.m__image).m__x*4);
			}
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2482>";
		this.m__dirty|=2;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2485>";
	this.m__texture.p_UpdateMipmaps();
	pop_err();
}
c_Canvas.prototype.p_Validate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2558>";
	if(this.m__seq!=webglGraphicsSeq){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2559>";
		this.m__seq=webglGraphicsSeq;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2560>";
		bb_graphics2_InitVbos();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2561>";
		if(!((this.m__texture)!=null)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2562>";
			this.m__width=bb_app_DeviceWidth();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2563>";
			this.m__height=bb_app_DeviceHeight();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2564>";
			this.m__twidth=this.m__width;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2565>";
			this.m__theight=this.m__height;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2567>";
		this.m__dirty=-1;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2570>";
	if(c_Canvas.m__active==this){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2571>";
		if(!((this.m__dirty)!=0)){
			pop_err();
			return;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2573>";
		if((c_Canvas.m__active)!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2573>";
			c_Canvas.m__active.p_Flush();
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2574>";
		c_Canvas.m__active=this;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2575>";
		this.m__dirty=-1;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2580>";
	if((this.m__dirty&1)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2582>";
		if((this.m__texture)!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2583>";
			_glBindFramebuffer(36160,this.m__texture.p_GLFramebuffer());
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2585>";
			_glBindFramebuffer(36160,bb_graphics2_defaultFbo);
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2589>";
	if((this.m__dirty&2)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2591>";
		if(!((this.m__texture)!=null)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2592>";
			this.m__width=bb_app_DeviceWidth();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2593>";
			this.m__height=bb_app_DeviceHeight();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2594>";
			this.m__twidth=this.m__width;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2595>";
			this.m__theight=this.m__height;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2598>";
		this.m__vpx=dbg_array(this.m__viewport,0)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2598>";
		this.m__vpy=dbg_array(this.m__viewport,1)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2598>";
		this.m__vpw=dbg_array(this.m__viewport,2)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2598>";
		this.m__vph=dbg_array(this.m__viewport,3)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2599>";
		if((this.m__image)!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2600>";
			this.m__vpx+=dbg_object(this.m__image).m__x;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2601>";
			this.m__vpy+=dbg_object(this.m__image).m__y;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2604>";
		this.m__scx=dbg_array(this.m__scissor,0)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2604>";
		this.m__scy=dbg_array(this.m__scissor,1)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2604>";
		this.m__scw=dbg_array(this.m__scissor,2)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2604>";
		this.m__sch=dbg_array(this.m__scissor,3)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2606>";
		if(this.m__scx<0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2606>";
			this.m__scx=0;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2606>";
			if(this.m__scx>this.m__vpw){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2606>";
				this.m__scx=this.m__vpw;
			}
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2607>";
		if(this.m__scw<0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2607>";
			this.m__scw=0;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2607>";
			if(this.m__scx+this.m__scw>this.m__vpw){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2607>";
				this.m__scw=this.m__vpw-this.m__scx;
			}
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2609>";
		if(this.m__scy<0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2609>";
			this.m__scy=0;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2609>";
			if(this.m__scy>this.m__vph){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2609>";
				this.m__scy=this.m__vph;
			}
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2610>";
		if(this.m__sch<0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2610>";
			this.m__sch=0;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2610>";
			if(this.m__scy+this.m__sch>this.m__vph){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2610>";
				this.m__sch=this.m__vph-this.m__scy;
			}
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2612>";
		this.m__scx+=this.m__vpx;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2612>";
		this.m__scy+=this.m__vpy;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2614>";
		if(!((this.m__texture)!=null)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2615>";
			this.m__vpy=this.m__theight-this.m__vpy-this.m__vph;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2616>";
			this.m__scy=this.m__theight-this.m__scy-this.m__sch;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2619>";
		gl.viewport(this.m__vpx,this.m__vpy,this.m__vpw,this.m__vph);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2621>";
		if(this.m__scx!=this.m__vpx || this.m__scy!=this.m__vpy || this.m__scw!=this.m__vpw || this.m__sch!=this.m__vph){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2622>";
			gl.enable(3089);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2623>";
			gl.scissor(this.m__scx,this.m__scy,this.m__scw,this.m__sch);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2624>";
			this.m__clsScissor=false;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2626>";
			gl.disable(3089);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2627>";
			this.m__clsScissor=this.m__scx!=0 || this.m__scy!=0 || this.m__vpw!=this.m__twidth || this.m__vph!=this.m__theight;
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2632>";
	if((this.m__dirty&4)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2634>";
		bb_graphics2_rs_program=null;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2636>";
		if((this.m__texture)!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2637>";
			dbg_array(bb_graphics2_rs_clipPosScale,1)[dbg_index]=1.0;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2638>";
			bb_math3d_Mat4Copy(this.m__projMatrix,bb_graphics2_rs_projMatrix);
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2640>";
			dbg_array(bb_graphics2_rs_clipPosScale,1)[dbg_index]=-1.0;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2641>";
			bb_math3d_Mat4Multiply(bb_graphics2_flipYMatrix,this.m__projMatrix,bb_graphics2_rs_projMatrix);
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2644>";
		bb_math3d_Mat4Multiply(this.m__viewMatrix,this.m__modelMatrix,bb_graphics2_rs_modelViewMatrix);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2645>";
		bb_math3d_Mat4Multiply(bb_graphics2_rs_projMatrix,bb_graphics2_rs_modelViewMatrix,bb_graphics2_rs_modelViewProjMatrix);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2646>";
		bb_math3d_Vec4Copy(this.m__ambientLight,bb_graphics2_rs_ambientLight);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2647>";
		bb_math3d_Vec4Copy(this.m__fogColor,bb_graphics2_rs_fogColor);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2649>";
		bb_graphics2_rs_numLights=0;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2650>";
		for(var t_i=0;t_i<4;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2652>";
			var t_light=dbg_array(this.m__lights,t_i)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2653>";
			if(!((dbg_object(t_light).m_type)!=0)){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2653>";
				continue;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2655>";
			bb_math3d_Mat4Transform(this.m__viewMatrix,dbg_object(t_light).m_vector,dbg_object(t_light).m_tvector);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2657>";
			dbg_array(bb_graphics2_rs_lightColors,bb_graphics2_rs_numLights*4+0)[dbg_index]=dbg_array(dbg_object(t_light).m_color,0)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2658>";
			dbg_array(bb_graphics2_rs_lightColors,bb_graphics2_rs_numLights*4+1)[dbg_index]=dbg_array(dbg_object(t_light).m_color,1)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2659>";
			dbg_array(bb_graphics2_rs_lightColors,bb_graphics2_rs_numLights*4+2)[dbg_index]=dbg_array(dbg_object(t_light).m_color,2)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2660>";
			dbg_array(bb_graphics2_rs_lightColors,bb_graphics2_rs_numLights*4+3)[dbg_index]=dbg_array(dbg_object(t_light).m_color,3)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2662>";
			dbg_array(bb_graphics2_rs_lightVectors,bb_graphics2_rs_numLights*4+0)[dbg_index]=dbg_array(dbg_object(t_light).m_tvector,0)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2663>";
			dbg_array(bb_graphics2_rs_lightVectors,bb_graphics2_rs_numLights*4+1)[dbg_index]=dbg_array(dbg_object(t_light).m_tvector,1)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2664>";
			dbg_array(bb_graphics2_rs_lightVectors,bb_graphics2_rs_numLights*4+2)[dbg_index]=dbg_array(dbg_object(t_light).m_tvector,2)[dbg_index];
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2665>";
			dbg_array(bb_graphics2_rs_lightVectors,bb_graphics2_rs_numLights*4+3)[dbg_index]=dbg_object(t_light).m_range;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2667>";
			bb_graphics2_rs_numLights+=1;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2670>";
		if((this.m__shadowMap)!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2671>";
			bb_graphics2_rs_shadowTexture=dbg_object(dbg_object(this.m__shadowMap).m__material).m__colorTexture;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2673>";
			bb_graphics2_rs_shadowTexture=null;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2676>";
		bb_graphics2_rs_blend=-1;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2680>";
	if((this.m__dirty&8)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2681>";
		gl.lineWidth(this.m__lineWidth);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2684>";
	if((this.m__dirty&16)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2685>";
		gl.colorMask(dbg_array(this.m__colorMask,0)[dbg_index],dbg_array(this.m__colorMask,1)[dbg_index],dbg_array(this.m__colorMask,2)[dbg_index],dbg_array(this.m__colorMask,3)[dbg_index]);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2688>";
	this.m__dirty=0;
	pop_err();
}
c_Canvas.prototype.p_FlushPrims=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2552>";
	if(c_DrawList.prototype.p_IsEmpty.call(this)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2553>";
	this.p_Validate();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2554>";
	c_DrawList.prototype.p_Flush.call(this);
	pop_err();
}
c_Canvas.prototype.p_SetRenderTarget2=function(t_target){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2115>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2117>";
	if(!((t_target)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2119>";
		this.m__image=null;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2120>";
		this.m__texture=null;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2121>";
		this.m__width=bb_app_DeviceWidth();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2122>";
		this.m__height=bb_app_DeviceHeight();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2123>";
		this.m__twidth=this.m__width;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2124>";
		this.m__theight=this.m__height;
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2126>";
		if((object_downcast((t_target),c_Image2))!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2128>";
			this.m__image=object_downcast((t_target),c_Image2);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2129>";
			this.m__texture=this.m__image.p_Material().p_ColorTexture();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2130>";
			if(!((this.m__texture.p_Flags()&16)!=0)){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2130>";
				error("Texture is not a render target texture");
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2131>";
			this.m__width=this.m__image.p_Width();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2132>";
			this.m__height=this.m__image.p_Height();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2133>";
			this.m__twidth=this.m__texture.p_Width();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2134>";
			this.m__theight=this.m__texture.p_Height();
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2136>";
			if((object_downcast((t_target),c_Texture))!=null){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2138>";
				this.m__image=null;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2139>";
				this.m__texture=object_downcast((t_target),c_Texture);
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2140>";
				if(!((this.m__texture.p_Flags()&16)!=0)){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2140>";
					error("Texture is not a render target texture");
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2141>";
				this.m__width=this.m__texture.p_Width();
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2142>";
				this.m__height=this.m__texture.p_Height();
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2143>";
				this.m__twidth=this.m__texture.p_Width();
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2144>";
				this.m__theight=this.m__texture.p_Height();
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2148>";
				error("RenderTarget object must an Image, a Texture or Null");
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2152>";
	this.m__dirty=-1;
	pop_err();
}
c_Canvas.prototype.p_SetViewport=function(t_x,t_y,t_w,t_h){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2182>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2183>";
	dbg_array(this.m__viewport,0)[dbg_index]=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2184>";
	dbg_array(this.m__viewport,1)[dbg_index]=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2185>";
	dbg_array(this.m__viewport,2)[dbg_index]=t_w;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2186>";
	dbg_array(this.m__viewport,3)[dbg_index]=t_h;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2187>";
	this.m__dirty|=2;
	pop_err();
}
c_Canvas.prototype.p_SetProjection2d=function(t_left,t_right,t_top,t_bottom,t_znear,t_zfar){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2218>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2219>";
	bb_math3d_Mat4Ortho(t_left,t_right,t_top,t_bottom,t_znear,t_zfar,this.m__projMatrix);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2220>";
	this.m__dirty|=4;
	pop_err();
}
c_Canvas.m_new=function(t_target){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2103>";
	c_DrawList.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2104>";
	this.p_Init3();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2105>";
	this.p_SetRenderTarget2(t_target);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2106>";
	this.p_SetViewport(0,0,this.m__width,this.m__height);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2107>";
	this.p_SetProjection2d(0.0,(this.m__width),0.0,(this.m__height),-1.0,1.0);
	pop_err();
	return this;
}
c_Canvas.prototype.p_Width=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2161>";
	pop_err();
	return this.m__width;
}
c_Canvas.prototype.p_Height=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2165>";
	pop_err();
	return this.m__height;
}
c_Canvas.prototype.p_Clear2=function(t_r,t_g,t_b,t_a){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2354>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2355>";
	this.p_Validate();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2356>";
	if(this.m__clsScissor){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2357>";
		gl.enable(3089);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2358>";
		gl.scissor(this.m__vpx,this.m__vpy,this.m__vpw,this.m__vph);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2360>";
	gl.clearColor(t_r,t_g,t_b,t_a);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2361>";
	gl.clear(16384);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2362>";
	if(this.m__clsScissor){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2362>";
		gl.disable(3089);
	}
	pop_err();
}
c_Canvas.prototype.p_SetLightType=function(t_index,t_type){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2282>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2283>";
	var t_light=dbg_array(this.m__lights,t_index)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2284>";
	dbg_object(t_light).m_type=t_type;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2285>";
	this.m__dirty|=4;
	pop_err();
}
c_Canvas.prototype.p_SetShadowMap=function(t_image){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2334>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2335>";
	this.m__shadowMap=t_image;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2336>";
	this.m__dirty|=4;
	pop_err();
}
c_Canvas.prototype.p_SetProjectionMatrix=function(t_projMatrix){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2208>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2209>";
	if((t_projMatrix).length!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2210>";
		bb_math3d_Mat4Copy(t_projMatrix,this.m__projMatrix);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2212>";
		bb_math3d_Mat4Init3(this.m__projMatrix);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2214>";
	this.m__dirty|=4;
	pop_err();
}
c_Canvas.prototype.p_SetViewMatrix=function(t_viewMatrix){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2228>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2229>";
	if((t_viewMatrix).length!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2230>";
		bb_math3d_Mat4Copy(t_viewMatrix,this.m__viewMatrix);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2232>";
		bb_math3d_Mat4Init3(this.m__viewMatrix);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2234>";
	this.m__dirty|=4;
	pop_err();
}
c_Canvas.prototype.p_SetModelMatrix=function(t_modelMatrix){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2242>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2243>";
	if((t_modelMatrix).length!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2244>";
		bb_math3d_Mat4Copy(t_modelMatrix,this.m__modelMatrix);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2246>";
		bb_math3d_Mat4Init3(this.m__modelMatrix);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2248>";
	this.m__dirty|=4;
	pop_err();
}
c_Canvas.prototype.p_SetAmbientLight2=function(t_r,t_g,t_b,t_a){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2256>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2257>";
	dbg_array(this.m__ambientLight,0)[dbg_index]=t_r;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2258>";
	dbg_array(this.m__ambientLight,1)[dbg_index]=t_g;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2259>";
	dbg_array(this.m__ambientLight,2)[dbg_index]=t_b;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2260>";
	dbg_array(this.m__ambientLight,3)[dbg_index]=t_a;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2261>";
	this.m__dirty|=4;
	pop_err();
}
c_Canvas.prototype.p_SetFogColor=function(t_r,t_g,t_b,t_a){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2269>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2270>";
	dbg_array(this.m__fogColor,0)[dbg_index]=t_r;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2271>";
	dbg_array(this.m__fogColor,1)[dbg_index]=t_g;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2272>";
	dbg_array(this.m__fogColor,2)[dbg_index]=t_b;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2273>";
	dbg_array(this.m__fogColor,3)[dbg_index]=t_a;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2274>";
	this.m__dirty|=4;
	pop_err();
}
c_Canvas.prototype.p_RenderDrawList=function(t_drawbuf){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2381>";
	var t_fast=this.m__ix==1.0 && this.m__iy==0.0 && this.m__jx==0.0 && this.m__jy==1.0 && this.m__tx==0.0 && this.m__ty==0.0 && dbg_array(this.m__color,0)[dbg_index]==1.0 && dbg_array(this.m__color,1)[dbg_index]==1.0 && dbg_array(this.m__color,2)[dbg_index]==1.0 && dbg_array(this.m__color,3)[dbg_index]==1.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2383>";
	if(t_fast){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2384>";
		this.p_FlushPrims();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2385>";
		this.p_Validate();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2386>";
		t_drawbuf.p_Render();
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2390>";
	dbg_array(bb_graphics2_tmpMat3d,0)[dbg_index]=this.m__ix;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2391>";
	dbg_array(bb_graphics2_tmpMat3d,1)[dbg_index]=this.m__iy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2392>";
	dbg_array(bb_graphics2_tmpMat3d,4)[dbg_index]=this.m__jx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2393>";
	dbg_array(bb_graphics2_tmpMat3d,5)[dbg_index]=this.m__jy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2394>";
	dbg_array(bb_graphics2_tmpMat3d,12)[dbg_index]=this.m__tx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2395>";
	dbg_array(bb_graphics2_tmpMat3d,13)[dbg_index]=this.m__ty;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2396>";
	dbg_array(bb_graphics2_tmpMat3d,10)[dbg_index]=1.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2397>";
	dbg_array(bb_graphics2_tmpMat3d,15)[dbg_index]=1.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2399>";
	bb_math3d_Mat4Multiply(this.m__modelMatrix,bb_graphics2_tmpMat3d,bb_graphics2_tmpMat3d2);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2401>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2403>";
	var t_tmp=this.m__modelMatrix;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2404>";
	this.m__modelMatrix=bb_graphics2_tmpMat3d2;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2405>";
	dbg_array(bb_graphics2_rs_globalColor,0)[dbg_index]=dbg_array(this.m__color,0)[dbg_index]*dbg_array(this.m__color,3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2406>";
	dbg_array(bb_graphics2_rs_globalColor,1)[dbg_index]=dbg_array(this.m__color,1)[dbg_index]*dbg_array(this.m__color,3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2407>";
	dbg_array(bb_graphics2_rs_globalColor,2)[dbg_index]=dbg_array(this.m__color,2)[dbg_index]*dbg_array(this.m__color,3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2408>";
	dbg_array(bb_graphics2_rs_globalColor,3)[dbg_index]=dbg_array(this.m__color,3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2409>";
	this.m__dirty|=4;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2411>";
	this.p_Validate();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2412>";
	t_drawbuf.p_Render();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2414>";
	this.m__modelMatrix=t_tmp;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2415>";
	dbg_array(bb_graphics2_rs_globalColor,0)[dbg_index]=1.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2416>";
	dbg_array(bb_graphics2_rs_globalColor,1)[dbg_index]=1.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2417>";
	dbg_array(bb_graphics2_rs_globalColor,2)[dbg_index]=1.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2418>";
	dbg_array(bb_graphics2_rs_globalColor,3)[dbg_index]=1.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2419>";
	this.m__dirty|=4;
	pop_err();
}
c_Canvas.prototype.p_RenderDrawList2=function(t_drawList,t_tx,t_ty,t_rz,t_sx,t_sy){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2423>";
	c_DrawList.prototype.p_PushMatrix.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2424>";
	c_DrawList.prototype.p_TranslateRotateScale.call(this,t_tx,t_ty,t_rz,t_sx,t_sy);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2425>";
	this.p_RenderDrawList(t_drawList);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2426>";
	c_DrawList.prototype.p_PopMatrix.call(this);
	pop_err();
}
c_Canvas.prototype.p_SetColorMask=function(t_r,t_g,t_b,t_a){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2169>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2170>";
	dbg_array(this.m__colorMask,0)[dbg_index]=t_r;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2171>";
	dbg_array(this.m__colorMask,1)[dbg_index]=t_g;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2172>";
	dbg_array(this.m__colorMask,2)[dbg_index]=t_b;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2173>";
	dbg_array(this.m__colorMask,3)[dbg_index]=t_a;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2174>";
	this.m__dirty|=16;
	pop_err();
}
c_Canvas.prototype.p_SetLightColor=function(t_index,t_r,t_g,t_b,t_a){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2293>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2294>";
	var t_light=dbg_array(this.m__lights,t_index)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2295>";
	dbg_array(dbg_object(t_light).m_color,0)[dbg_index]=t_r;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2296>";
	dbg_array(dbg_object(t_light).m_color,1)[dbg_index]=t_g;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2297>";
	dbg_array(dbg_object(t_light).m_color,2)[dbg_index]=t_b;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2298>";
	dbg_array(dbg_object(t_light).m_color,3)[dbg_index]=t_a;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2299>";
	this.m__dirty|=4;
	pop_err();
}
c_Canvas.prototype.p_SetLightPosition=function(t_index,t_x,t_y,t_z){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2307>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2308>";
	var t_light=dbg_array(this.m__lights,t_index)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2309>";
	dbg_array(dbg_object(t_light).m_position,0)[dbg_index]=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2310>";
	dbg_array(dbg_object(t_light).m_position,1)[dbg_index]=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2311>";
	dbg_array(dbg_object(t_light).m_position,2)[dbg_index]=t_z;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2312>";
	dbg_array(dbg_object(t_light).m_vector,0)[dbg_index]=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2313>";
	dbg_array(dbg_object(t_light).m_vector,1)[dbg_index]=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2314>";
	dbg_array(dbg_object(t_light).m_vector,2)[dbg_index]=t_z;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2315>";
	this.m__dirty|=4;
	pop_err();
}
c_Canvas.prototype.p_SetLightRange=function(t_index,t_range){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2323>";
	this.p_FlushPrims();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2324>";
	var t_light=dbg_array(this.m__lights,t_index)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2325>";
	dbg_object(t_light).m_range=t_range;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<2326>";
	this.m__dirty|=4;
	pop_err();
}
var bb_graphics2_inited=false;
var bb_graphics2_vbosSeq=0;
var bb_graphics2_rs_vbo=0;
function c_DataBuffer(){
	BBDataBuffer.call(this);
}
c_DataBuffer.prototype=extend_class(BBDataBuffer);
c_DataBuffer.m_new=function(t_length,t_direct){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<102>";
	if(!this._New(t_length)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<102>";
		error("Allocate DataBuffer failed");
	}
	pop_err();
	return this;
}
c_DataBuffer.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<96>";
	pop_err();
	return this;
}
c_DataBuffer.prototype.p_CopyBytes=function(t_address,t_dst,t_dstaddress,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<123>";
	if(t_address+t_count>this.Length()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<123>";
		t_count=this.Length()-t_address;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<124>";
	if(t_dstaddress+t_count>t_dst.Length()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<124>";
		t_count=t_dst.Length()-t_dstaddress;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<126>";
	if(t_dstaddress<=t_address){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<127>";
		for(var t_i=0;t_i<t_count;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<128>";
			t_dst.PokeByte(t_dstaddress+t_i,this.PeekByte(t_address+t_i));
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<131>";
		for(var t_i2=t_count-1;t_i2>=0;t_i2=t_i2+-1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/databuffer.monkey<132>";
			t_dst.PokeByte(t_dstaddress+t_i2,this.PeekByte(t_address+t_i2));
		}
	}
	pop_err();
}
var bb_graphics2_rs_ibo=0;
function bb_graphics2_InitVbos(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<116>";
	if(bb_graphics2_vbosSeq==webglGraphicsSeq){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<117>";
	bb_graphics2_vbosSeq=webglGraphicsSeq;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<121>";
	bb_graphics2_rs_vbo=gl.createBuffer();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<122>";
	_glBindBuffer(34962,bb_graphics2_rs_vbo);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<123>";
	_glBufferData(34962,65520,null,35040);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<124>";
	gl.enableVertexAttribArray(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<124>";
	gl.vertexAttribPointer(0,2,5126,false,28,0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<125>";
	gl.enableVertexAttribArray(1);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<125>";
	gl.vertexAttribPointer(1,2,5126,false,28,8);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<126>";
	gl.enableVertexAttribArray(2);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<126>";
	gl.vertexAttribPointer(2,2,5126,false,28,16);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<127>";
	gl.enableVertexAttribArray(3);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<127>";
	gl.vertexAttribPointer(3,4,5121,true,28,24);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<129>";
	bb_graphics2_rs_ibo=gl.createBuffer();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<130>";
	_glBindBuffer(34963,bb_graphics2_rs_ibo);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<131>";
	var t_idxs=c_DataBuffer.m_new.call(new c_DataBuffer,28080,true);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<132>";
	for(var t_j=0;t_j<4;t_j=t_j+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<133>";
		var t_k=t_j*3510*2;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<134>";
		for(var t_i=0;t_i<585;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<135>";
			t_idxs.PokeShort(t_i*12+t_k+0,t_i*4+t_j+0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<136>";
			t_idxs.PokeShort(t_i*12+t_k+2,t_i*4+t_j+1);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<137>";
			t_idxs.PokeShort(t_i*12+t_k+4,t_i*4+t_j+2);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<138>";
			t_idxs.PokeShort(t_i*12+t_k+6,t_i*4+t_j+0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<139>";
			t_idxs.PokeShort(t_i*12+t_k+8,t_i*4+t_j+2);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<140>";
			t_idxs.PokeShort(t_i*12+t_k+10,t_i*4+t_j+3);
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<143>";
	_glBufferData(34963,t_idxs.Length(),t_idxs,35044);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<144>";
	t_idxs.Discard();
	pop_err();
}
var bb_graphics2_tmpi=[];
var bb_graphics2_defaultFbo=0;
var bb_graphics2_mainShader="";
function c_Shader(){
	Object.call(this);
	this.m__source="";
	this.m__vsource="";
	this.m__fsource="";
	this.m__uniforms=c_StringSet.m_new.call(new c_StringSet);
	this.m__glPrograms=new_object_array(5);
	this.m__defaultMaterial=null;
	this.m__seq=0;
}
c_Shader.prototype.p_Build=function(t_numLights){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<691>";
	var t_defs="";
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<692>";
	t_defs=t_defs+("#define NUM_LIGHTS "+String(t_numLights)+"\n");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<694>";
	var t_vshader=bb_glutil_glCompile(35633,t_defs+this.m__vsource);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<695>";
	var t_fshader=bb_glutil_glCompile(35632,t_defs+this.m__fsource);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<697>";
	var t_program=gl.createProgram();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<698>";
	gl.attachShader(t_program,t_vshader);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<699>";
	gl.attachShader(t_program,t_fshader);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<700>";
	gl.deleteShader(t_vshader);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<701>";
	gl.deleteShader(t_fshader);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<703>";
	gl.bindAttribLocation(t_program,0,"Position");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<704>";
	gl.bindAttribLocation(t_program,1,"Texcoord0");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<705>";
	gl.bindAttribLocation(t_program,2,"Tangent");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<706>";
	gl.bindAttribLocation(t_program,3,"Color");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<708>";
	bb_glutil_glLink(t_program);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<711>";
	var t_matuniforms=c_Stack7.m_new.call(new c_Stack7);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<712>";
	var t_size=new_number_array(1);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<712>";
	var t_type=new_number_array(1);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<712>";
	var t_name=new_string_array(1);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<713>";
	_glGetProgramiv(t_program,35718,bb_graphics2_tmpi);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<714>";
	for(var t_i=0;t_i<dbg_array(bb_graphics2_tmpi,0)[dbg_index];t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<716>";
		_glGetActiveUniform(t_program,t_i,t_size,t_type,t_name);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<718>";
		if(this.m__uniforms.p_Contains2(dbg_array(t_name,0)[dbg_index])){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<720>";
			var t_location=_glGetUniformLocation(t_program,dbg_array(t_name,0)[dbg_index]);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<721>";
			if(t_location==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<721>";
				continue;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<723>";
			t_matuniforms.p_Push19(c_GLUniform.m_new.call(new c_GLUniform,dbg_array(t_name,0)[dbg_index],t_location,dbg_array(t_size,0)[dbg_index],dbg_array(t_type,0)[dbg_index]));
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<729>";
	var t_=c_GLProgram.m_new.call(new c_GLProgram,t_program,t_matuniforms.p_ToArray());
	pop_err();
	return t_;
}
c_Shader.prototype.p_Build2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<734>";
	bb_graphics2_InitMojo2();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<736>";
	var t_p=c_GlslParser.m_new.call(new c_GlslParser,this.m__source);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<738>";
	var t_vars=c_StringSet.m_new.call(new c_StringSet);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<740>";
	while((t_p.p_Toke()).length!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<742>";
		if(t_p.p_CParse("uniform")){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<744>";
			var t_ty=t_p.p_ParseType();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<745>";
			var t_id=t_p.p_ParseIdent();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<746>";
			t_p.p_Parse2(";");
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<747>";
			this.m__uniforms.p_Insert3(t_id);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<749>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<752>";
		var t_id2=t_p.p_CParseIdent();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<753>";
		if((t_id2).length!=0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<754>";
			if(string_startswith(t_id2,"gl_")){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<755>";
				t_vars.p_Insert3("B3D_"+t_id2.toUpperCase());
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<756>";
				if(string_startswith(t_id2,"b3d_")){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<757>";
					t_vars.p_Insert3(t_id2.toUpperCase());
				}
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<759>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<762>";
		t_p.p_Bump();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<765>";
	var t_vardefs="";
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<766>";
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<766>";
	var t_=t_vars.p_ObjectEnumerator();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<766>";
	while(t_.p_HasNext()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<766>";
		var t_var=t_.p_NextObject();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<767>";
		t_vardefs=t_vardefs+("#define "+t_var+" 1\n");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<772>";
	var t_source=bb_graphics2_mainShader;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<773>";
	var t_i0=t_source.indexOf("//@vertex",0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<774>";
	if(t_i0==-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<774>";
		error("Can't find //@vertex chunk");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<775>";
	var t_i1=t_source.indexOf("//@fragment",0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<776>";
	if(t_i1==-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<776>";
		error("Can't find //@fragment chunk");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<778>";
	var t_header=t_vardefs+t_source.slice(0,t_i0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<779>";
	this.m__vsource=t_header+t_source.slice(t_i0,t_i1);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<780>";
	this.m__fsource=t_header+string_replace(t_source.slice(t_i1),"${SHADER}",this.m__source);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<782>";
	for(var t_numLights=0;t_numLights<=4;t_numLights=t_numLights+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<784>";
		dbg_array(this.m__glPrograms,t_numLights)[dbg_index]=this.p_Build(t_numLights);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<786>";
		if(((t_numLights)!=0) || t_vars.p_Contains2("B3D_DIFFUSE") || t_vars.p_Contains2("B3D_SPECULAR")){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<786>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<788>";
		for(var t_i=1;t_i<=4;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<789>";
			dbg_array(this.m__glPrograms,t_i)[dbg_index]=dbg_array(this.m__glPrograms,0)[dbg_index];
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<792>";
		break;
	}
	pop_err();
}
c_Shader.prototype.p_Build3=function(t_source){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<636>";
	this.m__source=t_source;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<637>";
	this.p_Build2();
	pop_err();
}
c_Shader.m_new=function(t_source){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<596>";
	this.p_Build3(t_source);
	pop_err();
	return this;
}
c_Shader.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<593>";
	pop_err();
	return this;
}
c_Shader.prototype.p_OnInitMaterial=function(t_material){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<641>";
	t_material.p_SetTexture("ColorTexture",c_Texture.m_White());
	pop_err();
}
c_Shader.prototype.p_OnLoadMaterial=function(t_material,t_path,t_texFlags){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<645>";
	var t_texture=c_Texture.m_Load(t_path,4,t_texFlags);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<646>";
	if(!((t_texture)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<646>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<647>";
	t_material.p_SetTexture("ColorTexture",t_texture);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<648>";
	if((t_texture)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<648>";
		t_texture.p_Release();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<649>";
	pop_err();
	return t_material;
}
c_Shader.prototype.p_DefaultMaterial=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<600>";
	if(!((this.m__defaultMaterial)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<600>";
		this.m__defaultMaterial=c_Material.m_new.call(new c_Material,this);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<601>";
	pop_err();
	return this.m__defaultMaterial;
}
c_Shader.prototype.p_GLProgram=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<680>";
	if(this.m__seq!=webglGraphicsSeq){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<681>";
		this.m__seq=webglGraphicsSeq;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<682>";
		bb_graphics2_rs_program=null;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<683>";
		this.p_Build2();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<686>";
	pop_err();
	return dbg_array(this.m__glPrograms,bb_graphics2_rs_numLights)[dbg_index];
}
c_Shader.prototype.p_Bind=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<668>";
	var t_program=this.p_GLProgram();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<670>";
	if(t_program==bb_graphics2_rs_program){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<672>";
	bb_graphics2_rs_program=t_program;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<673>";
	bb_graphics2_rs_material=null;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<675>";
	t_program.p_Bind();
	pop_err();
}
c_Shader.m_ShadowShader=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<617>";
	pop_err();
	return bb_graphics2_shadowShader;
}
c_Shader.m_FastShader=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<605>";
	pop_err();
	return bb_graphics2_fastShader;
}
function c_GLProgram(){
	Object.call(this);
	this.m_program=0;
	this.m_matuniforms=[];
	this.m_mvpMatrix=0;
	this.m_mvMatrix=0;
	this.m_clipPosScale=0;
	this.m_globalColor=0;
	this.m_fogColor=0;
	this.m_ambientLight=0;
	this.m_lightColors=0;
	this.m_lightVectors=0;
	this.m_shadowTexture=0;
}
c_GLProgram.m_new=function(t_program,t_matuniforms){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<552>";
	dbg_object(this).m_program=t_program;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<553>";
	dbg_object(this).m_matuniforms=t_matuniforms;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<554>";
	this.m_mvpMatrix=_glGetUniformLocation(t_program,"ModelViewProjectionMatrix");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<555>";
	this.m_mvMatrix=_glGetUniformLocation(t_program,"ModelViewMatrix");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<556>";
	this.m_clipPosScale=_glGetUniformLocation(t_program,"ClipPosScale");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<557>";
	this.m_globalColor=_glGetUniformLocation(t_program,"GlobalColor");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<558>";
	this.m_fogColor=_glGetUniformLocation(t_program,"FogColor");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<559>";
	this.m_ambientLight=_glGetUniformLocation(t_program,"AmbientLight");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<560>";
	this.m_lightColors=_glGetUniformLocation(t_program,"LightColors");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<561>";
	this.m_lightVectors=_glGetUniformLocation(t_program,"LightVectors");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<562>";
	this.m_shadowTexture=_glGetUniformLocation(t_program,"ShadowTexture");
	pop_err();
	return this;
}
c_GLProgram.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<536>";
	pop_err();
	return this;
}
c_GLProgram.prototype.p_Bind=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<568>";
	gl.useProgram(this.m_program);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<570>";
	if(this.m_mvpMatrix!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<570>";
		_glUniformMatrix4fv(this.m_mvpMatrix,1,false,bb_graphics2_rs_modelViewProjMatrix);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<571>";
	if(this.m_mvMatrix!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<571>";
		_glUniformMatrix4fv(this.m_mvMatrix,1,false,bb_graphics2_rs_modelViewMatrix);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<572>";
	if(this.m_clipPosScale!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<572>";
		_glUniform4fv(this.m_clipPosScale,1,bb_graphics2_rs_clipPosScale);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<573>";
	if(this.m_globalColor!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<573>";
		_glUniform4fv(this.m_globalColor,1,bb_graphics2_rs_globalColor);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<574>";
	if(this.m_fogColor!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<574>";
		_glUniform4fv(this.m_fogColor,1,bb_graphics2_rs_fogColor);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<575>";
	if(this.m_ambientLight!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<575>";
		_glUniform4fv(this.m_ambientLight,1,bb_graphics2_rs_ambientLight);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<576>";
	if(this.m_lightColors!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<576>";
		_glUniform4fv(this.m_lightColors,bb_graphics2_rs_numLights,bb_graphics2_rs_lightColors);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<577>";
	if(this.m_lightVectors!=-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<577>";
		_glUniform4fv(this.m_lightVectors,bb_graphics2_rs_numLights,bb_graphics2_rs_lightVectors);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<579>";
	gl.activeTexture(33991);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<580>";
	if(this.m_shadowTexture!=-1 && ((bb_graphics2_rs_shadowTexture)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<581>";
		_glBindTexture(3553,bb_graphics2_rs_shadowTexture.p_GLTexture());
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<582>";
		gl.uniform1i(this.m_shadowTexture,7);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<584>";
		_glBindTexture(3553,c_Texture.m_White().p_GLTexture());
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<586>";
	gl.activeTexture(33984);
	pop_err();
}
var bb_glutil_tmpi=[];
function bb_glutil_glCompile(t_type,t_source){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<37>";
	t_source="precision mediump float;\n"+t_source;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<40>";
	var t_shader=gl.createShader(t_type);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<41>";
	gl.shaderSource(t_shader,t_source);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<42>";
	gl.compileShader(t_shader);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<43>";
	_glGetShaderiv(t_shader,35713,bb_glutil_tmpi);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<44>";
	if(!((dbg_array(bb_glutil_tmpi,0)[dbg_index])!=0)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<45>";
		print("Failed to compile fragment shader:"+gl.getShaderInfoLog(t_shader));
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<46>";
		var t_lines=t_source.split("\n");
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<47>";
		for(var t_i=0;t_i<t_lines.length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<48>";
			print(String(t_i+1)+":\t"+dbg_array(t_lines,t_i)[dbg_index]);
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<50>";
		error("Compile fragment shader failed");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<52>";
	pop_err();
	return t_shader;
}
function bb_glutil_glLink(t_program){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<57>";
	gl.linkProgram(t_program);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<58>";
	_glGetProgramiv(t_program,35714,bb_glutil_tmpi);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<59>";
	if(!((dbg_array(bb_glutil_tmpi,0)[dbg_index])!=0)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<59>";
		error("Failed to link program:"+gl.getProgramInfoLog(t_program));
	}
	pop_err();
}
function c_GLUniform(){
	Object.call(this);
	this.m_name="";
	this.m_location=0;
	this.m_size=0;
	this.m_type=0;
}
c_GLUniform.m_new=function(t_name,t_location,t_size,t_type){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<528>";
	dbg_object(this).m_name=t_name;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<529>";
	dbg_object(this).m_location=t_location;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<530>";
	dbg_object(this).m_size=t_size;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<531>";
	dbg_object(this).m_type=t_type;
	pop_err();
	return this;
}
c_GLUniform.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<521>";
	pop_err();
	return this;
}
function c_Stack7(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack7.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack7.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack7.prototype.p_Push19=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack7.prototype.p_Push20=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push19(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack7.prototype.p_Push21=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push20(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
c_Stack7.prototype.p_ToArray=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<18>";
	var t_t=new_object_array(this.m_length);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<19>";
	for(var t_i=0;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<20>";
		dbg_array(t_t,t_i)[dbg_index]=dbg_array(this.m_data,t_i)[dbg_index];
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<22>";
	pop_err();
	return t_t;
}
function c_Set(){
	Object.call(this);
	this.m_map=null;
}
c_Set.m_new=function(t_map){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/set.monkey<16>";
	dbg_object(this).m_map=t_map;
	pop_err();
	return this;
}
c_Set.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/set.monkey<13>";
	pop_err();
	return this;
}
c_Set.prototype.p_Contains2=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/set.monkey<32>";
	var t_=this.m_map.p_Contains2(t_value);
	pop_err();
	return t_;
}
c_Set.prototype.p_Insert3=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/set.monkey<36>";
	this.m_map.p_Insert4(t_value,null);
	pop_err();
	return 0;
}
c_Set.prototype.p_ObjectEnumerator=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/set.monkey<44>";
	var t_=this.m_map.p_Keys().p_ObjectEnumerator();
	pop_err();
	return t_;
}
function c_StringSet(){
	c_Set.call(this);
}
c_StringSet.prototype=extend_class(c_Set);
c_StringSet.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/set.monkey<69>";
	c_Set.m_new.call(this,(c_StringMap.m_new.call(new c_StringMap)));
	pop_err();
	return this;
}
function c_Map2(){
	Object.call(this);
	this.m_root=null;
}
c_Map2.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<7>";
	pop_err();
	return this;
}
c_Map2.prototype.p_Compare2=function(t_lhs,t_rhs){
}
c_Map2.prototype.p_FindNode2=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<157>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<159>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<160>";
		var t_cmp=this.p_Compare2(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<161>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<162>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<163>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<164>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<166>";
				pop_err();
				return t_node;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<169>";
	pop_err();
	return t_node;
}
c_Map2.prototype.p_Contains2=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<25>";
	var t_=this.p_FindNode2(t_key)!=null;
	pop_err();
	return t_;
}
c_Map2.prototype.p_RotateLeft2=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<251>";
	var t_child=dbg_object(t_node).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<252>";
	dbg_object(t_node).m_right=dbg_object(t_child).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<253>";
	if((dbg_object(t_child).m_left)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<254>";
		dbg_object(dbg_object(t_child).m_left).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<256>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<257>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<258>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<259>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<261>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<264>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<266>";
	dbg_object(t_child).m_left=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<267>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map2.prototype.p_RotateRight2=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<271>";
	var t_child=dbg_object(t_node).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<272>";
	dbg_object(t_node).m_left=dbg_object(t_child).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<273>";
	if((dbg_object(t_child).m_right)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<274>";
		dbg_object(dbg_object(t_child).m_right).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<276>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<277>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<278>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<279>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<281>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<284>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<286>";
	dbg_object(t_child).m_right=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<287>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map2.prototype.p_InsertFixup2=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<212>";
	while(((dbg_object(t_node).m_parent)!=null) && dbg_object(dbg_object(t_node).m_parent).m_color==-1 && ((dbg_object(dbg_object(t_node).m_parent).m_parent)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<213>";
		if(dbg_object(t_node).m_parent==dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<214>";
			var t_uncle=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_right;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<215>";
			if(((t_uncle)!=null) && dbg_object(t_uncle).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<216>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<217>";
				dbg_object(t_uncle).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<218>";
				dbg_object(dbg_object(t_uncle).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<219>";
				t_node=dbg_object(t_uncle).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<221>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<222>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<223>";
					this.p_RotateLeft2(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<225>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<226>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<227>";
				this.p_RotateRight2(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<230>";
			var t_uncle2=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<231>";
			if(((t_uncle2)!=null) && dbg_object(t_uncle2).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<232>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<233>";
				dbg_object(t_uncle2).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<234>";
				dbg_object(dbg_object(t_uncle2).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<235>";
				t_node=dbg_object(t_uncle2).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<237>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<238>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<239>";
					this.p_RotateRight2(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<241>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<242>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<243>";
				this.p_RotateLeft2(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<247>";
	dbg_object(this.m_root).m_color=1;
	pop_err();
	return 0;
}
c_Map2.prototype.p_Set3=function(t_key,t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<29>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_parent=null;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_cmp=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<32>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<33>";
		t_parent=t_node;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<34>";
		t_cmp=this.p_Compare2(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<35>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<36>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<37>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<38>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<40>";
				dbg_object(t_node).m_value=t_value;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<41>";
				pop_err();
				return false;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<45>";
	t_node=c_Node2.m_new.call(new c_Node2,t_key,t_value,-1,t_parent);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<47>";
	if((t_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<48>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<49>";
			dbg_object(t_parent).m_right=t_node;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<51>";
			dbg_object(t_parent).m_left=t_node;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<53>";
		this.p_InsertFixup2(t_node);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<55>";
		this.m_root=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<57>";
	pop_err();
	return true;
}
c_Map2.prototype.p_Insert4=function(t_key,t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<146>";
	var t_=this.p_Set3(t_key,t_value);
	pop_err();
	return t_;
}
c_Map2.prototype.p_Keys=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<113>";
	var t_=c_MapKeys.m_new.call(new c_MapKeys,this);
	pop_err();
	return t_;
}
c_Map2.prototype.p_FirstNode=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<125>";
	if(!((this.m_root)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<125>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<127>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<128>";
	while((dbg_object(t_node).m_left)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<129>";
		t_node=dbg_object(t_node).m_left;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<131>";
	pop_err();
	return t_node;
}
function c_StringMap(){
	c_Map2.call(this);
}
c_StringMap.prototype=extend_class(c_Map2);
c_StringMap.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<551>";
	c_Map2.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<551>";
	pop_err();
	return this;
}
c_StringMap.prototype.p_Compare2=function(t_lhs,t_rhs){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<554>";
	var t_=string_compare(t_lhs,t_rhs);
	pop_err();
	return t_;
}
function c_Node2(){
	Object.call(this);
	this.m_key="";
	this.m_right=null;
	this.m_left=null;
	this.m_value=null;
	this.m_color=0;
	this.m_parent=null;
}
c_Node2.m_new=function(t_key,t_value,t_color,t_parent){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<364>";
	dbg_object(this).m_key=t_key;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<365>";
	dbg_object(this).m_value=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<366>";
	dbg_object(this).m_color=t_color;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<367>";
	dbg_object(this).m_parent=t_parent;
	pop_err();
	return this;
}
c_Node2.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<361>";
	pop_err();
	return this;
}
c_Node2.prototype.p_NextNode=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<385>";
	var t_node=null;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<386>";
	if((this.m_right)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<387>";
		t_node=this.m_right;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<388>";
		while((dbg_object(t_node).m_left)!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<389>";
			t_node=dbg_object(t_node).m_left;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<391>";
		pop_err();
		return t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<393>";
	t_node=this;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<394>";
	var t_parent=dbg_object(this).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<395>";
	while(((t_parent)!=null) && t_node==dbg_object(t_parent).m_right){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<396>";
		t_node=t_parent;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<397>";
		t_parent=dbg_object(t_parent).m_parent;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<399>";
	pop_err();
	return t_parent;
}
function c_Parser(){
	Object.call(this);
	this.m__text="";
	this.m__pos=0;
	this.m__len=0;
	this.m__toke="";
	this.m__tokeType=0;
}
c_Parser.prototype.p_Bump=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<43>";
	while(this.m__pos<this.m__len){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<44>";
		var t_ch=dbg_charCodeAt(this.m__text,this.m__pos);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<45>";
		if(t_ch<=32){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<46>";
			this.m__pos+=1;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<47>";
			continue;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<49>";
		if(t_ch!=39){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<49>";
			break;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<50>";
		this.m__pos+=1;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<51>";
		while(this.m__pos<this.m__len && dbg_charCodeAt(this.m__text,this.m__pos)!=10){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<52>";
			this.m__pos+=1;
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<56>";
	if(this.m__pos==this.m__len){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<57>";
		this.m__toke="";
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<58>";
		this.m__tokeType=0;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<59>";
		pop_err();
		return this.m__toke;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<62>";
	var t_pos=this.m__pos;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<63>";
	var t_ch2=dbg_charCodeAt(this.m__text,this.m__pos);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<64>";
	this.m__pos+=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<66>";
	if(bb_glslparser_IsAlpha(t_ch2) || t_ch2==95){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<68>";
		while(this.m__pos<this.m__len){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<69>";
			var t_ch3=dbg_charCodeAt(this.m__text,this.m__pos);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<70>";
			if(!bb_glslparser_IsIdent(t_ch3)){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<70>";
				break;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<71>";
			this.m__pos+=1;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<73>";
		this.m__tokeType=1;
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<75>";
		if(bb_glslparser_IsDigit(t_ch2)){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<77>";
			while(this.m__pos<this.m__len){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<78>";
				if(!bb_glslparser_IsDigit(dbg_charCodeAt(this.m__text,this.m__pos))){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<78>";
					break;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<79>";
				this.m__pos+=1;
			}
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<81>";
			this.m__tokeType=2;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<83>";
			if(t_ch2==34){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<85>";
				while(this.m__pos<this.m__len){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<86>";
					var t_ch4=dbg_charCodeAt(this.m__text,this.m__pos);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<87>";
					if(t_ch4==34){
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<87>";
						break;
					}
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<88>";
					this.m__pos+=1;
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<90>";
				if(this.m__pos==this.m__len){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<90>";
					error("String literal missing closing quote");
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<91>";
				this.m__tokeType=4;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<92>";
				this.m__pos+=1;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<95>";
				var t_digraphs=[":="];
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<96>";
				if(this.m__pos<this.m__len){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<97>";
					var t_ch5=dbg_charCodeAt(this.m__text,this.m__pos);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<98>";
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<98>";
					var t_=t_digraphs;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<98>";
					var t_2=0;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<98>";
					while(t_2<t_.length){
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<98>";
						var t_t=dbg_array(t_,t_2)[dbg_index];
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<98>";
						t_2=t_2+1;
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<99>";
						if(t_ch5==dbg_charCodeAt(t_t,1)){
							err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<100>";
							this.m__pos+=1;
							err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<101>";
							break;
						}
					}
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<105>";
				this.m__tokeType=5;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<108>";
	this.m__toke=this.m__text.slice(t_pos,this.m__pos);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<110>";
	pop_err();
	return this.m__toke;
}
c_Parser.prototype.p_SetText=function(t_text){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<35>";
	this.m__text=t_text;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<36>";
	this.m__pos=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<37>";
	this.m__len=this.m__text.length;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<38>";
	this.p_Bump();
	pop_err();
}
c_Parser.m_new=function(t_text){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<31>";
	this.p_SetText(t_text);
	pop_err();
	return this;
}
c_Parser.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<28>";
	pop_err();
	return this;
}
c_Parser.prototype.p_Toke=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<114>";
	pop_err();
	return this.m__toke;
}
c_Parser.prototype.p_CParse=function(t_toke){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<122>";
	if(this.m__toke!=t_toke){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<122>";
		pop_err();
		return false;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<123>";
	this.p_Bump();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<124>";
	pop_err();
	return true;
}
c_Parser.prototype.p_CParseIdent=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<128>";
	if(this.m__tokeType!=1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<128>";
		pop_err();
		return "";
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<129>";
	var t_id=this.m__toke;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<130>";
	this.p_Bump();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<131>";
	pop_err();
	return t_id;
}
c_Parser.prototype.p_ParseIdent=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<152>";
	var t_id=this.p_CParseIdent();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<153>";
	if(!((t_id).length!=0)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<153>";
		error("Expecting identifier");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<154>";
	pop_err();
	return t_id;
}
c_Parser.prototype.p_Parse=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<142>";
	var t_toke=this.m__toke;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<143>";
	this.p_Bump();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<144>";
	pop_err();
	return t_toke;
}
c_Parser.prototype.p_Parse2=function(t_toke){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<148>";
	if(!this.p_CParse(t_toke)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<148>";
		error("Expecting '"+t_toke+"'");
	}
	pop_err();
}
function c_GlslParser(){
	c_Parser.call(this);
}
c_GlslParser.prototype=extend_class(c_Parser);
c_GlslParser.m_new=function(t_text){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<176>";
	c_Parser.m_new.call(this,t_text);
	pop_err();
	return this;
}
c_GlslParser.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<173>";
	c_Parser.m_new2.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<173>";
	pop_err();
	return this;
}
c_GlslParser.prototype.p_ParseType=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<180>";
	var t_id=this.p_ParseIdent();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<181>";
	pop_err();
	return t_id;
}
function bb_glslparser_IsAlpha(t_ch){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<21>";
	var t_=t_ch>=65 && t_ch<91 || t_ch>=97 && t_ch<123;
	pop_err();
	return t_;
}
function bb_glslparser_IsIdent(t_ch){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<25>";
	var t_=t_ch>=65 && t_ch<91 || t_ch>=97 && t_ch<123 || t_ch>=48 && t_ch<58 || t_ch==95;
	pop_err();
	return t_;
}
function bb_glslparser_IsDigit(t_ch){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glslparser.monkey<17>";
	var t_=t_ch>=48 && t_ch<58;
	pop_err();
	return t_;
}
function c_KeyEnumerator(){
	Object.call(this);
	this.m_node=null;
}
c_KeyEnumerator.m_new=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<459>";
	dbg_object(this).m_node=t_node;
	pop_err();
	return this;
}
c_KeyEnumerator.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<456>";
	pop_err();
	return this;
}
c_KeyEnumerator.prototype.p_HasNext=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<463>";
	var t_=this.m_node!=null;
	pop_err();
	return t_;
}
c_KeyEnumerator.prototype.p_NextObject=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<467>";
	var t_t=this.m_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<468>";
	this.m_node=this.m_node.p_NextNode();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<469>";
	pop_err();
	return dbg_object(t_t).m_key;
}
function c_MapKeys(){
	Object.call(this);
	this.m_map=null;
}
c_MapKeys.m_new=function(t_map){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<503>";
	dbg_object(this).m_map=t_map;
	pop_err();
	return this;
}
c_MapKeys.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<500>";
	pop_err();
	return this;
}
c_MapKeys.prototype.p_ObjectEnumerator=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<507>";
	var t_=c_KeyEnumerator.m_new.call(new c_KeyEnumerator,this.m_map.p_FirstNode());
	pop_err();
	return t_;
}
var bb_graphics2_fastShader=null;
function c_BumpShader(){
	c_Shader.call(this);
}
c_BumpShader.prototype=extend_class(c_Shader);
c_BumpShader.m_new=function(t_source){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<804>";
	c_Shader.m_new.call(this,t_source);
	pop_err();
	return this;
}
c_BumpShader.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<801>";
	c_Shader.m_new2.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<801>";
	pop_err();
	return this;
}
c_BumpShader.prototype.p_OnInitMaterial=function(t_material){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<810>";
	t_material.p_SetTexture("ColorTexture",c_Texture.m_White());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<811>";
	t_material.p_SetTexture("SpecularTexture",c_Texture.m_Black());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<812>";
	t_material.p_SetTexture("NormalTexture",c_Texture.m_Flat());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<813>";
	t_material.p_SetVector("AmbientColor",[1.0,1.0,1.0,1.0]);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<814>";
	t_material.p_SetScalar("Roughness",1.0);
	pop_err();
}
c_BumpShader.prototype.p_OnLoadMaterial=function(t_material,t_path,t_texFlags){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<819>";
	var t_format=4;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<821>";
	var t_ext=bb_filepath_ExtractExt(t_path);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<822>";
	if((t_ext).length!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<822>";
		t_path=bb_filepath_StripExt(t_path);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<822>";
		t_ext="png";
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<824>";
	var t_colorTex=c_Texture.m_Load(t_path+"."+t_ext,t_format,t_texFlags);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<825>";
	if(!((t_colorTex)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<825>";
		t_colorTex=c_Texture.m_Load(t_path+"_d."+t_ext,t_format,t_texFlags);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<826>";
	if(!((t_colorTex)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<826>";
		t_colorTex=c_Texture.m_Load(t_path+"_diff."+t_ext,t_format,t_texFlags);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<827>";
	if(!((t_colorTex)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<827>";
		t_colorTex=c_Texture.m_Load(t_path+"_diffuse."+t_ext,t_format,t_texFlags);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<829>";
	var t_specularTex=c_Texture.m_Load(t_path+"_s."+t_ext,t_format,t_texFlags);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<830>";
	if(!((t_specularTex)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<830>";
		t_specularTex=c_Texture.m_Load(t_path+"_spec."+t_ext,t_format,t_texFlags);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<831>";
	if(!((t_specularTex)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<831>";
		t_specularTex=c_Texture.m_Load(t_path+"_specular."+t_ext,t_format,t_texFlags);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<832>";
	if(!((t_specularTex)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<832>";
		t_specularTex=c_Texture.m_Load(t_path+"_SPECULAR."+t_ext,t_format,t_texFlags);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<834>";
	var t_normalTex=c_Texture.m_Load(t_path+"_n."+t_ext,t_format,t_texFlags);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<835>";
	if(!((t_normalTex)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<835>";
		t_normalTex=c_Texture.m_Load(t_path+"_norm."+t_ext,t_format,t_texFlags);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<836>";
	if(!((t_normalTex)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<836>";
		t_normalTex=c_Texture.m_Load(t_path+"_normal."+t_ext,t_format,t_texFlags);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<837>";
	if(!((t_normalTex)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<837>";
		t_normalTex=c_Texture.m_Load(t_path+"_NORMALS."+t_ext,t_format,t_texFlags);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<839>";
	if(!((t_colorTex)!=null) && !((t_specularTex)!=null) && !((t_normalTex)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<839>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<841>";
	t_material.p_SetTexture("ColorTexture",t_colorTex);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<842>";
	t_material.p_SetTexture("SpecularTexture",t_specularTex);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<843>";
	t_material.p_SetTexture("NormalTexture",t_normalTex);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<845>";
	if(((t_specularTex)!=null) || ((t_normalTex)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<846>";
		t_material.p_SetVector("AmbientColor",[0.0,0.0,0.0,1.0]);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<847>";
		t_material.p_SetScalar("Roughness",.5);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<850>";
	if((t_colorTex)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<850>";
		t_colorTex.p_Release();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<851>";
	if((t_specularTex)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<851>";
		t_specularTex.p_Release();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<852>";
	if((t_normalTex)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<852>";
		t_normalTex.p_Release();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<854>";
	pop_err();
	return t_material;
}
var bb_graphics2_bumpShader=null;
function c_MatteShader(){
	c_Shader.call(this);
}
c_MatteShader.prototype=extend_class(c_Shader);
c_MatteShader.m_new=function(t_source){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<862>";
	c_Shader.m_new.call(this,t_source);
	pop_err();
	return this;
}
c_MatteShader.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<859>";
	c_Shader.m_new2.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<859>";
	pop_err();
	return this;
}
c_MatteShader.prototype.p_OnInitMaterial=function(t_material){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<868>";
	t_material.p_SetTexture("ColorTexture",c_Texture.m_White());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<869>";
	t_material.p_SetVector("AmbientColor",[0.0,0.0,0.0,1.0]);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<870>";
	t_material.p_SetScalar("Roughness",1.0);
	pop_err();
}
var bb_graphics2_matteShader=null;
var bb_graphics2_shadowShader=null;
var bb_graphics2_lightMapShader=null;
var bb_graphics2_defaultShader=null;
function c_Font(){
	Object.call(this);
	this.m__glyphs=[];
	this.m__firstChar=0;
	this.m__height=.0;
}
c_Font.m_new=function(t_glyphs,t_firstChar,t_height){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1262>";
	this.m__glyphs=t_glyphs;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1263>";
	this.m__firstChar=t_firstChar;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1264>";
	this.m__height=t_height;
	pop_err();
	return this;
}
c_Font.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1259>";
	pop_err();
	return this;
}
c_Font.m_Load=function(t_path,t_firstChar,t_numChars,t_padded){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1289>";
	var t_image=c_Image2.m_Load(t_path,.5,.5,3,null);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1290>";
	if(!((t_image)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1290>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1292>";
	var t_cellWidth=((t_image.p_Width()/t_numChars)|0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1293>";
	var t_cellHeight=t_image.p_Height();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1294>";
	var t_glyphX=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1294>";
	var t_glyphY=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1294>";
	var t_glyphWidth=t_cellWidth;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1294>";
	var t_glyphHeight=t_cellHeight;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1295>";
	if(t_padded){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1295>";
		t_glyphX+=1;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1295>";
		t_glyphY+=1;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1295>";
		t_glyphWidth-=2;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1295>";
		t_glyphHeight-=2;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1297>";
	var t_w=((t_image.p_Width()/t_cellWidth)|0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1298>";
	var t_h=((t_image.p_Height()/t_cellHeight)|0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1300>";
	var t_glyphs=new_object_array(t_numChars);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1302>";
	for(var t_i=0;t_i<t_numChars;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1303>";
		var t_y=((t_i/t_w)|0);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1304>";
		var t_x=t_i % t_w;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1305>";
		var t_glyph=c_Glyph.m_new.call(new c_Glyph,t_image,t_firstChar+t_i,t_x*t_cellWidth+t_glyphX,t_y*t_cellHeight+t_glyphY,t_glyphWidth,t_glyphHeight,(t_glyphWidth));
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1306>";
		dbg_array(t_glyphs,t_i)[dbg_index]=t_glyph;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1309>";
	var t_=c_Font.m_new.call(new c_Font,t_glyphs,t_firstChar,(t_glyphHeight));
	pop_err();
	return t_;
}
c_Font.m_Load2=function(t_path,t_cellWidth,t_cellHeight,t_glyphX,t_glyphY,t_glyphWidth,t_glyphHeight,t_firstChar,t_numChars){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1315>";
	var t_image=c_Image2.m_Load(t_path,.5,.5,3,null);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1316>";
	if(!((t_image)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1316>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1318>";
	var t_w=((t_image.p_Width()/t_cellWidth)|0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1319>";
	var t_h=((t_image.p_Height()/t_cellHeight)|0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1321>";
	var t_glyphs=new_object_array(t_numChars);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1323>";
	for(var t_i=0;t_i<t_numChars;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1324>";
		var t_y=((t_i/t_w)|0);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1325>";
		var t_x=t_i % t_w;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1326>";
		var t_glyph=c_Glyph.m_new.call(new c_Glyph,t_image,t_firstChar+t_i,t_x*t_cellWidth+t_glyphX,t_y*t_cellHeight+t_glyphY,t_glyphWidth,t_glyphHeight,(t_glyphWidth));
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1327>";
		dbg_array(t_glyphs,t_i)[dbg_index]=t_glyph;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1330>";
	var t_=c_Font.m_new.call(new c_Font,t_glyphs,t_firstChar,(t_glyphHeight));
	pop_err();
	return t_;
}
function c_RefCounted(){
	Object.call(this);
	this.m__refs=1;
}
c_RefCounted.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<173>";
	pop_err();
	return this;
}
c_RefCounted.prototype.p_Retain=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<176>";
	if(this.m__refs<=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<176>";
		error("Internal error");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<177>";
	this.m__refs+=1;
	pop_err();
}
c_RefCounted.prototype.p_Destroy=function(){
}
c_RefCounted.prototype.p_Release=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<181>";
	if(this.m__refs<=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<181>";
		error("Internal error");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<182>";
	this.m__refs-=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<183>";
	if((this.m__refs)!=0){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<184>";
	this.m__refs=-1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<185>";
	this.p_Destroy();
	pop_err();
}
function c_Texture(){
	c_RefCounted.call(this);
	this.m__flags=0;
	this.m__width=0;
	this.m__height=0;
	this.m__format=0;
	this.m__seq=0;
	this.m__glTexture=0;
	this.m__glFramebuffer=0;
	this.m__data=null;
}
c_Texture.prototype=extend_class(c_RefCounted);
c_Texture.m__white=null;
c_Texture.m__colors=null;
c_Texture.prototype.p_Init3=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<454>";
	this.m__seq=webglGraphicsSeq;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<456>";
	this.m__glTexture=gl.createTexture();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<458>";
	bb_glutil_glPushTexture2d(this.m__glTexture);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<460>";
	if((this.m__flags&1)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<461>";
		gl.texParameteri(3553,10240,9729);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<463>";
		gl.texParameteri(3553,10240,9728);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<465>";
	if(((this.m__flags&2)!=0) && ((this.m__flags&1)!=0)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<466>";
		gl.texParameteri(3553,10241,9987);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<467>";
		if((this.m__flags&2)!=0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<468>";
			gl.texParameteri(3553,10241,9984);
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<469>";
			if((this.m__flags&1)!=0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<470>";
				gl.texParameteri(3553,10241,9729);
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<472>";
				gl.texParameteri(3553,10241,9728);
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<475>";
	if((this.m__flags&4)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<475>";
		gl.texParameteri(3553,10242,33071);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<476>";
	if((this.m__flags&8)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<476>";
		gl.texParameteri(3553,10243,33071);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<478>";
	_glTexImage2D(3553,0,6408,this.m__width,this.m__height,0,6408,5121,null);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<480>";
	bb_glutil_glPopTexture2d();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<482>";
	if((this.m__flags&16)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<484>";
		this.m__glFramebuffer=gl.createFramebuffer();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<486>";
		bb_glutil_glPushFramebuffer(this.m__glFramebuffer);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<488>";
		_glBindFramebuffer(36160,this.m__glFramebuffer);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<489>";
		gl.framebufferTexture2D(36160,36064,3553,this.m__glTexture,0);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<491>";
		if(gl.checkFramebufferStatus(36160)!=36053){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<491>";
			error("Incomplete framebuffer");
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<493>";
		bb_glutil_glPopFramebuffer();
	}
	pop_err();
}
c_Texture.prototype.p_Init4=function(t_width,t_height,t_format,t_flags){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<430>";
	bb_graphics2_InitMojo2();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<433>";
	if(t_format!=4){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<433>";
		error("Invalid texture format: "+String(t_format));
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<437>";
	if(!bb_graphics2_IsPow2(t_width) || !bb_graphics2_IsPow2(t_height)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<437>";
		t_flags&=-3;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<440>";
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<444>";
	this.m__width=t_width;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<445>";
	this.m__height=t_height;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<446>";
	this.m__format=t_format;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<447>";
	this.m__flags=t_flags;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<449>";
	this.p_Init3();
	pop_err();
}
c_Texture.m_new=function(t_width,t_height,t_format,t_flags){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<221>";
	c_RefCounted.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<222>";
	this.p_Init4(t_width,t_height,t_format,t_flags);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<224>";
	if((this.m__flags&256)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<225>";
		var t_data=c_DataBuffer.m_new.call(new c_DataBuffer,t_width*t_height*4,false);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<226>";
		for(var t_i=0;t_i<t_width*t_height*4;t_i=t_i+4){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<227>";
			t_data.PokeInt(t_i,-65281);
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<229>";
		this.m__data=(t_data);
	}
	pop_err();
	return this;
}
c_Texture.prototype.p_Validate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<245>";
	if(this.m__seq==webglGraphicsSeq){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<246>";
	this.p_Init3();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<247>";
	if((this.m__data)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<247>";
		this.p_LoadData(this.m__data);
	}
	pop_err();
}
c_Texture.prototype.p_GLTexture=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<316>";
	this.p_Validate();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<317>";
	pop_err();
	return this.m__glTexture;
}
c_Texture.prototype.p_UpdateMipmaps=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<296>";
	if(!((this.m__flags&2)!=0)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<298>";
	if(this.m__seq!=webglGraphicsSeq){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<300>";
	bb_glutil_glPushTexture2d(this.p_GLTexture());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<302>";
	_glGenerateMipmap(3553);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<304>";
	bb_glutil_glPopTexture2d();
	pop_err();
}
c_Texture.prototype.p_LoadData=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<499>";
	bb_glutil_glPushTexture2d(this.p_GLTexture());
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<504>";
	if((object_downcast((t_data),c_DataBuffer))!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<505>";
		_glTexImage2D(3553,0,6408,this.m__width,this.m__height,0,6408,5121,object_downcast((t_data),c_DataBuffer));
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<507>";
		_glTexImage2D2(3553,0,6408,6408,5121,t_data);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<510>";
	bb_glutil_glPopTexture2d();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<512>";
	this.p_UpdateMipmaps();
	pop_err();
}
c_Texture.m_new2=function(t_width,t_height,t_format,t_flags,t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<419>";
	c_RefCounted.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<420>";
	this.p_Init4(t_width,t_height,t_format,t_flags);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<422>";
	this.p_LoadData(t_data);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<424>";
	if(true){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<425>";
		this.m__data=t_data;
	}
	pop_err();
	return this;
}
c_Texture.m_new3=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<210>";
	c_RefCounted.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<210>";
	pop_err();
	return this;
}
c_Texture.m_Color=function(t_color){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<370>";
	var t_tex=c_Texture.m__colors.p_Get(t_color);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<371>";
	if((t_tex)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<371>";
		pop_err();
		return t_tex;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<372>";
	var t_data=c_DataBuffer.m_new.call(new c_DataBuffer,4,false);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<373>";
	t_data.PokeInt(0,t_color);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<374>";
	t_tex=c_Texture.m_new2.call(new c_Texture,1,1,4,12,(t_data));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<378>";
	c_Texture.m__colors.p_Set4(t_color,t_tex);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<379>";
	pop_err();
	return t_tex;
}
c_Texture.m_White=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<388>";
	if(!((c_Texture.m__white)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<388>";
		c_Texture.m__white=c_Texture.m_Color(-1);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<389>";
	pop_err();
	return c_Texture.m__white;
}
c_Texture.m_Load=function(t_path,t_format,t_flags){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<335>";
	var t_info=new_number_array(2);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<352>";
	var t_data=bb_gles20_LoadStaticTexImage(bb_graphics2_KludgePath(t_path),t_info);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<355>";
	if(!((t_data)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<357>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<360>";
	var t_tex=c_Texture.m_new2.call(new c_Texture,dbg_array(t_info,0)[dbg_index],dbg_array(t_info,1)[dbg_index],t_format,t_flags,t_data);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<366>";
	pop_err();
	return t_tex;
}
c_Texture.prototype.p_Loading=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<309>";
	var t_=BBTextureLoading(this.m__glTexture);
	pop_err();
	return t_;
}
c_Texture.prototype.p_GLFramebuffer=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<321>";
	this.p_Validate();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<322>";
	pop_err();
	return this.m__glFramebuffer;
}
c_Texture.prototype.p_Flags=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<263>";
	pop_err();
	return this.m__flags;
}
c_Texture.prototype.p_Width=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<251>";
	pop_err();
	return this.m__width;
}
c_Texture.prototype.p_Height=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<255>";
	pop_err();
	return this.m__height;
}
c_Texture.m__black=null;
c_Texture.m_Black=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<383>";
	if(!((c_Texture.m__black)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<383>";
		c_Texture.m__black=c_Texture.m_Color(-16777216);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<384>";
	pop_err();
	return c_Texture.m__black;
}
c_Texture.m__flat=null;
c_Texture.m_Flat=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<398>";
	if(!((c_Texture.m__flat)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<398>";
		c_Texture.m__flat=c_Texture.m_Color(-7829368);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<399>";
	pop_err();
	return c_Texture.m__flat;
}
c_Texture.prototype.p_Destroy=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<236>";
	if(this.m__seq==webglGraphicsSeq){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<237>";
		if((this.m__glTexture)!=0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<237>";
			gl.deleteTexture(this.m__glTexture);
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<238>";
		if((this.m__glFramebuffer)!=0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<238>";
			gl.deleteFramebuffer(this.m__glFramebuffer);
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<240>";
	this.m__glTexture=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<241>";
	this.m__glFramebuffer=0;
	pop_err();
}
function c_Material(){
	c_RefCounted.call(this);
	this.m__shader=null;
	this.m__inited=false;
	this.m__textures=c_StringMap2.m_new.call(new c_StringMap2);
	this.m__colorTexture=null;
	this.m__scalars=c_StringMap3.m_new.call(new c_StringMap3);
	this.m__vectors=c_StringMap4.m_new.call(new c_StringMap4);
}
c_Material.prototype=extend_class(c_RefCounted);
c_Material.prototype.p_SetTexture=function(t_param,t_texture){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<937>";
	if(!((t_texture)!=null)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<938>";
	if(this.m__inited && !this.m__textures.p_Contains2(t_param)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<940>";
	var t_old=this.m__textures.p_Get2(t_param);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<941>";
	t_texture.p_Retain();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<942>";
	this.m__textures.p_Set5(t_param,t_texture);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<943>";
	if((t_old)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<943>";
		t_old.p_Release();
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<945>";
	if(t_param=="ColorTexture"){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<945>";
		this.m__colorTexture=t_texture;
	}
	pop_err();
}
c_Material.m_new=function(t_shader){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<879>";
	c_RefCounted.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<880>";
	bb_graphics2_InitMojo2();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<882>";
	if(!((t_shader)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<882>";
		t_shader=bb_graphics2_defaultShader;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<883>";
	this.m__shader=t_shader;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<884>";
	this.m__shader.p_OnInitMaterial(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<885>";
	this.m__inited=true;
	pop_err();
	return this;
}
c_Material.prototype.p_Shader=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<899>";
	pop_err();
	return this.m__shader;
}
c_Material.m_Load=function(t_path,t_texFlags,t_shader){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<965>";
	var t_material=c_Material.m_new.call(new c_Material,t_shader);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<967>";
	t_material=t_material.p_Shader().p_OnLoadMaterial(t_material,t_path,t_texFlags);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<969>";
	pop_err();
	return t_material;
}
c_Material.prototype.p_Width=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<907>";
	if((this.m__colorTexture)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<907>";
		pop_err();
		return dbg_object(this.m__colorTexture).m__width;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<908>";
	pop_err();
	return 0;
}
c_Material.prototype.p_Height=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<912>";
	if((this.m__colorTexture)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<912>";
		pop_err();
		return dbg_object(this.m__colorTexture).m__height;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<913>";
	pop_err();
	return 0;
}
c_Material.prototype.p_ColorTexture=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<903>";
	pop_err();
	return this.m__colorTexture;
}
c_Material.prototype.p_GetScalar=function(t_param,t_defValue){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<922>";
	if(!this.m__scalars.p_Contains2(t_param)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<922>";
		pop_err();
		return t_defValue;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<923>";
	var t_=this.m__scalars.p_Get2(t_param);
	pop_err();
	return t_;
}
c_Material.prototype.p_GetVector=function(t_param,t_defValue){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<932>";
	if(!this.m__vectors.p_Contains2(t_param)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<932>";
		pop_err();
		return t_defValue;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<933>";
	var t_=this.m__vectors.p_Get2(t_param);
	pop_err();
	return t_;
}
c_Material.prototype.p_GetTexture=function(t_param,t_defValue){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<950>";
	if(!this.m__textures.p_Contains2(t_param)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<950>";
		pop_err();
		return t_defValue;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<951>";
	var t_=this.m__textures.p_Get2(t_param);
	pop_err();
	return t_;
}
c_Material.prototype.p_Bind=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<983>";
	this.m__shader.p_Bind();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<985>";
	if(bb_graphics2_rs_material==this){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<985>";
		pop_err();
		return true;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<987>";
	bb_graphics2_rs_material=this;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<989>";
	var t_texid=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<991>";
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<991>";
	var t_=dbg_object(bb_graphics2_rs_program).m_matuniforms;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<991>";
	var t_2=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<991>";
	while(t_2<t_.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<991>";
		var t_u=dbg_array(t_,t_2)[dbg_index];
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<991>";
		t_2=t_2+1;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<992>";
		var t_1=dbg_object(t_u).m_type;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<993>";
		if(t_1==5126){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<994>";
			gl.uniform1f(dbg_object(t_u).m_location,this.p_GetScalar(dbg_object(t_u).m_name,1.0));
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<995>";
			if(t_1==35666){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<996>";
				_glUniform4fv(dbg_object(t_u).m_location,1,this.p_GetVector(dbg_object(t_u).m_name,[1.0,1.0,1.0,1.0]));
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<997>";
				if(t_1==35678){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<998>";
					var t_tex=this.p_GetTexture(dbg_object(t_u).m_name,null);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<999>";
					if(t_tex.p_Loading()){
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1000>";
						bb_graphics2_rs_material=null;
						err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1001>";
						break;
					}
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1003>";
					gl.activeTexture(33984+t_texid);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1004>";
					_glBindTexture(3553,t_tex.p_GLTexture());
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1005>";
					gl.uniform1i(dbg_object(t_u).m_location,t_texid);
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1006>";
					t_texid+=1;
				}else{
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1008>";
					error("Unsupported uniform type: name="+dbg_object(t_u).m_name+", location="+String(dbg_object(t_u).m_location)+", size="+String(dbg_object(t_u).m_size)+", type="+String(dbg_object(t_u).m_type));
				}
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1017>";
	if((t_texid)!=0){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1017>";
		gl.activeTexture(33984);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1019>";
	var t_3=bb_graphics2_rs_material==this;
	pop_err();
	return t_3;
}
c_Material.prototype.p_SetVector=function(t_param,t_vector){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<927>";
	if(this.m__inited && !this.m__vectors.p_Contains2(t_param)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<928>";
	this.m__vectors.p_Set7(t_param,t_vector);
	pop_err();
}
c_Material.prototype.p_SetScalar=function(t_param,t_scalar){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<917>";
	if(this.m__inited && !this.m__scalars.p_Contains2(t_param)){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<918>";
	this.m__scalars.p_Set6(t_param,t_scalar);
	pop_err();
}
c_Material.prototype.p_Destroy=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<893>";
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<893>";
	var t_=this.m__textures.p_ObjectEnumerator();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<893>";
	while(t_.p_HasNext()){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<893>";
		var t_tex=t_.p_NextObject();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<894>";
		t_tex.p_Value().p_Release();
	}
	pop_err();
}
function c_Map3(){
	Object.call(this);
	this.m_root=null;
}
c_Map3.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<7>";
	pop_err();
	return this;
}
c_Map3.prototype.p_Compare=function(t_lhs,t_rhs){
}
c_Map3.prototype.p_FindNode=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<157>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<159>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<160>";
		var t_cmp=this.p_Compare(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<161>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<162>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<163>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<164>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<166>";
				pop_err();
				return t_node;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<169>";
	pop_err();
	return t_node;
}
c_Map3.prototype.p_Get=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<101>";
	var t_node=this.p_FindNode(t_key);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<102>";
	if((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<102>";
		pop_err();
		return dbg_object(t_node).m_value;
	}
	pop_err();
	return null;
}
c_Map3.prototype.p_RotateLeft3=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<251>";
	var t_child=dbg_object(t_node).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<252>";
	dbg_object(t_node).m_right=dbg_object(t_child).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<253>";
	if((dbg_object(t_child).m_left)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<254>";
		dbg_object(dbg_object(t_child).m_left).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<256>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<257>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<258>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<259>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<261>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<264>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<266>";
	dbg_object(t_child).m_left=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<267>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map3.prototype.p_RotateRight3=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<271>";
	var t_child=dbg_object(t_node).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<272>";
	dbg_object(t_node).m_left=dbg_object(t_child).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<273>";
	if((dbg_object(t_child).m_right)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<274>";
		dbg_object(dbg_object(t_child).m_right).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<276>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<277>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<278>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<279>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<281>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<284>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<286>";
	dbg_object(t_child).m_right=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<287>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map3.prototype.p_InsertFixup3=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<212>";
	while(((dbg_object(t_node).m_parent)!=null) && dbg_object(dbg_object(t_node).m_parent).m_color==-1 && ((dbg_object(dbg_object(t_node).m_parent).m_parent)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<213>";
		if(dbg_object(t_node).m_parent==dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<214>";
			var t_uncle=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_right;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<215>";
			if(((t_uncle)!=null) && dbg_object(t_uncle).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<216>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<217>";
				dbg_object(t_uncle).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<218>";
				dbg_object(dbg_object(t_uncle).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<219>";
				t_node=dbg_object(t_uncle).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<221>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<222>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<223>";
					this.p_RotateLeft3(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<225>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<226>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<227>";
				this.p_RotateRight3(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<230>";
			var t_uncle2=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<231>";
			if(((t_uncle2)!=null) && dbg_object(t_uncle2).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<232>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<233>";
				dbg_object(t_uncle2).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<234>";
				dbg_object(dbg_object(t_uncle2).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<235>";
				t_node=dbg_object(t_uncle2).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<237>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<238>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<239>";
					this.p_RotateRight3(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<241>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<242>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<243>";
				this.p_RotateLeft3(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<247>";
	dbg_object(this.m_root).m_color=1;
	pop_err();
	return 0;
}
c_Map3.prototype.p_Set4=function(t_key,t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<29>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_parent=null;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_cmp=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<32>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<33>";
		t_parent=t_node;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<34>";
		t_cmp=this.p_Compare(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<35>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<36>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<37>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<38>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<40>";
				dbg_object(t_node).m_value=t_value;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<41>";
				pop_err();
				return false;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<45>";
	t_node=c_Node3.m_new.call(new c_Node3,t_key,t_value,-1,t_parent);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<47>";
	if((t_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<48>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<49>";
			dbg_object(t_parent).m_right=t_node;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<51>";
			dbg_object(t_parent).m_left=t_node;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<53>";
		this.p_InsertFixup3(t_node);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<55>";
		this.m_root=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<57>";
	pop_err();
	return true;
}
function c_IntMap2(){
	c_Map3.call(this);
}
c_IntMap2.prototype=extend_class(c_Map3);
c_IntMap2.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<534>";
	c_Map3.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<534>";
	pop_err();
	return this;
}
c_IntMap2.prototype.p_Compare=function(t_lhs,t_rhs){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<537>";
	var t_=t_lhs-t_rhs;
	pop_err();
	return t_;
}
function c_Node3(){
	Object.call(this);
	this.m_key=0;
	this.m_right=null;
	this.m_left=null;
	this.m_value=null;
	this.m_color=0;
	this.m_parent=null;
}
c_Node3.m_new=function(t_key,t_value,t_color,t_parent){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<364>";
	dbg_object(this).m_key=t_key;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<365>";
	dbg_object(this).m_value=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<366>";
	dbg_object(this).m_color=t_color;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<367>";
	dbg_object(this).m_parent=t_parent;
	pop_err();
	return this;
}
c_Node3.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<361>";
	pop_err();
	return this;
}
function bb_graphics2_IsPow2(t_sz){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<98>";
	var t_=(t_sz&t_sz-1)==0;
	pop_err();
	return t_;
}
function bb_glutil_glPushTexture2d(t_tex){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<17>";
	_glGetIntegerv(32873,bb_glutil_tmpi);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<18>";
	_glBindTexture(3553,t_tex);
	pop_err();
}
function bb_glutil_glPopTexture2d(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<22>";
	_glBindTexture(3553,dbg_array(bb_glutil_tmpi,0)[dbg_index]);
	pop_err();
}
function bb_glutil_glPushFramebuffer(t_framebuf){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<26>";
	_glGetIntegerv(36006,bb_glutil_tmpi);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<27>";
	_glBindFramebuffer(36160,t_framebuf);
	pop_err();
}
function bb_glutil_glPopFramebuffer(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/glutil.monkey<31>";
	_glBindFramebuffer(36160,dbg_array(bb_glutil_tmpi,0)[dbg_index]);
	pop_err();
}
function c_Map4(){
	Object.call(this);
	this.m_root=null;
}
c_Map4.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<7>";
	pop_err();
	return this;
}
c_Map4.prototype.p_Compare2=function(t_lhs,t_rhs){
}
c_Map4.prototype.p_FindNode2=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<157>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<159>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<160>";
		var t_cmp=this.p_Compare2(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<161>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<162>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<163>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<164>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<166>";
				pop_err();
				return t_node;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<169>";
	pop_err();
	return t_node;
}
c_Map4.prototype.p_Contains2=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<25>";
	var t_=this.p_FindNode2(t_key)!=null;
	pop_err();
	return t_;
}
c_Map4.prototype.p_Get2=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<101>";
	var t_node=this.p_FindNode2(t_key);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<102>";
	if((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<102>";
		pop_err();
		return dbg_object(t_node).m_value;
	}
	pop_err();
	return null;
}
c_Map4.prototype.p_RotateLeft4=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<251>";
	var t_child=dbg_object(t_node).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<252>";
	dbg_object(t_node).m_right=dbg_object(t_child).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<253>";
	if((dbg_object(t_child).m_left)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<254>";
		dbg_object(dbg_object(t_child).m_left).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<256>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<257>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<258>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<259>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<261>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<264>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<266>";
	dbg_object(t_child).m_left=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<267>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map4.prototype.p_RotateRight4=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<271>";
	var t_child=dbg_object(t_node).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<272>";
	dbg_object(t_node).m_left=dbg_object(t_child).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<273>";
	if((dbg_object(t_child).m_right)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<274>";
		dbg_object(dbg_object(t_child).m_right).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<276>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<277>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<278>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<279>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<281>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<284>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<286>";
	dbg_object(t_child).m_right=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<287>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map4.prototype.p_InsertFixup4=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<212>";
	while(((dbg_object(t_node).m_parent)!=null) && dbg_object(dbg_object(t_node).m_parent).m_color==-1 && ((dbg_object(dbg_object(t_node).m_parent).m_parent)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<213>";
		if(dbg_object(t_node).m_parent==dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<214>";
			var t_uncle=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_right;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<215>";
			if(((t_uncle)!=null) && dbg_object(t_uncle).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<216>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<217>";
				dbg_object(t_uncle).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<218>";
				dbg_object(dbg_object(t_uncle).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<219>";
				t_node=dbg_object(t_uncle).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<221>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<222>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<223>";
					this.p_RotateLeft4(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<225>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<226>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<227>";
				this.p_RotateRight4(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<230>";
			var t_uncle2=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<231>";
			if(((t_uncle2)!=null) && dbg_object(t_uncle2).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<232>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<233>";
				dbg_object(t_uncle2).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<234>";
				dbg_object(dbg_object(t_uncle2).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<235>";
				t_node=dbg_object(t_uncle2).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<237>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<238>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<239>";
					this.p_RotateRight4(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<241>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<242>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<243>";
				this.p_RotateLeft4(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<247>";
	dbg_object(this.m_root).m_color=1;
	pop_err();
	return 0;
}
c_Map4.prototype.p_Set5=function(t_key,t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<29>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_parent=null;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_cmp=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<32>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<33>";
		t_parent=t_node;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<34>";
		t_cmp=this.p_Compare2(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<35>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<36>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<37>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<38>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<40>";
				dbg_object(t_node).m_value=t_value;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<41>";
				pop_err();
				return false;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<45>";
	t_node=c_Node4.m_new.call(new c_Node4,t_key,t_value,-1,t_parent);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<47>";
	if((t_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<48>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<49>";
			dbg_object(t_parent).m_right=t_node;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<51>";
			dbg_object(t_parent).m_left=t_node;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<53>";
		this.p_InsertFixup4(t_node);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<55>";
		this.m_root=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<57>";
	pop_err();
	return true;
}
c_Map4.prototype.p_FirstNode=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<125>";
	if(!((this.m_root)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<125>";
		pop_err();
		return null;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<127>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<128>";
	while((dbg_object(t_node).m_left)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<129>";
		t_node=dbg_object(t_node).m_left;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<131>";
	pop_err();
	return t_node;
}
c_Map4.prototype.p_ObjectEnumerator=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<121>";
	var t_=c_NodeEnumerator.m_new.call(new c_NodeEnumerator,this.p_FirstNode());
	pop_err();
	return t_;
}
function c_StringMap2(){
	c_Map4.call(this);
}
c_StringMap2.prototype=extend_class(c_Map4);
c_StringMap2.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<551>";
	c_Map4.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<551>";
	pop_err();
	return this;
}
c_StringMap2.prototype.p_Compare2=function(t_lhs,t_rhs){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<554>";
	var t_=string_compare(t_lhs,t_rhs);
	pop_err();
	return t_;
}
function c_Node4(){
	Object.call(this);
	this.m_key="";
	this.m_right=null;
	this.m_left=null;
	this.m_value=null;
	this.m_color=0;
	this.m_parent=null;
}
c_Node4.m_new=function(t_key,t_value,t_color,t_parent){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<364>";
	dbg_object(this).m_key=t_key;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<365>";
	dbg_object(this).m_value=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<366>";
	dbg_object(this).m_color=t_color;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<367>";
	dbg_object(this).m_parent=t_parent;
	pop_err();
	return this;
}
c_Node4.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<361>";
	pop_err();
	return this;
}
c_Node4.prototype.p_NextNode=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<385>";
	var t_node=null;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<386>";
	if((this.m_right)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<387>";
		t_node=this.m_right;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<388>";
		while((dbg_object(t_node).m_left)!=null){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<389>";
			t_node=dbg_object(t_node).m_left;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<391>";
		pop_err();
		return t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<393>";
	t_node=this;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<394>";
	var t_parent=dbg_object(this).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<395>";
	while(((t_parent)!=null) && t_node==dbg_object(t_parent).m_right){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<396>";
		t_node=t_parent;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<397>";
		t_parent=dbg_object(t_parent).m_parent;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<399>";
	pop_err();
	return t_parent;
}
c_Node4.prototype.p_Value=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<381>";
	pop_err();
	return this.m_value;
}
function bb_graphics2_KludgePath(t_path){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<196>";
	if(string_startswith(t_path,".") || string_startswith(t_path,"/")){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<196>";
		pop_err();
		return t_path;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<197>";
	var t_i=t_path.indexOf(":/",0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<198>";
	if(t_i!=-1 && t_path.indexOf("/",0)==t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<198>";
		pop_err();
		return t_path;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<199>";
	var t_="monkey://data/"+t_path;
	pop_err();
	return t_;
}
function bb_gles20_LoadStaticTexImage(t_path,t_info){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/opengl/gles20.monkey<957>";
	var t_=BBLoadStaticTexImage(t_path,t_info);
	pop_err();
	return t_;
}
function c_Glyph(){
	Object.call(this);
	this.m_image=null;
	this.m_char=0;
	this.m_x=0;
	this.m_y=0;
	this.m_width=0;
	this.m_height=0;
	this.m_advance=.0;
}
c_Glyph.m_new=function(t_image,t_char,t_x,t_y,t_width,t_height,t_advance){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1249>";
	dbg_object(this).m_image=t_image;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1250>";
	dbg_object(this).m_char=t_char;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1251>";
	dbg_object(this).m_x=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1252>";
	dbg_object(this).m_y=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1253>";
	dbg_object(this).m_width=t_width;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1254>";
	dbg_object(this).m_height=t_height;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1255>";
	dbg_object(this).m_advance=t_advance;
	pop_err();
	return this;
}
c_Glyph.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1239>";
	pop_err();
	return this;
}
var bb_graphics2_defaultFont=null;
function bb_math3d_Mat4New(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<17>";
	var t_=[1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0];
	pop_err();
	return t_;
}
var bb_graphics2_flipYMatrix=[];
function bb_graphics2_InitMojo2(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<150>";
	if(bb_graphics2_inited){
		pop_err();
		return;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<151>";
	bb_graphics2_inited=true;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<153>";
	bb_graphics2_InitVbos();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<155>";
	_glGetIntegerv(36006,bb_graphics2_tmpi);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<156>";
	bb_graphics2_defaultFbo=dbg_array(bb_graphics2_tmpi,0)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<158>";
	bb_graphics2_mainShader=bb_app_LoadString("monkey://data/mojo2_program.glsl");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<160>";
	bb_graphics2_fastShader=c_Shader.m_new.call(new c_Shader,bb_app_LoadString("monkey://data/mojo2_fastshader.glsl"));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<161>";
	bb_graphics2_bumpShader=(c_BumpShader.m_new.call(new c_BumpShader,bb_app_LoadString("monkey://data/mojo2_bumpshader.glsl")));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<162>";
	bb_graphics2_matteShader=(c_MatteShader.m_new.call(new c_MatteShader,bb_app_LoadString("monkey://data/mojo2_matteshader.glsl")));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<163>";
	bb_graphics2_shadowShader=c_Shader.m_new.call(new c_Shader,bb_app_LoadString("monkey://data/mojo2_shadowshader.glsl"));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<164>";
	bb_graphics2_lightMapShader=c_Shader.m_new.call(new c_Shader,bb_app_LoadString("monkey://data/mojo2_lightmapshader.glsl"));
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<165>";
	bb_graphics2_defaultShader=bb_graphics2_bumpShader;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<167>";
	bb_graphics2_defaultFont=c_Font.m_Load("monkey://data/mojo2_font.png",32,96,true);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<168>";
	if(!((bb_graphics2_defaultFont)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<168>";
		error("Can't load default font");
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<170>";
	dbg_array(bb_graphics2_flipYMatrix,5)[dbg_index]=-1.0;
	pop_err();
}
function c_LightData(){
	Object.call(this);
	this.m_type=0;
	this.m_vector=[0.0,0.0,-10.0,1.0];
	this.m_tvector=new_number_array(4);
	this.m_color=[1.0,1.0,1.0,1.0];
	this.m_range=10.0;
	this.m_position=[0.0,0.0,-10.0];
}
c_LightData.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<101>";
	pop_err();
	return this;
}
function c_DrawOp(){
	Object.call(this);
	this.m_material=null;
	this.m_blend=0;
	this.m_order=0;
	this.m_count=0;
}
c_DrawOp.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/graphics.monkey<1343>";
	pop_err();
	return this;
}
var bb_graphics2_rs_program=null;
var bb_graphics2_rs_numLights=0;
var bb_graphics2_rs_material=null;
var bb_graphics2_rs_modelViewProjMatrix=[];
var bb_graphics2_rs_modelViewMatrix=[];
var bb_graphics2_rs_clipPosScale=[];
var bb_graphics2_rs_globalColor=[];
var bb_graphics2_rs_fogColor=[];
var bb_graphics2_rs_ambientLight=[];
var bb_graphics2_rs_lightColors=[];
var bb_graphics2_rs_lightVectors=[];
var bb_graphics2_rs_shadowTexture=null;
function c_Map5(){
	Object.call(this);
	this.m_root=null;
}
c_Map5.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<7>";
	pop_err();
	return this;
}
c_Map5.prototype.p_Compare2=function(t_lhs,t_rhs){
}
c_Map5.prototype.p_FindNode2=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<157>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<159>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<160>";
		var t_cmp=this.p_Compare2(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<161>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<162>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<163>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<164>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<166>";
				pop_err();
				return t_node;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<169>";
	pop_err();
	return t_node;
}
c_Map5.prototype.p_Contains2=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<25>";
	var t_=this.p_FindNode2(t_key)!=null;
	pop_err();
	return t_;
}
c_Map5.prototype.p_Get2=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<101>";
	var t_node=this.p_FindNode2(t_key);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<102>";
	if((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<102>";
		pop_err();
		return dbg_object(t_node).m_value;
	}
	pop_err();
	return 0;
}
c_Map5.prototype.p_RotateLeft5=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<251>";
	var t_child=dbg_object(t_node).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<252>";
	dbg_object(t_node).m_right=dbg_object(t_child).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<253>";
	if((dbg_object(t_child).m_left)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<254>";
		dbg_object(dbg_object(t_child).m_left).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<256>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<257>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<258>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<259>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<261>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<264>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<266>";
	dbg_object(t_child).m_left=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<267>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map5.prototype.p_RotateRight5=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<271>";
	var t_child=dbg_object(t_node).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<272>";
	dbg_object(t_node).m_left=dbg_object(t_child).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<273>";
	if((dbg_object(t_child).m_right)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<274>";
		dbg_object(dbg_object(t_child).m_right).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<276>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<277>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<278>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<279>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<281>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<284>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<286>";
	dbg_object(t_child).m_right=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<287>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map5.prototype.p_InsertFixup5=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<212>";
	while(((dbg_object(t_node).m_parent)!=null) && dbg_object(dbg_object(t_node).m_parent).m_color==-1 && ((dbg_object(dbg_object(t_node).m_parent).m_parent)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<213>";
		if(dbg_object(t_node).m_parent==dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<214>";
			var t_uncle=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_right;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<215>";
			if(((t_uncle)!=null) && dbg_object(t_uncle).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<216>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<217>";
				dbg_object(t_uncle).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<218>";
				dbg_object(dbg_object(t_uncle).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<219>";
				t_node=dbg_object(t_uncle).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<221>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<222>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<223>";
					this.p_RotateLeft5(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<225>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<226>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<227>";
				this.p_RotateRight5(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<230>";
			var t_uncle2=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<231>";
			if(((t_uncle2)!=null) && dbg_object(t_uncle2).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<232>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<233>";
				dbg_object(t_uncle2).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<234>";
				dbg_object(dbg_object(t_uncle2).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<235>";
				t_node=dbg_object(t_uncle2).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<237>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<238>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<239>";
					this.p_RotateRight5(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<241>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<242>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<243>";
				this.p_RotateLeft5(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<247>";
	dbg_object(this.m_root).m_color=1;
	pop_err();
	return 0;
}
c_Map5.prototype.p_Set6=function(t_key,t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<29>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_parent=null;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_cmp=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<32>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<33>";
		t_parent=t_node;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<34>";
		t_cmp=this.p_Compare2(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<35>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<36>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<37>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<38>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<40>";
				dbg_object(t_node).m_value=t_value;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<41>";
				pop_err();
				return false;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<45>";
	t_node=c_Node5.m_new.call(new c_Node5,t_key,t_value,-1,t_parent);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<47>";
	if((t_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<48>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<49>";
			dbg_object(t_parent).m_right=t_node;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<51>";
			dbg_object(t_parent).m_left=t_node;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<53>";
		this.p_InsertFixup5(t_node);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<55>";
		this.m_root=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<57>";
	pop_err();
	return true;
}
function c_StringMap3(){
	c_Map5.call(this);
}
c_StringMap3.prototype=extend_class(c_Map5);
c_StringMap3.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<551>";
	c_Map5.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<551>";
	pop_err();
	return this;
}
c_StringMap3.prototype.p_Compare2=function(t_lhs,t_rhs){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<554>";
	var t_=string_compare(t_lhs,t_rhs);
	pop_err();
	return t_;
}
function c_Node5(){
	Object.call(this);
	this.m_key="";
	this.m_right=null;
	this.m_left=null;
	this.m_value=0;
	this.m_color=0;
	this.m_parent=null;
}
c_Node5.m_new=function(t_key,t_value,t_color,t_parent){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<364>";
	dbg_object(this).m_key=t_key;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<365>";
	dbg_object(this).m_value=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<366>";
	dbg_object(this).m_color=t_color;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<367>";
	dbg_object(this).m_parent=t_parent;
	pop_err();
	return this;
}
c_Node5.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<361>";
	pop_err();
	return this;
}
function c_Map6(){
	Object.call(this);
	this.m_root=null;
}
c_Map6.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<7>";
	pop_err();
	return this;
}
c_Map6.prototype.p_Compare2=function(t_lhs,t_rhs){
}
c_Map6.prototype.p_FindNode2=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<157>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<159>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<160>";
		var t_cmp=this.p_Compare2(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<161>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<162>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<163>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<164>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<166>";
				pop_err();
				return t_node;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<169>";
	pop_err();
	return t_node;
}
c_Map6.prototype.p_Contains2=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<25>";
	var t_=this.p_FindNode2(t_key)!=null;
	pop_err();
	return t_;
}
c_Map6.prototype.p_Get2=function(t_key){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<101>";
	var t_node=this.p_FindNode2(t_key);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<102>";
	if((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<102>";
		pop_err();
		return dbg_object(t_node).m_value;
	}
	pop_err();
	return [];
}
c_Map6.prototype.p_RotateLeft6=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<251>";
	var t_child=dbg_object(t_node).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<252>";
	dbg_object(t_node).m_right=dbg_object(t_child).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<253>";
	if((dbg_object(t_child).m_left)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<254>";
		dbg_object(dbg_object(t_child).m_left).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<256>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<257>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<258>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<259>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<261>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<264>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<266>";
	dbg_object(t_child).m_left=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<267>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map6.prototype.p_RotateRight6=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<271>";
	var t_child=dbg_object(t_node).m_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<272>";
	dbg_object(t_node).m_left=dbg_object(t_child).m_right;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<273>";
	if((dbg_object(t_child).m_right)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<274>";
		dbg_object(dbg_object(t_child).m_right).m_parent=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<276>";
	dbg_object(t_child).m_parent=dbg_object(t_node).m_parent;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<277>";
	if((dbg_object(t_node).m_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<278>";
		if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<279>";
			dbg_object(dbg_object(t_node).m_parent).m_right=t_child;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<281>";
			dbg_object(dbg_object(t_node).m_parent).m_left=t_child;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<284>";
		this.m_root=t_child;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<286>";
	dbg_object(t_child).m_right=t_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<287>";
	dbg_object(t_node).m_parent=t_child;
	pop_err();
	return 0;
}
c_Map6.prototype.p_InsertFixup6=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<212>";
	while(((dbg_object(t_node).m_parent)!=null) && dbg_object(dbg_object(t_node).m_parent).m_color==-1 && ((dbg_object(dbg_object(t_node).m_parent).m_parent)!=null)){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<213>";
		if(dbg_object(t_node).m_parent==dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<214>";
			var t_uncle=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_right;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<215>";
			if(((t_uncle)!=null) && dbg_object(t_uncle).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<216>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<217>";
				dbg_object(t_uncle).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<218>";
				dbg_object(dbg_object(t_uncle).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<219>";
				t_node=dbg_object(t_uncle).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<221>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_right){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<222>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<223>";
					this.p_RotateLeft6(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<225>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<226>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<227>";
				this.p_RotateRight6(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<230>";
			var t_uncle2=dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_left;
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<231>";
			if(((t_uncle2)!=null) && dbg_object(t_uncle2).m_color==-1){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<232>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<233>";
				dbg_object(t_uncle2).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<234>";
				dbg_object(dbg_object(t_uncle2).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<235>";
				t_node=dbg_object(t_uncle2).m_parent;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<237>";
				if(t_node==dbg_object(dbg_object(t_node).m_parent).m_left){
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<238>";
					t_node=dbg_object(t_node).m_parent;
					err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<239>";
					this.p_RotateRight6(t_node);
				}
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<241>";
				dbg_object(dbg_object(t_node).m_parent).m_color=1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<242>";
				dbg_object(dbg_object(dbg_object(t_node).m_parent).m_parent).m_color=-1;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<243>";
				this.p_RotateLeft6(dbg_object(dbg_object(t_node).m_parent).m_parent);
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<247>";
	dbg_object(this.m_root).m_color=1;
	pop_err();
	return 0;
}
c_Map6.prototype.p_Set7=function(t_key,t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<29>";
	var t_node=this.m_root;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_parent=null;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<30>";
	var t_cmp=0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<32>";
	while((t_node)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<33>";
		t_parent=t_node;
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<34>";
		t_cmp=this.p_Compare2(t_key,dbg_object(t_node).m_key);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<35>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<36>";
			t_node=dbg_object(t_node).m_right;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<37>";
			if(t_cmp<0){
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<38>";
				t_node=dbg_object(t_node).m_left;
			}else{
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<40>";
				dbg_object(t_node).m_value=t_value;
				err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<41>";
				pop_err();
				return false;
			}
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<45>";
	t_node=c_Node6.m_new.call(new c_Node6,t_key,t_value,-1,t_parent);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<47>";
	if((t_parent)!=null){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<48>";
		if(t_cmp>0){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<49>";
			dbg_object(t_parent).m_right=t_node;
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<51>";
			dbg_object(t_parent).m_left=t_node;
		}
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<53>";
		this.p_InsertFixup6(t_node);
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<55>";
		this.m_root=t_node;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<57>";
	pop_err();
	return true;
}
function c_StringMap4(){
	c_Map6.call(this);
}
c_StringMap4.prototype=extend_class(c_Map6);
c_StringMap4.m_new=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<551>";
	c_Map6.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<551>";
	pop_err();
	return this;
}
c_StringMap4.prototype.p_Compare2=function(t_lhs,t_rhs){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<554>";
	var t_=string_compare(t_lhs,t_rhs);
	pop_err();
	return t_;
}
function c_Node6(){
	Object.call(this);
	this.m_key="";
	this.m_right=null;
	this.m_left=null;
	this.m_value=[];
	this.m_color=0;
	this.m_parent=null;
}
c_Node6.m_new=function(t_key,t_value,t_color,t_parent){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<364>";
	dbg_object(this).m_key=t_key;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<365>";
	dbg_object(this).m_value=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<366>";
	dbg_object(this).m_color=t_color;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<367>";
	dbg_object(this).m_parent=t_parent;
	pop_err();
	return this;
}
c_Node6.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<361>";
	pop_err();
	return this;
}
var bb_graphics2_rs_blend=0;
function c_BlendMode(){
	Object.call(this);
}
function c_Stack8(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack8.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack8.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack8.prototype.p_Data=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<41>";
	pop_err();
	return dbg_object(this).m_data;
}
c_Stack8.m_NIL=null;
c_Stack8.prototype.p_Length=function(t_newlength){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<45>";
	if(t_newlength<this.m_length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<46>";
		for(var t_i=t_newlength;t_i<this.m_length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<47>";
			dbg_array(this.m_data,t_i)[dbg_index]=c_Stack8.m_NIL;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<49>";
		if(t_newlength>this.m_data.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<50>";
			this.m_data=resize_object_array(this.m_data,bb_math_Max(this.m_length*2+10,t_newlength));
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<52>";
	this.m_length=t_newlength;
	pop_err();
}
c_Stack8.prototype.p_Length2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<56>";
	pop_err();
	return this.m_length;
}
c_Stack8.prototype.p_Push22=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack8.prototype.p_Push23=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push22(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack8.prototype.p_Push24=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push23(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
c_Stack8.prototype.p_Clear=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<34>";
	for(var t_i=0;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<35>";
		dbg_array(this.m_data,t_i)[dbg_index]=c_Stack8.m_NIL;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<37>";
	this.m_length=0;
	pop_err();
}
c_Stack8.prototype.p_Pop=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<89>";
	this.m_length-=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<90>";
	var t_v=dbg_array(this.m_data,this.m_length)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<91>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=c_Stack8.m_NIL;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<92>";
	pop_err();
	return t_v;
}
var bb_graphics2_freeOps=null;
var bb_graphics2_nullOp=null;
function c_ShadowCaster(){
	Object.call(this);
	this.m__verts=[];
	this.m__type=0;
}
function c_Stack9(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack9.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack9.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack9.m_NIL=null;
c_Stack9.prototype.p_Clear=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<34>";
	for(var t_i=0;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<35>";
		dbg_array(this.m_data,t_i)[dbg_index]=c_Stack9.m_NIL;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<37>";
	this.m_length=0;
	pop_err();
}
c_Stack9.prototype.p_Push25=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack9.prototype.p_Push26=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push25(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack9.prototype.p_Push27=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push26(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
c_Stack9.prototype.p_Length=function(t_newlength){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<45>";
	if(t_newlength<this.m_length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<46>";
		for(var t_i=t_newlength;t_i<this.m_length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<47>";
			dbg_array(this.m_data,t_i)[dbg_index]=c_Stack9.m_NIL;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<49>";
		if(t_newlength>this.m_data.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<50>";
			this.m_data=resize_object_array(this.m_data,bb_math_Max(this.m_length*2+10,t_newlength));
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<52>";
	this.m_length=t_newlength;
	pop_err();
}
c_Stack9.prototype.p_Length2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<56>";
	pop_err();
	return this.m_length;
}
c_Stack9.prototype.p_Get=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<104>";
	pop_err();
	return dbg_array(this.m_data,t_index)[dbg_index];
}
function c_Stack10(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack10.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack10.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack10.m_NIL=0;
c_Stack10.prototype.p_Clear=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<34>";
	for(var t_i=0;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<35>";
		dbg_array(this.m_data,t_i)[dbg_index]=c_Stack10.m_NIL;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<37>";
	this.m_length=0;
	pop_err();
}
c_Stack10.prototype.p_Push28=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_number_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack10.prototype.p_Push29=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push28(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack10.prototype.p_Push30=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push29(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
c_Stack10.prototype.p_Data=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<41>";
	pop_err();
	return dbg_object(this).m_data;
}
function c_FloatStack(){
	c_Stack10.call(this);
}
c_FloatStack.prototype=extend_class(c_Stack10);
c_FloatStack.m_new=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<338>";
	c_Stack10.m_new2.call(this,t_data);
	pop_err();
	return this;
}
c_FloatStack.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<335>";
	c_Stack10.m_new.call(this);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<335>";
	pop_err();
	return this;
}
var bb_graphics2_rs_projMatrix=[];
function bb_math3d_Mat4Copy(t_m,t_r){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<39>";
	dbg_array(t_r,0)[dbg_index]=dbg_array(t_m,0)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<39>";
	dbg_array(t_r,1)[dbg_index]=dbg_array(t_m,1)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<39>";
	dbg_array(t_r,2)[dbg_index]=dbg_array(t_m,2)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<39>";
	dbg_array(t_r,3)[dbg_index]=dbg_array(t_m,3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<40>";
	dbg_array(t_r,4)[dbg_index]=dbg_array(t_m,4)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<40>";
	dbg_array(t_r,5)[dbg_index]=dbg_array(t_m,5)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<40>";
	dbg_array(t_r,6)[dbg_index]=dbg_array(t_m,6)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<40>";
	dbg_array(t_r,7)[dbg_index]=dbg_array(t_m,7)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<41>";
	dbg_array(t_r,8)[dbg_index]=dbg_array(t_m,8)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<41>";
	dbg_array(t_r,9)[dbg_index]=dbg_array(t_m,9)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<41>";
	dbg_array(t_r,10)[dbg_index]=dbg_array(t_m,10)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<41>";
	dbg_array(t_r,11)[dbg_index]=dbg_array(t_m,11)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<42>";
	dbg_array(t_r,12)[dbg_index]=dbg_array(t_m,12)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<42>";
	dbg_array(t_r,13)[dbg_index]=dbg_array(t_m,13)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<42>";
	dbg_array(t_r,14)[dbg_index]=dbg_array(t_m,14)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<42>";
	dbg_array(t_r,15)[dbg_index]=dbg_array(t_m,15)[dbg_index];
	pop_err();
}
function bb_math3d_Mat4Init(t_ix,t_jy,t_kz,t_tw,t_r){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<21>";
	dbg_array(t_r,0)[dbg_index]=t_ix;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<21>";
	dbg_array(t_r,1)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<21>";
	dbg_array(t_r,2)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<21>";
	dbg_array(t_r,3)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<22>";
	dbg_array(t_r,4)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<22>";
	dbg_array(t_r,5)[dbg_index]=t_jy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<22>";
	dbg_array(t_r,6)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<22>";
	dbg_array(t_r,7)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<23>";
	dbg_array(t_r,8)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<23>";
	dbg_array(t_r,9)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<23>";
	dbg_array(t_r,10)[dbg_index]=t_kz;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<23>";
	dbg_array(t_r,11)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<24>";
	dbg_array(t_r,12)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<24>";
	dbg_array(t_r,13)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<24>";
	dbg_array(t_r,14)[dbg_index]=0.0;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<24>";
	dbg_array(t_r,15)[dbg_index]=t_tw;
	pop_err();
}
function bb_math3d_Mat4Init2(t_ix,t_iy,t_iz,t_iw,t_jx,t_jy,t_jz,t_jw,t_kx,t_ky,t_kz,t_kw,t_tx,t_ty,t_tz,t_tw,t_r){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<28>";
	dbg_array(t_r,0)[dbg_index]=t_ix;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<28>";
	dbg_array(t_r,1)[dbg_index]=t_iy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<28>";
	dbg_array(t_r,2)[dbg_index]=t_iz;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<28>";
	dbg_array(t_r,3)[dbg_index]=t_iw;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<29>";
	dbg_array(t_r,4)[dbg_index]=t_jx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<29>";
	dbg_array(t_r,5)[dbg_index]=t_jy;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<29>";
	dbg_array(t_r,6)[dbg_index]=t_jz;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<29>";
	dbg_array(t_r,7)[dbg_index]=t_jw;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<30>";
	dbg_array(t_r,8)[dbg_index]=t_kx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<30>";
	dbg_array(t_r,9)[dbg_index]=t_ky;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<30>";
	dbg_array(t_r,10)[dbg_index]=t_kz;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<30>";
	dbg_array(t_r,11)[dbg_index]=t_kw;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<31>";
	dbg_array(t_r,12)[dbg_index]=t_tx;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<31>";
	dbg_array(t_r,13)[dbg_index]=t_ty;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<31>";
	dbg_array(t_r,14)[dbg_index]=t_tz;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<31>";
	dbg_array(t_r,15)[dbg_index]=t_tw;
	pop_err();
}
function bb_math3d_Mat4Init3(t_r){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<35>";
	bb_math3d_Mat4Init(1.0,1.0,1.0,1.0,t_r);
	pop_err();
}
function bb_math3d_Mat4Multiply(t_m,t_n,t_r){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<87>";
	bb_math3d_Mat4Init2(dbg_array(t_m,0)[dbg_index]*dbg_array(t_n,0)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_n,1)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_n,2)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_n,3)[dbg_index],dbg_array(t_m,1)[dbg_index]*dbg_array(t_n,0)[dbg_index]+dbg_array(t_m,5)[dbg_index]*dbg_array(t_n,1)[dbg_index]+dbg_array(t_m,9)[dbg_index]*dbg_array(t_n,2)[dbg_index]+dbg_array(t_m,13)[dbg_index]*dbg_array(t_n,3)[dbg_index],dbg_array(t_m,2)[dbg_index]*dbg_array(t_n,0)[dbg_index]+dbg_array(t_m,6)[dbg_index]*dbg_array(t_n,1)[dbg_index]+dbg_array(t_m,10)[dbg_index]*dbg_array(t_n,2)[dbg_index]+dbg_array(t_m,14)[dbg_index]*dbg_array(t_n,3)[dbg_index],dbg_array(t_m,3)[dbg_index]*dbg_array(t_n,0)[dbg_index]+dbg_array(t_m,7)[dbg_index]*dbg_array(t_n,1)[dbg_index]+dbg_array(t_m,11)[dbg_index]*dbg_array(t_n,2)[dbg_index]+dbg_array(t_m,15)[dbg_index]*dbg_array(t_n,3)[dbg_index],dbg_array(t_m,0)[dbg_index]*dbg_array(t_n,4)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_n,5)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_n,6)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_n,7)[dbg_index],dbg_array(t_m,1)[dbg_index]*dbg_array(t_n,4)[dbg_index]+dbg_array(t_m,5)[dbg_index]*dbg_array(t_n,5)[dbg_index]+dbg_array(t_m,9)[dbg_index]*dbg_array(t_n,6)[dbg_index]+dbg_array(t_m,13)[dbg_index]*dbg_array(t_n,7)[dbg_index],dbg_array(t_m,2)[dbg_index]*dbg_array(t_n,4)[dbg_index]+dbg_array(t_m,6)[dbg_index]*dbg_array(t_n,5)[dbg_index]+dbg_array(t_m,10)[dbg_index]*dbg_array(t_n,6)[dbg_index]+dbg_array(t_m,14)[dbg_index]*dbg_array(t_n,7)[dbg_index],dbg_array(t_m,3)[dbg_index]*dbg_array(t_n,4)[dbg_index]+dbg_array(t_m,7)[dbg_index]*dbg_array(t_n,5)[dbg_index]+dbg_array(t_m,11)[dbg_index]*dbg_array(t_n,6)[dbg_index]+dbg_array(t_m,15)[dbg_index]*dbg_array(t_n,7)[dbg_index],dbg_array(t_m,0)[dbg_index]*dbg_array(t_n,8)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_n,9)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_n,10)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_n,11)[dbg_index],dbg_array(t_m,1)[dbg_index]*dbg_array(t_n,8)[dbg_index]+dbg_array(t_m,5)[dbg_index]*dbg_array(t_n,9)[dbg_index]+dbg_array(t_m,9)[dbg_index]*dbg_array(t_n,10)[dbg_index]+dbg_array(t_m,13)[dbg_index]*dbg_array(t_n,11)[dbg_index],dbg_array(t_m,2)[dbg_index]*dbg_array(t_n,8)[dbg_index]+dbg_array(t_m,6)[dbg_index]*dbg_array(t_n,9)[dbg_index]+dbg_array(t_m,10)[dbg_index]*dbg_array(t_n,10)[dbg_index]+dbg_array(t_m,14)[dbg_index]*dbg_array(t_n,11)[dbg_index],dbg_array(t_m,3)[dbg_index]*dbg_array(t_n,8)[dbg_index]+dbg_array(t_m,7)[dbg_index]*dbg_array(t_n,9)[dbg_index]+dbg_array(t_m,11)[dbg_index]*dbg_array(t_n,10)[dbg_index]+dbg_array(t_m,15)[dbg_index]*dbg_array(t_n,11)[dbg_index],dbg_array(t_m,0)[dbg_index]*dbg_array(t_n,12)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_n,13)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_n,14)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_n,15)[dbg_index],dbg_array(t_m,1)[dbg_index]*dbg_array(t_n,12)[dbg_index]+dbg_array(t_m,5)[dbg_index]*dbg_array(t_n,13)[dbg_index]+dbg_array(t_m,9)[dbg_index]*dbg_array(t_n,14)[dbg_index]+dbg_array(t_m,13)[dbg_index]*dbg_array(t_n,15)[dbg_index],dbg_array(t_m,2)[dbg_index]*dbg_array(t_n,12)[dbg_index]+dbg_array(t_m,6)[dbg_index]*dbg_array(t_n,13)[dbg_index]+dbg_array(t_m,10)[dbg_index]*dbg_array(t_n,14)[dbg_index]+dbg_array(t_m,14)[dbg_index]*dbg_array(t_n,15)[dbg_index],dbg_array(t_m,3)[dbg_index]*dbg_array(t_n,12)[dbg_index]+dbg_array(t_m,7)[dbg_index]*dbg_array(t_n,13)[dbg_index]+dbg_array(t_m,11)[dbg_index]*dbg_array(t_n,14)[dbg_index]+dbg_array(t_m,15)[dbg_index]*dbg_array(t_n,15)[dbg_index],t_r);
	pop_err();
}
function bb_math3d_Vec4Copy(t_v,t_r){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<9>";
	dbg_array(t_r,0)[dbg_index]=dbg_array(t_v,0)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<9>";
	dbg_array(t_r,1)[dbg_index]=dbg_array(t_v,1)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<9>";
	dbg_array(t_r,2)[dbg_index]=dbg_array(t_v,2)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<9>";
	dbg_array(t_r,3)[dbg_index]=dbg_array(t_v,3)[dbg_index];
	pop_err();
}
function bb_math3d_Vec4Copy2(t_v,t_r,t_src,t_dst){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<13>";
	dbg_array(t_r,0+t_dst)[dbg_index]=dbg_array(t_v,0+t_src)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<13>";
	dbg_array(t_r,1+t_dst)[dbg_index]=dbg_array(t_v,1+t_src)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<13>";
	dbg_array(t_r,2+t_dst)[dbg_index]=dbg_array(t_v,2+t_src)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<13>";
	dbg_array(t_r,3+t_dst)[dbg_index]=dbg_array(t_v,3+t_src)[dbg_index];
	pop_err();
}
function bb_math3d_Vec4Init(t_x,t_y,t_z,t_w,t_r){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<5>";
	dbg_array(t_r,0)[dbg_index]=t_x;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<5>";
	dbg_array(t_r,1)[dbg_index]=t_y;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<5>";
	dbg_array(t_r,2)[dbg_index]=t_z;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<5>";
	dbg_array(t_r,3)[dbg_index]=t_w;
	pop_err();
}
function bb_math3d_Mat4Transform(t_m,t_v,t_r){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<95>";
	bb_math3d_Vec4Init(dbg_array(t_m,0)[dbg_index]*dbg_array(t_v,0)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_v,1)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_v,2)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_v,3)[dbg_index],dbg_array(t_m,1)[dbg_index]*dbg_array(t_v,0)[dbg_index]+dbg_array(t_m,5)[dbg_index]*dbg_array(t_v,1)[dbg_index]+dbg_array(t_m,9)[dbg_index]*dbg_array(t_v,2)[dbg_index]+dbg_array(t_m,13)[dbg_index]*dbg_array(t_v,3)[dbg_index],dbg_array(t_m,2)[dbg_index]*dbg_array(t_v,0)[dbg_index]+dbg_array(t_m,6)[dbg_index]*dbg_array(t_v,1)[dbg_index]+dbg_array(t_m,10)[dbg_index]*dbg_array(t_v,2)[dbg_index]+dbg_array(t_m,14)[dbg_index]*dbg_array(t_v,3)[dbg_index],dbg_array(t_m,3)[dbg_index]*dbg_array(t_v,0)[dbg_index]+dbg_array(t_m,7)[dbg_index]*dbg_array(t_v,1)[dbg_index]+dbg_array(t_m,11)[dbg_index]*dbg_array(t_v,2)[dbg_index]+dbg_array(t_m,15)[dbg_index]*dbg_array(t_v,3)[dbg_index],t_r);
	pop_err();
}
function bb_math3d_Mat4Ortho(t_left,t_right,t_bottom,t_top,t_znear,t_zfar,t_r){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<46>";
	var t_w=t_right-t_left;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<46>";
	var t_h=t_top-t_bottom;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<46>";
	var t_d=t_zfar-t_znear;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<47>";
	bb_math3d_Mat4Init2(2.0/t_w,0.0,0.0,0.0,0.0,2.0/t_h,0.0,0.0,0.0,0.0,2.0/t_d,0.0,-(t_right+t_left)/t_w,-(t_top+t_bottom)/t_h,-(t_zfar+t_znear)/t_d,1.0,t_r);
	pop_err();
}
function c_Stack11(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack11.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack11.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack11.m_NIL=null;
c_Stack11.prototype.p_Clear=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<34>";
	for(var t_i=0;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<35>";
		dbg_array(this.m_data,t_i)[dbg_index]=c_Stack11.m_NIL;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<37>";
	this.m_length=0;
	pop_err();
}
c_Stack11.prototype.p_Push31=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack11.prototype.p_Push32=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push31(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack11.prototype.p_Push33=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push32(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
c_Stack11.prototype.p_Length=function(t_newlength){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<45>";
	if(t_newlength<this.m_length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<46>";
		for(var t_i=t_newlength;t_i<this.m_length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<47>";
			dbg_array(this.m_data,t_i)[dbg_index]=c_Stack11.m_NIL;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<49>";
		if(t_newlength>this.m_data.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<50>";
			this.m_data=resize_object_array(this.m_data,bb_math_Max(this.m_length*2+10,t_newlength));
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<52>";
	this.m_length=t_newlength;
	pop_err();
}
c_Stack11.prototype.p_Length2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<56>";
	pop_err();
	return this.m_length;
}
c_Stack11.prototype.p_Get=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<104>";
	pop_err();
	return dbg_array(this.m_data,t_index)[dbg_index];
}
function bb_math3d_Mat4Inverse(t_m,t_r){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<60>";
	dbg_array(t_r,0)[dbg_index]=dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,10)[dbg_index]*dbg_array(t_m,15)[dbg_index]-dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,11)[dbg_index]*dbg_array(t_m,14)[dbg_index]-dbg_array(t_m,9)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,15)[dbg_index]+dbg_array(t_m,9)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,14)[dbg_index]+dbg_array(t_m,13)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,11)[dbg_index]-dbg_array(t_m,13)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,10)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<61>";
	dbg_array(t_r,4)[dbg_index]=-dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,10)[dbg_index]*dbg_array(t_m,15)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,11)[dbg_index]*dbg_array(t_m,14)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,15)[dbg_index]-dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,14)[dbg_index]-dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,11)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,10)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<62>";
	dbg_array(t_r,8)[dbg_index]=dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,9)[dbg_index]*dbg_array(t_m,15)[dbg_index]-dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,11)[dbg_index]*dbg_array(t_m,13)[dbg_index]-dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,15)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,13)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,11)[dbg_index]-dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,9)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<63>";
	dbg_array(t_r,12)[dbg_index]=-dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,9)[dbg_index]*dbg_array(t_m,14)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,10)[dbg_index]*dbg_array(t_m,13)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,14)[dbg_index]-dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,13)[dbg_index]-dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,10)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,9)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<64>";
	dbg_array(t_r,1)[dbg_index]=-dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,10)[dbg_index]*dbg_array(t_m,15)[dbg_index]+dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,11)[dbg_index]*dbg_array(t_m,14)[dbg_index]+dbg_array(t_m,9)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,15)[dbg_index]-dbg_array(t_m,9)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,14)[dbg_index]-dbg_array(t_m,13)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,11)[dbg_index]+dbg_array(t_m,13)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,10)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<65>";
	dbg_array(t_r,5)[dbg_index]=dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,10)[dbg_index]*dbg_array(t_m,15)[dbg_index]-dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,11)[dbg_index]*dbg_array(t_m,14)[dbg_index]-dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,15)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,14)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,11)[dbg_index]-dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,10)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<66>";
	dbg_array(t_r,9)[dbg_index]=-dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,9)[dbg_index]*dbg_array(t_m,15)[dbg_index]+dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,11)[dbg_index]*dbg_array(t_m,13)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,15)[dbg_index]-dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,13)[dbg_index]-dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,11)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,9)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<67>";
	dbg_array(t_r,13)[dbg_index]=dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,9)[dbg_index]*dbg_array(t_m,14)[dbg_index]-dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,10)[dbg_index]*dbg_array(t_m,13)[dbg_index]-dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,14)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,13)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,10)[dbg_index]-dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,9)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<68>";
	dbg_array(t_r,2)[dbg_index]=dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,15)[dbg_index]-dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,14)[dbg_index]-dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,15)[dbg_index]+dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,14)[dbg_index]+dbg_array(t_m,13)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,7)[dbg_index]-dbg_array(t_m,13)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,6)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<69>";
	dbg_array(t_r,6)[dbg_index]=-dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,15)[dbg_index]+dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,14)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,15)[dbg_index]-dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,14)[dbg_index]-dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,7)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,6)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<70>";
	dbg_array(t_r,10)[dbg_index]=dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,15)[dbg_index]-dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,13)[dbg_index]-dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,15)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,13)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,7)[dbg_index]-dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,5)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<71>";
	dbg_array(t_r,14)[dbg_index]=-dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,14)[dbg_index]+dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,13)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,14)[dbg_index]-dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,13)[dbg_index]-dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,6)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,5)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<72>";
	dbg_array(t_r,3)[dbg_index]=-dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,11)[dbg_index]+dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,10)[dbg_index]+dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,11)[dbg_index]-dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,10)[dbg_index]-dbg_array(t_m,9)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,7)[dbg_index]+dbg_array(t_m,9)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,6)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<73>";
	dbg_array(t_r,7)[dbg_index]=dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,11)[dbg_index]-dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,10)[dbg_index]-dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,11)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,10)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,7)[dbg_index]-dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,6)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<74>";
	dbg_array(t_r,11)[dbg_index]=-dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,11)[dbg_index]+dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,7)[dbg_index]*dbg_array(t_m,9)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,11)[dbg_index]-dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,9)[dbg_index]-dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,7)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,3)[dbg_index]*dbg_array(t_m,5)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<75>";
	dbg_array(t_r,15)[dbg_index]=dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,5)[dbg_index]*dbg_array(t_m,10)[dbg_index]-dbg_array(t_m,0)[dbg_index]*dbg_array(t_m,6)[dbg_index]*dbg_array(t_m,9)[dbg_index]-dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,10)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,9)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,1)[dbg_index]*dbg_array(t_m,6)[dbg_index]-dbg_array(t_m,8)[dbg_index]*dbg_array(t_m,2)[dbg_index]*dbg_array(t_m,5)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<76>";
	var t_c=1.0/(dbg_array(t_m,0)[dbg_index]*dbg_array(t_r,0)[dbg_index]+dbg_array(t_m,1)[dbg_index]*dbg_array(t_r,4)[dbg_index]+dbg_array(t_m,2)[dbg_index]*dbg_array(t_r,8)[dbg_index]+dbg_array(t_m,3)[dbg_index]*dbg_array(t_r,12)[dbg_index]);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<77>";
	for(var t_i=0;t_i<16;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<78>";
		dbg_array(t_r,t_i)[dbg_index]*=t_c;
	}
	pop_err();
}
function c_Stack12(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack12.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack12.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack12.m_NIL=null;
c_Stack12.prototype.p_Clear=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<34>";
	for(var t_i=0;t_i<this.m_length;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<35>";
		dbg_array(this.m_data,t_i)[dbg_index]=c_Stack12.m_NIL;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<37>";
	this.m_length=0;
	pop_err();
}
c_Stack12.prototype.p_Length=function(t_newlength){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<45>";
	if(t_newlength<this.m_length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<46>";
		for(var t_i=t_newlength;t_i<this.m_length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<47>";
			dbg_array(this.m_data,t_i)[dbg_index]=c_Stack12.m_NIL;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<49>";
		if(t_newlength>this.m_data.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<50>";
			this.m_data=resize_object_array(this.m_data,bb_math_Max(this.m_length*2+10,t_newlength));
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<52>";
	this.m_length=t_newlength;
	pop_err();
}
c_Stack12.prototype.p_Length2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<56>";
	pop_err();
	return this.m_length;
}
c_Stack12.prototype.p_Get=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<104>";
	pop_err();
	return dbg_array(this.m_data,t_index)[dbg_index];
}
c_Stack12.prototype.p_Push34=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack12.prototype.p_Push35=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push34(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack12.prototype.p_Push36=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push35(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
function c_Stack13(){
	Object.call(this);
	this.m_data=[];
	this.m_length=0;
}
c_Stack13.m_new=function(){
	push_err();
	pop_err();
	return this;
}
c_Stack13.m_new2=function(t_data){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<13>";
	dbg_object(this).m_data=t_data.slice(0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<14>";
	dbg_object(this).m_length=t_data.length;
	pop_err();
	return this;
}
c_Stack13.m_NIL=null;
c_Stack13.prototype.p_Length=function(t_newlength){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<45>";
	if(t_newlength<this.m_length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<46>";
		for(var t_i=t_newlength;t_i<this.m_length;t_i=t_i+1){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<47>";
			dbg_array(this.m_data,t_i)[dbg_index]=c_Stack13.m_NIL;
		}
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<49>";
		if(t_newlength>this.m_data.length){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<50>";
			this.m_data=resize_object_array(this.m_data,bb_math_Max(this.m_length*2+10,t_newlength));
		}
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<52>";
	this.m_length=t_newlength;
	pop_err();
}
c_Stack13.prototype.p_Length2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<56>";
	pop_err();
	return this.m_length;
}
c_Stack13.prototype.p_Get=function(t_index){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<104>";
	pop_err();
	return dbg_array(this.m_data,t_index)[dbg_index];
}
c_Stack13.prototype.p_Push37=function(t_value){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<71>";
	if(this.m_length==this.m_data.length){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<72>";
		this.m_data=resize_object_array(this.m_data,this.m_length*2+10);
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<74>";
	dbg_array(this.m_data,this.m_length)[dbg_index]=t_value;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<75>";
	this.m_length+=1;
	pop_err();
}
c_Stack13.prototype.p_Push38=function(t_values,t_offset,t_count){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<83>";
	for(var t_i=0;t_i<t_count;t_i=t_i+1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<84>";
		this.p_Push37(dbg_array(t_values,t_offset+t_i)[dbg_index]);
	}
	pop_err();
}
c_Stack13.prototype.p_Push39=function(t_values,t_offset){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/stack.monkey<79>";
	this.p_Push38(t_values,t_offset,t_values.length-t_offset);
	pop_err();
}
var bb_graphics2_tmpMat3d=[];
var bb_graphics2_tmpMat3d2=[];
function bb_math_Min(t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<51>";
	if(t_x<t_y){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<51>";
		pop_err();
		return t_x;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<52>";
	pop_err();
	return t_y;
}
function bb_math_Min2(t_x,t_y){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<78>";
	if(t_x<t_y){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<78>";
		pop_err();
		return t_x;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/math.monkey<79>";
	pop_err();
	return t_y;
}
var bb_renderer_lvector=[];
var bb_renderer_tvector=[];
function bb_math3d_Mat4Project(t_m,t_v,t_r){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<103>";
	bb_math3d_Vec4Init(dbg_array(t_m,0)[dbg_index]*dbg_array(t_v,0)[dbg_index]+dbg_array(t_m,4)[dbg_index]*dbg_array(t_v,1)[dbg_index]+dbg_array(t_m,8)[dbg_index]*dbg_array(t_v,2)[dbg_index]+dbg_array(t_m,12)[dbg_index]*dbg_array(t_v,3)[dbg_index],dbg_array(t_m,1)[dbg_index]*dbg_array(t_v,0)[dbg_index]+dbg_array(t_m,5)[dbg_index]*dbg_array(t_v,1)[dbg_index]+dbg_array(t_m,9)[dbg_index]*dbg_array(t_v,2)[dbg_index]+dbg_array(t_m,13)[dbg_index]*dbg_array(t_v,3)[dbg_index],dbg_array(t_m,2)[dbg_index]*dbg_array(t_v,0)[dbg_index]+dbg_array(t_m,6)[dbg_index]*dbg_array(t_v,1)[dbg_index]+dbg_array(t_m,10)[dbg_index]*dbg_array(t_v,2)[dbg_index]+dbg_array(t_m,14)[dbg_index]*dbg_array(t_v,3)[dbg_index],dbg_array(t_m,3)[dbg_index]*dbg_array(t_v,0)[dbg_index]+dbg_array(t_m,7)[dbg_index]*dbg_array(t_v,1)[dbg_index]+dbg_array(t_m,11)[dbg_index]*dbg_array(t_v,2)[dbg_index]+dbg_array(t_m,15)[dbg_index]*dbg_array(t_v,3)[dbg_index],t_r);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<104>";
	dbg_array(t_r,0)[dbg_index]/=dbg_array(t_r,3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<104>";
	dbg_array(t_r,1)[dbg_index]/=dbg_array(t_r,3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<104>";
	dbg_array(t_r,2)[dbg_index]/=dbg_array(t_r,3)[dbg_index];
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/mojo2/math3d.monkey<104>";
	dbg_array(t_r,3)[dbg_index]=1.0;
	pop_err();
}
var bb_math3d_Mat4Identity=[];
function c_Puzzle(){
	Object.call(this);
	this.m_puzzle=null;
	this.m_currentRotation=0;
}
c_Puzzle.m_new=function(t_layer,t_img,t_numRows,t_numCols){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/entities/puzzle.monkey<12>";
	dbg_object(this).m_puzzle=c_prSprite.m_new.call(new c_prSprite,t_layer,t_img);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/entities/puzzle.monkey<13>";
	dbg_object(this).m_puzzle.p_SetPosition(320.0,240.0);
	pop_err();
	return this;
}
c_Puzzle.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/entities/puzzle.monkey<5>";
	pop_err();
	return this;
}
c_Puzzle.prototype.p_OnUpdate=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/entities/puzzle.monkey<28>";
	dbg_object(this).m_puzzle.p_SetRotation(dbg_object(this).m_currentRotation);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/entities/puzzle.monkey<30>";
	dbg_object(this).m_currentRotation+=1;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/entities/puzzle.monkey<31>";
	if(dbg_object(this).m_currentRotation==360){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Projects/pyro-projects/livepuzzle/entities/puzzle.monkey<32>";
		dbg_object(this).m_currentRotation=0;
	}
	pop_err();
}
function c_prSorter(){
	Object.call(this);
}
c_prSorter.prototype.p_Update3=function(t_entities){
	push_err();
	pop_err();
}
function c_prTileset(){
	Object.call(this);
	this.m__tilesetTile=new_object_array(1);
}
function c_prTilesetTile(){
	Object.call(this);
	this.m__padding=false;
	this.m__width=0;
	this.m__image=null;
	this.m__height=0;
}
c_prTilesetTile.prototype.p_Height=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<5339>";
	pop_err();
	return this.m__height;
}
c_prTilesetTile.prototype.p_Width=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<5392>";
	pop_err();
	return this.m__width;
}
function bb_scenegraph_prOrthoToIsoX(t_pointX,t_pointY){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<234>";
	var t_=t_pointX-t_pointY;
	pop_err();
	return t_;
}
function bb_scenegraph_prOrthoToIsoY(t_pointX,t_pointY){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/engine/scenegraph.monkey<238>";
	var t_=(t_pointX+t_pointY)/2.0;
	pop_err();
	return t_;
}
function bb_gfx_prDrawRect(t_drawList,t_x,t_y,t_width,t_height,t_pixels){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<144>";
	t_drawList.p_DrawRect(t_x,t_y,(t_width),(t_pixels),null,0.0,0.0,1.0,1.0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<145>";
	t_drawList.p_DrawRect(t_x,t_y+(t_pixels),(t_pixels),(t_height-t_pixels*2),null,0.0,0.0,1.0,1.0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<146>";
	t_drawList.p_DrawRect(t_x,t_y-(t_pixels)+(t_height),(t_width),(t_pixels),null,0.0,0.0,1.0,1.0);
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<147>";
	t_drawList.p_DrawRect(t_x-(t_pixels)+(t_width),t_y+(t_pixels),(t_pixels),(t_height-t_pixels*2),null,0.0,0.0,1.0,1.0);
	pop_err();
}
function bb_gfx_prDrawImage(t_drawList,t_image,t_x,t_y,t_rotation,t_scaleX,t_scaleY,t_handleX,t_handleY,t_padding){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<83>";
	if(t_padding){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<85>";
		t_drawList.p_PushMatrix();
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<86>";
		t_drawList.p_Translate(t_x,t_y);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<87>";
		t_drawList.p_Rotate(t_rotation);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<88>";
		t_drawList.p_Scale(t_scaleX,t_scaleY);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<89>";
		t_drawList.p_Translate(-t_handleX*(t_image.p_Width()-2),-t_handleY*(t_image.p_Height()-2));
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<90>";
		t_drawList.p_DrawRect3(0.0,0.0,t_image,1,1,t_image.p_Width()-2,t_image.p_Height()-2);
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<91>";
		t_drawList.p_PopMatrix();
	}else{
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<95>";
		if(t_image.p_HandleX()==t_handleX && t_image.p_HandleY()==t_handleY){
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<97>";
			t_drawList.p_DrawImage2(t_image,t_x,t_y,t_rotation,t_scaleX,t_scaleY);
		}else{
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<101>";
			var t_oldHandleX=t_image.p_HandleX();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<102>";
			var t_oldHandleY=t_image.p_HandleY();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<104>";
			t_image.p_SetHandle(0.0,0.0);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<106>";
			t_drawList.p_PushMatrix();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<108>";
			t_drawList.p_Translate(t_x,t_y);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<109>";
			t_drawList.p_Rotate(t_rotation);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<110>";
			t_drawList.p_Scale(t_scaleX,t_scaleY);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<111>";
			t_drawList.p_Translate(-t_handleX*(t_image.p_Width()),-t_handleY*(t_image.p_Height()));
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<113>";
			t_drawList.p_DrawImage(t_image);
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<115>";
			t_drawList.p_PopMatrix();
			err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/playniax/pyro/framework/gfx.monkey<117>";
			t_image.p_SetHandle(t_oldHandleX,t_oldHandleY);
		}
	}
	pop_err();
}
function bb_filepath_ExtractExt(t_path){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/filepath.monkey<22>";
	var t_i=t_path.lastIndexOf(".");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/filepath.monkey<23>";
	if(t_i!=-1 && t_path.indexOf("/",t_i+1)==-1 && t_path.indexOf("\\",t_i+1)==-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/filepath.monkey<23>";
		var t_=t_path.slice(t_i+1);
		pop_err();
		return t_;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/filepath.monkey<24>";
	pop_err();
	return "";
}
function bb_filepath_StripExt(t_path){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/filepath.monkey<16>";
	var t_i=t_path.lastIndexOf(".");
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/filepath.monkey<17>";
	if(t_i!=-1 && t_path.indexOf("/",t_i+1)==-1 && t_path.indexOf("\\",t_i+1)==-1){
		err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/filepath.monkey<17>";
		var t_=t_path.slice(0,t_i);
		pop_err();
		return t_;
	}
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/brl/filepath.monkey<18>";
	pop_err();
	return t_path;
}
function c_NodeEnumerator(){
	Object.call(this);
	this.m_node=null;
}
c_NodeEnumerator.m_new=function(t_node){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<437>";
	dbg_object(this).m_node=t_node;
	pop_err();
	return this;
}
c_NodeEnumerator.m_new2=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<434>";
	pop_err();
	return this;
}
c_NodeEnumerator.prototype.p_HasNext=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<441>";
	var t_=this.m_node!=null;
	pop_err();
	return t_;
}
c_NodeEnumerator.prototype.p_NextObject=function(){
	push_err();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<445>";
	var t_t=this.m_node;
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<446>";
	this.m_node=this.m_node.p_NextNode();
	err_info="C:/Users/Quang Nguyen/Projects/Sheep Apps/Monkey-X/modules/monkey/map.monkey<447>";
	pop_err();
	return t_t;
}
function bbInit(){
	bb_app__app=null;
	bb_app__delegate=null;
	bb_app__game=BBGame.Game();
	bb_graphics_device=null;
	bb_graphics_context=c_GraphicsContext.m_new.call(new c_GraphicsContext);
	c_Image.m_DefaultFlags=0;
	bb_audio_device=null;
	bb_input_device=null;
	bb_app__devWidth=0;
	bb_app__devHeight=0;
	bb_app__displayModes=[];
	bb_app__desktopMode=null;
	bb_graphics_renderDevice=null;
	c_prScreenManager.m__screenPointer=null;
	c_prScreenManager.m__nextScreen=null;
	bb_globals__pause=false;
	bb_contentmanager_prContent=c_prContentManager.m_new.call(new c_prContentManager);
	bb_screenanchor_screenAnchor=c_prScreenAnchor.m_new.call(new c_prScreenAnchor);
	bb_tweens_TweenFunc=new_object_array(31);
	bb_tweens_prTweens=c_prTweenPool.m_new.call(new c_prTweenPool,100);
	c_Stack2.m_NIL=null;
	c_Stack3.m_NIL=null;
	c_Stack4.m_NIL=null;
	c_Stack5.m_NIL=null;
	c_Stack6.m_NIL=null;
	bb_graphics2_inited=false;
	bb_graphics2_vbosSeq=0;
	bb_graphics2_rs_vbo=0;
	bb_graphics2_rs_ibo=0;
	bb_graphics2_tmpi=new_number_array(16);
	bb_graphics2_defaultFbo=0;
	bb_graphics2_mainShader="";
	bb_glutil_tmpi=new_number_array(16);
	bb_graphics2_fastShader=null;
	bb_graphics2_bumpShader=null;
	bb_graphics2_matteShader=null;
	bb_graphics2_shadowShader=null;
	bb_graphics2_lightMapShader=null;
	bb_graphics2_defaultShader=null;
	c_Image2.m__flagsMask=259;
	c_Texture.m__white=null;
	c_Texture.m__colors=c_IntMap2.m_new.call(new c_IntMap2);
	bb_graphics2_defaultFont=null;
	bb_graphics2_flipYMatrix=bb_math3d_Mat4New();
	c_Canvas.m__active=null;
	bb_graphics2_rs_program=null;
	bb_graphics2_rs_numLights=0;
	bb_graphics2_rs_material=null;
	bb_graphics2_rs_modelViewProjMatrix=bb_math3d_Mat4New();
	bb_graphics2_rs_modelViewMatrix=bb_math3d_Mat4New();
	bb_graphics2_rs_clipPosScale=[1.0,1.0,1.0,1.0];
	bb_graphics2_rs_globalColor=[1.0,1.0,1.0,1.0];
	bb_graphics2_rs_fogColor=[0.0,0.0,0.0,0.0];
	bb_graphics2_rs_ambientLight=[0.0,0.0,0.0,1.0];
	bb_graphics2_rs_lightColors=new_number_array(16);
	bb_graphics2_rs_lightVectors=new_number_array(16);
	bb_graphics2_rs_shadowTexture=null;
	bb_graphics2_rs_blend=-1;
	c_Stack8.m_NIL=null;
	bb_graphics2_freeOps=c_Stack8.m_new.call(new c_Stack8);
	bb_graphics2_nullOp=c_DrawOp.m_new.call(new c_DrawOp);
	c_Stack9.m_NIL=null;
	c_Stack10.m_NIL=0;
	bb_graphics2_rs_projMatrix=bb_math3d_Mat4New();
	c_Stack11.m_NIL=null;
	c_Stack12.m_NIL=null;
	c_Stack13.m_NIL=null;
	bb_graphics2_tmpMat3d=new_number_array(16);
	bb_graphics2_tmpMat3d2=new_number_array(16);
	bb_renderer_lvector=new_number_array(4);
	bb_renderer_tvector=new_number_array(4);
	bb_math3d_Mat4Identity=[1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0];
	c_Texture.m__black=null;
	c_Texture.m__flat=null;
}
//${TRANSCODE_END}
